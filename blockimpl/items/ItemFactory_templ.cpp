// Copyright (c) 2018 extremeheat. All rights reserved.

#include "stdafx.h"
#include "ItemFactory.h"
#include "BlockItem.h"
#include "DamageableItem.h"
#include "blocks/BlockFactory.h"
#include "data/uniform/AItemList.h"


%(HeaderDefs)s

namespace Andromeda {
	Item* ItemFactory::createAndromedaItemFromJava(short java_id, short java_damage) {
		%(FromJavaLegacyNumerical)s
	}
	Item* ItemFactory::createAndromedaItemFromJava(RoString java_id, short java_damage) {
		%(FromJavaLegacyNames)s
	}

	Item* ItemFactory::createAndromedaItemFromJava(RoString java_id /* Nominal */) {
		%(FromJavaNominal)s
	}

	Item* ItemFactory::createAndromedaItemFromPocket(short pocket_id, short pocket_damage) {
		%(FromPocketNumerical)s
	}
	
	Item* ItemFactory::createAndromedaItemFromPocket(RoString pocket_id, short pocket_damage) {
// TODO: There does not appear to be any BlockFactory methods to lookup PE blocks by names, due to no exisiting map
// When block inits are re-worked this needs to be looked into.
#if 0
		%(FromPocketNames)s
#endif
		return nullptr;
	}

	Item* ItemFactory::createAndromedaItem(RoString andromeda_item_name, RoString andromeda_item_variant) {
		%(AndromedaItems)s
	}

	Item* ItemFactory::createAndromedaItem(const AItemID &andromeda_item_id) {
		return nullptr;
	}
}