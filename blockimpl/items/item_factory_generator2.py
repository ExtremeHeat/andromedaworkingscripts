ITEM_FILE = "items2.csv"
ITEM_FILE_DIL = ","
BLOCK_FILE = "../_blocks/blocks2.txt"
BLOCK_FILE_DIL = "\t"

"""
# WHERE YOU LEFT OFF:
every block with a variation on MCPE/MCJE will need an Andromeda Variant
the Variants are definined in block implementations, and the IdMaps are only for getting the Java/Pocket data
for exporting purposes.
may need to make Variants similar to BlockStates on java for sanity
no major code changes needed, just add onto blocks spreadsheet and add more variants & their JE1.13 nominal ids

Item* createAndroemdaItem(std::string_view andromeda_item_name) {}
if (andromeda_item_name == "%s")
	return new Item() or new BlockItem(ItemFactory::getBlock())
"""

class AItem():
	aItemName = ""
	aItemVariant = ""
	pocketName = ""
	pocketId = ""
	pocketMeta = ""
	javaLegacyName = ""
	javaLegacyId = ""
	javaLegacyMeta = ""
	javaNominalId = ""
	pocketNominalId = ""

	def __init__(self, andromeda_item_name, andromeda_item_variant, pocket_name, pocket_id, pocket_meta, java_legacy_name, java_legacy_id, java_legacy_meta, java_nominal_id, pocket_nominal_id):
		self.aItemName = andromeda_item_name
		self.aItemVariant = andromeda_item_variant
		self.pocketName = pocket_name
		self.pocketId = pocket_id
		self.pocketMeta = pocket_meta
		self.javaLegacyName = java_legacy_name
		self.javaLegacyId = java_legacy_id
		self.javaLegacyMeta = java_legacy_meta
		self.javaNominalId = java_nominal_id
		self.pocketNominalId = pocket_nominal_id

header_def = """#define DEFAULT_GET_ITEMBLOCK ABlock<Block> block; \\
	bool ret = BlockFactory::getBlock(ABlockID{std::string(andromeda_item_name), std::string(andromeda_item_variant)}, block); \\
	if (ret) \\
		return new BlockItem(block); \\
	else \\
		return nullptr;"""

ANDROMEDA_ITEM_LINE = 'if (andromeda_item_name == "%s") {\n\treturn new /*%s*/Item(%s);\n}'
ANDROMEDA_ITEMBLOCK_LINE = """if (andromeda_item_name == "%s") {
	ABlock<Block> block;
	bool ret = BlockFactory::getBlock(Block::blockMap.at(andromeda_item_name), block);
	if (ret)
		return new BlockItem(block);
	else
		return nullptr;
}
"""
ANDROMEDA_ITEMBLOCK_LINE = """if (andromeda_item_name == "%s") {
	DEFAULT_GET_ITEMBLOCK;
}
"""

JAVA_ITEM_LINE = 'if (java_id == %s) {'
JAVA_ITEM_LINE2 = '\tif (java_damage == %s) {'
JAVA_ITEM_LINE3 = '\t\treturn new %s(%s);'
JAVA_ITEM_LINE4 = '\treturn nullptr;';
JAVA_ITEM_LINE_END = '}'

POCKET_ITEM_LINE = 'if (pocket_id == %s) {'
POCKET_ITEM_LINE2 = '\tif (pocket_damage == %s) {'
POCKET_ITEM_LINE3 = '\t\treturn new %s(%s);'
POCKET_ITEM_LINE4 = '\treturn nullptr;';
POCKET_ITEM_LINE_END = '}'

JAVA_BLOCK_LINE_FACTORY = "\treturn new BlockItem(BlockFactory::getBlockFromJava(%s, static_cast<JavaBlockMetadata>(%s)));"
POCKET_BLOCK_LINE_FACTORY = "\treturn new BlockItem(BlockFactory::getBlockFromPocket(%s, static_cast<PocketBlockMetadata>(%s)));"

def read_file(path):
	with open(path) as f:
   		data = f.readlines()
   	return data

def read_file_as_str(path):
	with open(path) as f:
   		data = f.read()
   	return data

java_legacy_numerical_item_map = {}
java_legacy_item_map = {}
java_nominal_item_map = {}
pocket_numerical_item_map = {}
pocket_item_map = {}

java_legacy_numerical_block_map = {}
java_legacy_block_map = {}
java_nominal_block_map = {}
pocket_numerical_block_map = {}
pocket_block_map = {}

completed_blocks_to_avoid_dupes = []

def add_java_legacy_numerical_factory_def(java_item_legacy_id, java_item_damage, andromeda_item_obj):

	if java_item_legacy_id not in java_legacy_numerical_item_map:
		java_legacy_numerical_item_map[java_item_legacy_id] = []

	java_legacy_numerical_item_map[java_item_legacy_id].append([java_item_damage, andromeda_item_obj])

def add_java_legacy_factory_def(java_item_name, java_item_damage, andromeda_item_obj):

	if java_item_name not in java_legacy_item_map:
		java_legacy_item_map[java_item_name] = []

	java_legacy_item_map[java_item_name].append([java_item_damage, andromeda_item_obj])

def add_java_nominal_factory_def(java_nominal_id, andromeda_item_obj):

	# if java_nominal_id not in java_nominal_item_map:
	# 	java_nominal_item_map[java_nominal_id] = []

	java_nominal_item_map[java_nominal_id] = andromeda_item_obj

def add_pocket_numerical_factory_def(pocket_item_id, pocket_item_damage, andromeda_item_obj):

	if pocket_item_id not in pocket_numerical_item_map:
		pocket_numerical_item_map[pocket_item_id] = []

	pocket_numerical_item_map[pocket_item_id].append([pocket_item_damage, andromeda_item_obj])

def add_pocket_factory_def(pocket_item_name, pocket_item_damage, andromeda_item_obj):

	if pocket_item_name not in pocket_item_map:
		pocket_item_map[pocket_item_name] = []

	pocket_item_map[pocket_item_name].append([pocket_item_damage, andromeda_item_obj])

## BLOCKs

def add_java_legacy_numerical_block_def(java_block_id, java_block_meta, andromeda_block_name, andromeda_block_variant):
	if java_block_id not in java_legacy_numerical_block_map:
		java_legacy_numerical_block_map[java_block_id] = []
	java_legacy_numerical_block_map[java_block_id].append([java_block_meta, andromeda_block_name, andromeda_block_variant])
	pass

def add_java_legacy_block_def(java_block_name, java_block_meta, andromeda_block_name, andromeda_block_variant):
	if java_block_name not in java_legacy_block_map:
		java_legacy_block_map[java_block_name] = []
	java_legacy_block_map[java_block_name].append([java_block_meta, andromeda_block_name, andromeda_block_variant])
	pass

def add_java_nominal_block_def(java_nominal_id, andromeda_block_name, andromeda_block_variant):
	java_nominal_block_map[java_nominal_id.strip()] = [andromeda_block_name, andromeda_block_variant]
	pass

def add_pocket_numerical_block_def(pocket_block_id, pocket_block_meta, andromeda_block_name, andromeda_block_variant):
	if pocket_block_id not in pocket_numerical_block_map:
		pocket_numerical_block_map[pocket_block_id] = []
	pocket_numerical_block_map[pocket_block_id].append([pocket_block_meta, andromeda_block_name, andromeda_block_variant])
	pass

def add_pocket_block_def(pocket_block_name, pocket_block_meta, andromeda_block_name, andromeda_block_variant):
	if pocket_block_name not in pocket_block_map:
		pocket_block_map[pocket_block_name] = []
	pocket_block_map[pocket_block_name].append([pocket_block_meta, andromeda_block_name, andromeda_block_variant])	
	pass

pb_andromeda_item_block_line = []
pb_andromeda_item_line = []
pb_java_legacy_numerical_item_map = []
pb_java_legacy_numerical_block_map = []
pb_java_legacy_item_map = []
pb_java_legacy_block_map = []
pb_java_nominal_item_map = []
pb_java_nominal_block_map = []
pb_pocket_numerical_item_map = []
pb_pocket_numerical_block_map = []
pb_pocket_numerical_item_map = []
pb_pocket_numerical_block_map = []
pb_pocket_item_map = []
pb_pocket_block_map = []

for line in read_file(ITEM_FILE):

	if line.startswith('#'):
		continue

	andromeda_item_name, andromeda_item_variant, java_item_name, java_item_legacy_id, java_item_damage, \
	java_nominal_id, pocket_item_id, pocket_item_damage, pocket_nominal_id, item_class = line.split(ITEM_FILE_DIL)

	if andromeda_item_name == "":
		andromeda_item_name = java_item_name

	if andromeda_item_variant == "":
		andromeda_item_variant = "default"

	if java_item_damage == "":
		java_item_damage = "0"

	if pocket_item_damage == "":
		pocket_item_damage = "0"
	andromeda_item_obj = AItem(andromeda_item_name, andromeda_item_variant, pocket_nominal_id, pocket_item_id, pocket_item_damage, java_item_name, java_item_legacy_id, java_item_damage, java_nominal_id, "")

	add_java_legacy_numerical_factory_def(java_item_legacy_id, java_item_damage, andromeda_item_obj)
	add_java_legacy_factory_def(java_item_name, java_item_damage, andromeda_item_obj)
	add_java_nominal_factory_def(java_nominal_id, andromeda_item_obj)
	add_pocket_numerical_factory_def(pocket_item_id, pocket_item_damage, andromeda_item_obj)
	add_pocket_factory_def(pocket_nominal_id, pocket_item_damage, andromeda_item_obj)

	pb_andromeda_item_line.append(ANDROMEDA_ITEM_LINE % (andromeda_item_name, item_class.strip(), 'AndromedaItemMap.at(std::make_pair("%s", "%s"))'%(andromeda_item_name, andromeda_item_variant)))
	# print ANDROMEDA_ITEM_LINE % (andromeda_item_name, item_class.strip(), 'AndromedaItemMap.at(std::make_pair("%s", "%s"))'%(andromeda_item_name, andromeda_item_variant))

for line in read_file(BLOCK_FILE):

	if line.startswith('#'):
		continue

	andromedaBlockName, andromedaBlockVariant, javaLegacyBlockName, javaLegacyBlockMeta, pocketBlockId, pocketBlockMeta, \
	javaLegacyBlockId, pocketBlockName, notes, javaNominalId = line.split(BLOCK_FILE_DIL)

	add_java_legacy_numerical_block_def(javaLegacyBlockId, javaLegacyBlockMeta, andromedaBlockName, andromedaBlockVariant)
	add_java_legacy_block_def(javaLegacyBlockName, javaLegacyBlockMeta, andromedaBlockName, andromedaBlockVariant)
	add_java_nominal_block_def(javaNominalId, andromedaBlockName, andromedaBlockVariant)
	add_pocket_numerical_block_def(pocketBlockId, pocketBlockMeta, andromedaBlockName, andromedaBlockVariant)
	add_pocket_block_def(pocketBlockName, pocketBlockMeta, andromedaBlockName, andromedaBlockVariant)

	if andromedaBlockName not in completed_blocks_to_avoid_dupes:
		#print ANDROMEDA_ITEMBLOCK_LINE % andromedaBlockName
		pb_andromeda_item_block_line.append(ANDROMEDA_ITEMBLOCK_LINE % andromedaBlockName)
		completed_blocks_to_avoid_dupes.append(andromedaBlockName)


# for ji in java_legacy_numerical_item_map:
# 	print JAVA_ITEM_LINE % ji
# 	for i in java_legacy_numerical_item_map[ji]:
# 		print JAVA_ITEM_LINE2 % i[0]
# 		print JAVA_ITEM_LINE3 % ("Item", 'AndromedaItemMap.at(std::make_pair("%s", "%s"))'%(i[1].aItemName, i[1].aItemVariant))
# 		print "\t", JAVA_ITEM_LINE_END
# 	print JAVA_ITEM_LINE4
# 	print JAVA_ITEM_LINE_END

for ji in java_legacy_numerical_item_map:
	pb_java_legacy_numerical_item_map.append(JAVA_ITEM_LINE % ji)
	for i in java_legacy_numerical_item_map[ji]:
		pb_java_legacy_numerical_item_map.append( JAVA_ITEM_LINE2 % i[0])
		pb_java_legacy_numerical_item_map.append( JAVA_ITEM_LINE3 % ("Item", 'AndromedaItemMap.at(std::make_pair("%s", "%s"))'%(i[1].aItemName, i[1].aItemVariant)))
		pb_java_legacy_numerical_item_map.append( "\t" + JAVA_ITEM_LINE_END)
	pb_java_legacy_numerical_item_map.append( JAVA_ITEM_LINE4)
	pb_java_legacy_numerical_item_map.append( JAVA_ITEM_LINE_END)

# for ji in java_legacy_numerical_block_map:
# 	print JAVA_ITEM_LINE % ji, " /* Block ID */"
# 	# for i in java_legacy_numerical_block_map[ji]:
# 	# 	if i[0] == "-1":
# 	# 		print JAVA_ITEM_LINE2 % "0", "/* SPECIAL DATA */"
# 	# 	print JAVA_ITEM_LINE2 % i[0]
# 	# 	print JAVA_ITEM_LINE3 % ("Item", 'AndromedaItemMap.at(std::make_pair("%s", "%s"))'%(i[1], i[2]))
# 	# 	print "\t", JAVA_ITEM_LINE_END
# 	print JAVA_BLOCK_LINE_FACTORY % ("java_id", "java_damage")
# 	print JAVA_ITEM_LINE_END

for ji in java_legacy_numerical_block_map:
	pb_java_legacy_numerical_block_map.append((JAVA_ITEM_LINE % ji) + " /* Block ID */")
	pb_java_legacy_numerical_block_map.append(JAVA_BLOCK_LINE_FACTORY % ("static_cast<unsigned char>java_id", "java_damage"))
	pb_java_legacy_numerical_block_map.append(JAVA_ITEM_LINE_END)

# for ji in java_legacy_item_map:
# 	print JAVA_ITEM_LINE % '"%s"'%ji
# 	for i in java_legacy_item_map[ji]:
# 		print JAVA_ITEM_LINE2 % i[0]
# 		print JAVA_ITEM_LINE3 % ("Item", 'AndromedaItemMap.at(std::make_pair("%s", "%s"))'%(i[1].aItemName, i[1].aItemVariant))
# 		print "\t", JAVA_ITEM_LINE_END
# 	print JAVA_ITEM_LINE4
# 	print JAVA_ITEM_LINE_END

for ji in java_legacy_item_map:
	pb_java_legacy_item_map.append( JAVA_ITEM_LINE % '"%s"'%ji)
	for i in java_legacy_item_map[ji]:
		pb_java_legacy_item_map.append( JAVA_ITEM_LINE2 % i[0])
		pb_java_legacy_item_map.append( JAVA_ITEM_LINE3 % ("Item", 'AndromedaItemMap.at(std::make_pair("%s", "%s"))'%(i[1].aItemName, i[1].aItemVariant)))
		pb_java_legacy_item_map.append( "\t" + JAVA_ITEM_LINE_END)
	pb_java_legacy_item_map.append( JAVA_ITEM_LINE4)
	pb_java_legacy_item_map.append( JAVA_ITEM_LINE_END)

# for ji in java_legacy_block_map:
# 	print JAVA_ITEM_LINE % '"%s"' % ji, " /* Block ID */"
# 	# for i in java_legacy_numerical_block_map[ji]:
# 	# 	if i[0] == "-1":
# 	# 		print JAVA_ITEM_LINE2 % "0", "/* SPECIAL DATA */"
# 	# 	print JAVA_ITEM_LINE2 % i[0]
# 	# 	print JAVA_ITEM_LINE3 % ("Item", 'AndromedaItemMap.at("%s", "%s")'%(i[1], i[2]))
# 	# 	print "\t", JAVA_ITEM_LINE_END
# 	print JAVA_BLOCK_LINE_FACTORY % ("java_id", "java_damage")
# 	print JAVA_ITEM_LINE_END

for ji in java_legacy_block_map:
	pb_java_legacy_block_map.append( (JAVA_ITEM_LINE % '"%s"' % ji) + " /* Block ID */")
	pb_java_legacy_block_map.append( JAVA_BLOCK_LINE_FACTORY % ("java_id", "java_damage"))
	pb_java_legacy_block_map.append( JAVA_ITEM_LINE_END)

# for ji in java_legacy_block_map:
# 	print JAVA_ITEM_LINE % '"%s"' % ji, " /* Block ID */"
# 	# for i in java_legacy_numerical_block_map[ji]:
# 	# 	if i[0] == "-1":
# 	# 		print JAVA_ITEM_LINE2 % "0", "/* SPECIAL DATA */"
# 	# 	print JAVA_ITEM_LINE2 % i[0]
# 	# 	print JAVA_ITEM_LINE3 % ("Item", 'AndromedaItemMap.at(std::make_pair("%s", "%s"))'%(i[1], i[2]))
# 	# 	print "\t", JAVA_ITEM_LINE_END
# 	print JAVA_BLOCK_LINE_FACTORY % ("java_id", "java_damage")
# 	print JAVA_ITEM_LINE_END

for ji in java_legacy_block_map:
	pb_java_legacy_block_map.append( (JAVA_ITEM_LINE % '"%s"' % ji) + " /* Block ID */")
	pb_java_legacy_block_map.append( JAVA_BLOCK_LINE_FACTORY % ("java_id", "java_damage"))
	pb_java_legacy_block_map.append( JAVA_ITEM_LINE_END)

# for ji in java_nominal_item_map:
# 	print JAVA_ITEM_LINE % '"%s"'%ji
# 	print JAVA_ITEM_LINE3 % ("Item", 'AndromedaItemMap.at(std::make_pair("%s", "%s"))'%(java_nominal_item_map[ji].aItemName, java_nominal_item_map[ji].aItemVariant))
# 	print JAVA_ITEM_LINE_END

for ji in java_nominal_item_map:
	pb_java_nominal_item_map.append( JAVA_ITEM_LINE % '"%s"'%ji)
	pb_java_nominal_item_map.append( JAVA_ITEM_LINE3 % ("Item", 'AndromedaItemMap.at(std::make_pair("%s", "%s"))'%(java_nominal_item_map[ji].aItemName, java_nominal_item_map[ji].aItemVariant)))
	pb_java_nominal_item_map.append( JAVA_ITEM_LINE_END)

# for ji in java_nominal_block_map:
# 	print JAVA_ITEM_LINE % '"%s"' % ji, " /* Block ID */"
# 	# for i in java_legacy_numerical_block_map[ji]:
# 	# 	if i[0] == "-1":
# 	# 		print JAVA_ITEM_LINE2 % "0", "/* SPECIAL DATA */"
# 	# 	print JAVA_ITEM_LINE2 % i[0]
# 	# 	print JAVA_ITEM_LINE3 % ("Item", 'AndromedaItemMap.at(std::make_pair("%s", "%s"))'%(i[1], i[2]))
# 	# 	print "\t", JAVA_ITEM_LINE_END
# 	print JAVA_BLOCK_LINE_FACTORY % ("java_id", "java_damage")
# 	print JAVA_ITEM_LINE_END

for ji in java_nominal_block_map:
	pb_java_nominal_block_map.append( (JAVA_ITEM_LINE % '"%s"' % ji) + " /* Block ID */")
	pb_java_nominal_block_map.append( JAVA_ITEM_LINE3 % ("Item", 'AndromedaItemMap.at(std::make_pair("%s", "%s"))'%(java_nominal_item_map[ji].aItemName, java_nominal_item_map[ji].aItemVariant)))
	pb_java_nominal_block_map.append( JAVA_ITEM_LINE_END)

# for ji in pocket_numerical_item_map:
# 	print POCKET_ITEM_LINE % ji
# 	for i in pocket_numerical_item_map[ji]:
# 		print POCKET_ITEM_LINE2 % i[0]
# 		print POCKET_ITEM_LINE3 % ("Item", 'AndromedaItemMap.at(std::make_pair("%s", "%s"))'%(i[1].aItemName, i[1].aItemVariant))
# 		print "\t", POCKET_ITEM_LINE_END
# 	print POCKET_ITEM_LINE4
# 	print POCKET_ITEM_LINE_END

for ji in pocket_numerical_item_map:
	pb_pocket_numerical_item_map.append( POCKET_ITEM_LINE % ji)
	for i in pocket_numerical_item_map[ji]:
		pb_pocket_numerical_item_map.append( POCKET_ITEM_LINE2 % i[0])
		pb_pocket_numerical_item_map.append( POCKET_ITEM_LINE3 % ("Item", 'AndromedaItemMap.at(std::make_pair("%s", "%s"))'%(i[1].aItemName, i[1].aItemVariant)))
		pb_pocket_numerical_item_map.append( "\t" + POCKET_ITEM_LINE_END)
	pb_pocket_numerical_item_map.append( POCKET_ITEM_LINE4)
	pb_pocket_numerical_item_map.append( POCKET_ITEM_LINE_END)

# for ji in pocket_numerical_block_map:
# 	print POCKET_ITEM_LINE % ji, " /* Block ID */"
# 	print POCKET_BLOCK_LINE_FACTORY % ("pocket_id", "pocket_damage")
# 	print POCKET_ITEM_LINE_END


for ji in pocket_numerical_block_map:
	pb_pocket_numerical_block_map.append( (POCKET_ITEM_LINE % ji) + " /* Block ID */")
	pb_pocket_numerical_block_map.append( POCKET_BLOCK_LINE_FACTORY % ("static_cast<PocketBlockID>(pocket_id)", "pocket_damage"))
	pb_pocket_numerical_block_map.append( POCKET_ITEM_LINE_END)

# for ji in pocket_item_map:
# 	print POCKET_ITEM_LINE % '"%s"'%ji
# 	for i in pocket_item_map[ji]:
# 		print POCKET_ITEM_LINE2 % i[0]
# 		print POCKET_ITEM_LINE3 % ("Item", 'AndromedaItemMap.at(std::make_pair("%s", "%s"))'%(i[1].aItemName, i[1].aItemVariant))
# 		print "\t", POCKET_ITEM_LINE_END
# 	print POCKET_ITEM_LINE4
# 	print POCKET_ITEM_LINE_END

for ji in pocket_item_map:
	pb_pocket_item_map.append( POCKET_ITEM_LINE % '"%s"'%ji)
	for i in pocket_item_map[ji]:
		pb_pocket_item_map.append( POCKET_ITEM_LINE2 % i[0])
		pb_pocket_item_map.append( POCKET_ITEM_LINE3 % ("Item", 'AndromedaItemMap.at(std::make_pair("%s", "%s"))'%(i[1].aItemName, i[1].aItemVariant)))
		pb_pocket_item_map.append( "\t" + POCKET_ITEM_LINE_END)
	pb_pocket_item_map.append( POCKET_ITEM_LINE4)
	pb_pocket_item_map.append( POCKET_ITEM_LINE_END)

# for ji in pocket_block_map:
# 	print POCKET_ITEM_LINE % '"%s"'%ji, " /* Block ID */"
# 	print POCKET_BLOCK_LINE_FACTORY % ("pocket_id", "pocket_damage")
# 	print POCKET_ITEM_LINE_END

for ji in pocket_block_map:
	pb_pocket_block_map.append( (POCKET_ITEM_LINE % '"%s"'%ji) + " /* Block ID */")
	pb_pocket_block_map.append( POCKET_BLOCK_LINE_FACTORY % ("pocket_id", "pocket_damage"))
	pb_pocket_block_map.append( POCKET_ITEM_LINE_END)

of = read_file_as_str("./ItemFactory_templ.cpp")
print of % {
	"HeaderDefs": header_def,
	"FromJavaLegacyNumerical": "%s\n\t\t%s" % ("\n\t\t".join(pb_java_legacy_numerical_item_map), "\n\t\t".join(pb_java_legacy_numerical_block_map)),
	"FromJavaLegacyNames": "%s\n\t\t%s" % ("\n\t\t".join(pb_java_legacy_item_map), "\n\t\t".join(pb_java_legacy_block_map)),
	"FromJavaNominal": "%s\n\t\t%s" % ("\n\t\t".join(pb_java_nominal_item_map), "\n\t\t".join(pb_java_nominal_block_map)),
	"FromPocketNumerical": "%s\n\t\t%s" % ("\n\t\t".join(pb_pocket_numerical_item_map), "\n\t\t".join(pb_pocket_numerical_block_map)),
	"FromPocketNames": "%s\n\t\t%s" % ("\n\t\t".join(pb_pocket_item_map), "\n\t\t".join(pb_pocket_block_map)),
	"AndromedaItems": "%s\n\t\t%s" % ("\n".join(pb_andromeda_item_line).replace("\n", "\n\t\t"), "\n".join(pb_andromeda_item_block_line).replace("\n", "\n\t\t"))
}