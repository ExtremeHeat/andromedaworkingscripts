ITEM_FILE = "items2.csv"
FILE_DILEM = ","

def read_file(path):
	with open(path) as f:
   		data = f.readlines()
   	return data


BEGIN_JAVALEGACYNUMERICAL_MAP = "extern const std::map<std::pair<short, short>, AItemID> JavaLegacyNumericalMap = {";
BEGIN_JAVANOMINAL_MAP = "extern const std::map<std::string, AItemID> JavaNominalItemMap = {"
BEGIN_POCKETNUMERICAL_MAP = "extern const std::map<std::pair<short,short>, AItemID> PocketNumericalItemMap = {"
BEGIN_POCKET_MAP = "extern const std::map<std::pair<std::string, short>, AItemID> PocketItemMap = {"
BEGIN_ANDROMEDAITEM_MAP = "extern const std::map<std::pair<std::string, std::string>, AItemID> AndromedaItemMap = {"

BEGIN_POCKETNUMERICAL_LINE = JAVALEGACYNUMERICAL_LINE = "\t{std::make_pair(%s, %s), %s},"
JAVANOMINAL_LINE = '\t{"%s", %s},'
POCKET_LINE = '\t{std::make_pair("%s", %s), %s},'
ANDROMEDAITEM_LINE = '\t{std::make_pair("%s", "%s"), %s},'

END_MAP = "};"

java_legacy_numerical_map_list = []
java_nominal_map_list = []
pocket_numerical_map_list = []
pocket_map_list = []
andromeda_item_list = []

class AItem():
	aItemName = ""
	aItemVariant = ""
	pocketName = ""
	pocketId = ""
	pocketMeta = ""
	javaLegacyName = ""
	javaLegacyId = ""
	javaLegacyMeta = ""
	javaNominalId = ""
	pocketNominalId = ""

	def __init__(self, andromeda_item_name, andromeda_item_variant, pocket_name, pocket_id, pocket_meta, java_legacy_name, java_legacy_id, java_legacy_meta, java_nominal_id, pocket_nominal_id):
		self.aItemName = andromeda_item_name
		self.aItemVariant = andromeda_item_variant
		self.pocketName = pocket_name
		self.pocketId = pocket_id
		self.pocketMeta = pocket_meta
		self.javaLegacyName = java_legacy_name
		self.javaLegacyId = java_legacy_id
		self.javaLegacyMeta = java_legacy_meta
		self.javaNominalId = java_nominal_id
		self.pocketNominalId = pocket_nominal_id

def create_aitem_str(aio):
	return 'AItemID{"%s", "%s", "%s", %s, %s, "%s", %s, %s, "%s", "%s"}' % \
	(aio.aItemName, aio.aItemVariant, aio.pocketName, aio.pocketId, aio.pocketMeta, aio.javaLegacyName, aio.javaLegacyId, aio.javaLegacyMeta, aio.javaNominalId, aio.pocketNominalId)

def write_legacy_numerical_map_line(java_item_legacy_id, java_item_damage, andromeda_item_obj):
	pps = ""
	if java_item_legacy_id == "0":
		pps = "//"
	java_legacy_numerical_map_list.append(pps+JAVALEGACYNUMERICAL_LINE % (java_item_legacy_id, java_item_damage, create_aitem_str(andromeda_item_obj)))

def write_java_nominal_map_line(java_nominal_id, andromeda_item_obj):
	pps = ""
	if java_nominal_id == "":
		pps = "//"
	java_nominal_map_list.append(pps+JAVANOMINAL_LINE % (java_nominal_id, create_aitem_str(andromeda_item_obj)))

def write_pocket_numerical_map_line(pocket_item_id, pocket_item_damage, andromeda_item_obj):
	pps = ""
	if pocket_item_id == "0":
		pps = "//"
	pocket_numerical_map_list.append(pps+BEGIN_POCKETNUMERICAL_LINE % (pocket_item_id, pocket_item_damage, create_aitem_str(andromeda_item_obj)))

def write_pocket_map_list_line(pocket_item_name, pocket_item_damage, andromeda_item_obj):
	pps = ""
	if pocket_item_name == "":
		pps = "//"
	pocket_map_list.append(pps+POCKET_LINE % (pocket_item_name, pocket_item_damage, create_aitem_str(andromeda_item_obj)))

def write_andromeda_item_line(andromeda_item_obj):
	andromeda_item_list.append(ANDROMEDAITEM_LINE % (andromeda_item_obj.aItemName, andromeda_item_obj.aItemVariant, create_aitem_str(andromeda_item_obj)))

for line in read_file(ITEM_FILE):
	if line.startswith('#'):
		continue
	#print line

	andromeda_item_name, andromeda_item_variant, java_item_name, java_item_legacy_id, java_item_damage, \
	java_nominal_id, pocket_item_id, pocket_item_damage, pocket_nominal_id, item_class = line.split(FILE_DILEM)

	if andromeda_item_name == "":
		andromeda_item_name = java_item_name

	if andromeda_item_variant == "":
		andromeda_item_variant = "default"

	if java_item_damage == "":
		java_item_damage = "0"

	if pocket_item_damage == "":
		pocket_item_damage = "0"

	andromeda_item_obj = AItem(andromeda_item_name, andromeda_item_variant, pocket_nominal_id, pocket_item_id, pocket_item_damage, java_item_name, java_item_legacy_id, java_item_damage, java_nominal_id, "")

	#print '%s:%s:\n\tJava NumID: %s Pocket NumID: %s' % (andromeda_item_name, andromeda_item_variant, java_item_legacy_id, pocket_item_id)

	write_legacy_numerical_map_line(java_item_legacy_id, java_item_damage, andromeda_item_obj)
	write_java_nominal_map_line(java_nominal_id, andromeda_item_obj)
	write_pocket_numerical_map_line(pocket_item_id, pocket_item_damage, andromeda_item_obj)
	write_pocket_map_list_line(pocket_nominal_id, pocket_item_damage, andromeda_item_obj)
	write_andromeda_item_line(andromeda_item_obj)


print BEGIN_JAVALEGACYNUMERICAL_MAP
for i in java_legacy_numerical_map_list:
	print i
print END_MAP

print BEGIN_JAVANOMINAL_MAP
for i in java_nominal_map_list:
	print i
print END_MAP

print BEGIN_POCKETNUMERICAL_MAP
for i in pocket_numerical_map_list:
	print i
print END_MAP

print BEGIN_POCKET_MAP
for i in pocket_map_list:
	print i
print END_MAP

print BEGIN_ANDROMEDAITEM_MAP
for i in andromeda_item_list:
	print i
print END_MAP