namespace Andromeda {
	struct BlockVariant {
		std::string variant_id;
		JavaBlockID java_block_id;
		JavaBlockMetadata java_block_metadata;
		PocketBlockID pocket_block_id;
		PocketBlockMetadata pocket_block_metadata;
	}
}