namespace Andromeda {
	class Block;

	struct ABlock {
		Block* block;
		std::string variant;
	}
}