namespace Andromeda {
	void initBlocks() {
		Block* AIR_ID = new Block("AIR_ID", Material::air, -1, -1, 0);
		Block* stone = new Block("stone", Material::rock, 1.5, 10, 0);
		Block* grass = new Block("grass", Material::grass, 0.6, -1, 0);
		Block* dirt = new Block("dirt", Material::ground, 0.5, -1, 0);
		Block* cobblestone = new Block("cobblestone", Material::rock, 2, 10, 0);
		Block* planks = new Block("planks", Material::wood, 2, 5, 0);
		Block* sapling = new Block("sapling", Material::plants, 0, -1, 0);
		Block* bedrock = new Block("bedrock", Material::rock, -1, 6000000, 0);
		Block* flowing_water = new Block("flowing_water", Material::water, 100, -1, 0);
		Block* water = new Block("water", Material::water, 100, -1, 0);
		Block* flowing_lava = new Block("flowing_lava", Material::lava, 100, -1, 1);
		Block* lava = new Block("lava", Material::lava, 100, -1, 1);
		Block* sand = new Block("sand", Material::sand, 0.5, -1, 0);
		Block* gravel = new Block("gravel", Material::sand, 0.6, -1, 0);
		Block* gold_ore = new Block("gold_ore", Material::rock, 3, 5, 0);
		Block* iron_ore = new Block("iron_ore", Material::rock, 3, 5, 0);
		Block* coal_ore = new Block("coal_ore", Material::rock, 3, 5, 0);
		Block* log = new DirectionalBlock("log", Material::wood, -1, -1, 0);
		Block* leaves = new Block("leaves", Material::leaves, -1, -1, 0);
		Block* sponge = new Block("sponge", Material::sponge, 0.6, -1, 0);
		Block* glass = new Block("glass", Material::glass, -1, -1, 0);
		Block* lapis_ore = new Block("lapis_ore", Material::rock, 3, 5, 0);
		Block* lapis_block = new Block("lapis_block", Material::-, -1, -1, 0);
		Block* dispenser = new DirectionalBlock("dispenser", Material::rock, 3.5, -1, 0);
		Block* sandstone = new Block("sandstone", Material::-, -1, -1, 0);
		Block* noteblock = new PartialBlock("noteblock", Material::wood, 0.8, -1, 0);
		Block* bed = new DirectionalBlock("bed", Material::cloth, 0.2, -1, 0);
		Block* golden_rail = new DirectionalBlock("golden_rail", Material::circuits, 0.7, -1, 0);
		Block* detector_rail = new DirectionalBlock("detector_rail", Material::circuits, 0.7, -1, 0);
		Block* sticky_piston = new DirectionalBlock("sticky_piston", Material::piston, -1, -1, 0);
		Block* web = new Block("web", Material::web, 4, -1, 0);
		Block* tallgrass = new Block("tallgrass", Material::vine, 0, -1, 0);
		Block* deadbush = new Block("deadbush", Material::vine, 0, -1, 0);
		Block* piston = new DirectionalBlock("piston", Material::piston, -1, -1, 0);
		Block* piston_head = new DirectionalBlock("piston_head", Material::piston, -1, -1, 0);
		Block* wool = new Block("wool", Material::cloth, 0.8, -1, 0);
		Block* piston_moving_piece = new Block("piston_moving_piece", Material::piston, -1, -1, 0);
		Block* yellow_flower = new Block("yellow_flower", Material::plants, 0, -1, 0);
		Block* red_flower = new Block("red_flower", Material::plants, 0, -1, 0);
		Block* brown_mushroom = new Block("brown_mushroom", Material::plants, 0, -1, 0.125);
		Block* red_mushroom = new Block("red_mushroom", Material::plants, -1, -1, 0);
		Block* gold_block = new Block("gold_block", Material::iron, -1, -1, 0);
		Block* iron_block = new Block("iron_block", Material::iron, -1, -1, 0);
		Block* double_stone_slab = new Block("double_stone_slab", Material::rock, 2, 10, 0);
		Block* stone_slab = new DirectionalBlock("stone_slab", Material::rock, 2, 10, 0);
		Block* brick_block = new Block("brick_block", Material::rock, -1, -1, 0);
		Block* tnt = new Block("tnt", Material::tnt, 0, -1, 0);
		Block* bookshelf = new Block("bookshelf", Material::wood, 1.5, -1, 0);
		Block* mossy_cobblestone = new Block("mossy_cobblestone", Material::rock, 2, 10, 0);
		Block* obsidian = new Block("obsidian", Material::rock, 50, 2000, 0);
		Block* torch = new DirectionalBlock("torch", Material::circuits, 0, -1, 0.9375);
		Block* fire = new Block("fire", Material::fire, 0, -1, 1);
		Block* mob_spawner = new Block("mob_spawner", Material::rock, 5, -1, 0);
		Block* oak_stairs = new DirectionalBlock("oak_stairs", Material::rock, -1, -1, 0);
		Block* chest = new DirectionalBlock("chest", Material::wood, 2.5, -1, 0);
		Block* redstone_wire = new Blocks::RedstoneWire("redstone_wire", Material::circuits, 0, -1, 0);
		Block* diamond_ore = new Block("diamond_ore", Material::rock, 3, 5, 0);
		Block* diamond_block = new Block("diamond_block", Material::iron, -1, -1, 0);
		Block* crafting_table = new Block("crafting_table", Material::wood, 2.5, -1, 0);
		Block* wheat = new Block("wheat", Material::plants, -1, -1, 0);
		Block* farmland = new Block("farmland", Material::ground, -1, -1, 0);
		Block* furnace = new Block("furnace", Material::rock, 3.5, -1, 0);
		Block* burning_furnace = new Block("burning_furnace", Material::rock, 3.5, -1, 0.875);
		Block* sign_post = new DirectionalBlock("sign_post", Material::wood, 1, -1, 0);
		Block* door = new DirectionalBlock("door", Material::wood, 3, -1, 0);
		Block* ladder = new DirectionalBlock("ladder", Material::circuits, 0.4, -1, 0);
		Block* rail = new DirectionalBlock("rail", Material::circuits, 0.7, -1, 0);
		Block* cobblestone_stairs = new DirectionalBlock("cobblestone_stairs", Material::rock, -1, -1, 0);
		Block* wall_sign = new DirectionalBlock("wall_sign", Material::wood, 1, -1, 0);
		Block* lever = new DirectionalBlock("lever", Material::circuits, 0.5, -1, 0);
		Block* stone_pressure_plate = new Block("stone_pressure_plate", Material::rock, -1, -1, 0);
		Block* iron_door = new DirectionalBlock("iron_door", Material::iron, 5, -1, 0);
		Block* wooden_pressure_plate = new Block("wooden_pressure_plate", Material::wood, -1, -1, 0);
		Block* redstone_ore = new Block("redstone_ore", Material::rock, 3, 5, 0);
		Block* glowing_redstone_ore = new Block("glowing_redstone_ore", Material::rock, 3, 5, 0.625);
		Block* redstone_torch_off = new DirectionalBlock("redstone_torch_off", Material::circuits, 0, -1, 0);
		Block* redstone_torch_on = new DirectionalBlock("redstone_torch_on", Material::circuits, 0, -1, 0.5);
		Block* stone_button = new DirectionalBlock("stone_button", Material::circuits, 0.5, -1, 0);
		Block* snow_layer = new Block("snow_layer", Material::snow, 0.1, -1, 0);
		Block* ice = new Block("ice", Material::ice, 0.5, -1, 0);
		Block* snow = new Block("snow", Material::crafted_snow, 0.2, -1, 0);
		Block* cactus = new Block("cactus", Material::cactus, 0.4, -1, 0);
		Block* clay = new Block("clay", Material::clay, 0.6, -1, 0);
		Block* sugar_cane_block = new Block("sugar_cane_block", Material::plants, 0, -1, 0);
		Block* jukebox = new Block("jukebox", Material::wood, 2, 10, 0);
		Block* fence = new Block("fence", Material::wood, 2, 5, 0);
		Block* pumpkin = new DirectionalBlock("pumpkin", Material::ground, -1, -1, 0);
		Block* netherrack = new Block("netherrack", Material::rock, 0.4, -1, 0);
		Block* soul_sand = new Block("soul_sand", Material::sand, 0.5, -1, 0);
		Block* glowstone = new Block("glowstone", Material::glass, 0.3, -1, 1);
		Block* portal = new DirectionalBlock("portal", Material::portal, -1, -1, 0.75);
		Block* jack_o_lantern = new DirectionalBlock("jack_o_lantern", Material::gourd, 1, -1, 0);
		Block* cake = new Block("cake", Material::cake, 0.5, -1, 0);
		Block* unpowered_repeater = new Block("unpowered_repeater", Material::circuits, 0, -1, 0);
		Block* powered_repeater = new Block("powered_repeater", Material::circuits, 0, -1, 0);
		Block* stained_glass = new Block("stained_glass", Material::glass, 0.3, -1, 0);
		Block* trapdoor = new DirectionalBlock("trapdoor", Material::wood, 3, -1, 0);
		Block* monster_egg = new Block("monster_egg", Material::clay, 0.75, -1, 0);
		Block* stonebrick = new Block("stonebrick", Material::rock, -1, -1, 0);
		Block* brown_mushroom_block = new Block("brown_mushroom_block", Material::wood, -1, -1, 0);
		Block* red_mushroom_block = new Block("red_mushroom_block", Material::wood, -1, -1, 0);
		Block* iron_bars = new Block("iron_bars", Material::iron, -1, -1, 0);
		Block* glass_pane = new Block("glass_pane", Material::glass, -1, -1, 0);
		Block* melon_block = new Block("melon_block", Material::ground, -1, -1, 0);
		Block* pumpkin_stem = new Block("pumpkin_stem", Material::plants, 0, -1, 0);
		Block* melon_stem = new Block("melon_stem", Material::plants, 0, -1, 0);
		Block* vine = new DirectionalBlock("vine", Material::vine, 0.2, -1, 0);
		Block* fence_gate = new Block("fence_gate", Material::wood, 2, 5, 0);
		Block* brick_stairs = new DirectionalBlock("brick_stairs", Material::rock, -1, -1, 0);
		Block* stone_brick_stairs = new DirectionalBlock("stone_brick_stairs", Material::rock, -1, -1, 0);
		Block* mycelium = new Block("mycelium", Material::grass, 0.6, -1, 0);
		Block* waterlily = new DirectionalBlock("waterlily", Material::plants, 0, -1, 0);
		Block* nether_brick = new Block("nether_brick", Material::rock, -1, -1, 0);
		Block* nether_brick_fence = new Block("nether_brick_fence", Material::rock, -1, -1, 0);
		Block* nether_brick_stairs = new DirectionalBlock("nether_brick_stairs", Material::rock, -1, -1, 0);
		Block* nether_wart = new Block("nether_wart", Material::plants, -1, -1, 0);
		Block* enchanting_table = new Block("enchanting_table", Material::rock, 5, 2000, 0);
		Block* brewing_stand = new DirectionalBlock("brewing_stand", Material::iron, 0.5, -1, 0);
		Block* cauldron = new Block("cauldron", Material::iron, 2, -1, 0);
		Block* end_portal = new Block("end_portal", Material::portal, -1, 6000000, 0);
		Block* end_portal_frame = new Block("end_portal_frame", Material::rock, -1, 6000000, 0);
		Block* end_stone = new Block("end_stone", Material::rock, -1, -1, 0);
		Block* dragon_egg = new Block("dragon_egg", Material::dragon_egg, 3, 15, 0);
		Block* redstone_lamp_off = new Block("redstone_lamp_off", Material::redstone_light, 0.3, -1, 0);
		Block* redstone_lamp_on = new Block("redstone_lamp_on", Material::redstone_light, 0.3, -1, 0);
		Block* double_wooden_slab = new Block("double_wooden_slab", Material::wood, 2, 5, 0);
		Block* wooden_slab = new DirectionalBlock("wooden_slab", Material::wood, 2, 5, 0);
		Block* cocoa = new DirectionalBlock("cocoa", Material::plants, 0.2, 5, 0);
		Block* sandstone_stairs = new DirectionalBlock("sandstone_stairs", Material::rock, -1, -1, 0);
		Block* emerald_ore = new Block("emerald_ore", Material::rock, 3, 5, 0);
		Block* ender_chest = new DirectionalBlock("ender_chest", Material::rock, 22.5, 1000, 0);
		Block* tripwire_hook = new Block("tripwire_hook", Material::circuits, -1, -1, 0);
		Block* tripwire = new Block("tripwire", Material::circuits, -1, -1, 0);
		Block* emerald_block = new Block("emerald_block", Material::iron, -1, -1, 0);
		Block* spruce_stairs = new DirectionalBlock("spruce_stairs", Material::rock, -1, -1, 0);
		Block* birch_stairs = new DirectionalBlock("birch_stairs", Material::rock, -1, -1, 0);
		Block* jungle_stairs = new DirectionalBlock("jungle_stairs", Material::rock, -1, -1, 0);
		Block* command_block = new PartialBlock("command_block", Material::iron, -1, 6000000, 0);
		Block* beacon = new Block("beacon", Material::glass, -1, -1, 0);
		Block* cobblestone_wall = new Block("cobblestone_wall", Material::rock, -1, -1, 0);
		Block* flower_pot = new Block("flower_pot", Material::circuits, 0, -1, 0);
		Block* carrots = new Block("carrots", Material::plants, -1, -1, 0);
		Block* potatoes = new Block("potatoes", Material::plants, -1, -1, 0);
		Block* wooden_button = new DirectionalBlock("wooden_button", Material::circuits, 0.5, -1, 0);
		Block* skull = new DirectionalBlock("skull", Material::circuits, 1, -1, 0);
		Block* anvil = new Blocks::Anvil("anvil", Material::anvil, 5, 2000, 0);
		Block* trapped_chest = new DirectionalBlock("trapped_chest", Material::wood, 2.5, -1, 0);
		Block* gold_pressure_plate = new Block("gold_pressure_plate", Material::iron, -1, -1, 0);
		Block* iron_pressure_plate = new Block("iron_pressure_plate", Material::iron, -1, -1, 0);
		Block* redstone_comparator_off = new DirectionalBlock("redstone_comparator_off", Material::circuits, 0, -1, 0);
		Block* powered_comparator_on = new DirectionalBlock("powered_comparator_on", Material::circuits, 0, -1, 0);
		Block* daylight_detector = new PartialBlock("daylight_detector", Material::wood, -1, -1, 0);
		Block* redstone_block = new Block("redstone_block", Material::iron, -1, -1, 0);
		Block* quartz_ore = new Block("quartz_ore", Material::rock, 3, 5, 0);
		Block* hopper = new Blocks::Hopper("hopper", Material::iron, 3, 8, 0);
		Block* quartz_block = new Block("quartz_block", Material::rock, -1, -1, 0);
		Block* quartz_stairs = new DirectionalBlock("quartz_stairs", Material::rock, -1, -1, 0);
		Block* activator_rail = new DirectionalBlock("activator_rail", Material::circuits, 0.7, -1, 0);
		Block* dropper = new DirectionalBlock("dropper", Material::rock, 3.5, -1, 0);
		Block* stained_hardened_clay = new Block("stained_hardened_clay", Material::rock, 1.25, 7, 0);
		Block* stained_glass_pane = new Block("stained_glass_pane", Material::glass, 0.3, -1, 0);
		Block* leaves2 = new Block("leaves2", Material::leaves, -1, -1, 0);
		Block* log2 = new DirectionalBlock("log2", Material::wood, -1, -1, 0);
		Block* acacia_stairs = new DirectionalBlock("acacia_stairs", Material::rock, -1, -1, 0);
		Block* dark_oak_stairs = new Block("dark_oak_stairs", Material::rock, -1, -1, 0);
		Block* slime = new Block("slime", Material::clay, -1, -1, 0);
		Block* barrier = new Block("barrier", Material::barrier, -1, -1, 0);
		Block* iron_trapdoor = new DirectionalBlock("iron_trapdoor", Material::iron, 5, -1, 0);
		Block* prismarine = new Block("prismarine", Material::rock, 1.5, 10, 0);
		Block* sea_lantern = new Block("sea_lantern", Material::glass, 0.3, -1, 0);
		Block* hay_block = new Block("hay_block", Material::grass, 0.5, -1, 0);
		Block* carpet = new Block("carpet", Material::carpet, 0.1, -1, 0);
		Block* hardened_clay = new Block("hardened_clay", Material::rock, 1.25, 7, 0);
		Block* coal_block = new Block("coal_block", Material::rock, -1, -1, 0);
		Block* packed_ice = new Block("packed_ice", Material::packed_ice, 0.5, -1, 0);
		Block* double_plant = new DoubleBlock("double_plant", Material::vine, -1, -1, 0);
		Block* daylight_detector_inverted = new PartialBlock("daylight_detector_inverted", Material::wood, -1, -1, 0);
		Block* red_sandstone = new Block("red_sandstone", Material::rock, -1, -1, 0);
		Block* red_sandstone_stairs = new DirectionalBlock("red_sandstone_stairs", Material::rock, -1, -1, 0);
		Block* double_stone_slab2 = new Block("double_stone_slab2", Material::rock, 2, 10, 0);
		Block* stone_slab2 = new Block("stone_slab2", Material::rock, 2, 10, 0);
		Block* spruce_fence_gate = new DirectionalBlock("spruce_fence_gate", Material::wood, 2, 5, 0);
		Block* birch_fence_gate = new DirectionalBlock("birch_fence_gate", Material::wood, 2, 5, 0);
		Block* jungle_fence_gate = new DirectionalBlock("jungle_fence_gate", Material::wood, 2, 5, 0);
		Block* dark_oak_fence_gate = new DirectionalBlock("dark_oak_fence_gate", Material::wood, 2, 5, 0);
		Block* acacia_fence_gate = new DirectionalBlock("acacia_fence_gate", Material::wood, 2, 5, 0);
		Block* end_rod = new Block("end_rod", Material::circuits, 0, -1, 0);
		Block* chorus_plant = new Block("chorus_plant", Material::plants, 0.4, -1, 0);
		Block* chorus_flower = new Block("chorus_flower", Material::plants, 0.4, -1, 0);
		Block* purpur_block = new Block("purpur_block", Material::rock, 1.5, 10, 0);
		Block* purpur_stairs = new DirectionalBlock("purpur_stairs", Material::rock, -1, -1, 0);
		Block* end_bricks = new Block("end_bricks", Material::rock, 0.8, -1, 0);
		Block* beetroots = new Block("beetroots", Material::-, -1, -1, 0);
		Block* grass_path = new Block("grass_path", Material::-, -1, -1, 0);
		Block* end_gateway = new Block("end_gateway", Material::portal, -1, 6000000, 0);
		Block* repeating_command_block = new PartialBlock("repeating_command_block", Material::iron, -1, 6000000, 0);
		Block* chain_command_block = new PartialBlock("chain_command_block", Material::iron, -1, 6000000, 0);
		Block* frosted_ice = new Block("frosted_ice", Material::ice, 0.5, -1, 0);
		Block* magma = new Block("magma", Material::-, 0.5, -1, 0);
		Block* nether_wart_block = new Block("nether_wart_block", Material::-, -1, -1, 0);
		Block* red_nether_brick = new Block("red_nether_brick", Material::-, 2, 10, 0);
		Block* bone_block = new Block("bone_block", Material::-, -1, -1, 0);
		Block* structure_void = new Block("structure_void", Material::-, -1, -1, 0);
		Block* structure_block = new Block("structure_block", Material::iron, -1, 6000000, 0);
		Block* observer = new DirectionalBlock("observer", Material::, , , 0);
		Block* shulker_box = new Blocks::ShulkerBox("shulker_box", Material::, , , 0);
		Block* glazed_terracotta = new DirectionalBlock("glazed_terracotta", Material::, , , 0);
		Block* concrete = new Block("concrete", Material::, , , 0);
		Block* concrete_powder = new Block("concrete_powder", Material::, , , 0);
		std::map<std::string, Block*> Block::blockMap = {
		        {"AIR_ID", AIR_ID} ,
		        {"stone", stone} ,
		        {"grass", grass} ,
		        {"dirt", dirt} ,
		        {"cobblestone", cobblestone} ,
		        {"planks", planks} ,
		        {"sapling", sapling} ,
		        {"bedrock", bedrock} ,
		        {"flowing_water", flowing_water} ,
		        {"water", water} ,
		        {"flowing_lava", flowing_lava} ,
		        {"lava", lava} ,
		        {"sand", sand} ,
		        {"gravel", gravel} ,
		        {"gold_ore", gold_ore} ,
		        {"iron_ore", iron_ore} ,
		        {"coal_ore", coal_ore} ,
		        {"log", log} ,
		        {"leaves", leaves} ,
		        {"sponge", sponge} ,
		        {"glass", glass} ,
		        {"lapis_ore", lapis_ore} ,
		        {"lapis_block", lapis_block} ,
		        {"dispenser", dispenser} ,
		        {"sandstone", sandstone} ,
		        {"noteblock", noteblock} ,
		        {"bed", bed} ,
		        {"golden_rail", golden_rail} ,
		        {"detector_rail", detector_rail} ,
		        {"sticky_piston", sticky_piston} ,
		        {"web", web} ,
		        {"tallgrass", tallgrass} ,
		        {"deadbush", deadbush} ,
		        {"piston", piston} ,
		        {"piston_head", piston_head} ,
		        {"wool", wool} ,
		        {"piston_moving_piece", piston_moving_piece} ,
		        {"yellow_flower", yellow_flower} ,
		        {"red_flower", red_flower} ,
		        {"brown_mushroom", brown_mushroom} ,
		        {"red_mushroom", red_mushroom} ,
		        {"gold_block", gold_block} ,
		        {"iron_block", iron_block} ,
		        {"double_stone_slab", double_stone_slab} ,
		        {"stone_slab", stone_slab} ,
		        {"brick_block", brick_block} ,
		        {"tnt", tnt} ,
		        {"bookshelf", bookshelf} ,
		        {"mossy_cobblestone", mossy_cobblestone} ,
		        {"obsidian", obsidian} ,
		        {"torch", torch} ,
		        {"fire", fire} ,
		        {"mob_spawner", mob_spawner} ,
		        {"oak_stairs", oak_stairs} ,
		        {"chest", chest} ,
		        {"redstone_wire", redstone_wire} ,
		        {"diamond_ore", diamond_ore} ,
		        {"diamond_block", diamond_block} ,
		        {"crafting_table", crafting_table} ,
		        {"wheat", wheat} ,
		        {"farmland", farmland} ,
		        {"furnace", furnace} ,
		        {"burning_furnace", burning_furnace} ,
		        {"sign_post", sign_post} ,
		        {"door", door} ,
		        {"ladder", ladder} ,
		        {"rail", rail} ,
		        {"cobblestone_stairs", cobblestone_stairs} ,
		        {"wall_sign", wall_sign} ,
		        {"lever", lever} ,
		        {"stone_pressure_plate", stone_pressure_plate} ,
		        {"iron_door", iron_door} ,
		        {"wooden_pressure_plate", wooden_pressure_plate} ,
		        {"redstone_ore", redstone_ore} ,
		        {"glowing_redstone_ore", glowing_redstone_ore} ,
		        {"redstone_torch_off", redstone_torch_off} ,
		        {"redstone_torch_on", redstone_torch_on} ,
		        {"stone_button", stone_button} ,
		        {"snow_layer", snow_layer} ,
		        {"ice", ice} ,
		        {"snow", snow} ,
		        {"cactus", cactus} ,
		        {"clay", clay} ,
		        {"sugar_cane_block", sugar_cane_block} ,
		        {"jukebox", jukebox} ,
		        {"fence", fence} ,
		        {"pumpkin", pumpkin} ,
		        {"netherrack", netherrack} ,
		        {"soul_sand", soul_sand} ,
		        {"glowstone", glowstone} ,
		        {"portal", portal} ,
		        {"jack_o_lantern", jack_o_lantern} ,
		        {"cake", cake} ,
		        {"unpowered_repeater", unpowered_repeater} ,
		        {"powered_repeater", powered_repeater} ,
		        {"stained_glass", stained_glass} ,
		        {"trapdoor", trapdoor} ,
		        {"monster_egg", monster_egg} ,
		        {"stonebrick", stonebrick} ,
		        {"brown_mushroom_block", brown_mushroom_block} ,
		        {"red_mushroom_block", red_mushroom_block} ,
		        {"iron_bars", iron_bars} ,
		        {"glass_pane", glass_pane} ,
		        {"melon_block", melon_block} ,
		        {"pumpkin_stem", pumpkin_stem} ,
		        {"melon_stem", melon_stem} ,
		        {"vine", vine} ,
		        {"fence_gate", fence_gate} ,
		        {"brick_stairs", brick_stairs} ,
		        {"stone_brick_stairs", stone_brick_stairs} ,
		        {"mycelium", mycelium} ,
		        {"waterlily", waterlily} ,
		        {"nether_brick", nether_brick} ,
		        {"nether_brick_fence", nether_brick_fence} ,
		        {"nether_brick_stairs", nether_brick_stairs} ,
		        {"nether_wart", nether_wart} ,
		        {"enchanting_table", enchanting_table} ,
		        {"brewing_stand", brewing_stand} ,
		        {"cauldron", cauldron} ,
		        {"end_portal", end_portal} ,
		        {"end_portal_frame", end_portal_frame} ,
		        {"end_stone", end_stone} ,
		        {"dragon_egg", dragon_egg} ,
		        {"redstone_lamp_off", redstone_lamp_off} ,
		        {"redstone_lamp_on", redstone_lamp_on} ,
		        {"double_wooden_slab", double_wooden_slab} ,
		        {"wooden_slab", wooden_slab} ,
		        {"cocoa", cocoa} ,
		        {"sandstone_stairs", sandstone_stairs} ,
		        {"emerald_ore", emerald_ore} ,
		        {"ender_chest", ender_chest} ,
		        {"tripwire_hook", tripwire_hook} ,
		        {"tripwire", tripwire} ,
		        {"emerald_block", emerald_block} ,
		        {"spruce_stairs", spruce_stairs} ,
		        {"birch_stairs", birch_stairs} ,
		        {"jungle_stairs", jungle_stairs} ,
		        {"command_block", command_block} ,
		        {"beacon", beacon} ,
		        {"cobblestone_wall", cobblestone_wall} ,
		        {"flower_pot", flower_pot} ,
		        {"carrots", carrots} ,
		        {"potatoes", potatoes} ,
		        {"wooden_button", wooden_button} ,
		        {"skull", skull} ,
		        {"anvil", anvil} ,
		        {"trapped_chest", trapped_chest} ,
		        {"gold_pressure_plate", gold_pressure_plate} ,
		        {"iron_pressure_plate", iron_pressure_plate} ,
		        {"redstone_comparator_off", redstone_comparator_off} ,
		        {"powered_comparator_on", powered_comparator_on} ,
		        {"daylight_detector", daylight_detector} ,
		        {"redstone_block", redstone_block} ,
		        {"quartz_ore", quartz_ore} ,
		        {"hopper", hopper} ,
		        {"quartz_block", quartz_block} ,
		        {"quartz_stairs", quartz_stairs} ,
		        {"activator_rail", activator_rail} ,
		        {"dropper", dropper} ,
		        {"stained_hardened_clay", stained_hardened_clay} ,
		        {"stained_glass_pane", stained_glass_pane} ,
		        {"leaves2", leaves2} ,
		        {"log2", log2} ,
		        {"acacia_stairs", acacia_stairs} ,
		        {"dark_oak_stairs", dark_oak_stairs} ,
		        {"slime", slime} ,
		        {"barrier", barrier} ,
		        {"iron_trapdoor", iron_trapdoor} ,
		        {"prismarine", prismarine} ,
		        {"sea_lantern", sea_lantern} ,
		        {"hay_block", hay_block} ,
		        {"carpet", carpet} ,
		        {"hardened_clay", hardened_clay} ,
		        {"coal_block", coal_block} ,
		        {"packed_ice", packed_ice} ,
		        {"double_plant", double_plant} ,
		        {"daylight_detector_inverted", daylight_detector_inverted} ,
		        {"red_sandstone", red_sandstone} ,
		        {"red_sandstone_stairs", red_sandstone_stairs} ,
		        {"double_stone_slab2", double_stone_slab2} ,
		        {"stone_slab2", stone_slab2} ,
		        {"spruce_fence_gate", spruce_fence_gate} ,
		        {"birch_fence_gate", birch_fence_gate} ,
		        {"jungle_fence_gate", jungle_fence_gate} ,
		        {"dark_oak_fence_gate", dark_oak_fence_gate} ,
		        {"acacia_fence_gate", acacia_fence_gate} ,
		        {"end_rod", end_rod} ,
		        {"chorus_plant", chorus_plant} ,
		        {"chorus_flower", chorus_flower} ,
		        {"purpur_block", purpur_block} ,
		        {"purpur_stairs", purpur_stairs} ,
		        {"end_bricks", end_bricks} ,
		        {"beetroots", beetroots} ,
		        {"grass_path", grass_path} ,
		        {"end_gateway", end_gateway} ,
		        {"repeating_command_block", repeating_command_block} ,
		        {"chain_command_block", chain_command_block} ,
		        {"frosted_ice", frosted_ice} ,
		        {"magma", magma} ,
		        {"nether_wart_block", nether_wart_block} ,
		        {"red_nether_brick", red_nether_brick} ,
		        {"bone_block", bone_block} ,
		        {"structure_void", structure_void} ,
		        {"structure_block", structure_block} ,
		        {"observer", observer} ,
		        {"shulker_box", shulker_box} ,
		        {"glazed_terracotta", glazed_terracotta} ,
		        {"concrete", concrete} ,
		        {"concrete_powder", concrete_powder} ,
		};
		air->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "air", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 0, /* PocketBlockMetadata */ 0);
		stone->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "stone", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 1, /* PocketBlockMetadata */ 0);
		stone->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "stone", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 1, /* PocketBlockMetadata */ 0);
		stone->registerVariant(/* VariantName */ "granite", /* JavaBlockID */ "stone", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 1, /* PocketBlockMetadata */ 1);
		stone->registerVariant(/* VariantName */ "graniteSmooth", /* JavaBlockID */ "stone", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 1, /* PocketBlockMetadata */ 2);
		stone->registerVariant(/* VariantName */ "diorite", /* JavaBlockID */ "stone", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 1, /* PocketBlockMetadata */ 3);
		stone->registerVariant(/* VariantName */ "dioriteSmooth", /* JavaBlockID */ "stone", /* JavaBlockMetadata */ 4, /* PocketBlockID */ 1, /* PocketBlockMetadata */ 4);
		stone->registerVariant(/* VariantName */ "andesite", /* JavaBlockID */ "stone", /* JavaBlockMetadata */ 5, /* PocketBlockID */ 1, /* PocketBlockMetadata */ 5);
		stone->registerVariant(/* VariantName */ "andesiteSmooth", /* JavaBlockID */ "stone", /* JavaBlockMetadata */ 6, /* PocketBlockID */ 1, /* PocketBlockMetadata */ 6);
		grass->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "grass", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 2, /* PocketBlockMetadata */ 0);
		dirt->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "dirt", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 3, /* PocketBlockMetadata */ 0);
		cobblestone->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "cobblestone", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 4, /* PocketBlockMetadata */ 0);
		planks->registerVariant(/* VariantName */ "oak", /* JavaBlockID */ "wood", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 5, /* PocketBlockMetadata */ 0);
		planks->registerVariant(/* VariantName */ "spruce", /* JavaBlockID */ "wood", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 5, /* PocketBlockMetadata */ 1);
		planks->registerVariant(/* VariantName */ "birch", /* JavaBlockID */ "wood", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 5, /* PocketBlockMetadata */ 2);
		planks->registerVariant(/* VariantName */ "jungle", /* JavaBlockID */ "wood", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 5, /* PocketBlockMetadata */ 3);
		planks->registerVariant(/* VariantName */ "acacia", /* JavaBlockID */ "wood", /* JavaBlockMetadata */ 4, /* PocketBlockID */ 5, /* PocketBlockMetadata */ 4);
		planks->registerVariant(/* VariantName */ "big_oak", /* JavaBlockID */ "wood", /* JavaBlockMetadata */ 5, /* PocketBlockID */ 5, /* PocketBlockMetadata */ 5);
		sapling->registerVariant(/* VariantName */ "oak", /* JavaBlockID */ "sapling", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 6, /* PocketBlockMetadata */ 0);
		sapling->registerVariant(/* VariantName */ "spruce", /* JavaBlockID */ "sapling", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 6, /* PocketBlockMetadata */ 1);
		sapling->registerVariant(/* VariantName */ "birch", /* JavaBlockID */ "sapling", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 6, /* PocketBlockMetadata */ 2);
		sapling->registerVariant(/* VariantName */ "jungle", /* JavaBlockID */ "sapling", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 6, /* PocketBlockMetadata */ 3);
		sapling->registerVariant(/* VariantName */ "acacia", /* JavaBlockID */ "sapling", /* JavaBlockMetadata */ 4, /* PocketBlockID */ 6, /* PocketBlockMetadata */ 4);
		sapling->registerVariant(/* VariantName */ "big_oak", /* JavaBlockID */ "sapling", /* JavaBlockMetadata */ 5, /* PocketBlockID */ 6, /* PocketBlockMetadata */ 5);
		bedrock->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "bedrock", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 7, /* PocketBlockMetadata */ 0);
		flowing_water->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "flowing_water", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 8, /* PocketBlockMetadata */ 0);
		water->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "water", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 9, /* PocketBlockMetadata */ 0);
		flowing_lava->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "flowing_lava", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 10, /* PocketBlockMetadata */ 0);
		lava->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "lava", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 11, /* PocketBlockMetadata */ 0);
		sand->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "sand", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 12, /* PocketBlockMetadata */ 0);
		sand->registerVariant(/* VariantName */ "red", /* JavaBlockID */ "sand", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 12, /* PocketBlockMetadata */ 1);
		gravel->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "gravel", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 13, /* PocketBlockMetadata */ 0);
		gold_ore->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "gold_ore", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 14, /* PocketBlockMetadata */ 0);
		iron_ore->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "iron_ore", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 15, /* PocketBlockMetadata */ 0);
		coal_ore->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "coal_ore", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 16, /* PocketBlockMetadata */ 0);
		log->registerVariant(/* VariantName */ "oak", /* JavaBlockID */ "log", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 17, /* PocketBlockMetadata */ 0);
		log->registerVariant(/* VariantName */ "spruce", /* JavaBlockID */ "log", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 17, /* PocketBlockMetadata */ 1);
		log->registerVariant(/* VariantName */ "birch", /* JavaBlockID */ "log", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 17, /* PocketBlockMetadata */ 2);
		log->registerVariant(/* VariantName */ "jungle", /* JavaBlockID */ "log", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 17, /* PocketBlockMetadata */ 3);
		leaves->registerVariant(/* VariantName */ "oak", /* JavaBlockID */ "leaves", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 18, /* PocketBlockMetadata */ 0);
		leaves->registerVariant(/* VariantName */ "spruce", /* JavaBlockID */ "leaves", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 18, /* PocketBlockMetadata */ 1);
		leaves->registerVariant(/* VariantName */ "birch", /* JavaBlockID */ "leaves", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 18, /* PocketBlockMetadata */ 2);
		leaves->registerVariant(/* VariantName */ "jungle", /* JavaBlockID */ "leaves", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 18, /* PocketBlockMetadata */ 3);
		sponge->registerVariant(/* VariantName */ "dry", /* JavaBlockID */ "sponge", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 19, /* PocketBlockMetadata */ 0);
		sponge->registerVariant(/* VariantName */ "wet", /* JavaBlockID */ "sponge", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 19, /* PocketBlockMetadata */ 1);
		glass->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "glass", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 20, /* PocketBlockMetadata */ 0);
		lapis_ore->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "lapis_ore", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 21, /* PocketBlockMetadata */ 0);
		lapis_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "lapis_block", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 22, /* PocketBlockMetadata */ 0);
		dispenser->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "dispenser", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 23, /* PocketBlockMetadata */ 0);
		sandstone->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "sandstone", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 24, /* PocketBlockMetadata */ 0);
		sandstone->registerVariant(/* VariantName */ "chiseled", /* JavaBlockID */ "sandstone", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 24, /* PocketBlockMetadata */ 1);
		sandstone->registerVariant(/* VariantName */ "smooth", /* JavaBlockID */ "sandstone", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 24, /* PocketBlockMetadata */ 2);
		noteblock->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "noteblock", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 25, /* PocketBlockMetadata */ 0);
		bed->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "bed", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 26, /* PocketBlockMetadata */ 0);
		golden_rail->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "golden_rail", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 27, /* PocketBlockMetadata */ 0);
		detector_rail->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "detector_rail", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 28, /* PocketBlockMetadata */ 0);
		sticky_piston->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "sticky_piston", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 29, /* PocketBlockMetadata */ 0);
		web->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "web", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 30, /* PocketBlockMetadata */ 0);
		tallgrass->registerVariant(/* VariantName */ "shrub", /* JavaBlockID */ "tallgrass", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 31, /* PocketBlockMetadata */ 0);
		tallgrass->registerVariant(/* VariantName */ "tallgrass", /* JavaBlockID */ "tallgrass", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 31, /* PocketBlockMetadata */ 1);
		tallgrass->registerVariant(/* VariantName */ "fern", /* JavaBlockID */ "tallgrass", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 31, /* PocketBlockMetadata */ 2);
		deadbush->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "deadbush", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 32, /* PocketBlockMetadata */ 0);
		piston->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "piston", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 33, /* PocketBlockMetadata */ 0);
		piston_head->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "piston_head", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 34, /* PocketBlockMetadata */ 0);
		wool->registerVariant(/* VariantName */ "white", /* JavaBlockID */ "wool", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 35, /* PocketBlockMetadata */ 0);
		wool->registerVariant(/* VariantName */ "orange", /* JavaBlockID */ "wool", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 35, /* PocketBlockMetadata */ 1);
		wool->registerVariant(/* VariantName */ "magenta", /* JavaBlockID */ "wool", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 35, /* PocketBlockMetadata */ 2);
		wool->registerVariant(/* VariantName */ "lightBlue", /* JavaBlockID */ "wool", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 35, /* PocketBlockMetadata */ 3);
		wool->registerVariant(/* VariantName */ "yellow", /* JavaBlockID */ "wool", /* JavaBlockMetadata */ 4, /* PocketBlockID */ 35, /* PocketBlockMetadata */ 4);
		wool->registerVariant(/* VariantName */ "lime", /* JavaBlockID */ "wool", /* JavaBlockMetadata */ 5, /* PocketBlockID */ 35, /* PocketBlockMetadata */ 5);
		wool->registerVariant(/* VariantName */ "pink", /* JavaBlockID */ "wool", /* JavaBlockMetadata */ 6, /* PocketBlockID */ 35, /* PocketBlockMetadata */ 6);
		wool->registerVariant(/* VariantName */ "gray", /* JavaBlockID */ "wool", /* JavaBlockMetadata */ 7, /* PocketBlockID */ 35, /* PocketBlockMetadata */ 7);
		wool->registerVariant(/* VariantName */ "silver", /* JavaBlockID */ "wool", /* JavaBlockMetadata */ 8, /* PocketBlockID */ 35, /* PocketBlockMetadata */ 8);
		wool->registerVariant(/* VariantName */ "cyan", /* JavaBlockID */ "wool", /* JavaBlockMetadata */ 9, /* PocketBlockID */ 35, /* PocketBlockMetadata */ 9);
		wool->registerVariant(/* VariantName */ "purple", /* JavaBlockID */ "wool", /* JavaBlockMetadata */ 10, /* PocketBlockID */ 35, /* PocketBlockMetadata */ 10);
		wool->registerVariant(/* VariantName */ "blue", /* JavaBlockID */ "wool", /* JavaBlockMetadata */ 11, /* PocketBlockID */ 35, /* PocketBlockMetadata */ 11);
		wool->registerVariant(/* VariantName */ "brown", /* JavaBlockID */ "wool", /* JavaBlockMetadata */ 12, /* PocketBlockID */ 35, /* PocketBlockMetadata */ 12);
		wool->registerVariant(/* VariantName */ "green", /* JavaBlockID */ "wool", /* JavaBlockMetadata */ 13, /* PocketBlockID */ 35, /* PocketBlockMetadata */ 13);
		wool->registerVariant(/* VariantName */ "red", /* JavaBlockID */ "wool", /* JavaBlockMetadata */ 14, /* PocketBlockID */ 35, /* PocketBlockMetadata */ 14);
		wool->registerVariant(/* VariantName */ "black", /* JavaBlockID */ "wool", /* JavaBlockMetadata */ 15, /* PocketBlockID */ 35, /* PocketBlockMetadata */ 15);
		piston_moving_piece->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "piston_extension", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 250, /* PocketBlockMetadata */ 0);
		yellow_flower->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "yellow_flower", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 37, /* PocketBlockMetadata */ 0);
		red_flower->registerVariant(/* VariantName */ "poppy", /* JavaBlockID */ "red_flower", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 38, /* PocketBlockMetadata */ 0);
		red_flower->registerVariant(/* VariantName */ "blueOrchid", /* JavaBlockID */ "red_flower", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 38, /* PocketBlockMetadata */ 1);
		red_flower->registerVariant(/* VariantName */ "allium", /* JavaBlockID */ "red_flower", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 38, /* PocketBlockMetadata */ 2);
		red_flower->registerVariant(/* VariantName */ "houstonia", /* JavaBlockID */ "red_flower", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 38, /* PocketBlockMetadata */ 3);
		red_flower->registerVariant(/* VariantName */ "tulipRed", /* JavaBlockID */ "red_flower", /* JavaBlockMetadata */ 4, /* PocketBlockID */ 38, /* PocketBlockMetadata */ 4);
		red_flower->registerVariant(/* VariantName */ "tulipOrange", /* JavaBlockID */ "red_flower", /* JavaBlockMetadata */ 5, /* PocketBlockID */ 38, /* PocketBlockMetadata */ 5);
		red_flower->registerVariant(/* VariantName */ "tulipWhite", /* JavaBlockID */ "red_flower", /* JavaBlockMetadata */ 6, /* PocketBlockID */ 38, /* PocketBlockMetadata */ 6);
		red_flower->registerVariant(/* VariantName */ "tulipPink", /* JavaBlockID */ "red_flower", /* JavaBlockMetadata */ 7, /* PocketBlockID */ 38, /* PocketBlockMetadata */ 7);
		red_flower->registerVariant(/* VariantName */ "oxeyeDaisy", /* JavaBlockID */ "red_flower", /* JavaBlockMetadata */ 8, /* PocketBlockID */ 38, /* PocketBlockMetadata */ 8);
		brown_mushroom->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "brown_mushroom", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 39, /* PocketBlockMetadata */ 0);
		red_mushroom->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "red_mushroom", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 40, /* PocketBlockMetadata */ 0);
		gold_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "gold_block", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 41, /* PocketBlockMetadata */ 0);
		iron_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "iron_block", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 42, /* PocketBlockMetadata */ 0);
		double_stone_slab->registerVariant(/* VariantName */ "stone", /* JavaBlockID */ "double_stone_slab", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 43, /* PocketBlockMetadata */ 0);
		double_stone_slab->registerVariant(/* VariantName */ "sand", /* JavaBlockID */ "double_stone_slab", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 43, /* PocketBlockMetadata */ 1);
		double_stone_slab->registerVariant(/* VariantName */ "wood", /* JavaBlockID */ "double_stone_slab", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 43, /* PocketBlockMetadata */ 2);
		double_stone_slab->registerVariant(/* VariantName */ "cobble", /* JavaBlockID */ "double_stone_slab", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 43, /* PocketBlockMetadata */ 3);
		double_stone_slab->registerVariant(/* VariantName */ "brick", /* JavaBlockID */ "double_stone_slab", /* JavaBlockMetadata */ 4, /* PocketBlockID */ 43, /* PocketBlockMetadata */ 4);
		double_stone_slab->registerVariant(/* VariantName */ "smoothStoneBrick", /* JavaBlockID */ "double_stone_slab", /* JavaBlockMetadata */ 5, /* PocketBlockID */ 43, /* PocketBlockMetadata */ 5);
		double_stone_slab->registerVariant(/* VariantName */ "quartz", /* JavaBlockID */ "double_stone_slab", /* JavaBlockMetadata */ 7, /* PocketBlockID */ 43, /* PocketBlockMetadata */ 6);
		double_stone_slab->registerVariant(/* VariantName */ "netherBrick", /* JavaBlockID */ "double_stone_slab", /* JavaBlockMetadata */ 6, /* PocketBlockID */ 43, /* PocketBlockMetadata */ 7);
		stone_slab->registerVariant(/* VariantName */ "stone", /* JavaBlockID */ "stone_slab", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 44, /* PocketBlockMetadata */ 0);
		stone_slab->registerVariant(/* VariantName */ "sand", /* JavaBlockID */ "stone_slab", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 44, /* PocketBlockMetadata */ 1);
		stone_slab->registerVariant(/* VariantName */ "wood", /* JavaBlockID */ "stone_slab", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 44, /* PocketBlockMetadata */ 2);
		stone_slab->registerVariant(/* VariantName */ "cobble", /* JavaBlockID */ "stone_slab", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 44, /* PocketBlockMetadata */ 3);
		stone_slab->registerVariant(/* VariantName */ "brick", /* JavaBlockID */ "stone_slab", /* JavaBlockMetadata */ 4, /* PocketBlockID */ 44, /* PocketBlockMetadata */ 4);
		stone_slab->registerVariant(/* VariantName */ "smoothStoneBrick", /* JavaBlockID */ "stone_slab", /* JavaBlockMetadata */ 5, /* PocketBlockID */ 44, /* PocketBlockMetadata */ 5);
		stone_slab->registerVariant(/* VariantName */ "quartz", /* JavaBlockID */ "stone_slab", /* JavaBlockMetadata */ 7, /* PocketBlockID */ 44, /* PocketBlockMetadata */ 6);
		stone_slab->registerVariant(/* VariantName */ "netherBrick", /* JavaBlockID */ "stone_slab", /* JavaBlockMetadata */ 6, /* PocketBlockID */ 44, /* PocketBlockMetadata */ 7);
		brick_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "brick_block", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 45, /* PocketBlockMetadata */ 0);
		tnt->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "tnt", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 46, /* PocketBlockMetadata */ 0);
		bookshelf->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "bookshelf", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 47, /* PocketBlockMetadata */ 0);
		mossy_cobblestone->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "mossy_cobblestone", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 48, /* PocketBlockMetadata */ 0);
		obsidian->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "obsidian", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 49, /* PocketBlockMetadata */ 0);
		torch->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "torch", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 50, /* PocketBlockMetadata */ 0);
		fire->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "fire", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 51, /* PocketBlockMetadata */ 0);
		mob_spawner->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "mob_spawner", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 52, /* PocketBlockMetadata */ 0);
		oak_stairs->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "oak_stairs", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 53, /* PocketBlockMetadata */ 0);
		chest->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "chest", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 54, /* PocketBlockMetadata */ 0);
		redstone_wire->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "redstone_wire", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 55, /* PocketBlockMetadata */ 0);
		diamond_ore->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "diamond_ore", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 56, /* PocketBlockMetadata */ 0);
		diamond_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "diamond_block", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 57, /* PocketBlockMetadata */ 0);
		crafting_table->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "crafting_table", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 58, /* PocketBlockMetadata */ 0);
		wheat->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "wheat", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 59, /* PocketBlockMetadata */ 0);
		farmland->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "farmland", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 60, /* PocketBlockMetadata */ 0);
		furnace->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "furnace", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 61, /* PocketBlockMetadata */ 0);
		burning_furnace->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "lit_furnace", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 62, /* PocketBlockMetadata */ 0);
		sign_post->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "standing_sign", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 63, /* PocketBlockMetadata */ 0);
		door->registerVariant(/* VariantName */ "wooden_door", /* JavaBlockID */ "wooden_door", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 64, /* PocketBlockMetadata */ 0);
		ladder->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "ladder", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 65, /* PocketBlockMetadata */ 0);
		rail->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "rail", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 66, /* PocketBlockMetadata */ 0);
		cobblestone_stairs->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "stone_stairs", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 67, /* PocketBlockMetadata */ 0);
		wall_sign->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "wall_sign", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 68, /* PocketBlockMetadata */ 0);
		lever->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "lever", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 69, /* PocketBlockMetadata */ 0);
		stone_pressure_plate->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "stone_pressure_plate", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 70, /* PocketBlockMetadata */ 0);
		iron_door->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "iron_door", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 71, /* PocketBlockMetadata */ 0);
		wooden_pressure_plate->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "wooden_pressure_plate", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 72, /* PocketBlockMetadata */ 0);
		redstone_ore->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "redstone_ore", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 73, /* PocketBlockMetadata */ 0);
		glowing_redstone_ore->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "lit_redstone_ore", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 74, /* PocketBlockMetadata */ 0);
		redstone_torch_off->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "unlit_redstone_torch", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 75, /* PocketBlockMetadata */ 0);
		redstone_torch_on->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "redstone_torch", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 76, /* PocketBlockMetadata */ 0);
		stone_button->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "stone_button", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 77, /* PocketBlockMetadata */ 0);
		snow_layer->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "snow_layer", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 78, /* PocketBlockMetadata */ 0);
		ice->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "ice", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 79, /* PocketBlockMetadata */ 0);
		snow->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "snow", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 80, /* PocketBlockMetadata */ 0);
		cactus->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "cactus", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 81, /* PocketBlockMetadata */ 0);
		clay->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "clay", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 82, /* PocketBlockMetadata */ 0);
		sugar_cane_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "reeds", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 83, /* PocketBlockMetadata */ 0);
		jukebox->registerVariant(/* VariantName */ "", /* JavaBlockID */ "jukebox", /* JavaBlockMetadata */ , /* PocketBlockID */ 255, /* PocketBlockMetadata */ );
		fence->registerVariant(/* VariantName */ "oak_fence", /* JavaBlockID */ "fence", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 85, /* PocketBlockMetadata */ 0);
		pumpkin->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "pumpkin", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 86, /* PocketBlockMetadata */ 0);
		netherrack->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "netherrack", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 87, /* PocketBlockMetadata */ 0);
		soul_sand->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "soul_sand", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 88, /* PocketBlockMetadata */ 0);
		glowstone->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "glowstone", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 89, /* PocketBlockMetadata */ 0);
		portal->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "portal", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 90, /* PocketBlockMetadata */ 0);
		jack_o_lantern->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "lit_pumpkin", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 91, /* PocketBlockMetadata */ 0);
		cake->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "cake", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 92, /* PocketBlockMetadata */ 0);
		unpowered_repeater->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "unpowered_repeater", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 93, /* PocketBlockMetadata */ 0);
		powered_repeater->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "powered_repeater", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 94, /* PocketBlockMetadata */ 0);
		stained_glass->registerVariant(/* VariantName */ "white", /* JavaBlockID */ "stained_glass", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 241, /* PocketBlockMetadata */ 0);
		stained_glass->registerVariant(/* VariantName */ "orange", /* JavaBlockID */ "stained_glass", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 241, /* PocketBlockMetadata */ 1);
		stained_glass->registerVariant(/* VariantName */ "magenta", /* JavaBlockID */ "stained_glass", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 241, /* PocketBlockMetadata */ 2);
		stained_glass->registerVariant(/* VariantName */ "lightBlue", /* JavaBlockID */ "stained_glass", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 241, /* PocketBlockMetadata */ 3);
		stained_glass->registerVariant(/* VariantName */ "yellow", /* JavaBlockID */ "stained_glass", /* JavaBlockMetadata */ 4, /* PocketBlockID */ 241, /* PocketBlockMetadata */ 4);
		stained_glass->registerVariant(/* VariantName */ "lime", /* JavaBlockID */ "stained_glass", /* JavaBlockMetadata */ 5, /* PocketBlockID */ 241, /* PocketBlockMetadata */ 5);
		stained_glass->registerVariant(/* VariantName */ "pink", /* JavaBlockID */ "stained_glass", /* JavaBlockMetadata */ 6, /* PocketBlockID */ 241, /* PocketBlockMetadata */ 6);
		stained_glass->registerVariant(/* VariantName */ "gray", /* JavaBlockID */ "stained_glass", /* JavaBlockMetadata */ 7, /* PocketBlockID */ 241, /* PocketBlockMetadata */ 7);
		stained_glass->registerVariant(/* VariantName */ "silver", /* JavaBlockID */ "stained_glass", /* JavaBlockMetadata */ 8, /* PocketBlockID */ 241, /* PocketBlockMetadata */ 8);
		stained_glass->registerVariant(/* VariantName */ "cyan", /* JavaBlockID */ "stained_glass", /* JavaBlockMetadata */ 9, /* PocketBlockID */ 241, /* PocketBlockMetadata */ 9);
		stained_glass->registerVariant(/* VariantName */ "purple", /* JavaBlockID */ "stained_glass", /* JavaBlockMetadata */ 10, /* PocketBlockID */ 241, /* PocketBlockMetadata */ 10);
		stained_glass->registerVariant(/* VariantName */ "blue", /* JavaBlockID */ "stained_glass", /* JavaBlockMetadata */ 11, /* PocketBlockID */ 241, /* PocketBlockMetadata */ 11);
		stained_glass->registerVariant(/* VariantName */ "brown", /* JavaBlockID */ "stained_glass", /* JavaBlockMetadata */ 12, /* PocketBlockID */ 241, /* PocketBlockMetadata */ 12);
		stained_glass->registerVariant(/* VariantName */ "green", /* JavaBlockID */ "stained_glass", /* JavaBlockMetadata */ 13, /* PocketBlockID */ 241, /* PocketBlockMetadata */ 13);
		stained_glass->registerVariant(/* VariantName */ "red", /* JavaBlockID */ "stained_glass", /* JavaBlockMetadata */ 14, /* PocketBlockID */ 241, /* PocketBlockMetadata */ 14);
		stained_glass->registerVariant(/* VariantName */ "black", /* JavaBlockID */ "stained_glass", /* JavaBlockMetadata */ 15, /* PocketBlockID */ 241, /* PocketBlockMetadata */ 15);
		trap_door->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "trap_door", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 96, /* PocketBlockMetadata */ 0);
		monster_egg->registerVariant(/* VariantName */ "stone", /* JavaBlockID */ "monster_egg", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 97, /* PocketBlockMetadata */ 0);
		monster_egg->registerVariant(/* VariantName */ "cobble", /* JavaBlockID */ "monster_egg", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 97, /* PocketBlockMetadata */ 1);
		monster_egg->registerVariant(/* VariantName */ "brick", /* JavaBlockID */ "monster_egg", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 97, /* PocketBlockMetadata */ 2);
		monster_egg->registerVariant(/* VariantName */ "mossybrick", /* JavaBlockID */ "monster_egg", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 97, /* PocketBlockMetadata */ 3);
		monster_egg->registerVariant(/* VariantName */ "crackedbrick", /* JavaBlockID */ "monster_egg", /* JavaBlockMetadata */ 4, /* PocketBlockID */ 97, /* PocketBlockMetadata */ 4);
		monster_egg->registerVariant(/* VariantName */ "chiseledbrick", /* JavaBlockID */ "monster_egg", /* JavaBlockMetadata */ 5, /* PocketBlockID */ 97, /* PocketBlockMetadata */ 5);
		stonebrick->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "stonebrick", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 98, /* PocketBlockMetadata */ 0);
		stonebrick->registerVariant(/* VariantName */ "mossy", /* JavaBlockID */ "stonebrick", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 98, /* PocketBlockMetadata */ 1);
		stonebrick->registerVariant(/* VariantName */ "cracked", /* JavaBlockID */ "stonebrick", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 98, /* PocketBlockMetadata */ 2);
		stonebrick->registerVariant(/* VariantName */ "chiseled", /* JavaBlockID */ "stonebrick", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 98, /* PocketBlockMetadata */ 3);
		brown_mushroom_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "brown_mushroom_block", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 99, /* PocketBlockMetadata */ 0);
		red_mushroom_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "red_mushroom_block", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 100, /* PocketBlockMetadata */ 0);
		iron_bars->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "iron_bars", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 101, /* PocketBlockMetadata */ 0);
		glass_pane->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "glass_pane", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 102, /* PocketBlockMetadata */ 0);
		melon_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "melon_block", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 103, /* PocketBlockMetadata */ 0);
		pumpkin_stem->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "pumpkin_stem", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 104, /* PocketBlockMetadata */ 0);
		melon_stem->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "melon_stem", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 105, /* PocketBlockMetadata */ 0);
		vine->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "vine", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 106, /* PocketBlockMetadata */ 0);
		fence_gate->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "fence_gate", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 107, /* PocketBlockMetadata */ 0);
		brick_stairs->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "brick_stairs", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 108, /* PocketBlockMetadata */ 0);
		stone_brick_stairs->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "stone_brick_stairs", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 109, /* PocketBlockMetadata */ 0);
		mycelium->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "mycelium", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 110, /* PocketBlockMetadata */ 0);
		waterlily->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "waterlily", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 111, /* PocketBlockMetadata */ 0);
		nether_brick->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "nether_brick", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 112, /* PocketBlockMetadata */ 0);
		nether_brick_fence->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "nether_brick_fence", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 113, /* PocketBlockMetadata */ 0);
		nether_brick_stairs->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "nether_brick_stairs", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 114, /* PocketBlockMetadata */ 0);
		nether_wart->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "nether_wart", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 115, /* PocketBlockMetadata */ 0);
		enchanting_table->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "enchanting_table", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 116, /* PocketBlockMetadata */ 0);
		brewing_stand->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "brewing_stand", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 117, /* PocketBlockMetadata */ 0);
		cauldron->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "cauldron", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 118, /* PocketBlockMetadata */ 0);
		end_portal->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "end_portal", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 119, /* PocketBlockMetadata */ 0);
		end_portal_frame->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "end_portal_frame", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 120, /* PocketBlockMetadata */ 0);
		end_stone->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "end_stone", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 121, /* PocketBlockMetadata */ 0);
		dragon_egg->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "dragon_egg", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 122, /* PocketBlockMetadata */ 0);
		redstone_lamp_off->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "redstone_lamp", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 123, /* PocketBlockMetadata */ 0);
		redstone_lamp_on->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "lit_redstone_lamp", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 124, /* PocketBlockMetadata */ 0);
		double_wooden_slab->registerVariant(/* VariantName */ "oak", /* JavaBlockID */ "double_wooden_slab", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 157, /* PocketBlockMetadata */ 0);
		double_wooden_slab->registerVariant(/* VariantName */ "spruce", /* JavaBlockID */ "double_wooden_slab", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 157, /* PocketBlockMetadata */ 1);
		double_wooden_slab->registerVariant(/* VariantName */ "birch", /* JavaBlockID */ "double_wooden_slab", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 157, /* PocketBlockMetadata */ 2);
		double_wooden_slab->registerVariant(/* VariantName */ "jungle", /* JavaBlockID */ "double_wooden_slab", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 157, /* PocketBlockMetadata */ 3);
		double_wooden_slab->registerVariant(/* VariantName */ "acacia", /* JavaBlockID */ "double_wooden_slab", /* JavaBlockMetadata */ 4, /* PocketBlockID */ 157, /* PocketBlockMetadata */ 4);
		double_wooden_slab->registerVariant(/* VariantName */ "big_oak", /* JavaBlockID */ "double_wooden_slab", /* JavaBlockMetadata */ 5, /* PocketBlockID */ 157, /* PocketBlockMetadata */ 5);
		wooden_slab->registerVariant(/* VariantName */ "oak", /* JavaBlockID */ "wooden_slab", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 158, /* PocketBlockMetadata */ 0);
		wooden_slab->registerVariant(/* VariantName */ "spruce", /* JavaBlockID */ "wooden_slab", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 158, /* PocketBlockMetadata */ 1);
		wooden_slab->registerVariant(/* VariantName */ "birch", /* JavaBlockID */ "wooden_slab", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 158, /* PocketBlockMetadata */ 2);
		wooden_slab->registerVariant(/* VariantName */ "jungle", /* JavaBlockID */ "wooden_slab", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 158, /* PocketBlockMetadata */ 3);
		wooden_slab->registerVariant(/* VariantName */ "acacia", /* JavaBlockID */ "wooden_slab", /* JavaBlockMetadata */ 4, /* PocketBlockID */ 158, /* PocketBlockMetadata */ 4);
		wooden_slab->registerVariant(/* VariantName */ "big_oak", /* JavaBlockID */ "wooden_slab", /* JavaBlockMetadata */ 5, /* PocketBlockID */ 158, /* PocketBlockMetadata */ 5);
		cocoa->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "cocoa", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 127, /* PocketBlockMetadata */ 0);
		sandstone_stairs->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "sandstone_stairs", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 128, /* PocketBlockMetadata */ 0);
		emerald_ore->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "emerald_ore", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 129, /* PocketBlockMetadata */ 0);
		ender_chest->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "ender_chest", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 130, /* PocketBlockMetadata */ 0);
		tripwire_hook->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "tripwire_hook", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 131, /* PocketBlockMetadata */ 0);
		tripwire->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "tripwire", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 132, /* PocketBlockMetadata */ 0);
		emerald_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "emerald_block", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 133, /* PocketBlockMetadata */ 0);
		spruce_stairs->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "spruce_stairs", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 134, /* PocketBlockMetadata */ 0);
		birch_stairs->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "birch_stairs", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 135, /* PocketBlockMetadata */ 0);
		jungle_stairs->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "jungle_stairs", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 136, /* PocketBlockMetadata */ 0);
		command_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "command_block", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 137, /* PocketBlockMetadata */ 0);
		beacon->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "beacon", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 138, /* PocketBlockMetadata */ 0);
		cobblestone_wall->registerVariant(/* VariantName */ "normal", /* JavaBlockID */ "cobblestone_wall", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 139, /* PocketBlockMetadata */ 0);
		cobblestone_wall->registerVariant(/* VariantName */ "mossy", /* JavaBlockID */ "cobblestone_wall", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 139, /* PocketBlockMetadata */ 1);
		flower_pot->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "flower_pot", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 140, /* PocketBlockMetadata */ 0);
		carrots->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "carrots", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 141, /* PocketBlockMetadata */ 0);
		potatos->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "potatos", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 142, /* PocketBlockMetadata */ 0);
		wooden_button->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "wooden_button", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 143, /* PocketBlockMetadata */ 0);
		skull->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "skull", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 144, /* PocketBlockMetadata */ 0);
		anvil->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "anvil", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 145, /* PocketBlockMetadata */ 0);
		anvil->registerVariant(/* VariantName */ "slightlyDamaged", /* JavaBlockID */ "anvil", /* JavaBlockMetadata */ -1, /* PocketBlockID */ 145, /* PocketBlockMetadata */ 4);
		anvil->registerVariant(/* VariantName */ "veryDamaged", /* JavaBlockID */ "anvil", /* JavaBlockMetadata */ -1, /* PocketBlockID */ 145, /* PocketBlockMetadata */ 8);
		trapped_chest->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "trapped_chest", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 146, /* PocketBlockMetadata */ 0);
		gold_pressure_plate->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "light_weighted_pressure_plate", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 147, /* PocketBlockMetadata */ 0);
		iron_pressure_plate->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "heavy_weighted_pressure_plate", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 148, /* PocketBlockMetadata */ 0);
		redstone_comparator_off->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "unpowered_comparator", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 149, /* PocketBlockMetadata */ 0);
		redstone_comparator_on->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "powered_comparator", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 150, /* PocketBlockMetadata */ 0);
		daylight_detector->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "daylight_detector", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 151, /* PocketBlockMetadata */ 0);
		redstone_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "redstone_block", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 152, /* PocketBlockMetadata */ 0);
		quartz_ore->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "quartz_ore", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 153, /* PocketBlockMetadata */ 0);
		hopper->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "hopper", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 154, /* PocketBlockMetadata */ 0);
		quartz_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "quartz_block", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 155, /* PocketBlockMetadata */ 0);
		quartz_block->registerVariant(/* VariantName */ "chiseled", /* JavaBlockID */ "quartz_block", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 155, /* PocketBlockMetadata */ 1);
		quartz_block->registerVariant(/* VariantName */ "vertical", /* JavaBlockID */ "quartz_block", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 155, /* PocketBlockMetadata */ 2);
		quartz_stairs->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "quartz_stairs", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 156, /* PocketBlockMetadata */ 0);
		activator_rail->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "activator_rail", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 126, /* PocketBlockMetadata */ 0);
		dropper->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "dropper", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 125, /* PocketBlockMetadata */ 0);
		stained_hardened_clay->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "stained_hardened_clay", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 159, /* PocketBlockMetadata */ 0);
		stained_glass_pane->registerVariant(/* VariantName */ "white", /* JavaBlockID */ "stained_glass_pane", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 160, /* PocketBlockMetadata */ 0);
		stained_glass_pane->registerVariant(/* VariantName */ "orange", /* JavaBlockID */ "stained_glass_pane", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 160, /* PocketBlockMetadata */ 1);
		stained_glass_pane->registerVariant(/* VariantName */ "magenta", /* JavaBlockID */ "stained_glass_pane", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 160, /* PocketBlockMetadata */ 2);
		stained_glass_pane->registerVariant(/* VariantName */ "lightBlue", /* JavaBlockID */ "stained_glass_pane", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 160, /* PocketBlockMetadata */ 3);
		stained_glass_pane->registerVariant(/* VariantName */ "yellow", /* JavaBlockID */ "stained_glass_pane", /* JavaBlockMetadata */ 4, /* PocketBlockID */ 160, /* PocketBlockMetadata */ 4);
		stained_glass_pane->registerVariant(/* VariantName */ "lime", /* JavaBlockID */ "stained_glass_pane", /* JavaBlockMetadata */ 5, /* PocketBlockID */ 160, /* PocketBlockMetadata */ 5);
		stained_glass_pane->registerVariant(/* VariantName */ "pink", /* JavaBlockID */ "stained_glass_pane", /* JavaBlockMetadata */ 6, /* PocketBlockID */ 160, /* PocketBlockMetadata */ 6);
		stained_glass_pane->registerVariant(/* VariantName */ "gray", /* JavaBlockID */ "stained_glass_pane", /* JavaBlockMetadata */ 7, /* PocketBlockID */ 160, /* PocketBlockMetadata */ 7);
		stained_glass_pane->registerVariant(/* VariantName */ "silver", /* JavaBlockID */ "stained_glass_pane", /* JavaBlockMetadata */ 8, /* PocketBlockID */ 160, /* PocketBlockMetadata */ 8);
		stained_glass_pane->registerVariant(/* VariantName */ "cyan", /* JavaBlockID */ "stained_glass_pane", /* JavaBlockMetadata */ 9, /* PocketBlockID */ 160, /* PocketBlockMetadata */ 9);
		stained_glass_pane->registerVariant(/* VariantName */ "purple", /* JavaBlockID */ "stained_glass_pane", /* JavaBlockMetadata */ 10, /* PocketBlockID */ 160, /* PocketBlockMetadata */ 10);
		stained_glass_pane->registerVariant(/* VariantName */ "blue", /* JavaBlockID */ "stained_glass_pane", /* JavaBlockMetadata */ 11, /* PocketBlockID */ 160, /* PocketBlockMetadata */ 11);
		stained_glass_pane->registerVariant(/* VariantName */ "brown", /* JavaBlockID */ "stained_glass_pane", /* JavaBlockMetadata */ 12, /* PocketBlockID */ 160, /* PocketBlockMetadata */ 12);
		stained_glass_pane->registerVariant(/* VariantName */ "green", /* JavaBlockID */ "stained_glass_pane", /* JavaBlockMetadata */ 13, /* PocketBlockID */ 160, /* PocketBlockMetadata */ 13);
		stained_glass_pane->registerVariant(/* VariantName */ "red", /* JavaBlockID */ "stained_glass_pane", /* JavaBlockMetadata */ 14, /* PocketBlockID */ 160, /* PocketBlockMetadata */ 14);
		stained_glass_pane->registerVariant(/* VariantName */ "black", /* JavaBlockID */ "stained_glass_pane", /* JavaBlockMetadata */ 15, /* PocketBlockID */ 160, /* PocketBlockMetadata */ 15);
		leaves2->registerVariant(/* VariantName */ "acacia", /* JavaBlockID */ "leaves2", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 161, /* PocketBlockMetadata */ 0);
		leaves2->registerVariant(/* VariantName */ "big_oak", /* JavaBlockID */ "leaves2", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 161, /* PocketBlockMetadata */ 1);
		log2->registerVariant(/* VariantName */ "acacia", /* JavaBlockID */ "log2", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 162, /* PocketBlockMetadata */ 0);
		log2->registerVariant(/* VariantName */ "big_oak", /* JavaBlockID */ "log2", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 162, /* PocketBlockMetadata */ 1);
		acacia_stairs->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "acacia_stairs", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 163, /* PocketBlockMetadata */ 0);
		dark_oak_stairs->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "dark_oak_stairs", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 164, /* PocketBlockMetadata */ 0);
		slime->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "slime", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 165, /* PocketBlockMetadata */ 0);
		barrier->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "barrier", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 75, /* PocketBlockMetadata */ 0);
		iron_trapdoor->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "iron_trapdoor", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 167, /* PocketBlockMetadata */ 0);
		prismarine->registerVariant(/* VariantName */ "rough", /* JavaBlockID */ "prismarine", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 168, /* PocketBlockMetadata */ 0);
		prismarine->registerVariant(/* VariantName */ "dark", /* JavaBlockID */ "prismarine", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 168, /* PocketBlockMetadata */ 1);
		prismarine->registerVariant(/* VariantName */ "bricks", /* JavaBlockID */ "prismarine", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 168, /* PocketBlockMetadata */ 2);
		sea_lantern->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "sea_lantern", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 169, /* PocketBlockMetadata */ 0);
		hay_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "hay_block", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 170, /* PocketBlockMetadata */ 0);
		carpet->registerVariant(/* VariantName */ "white", /* JavaBlockID */ "carpet", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 171, /* PocketBlockMetadata */ 0);
		carpet->registerVariant(/* VariantName */ "orange", /* JavaBlockID */ "carpet", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 171, /* PocketBlockMetadata */ 1);
		carpet->registerVariant(/* VariantName */ "magenta", /* JavaBlockID */ "carpet", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 171, /* PocketBlockMetadata */ 2);
		carpet->registerVariant(/* VariantName */ "lightBlue", /* JavaBlockID */ "carpet", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 171, /* PocketBlockMetadata */ 3);
		carpet->registerVariant(/* VariantName */ "yellow", /* JavaBlockID */ "carpet", /* JavaBlockMetadata */ 4, /* PocketBlockID */ 171, /* PocketBlockMetadata */ 4);
		carpet->registerVariant(/* VariantName */ "lime", /* JavaBlockID */ "carpet", /* JavaBlockMetadata */ 5, /* PocketBlockID */ 171, /* PocketBlockMetadata */ 5);
		carpet->registerVariant(/* VariantName */ "pink", /* JavaBlockID */ "carpet", /* JavaBlockMetadata */ 6, /* PocketBlockID */ 171, /* PocketBlockMetadata */ 6);
		carpet->registerVariant(/* VariantName */ "gray", /* JavaBlockID */ "carpet", /* JavaBlockMetadata */ 7, /* PocketBlockID */ 171, /* PocketBlockMetadata */ 7);
		carpet->registerVariant(/* VariantName */ "silver", /* JavaBlockID */ "carpet", /* JavaBlockMetadata */ 8, /* PocketBlockID */ 171, /* PocketBlockMetadata */ 8);
		carpet->registerVariant(/* VariantName */ "cyan", /* JavaBlockID */ "carpet", /* JavaBlockMetadata */ 9, /* PocketBlockID */ 171, /* PocketBlockMetadata */ 9);
		carpet->registerVariant(/* VariantName */ "purple", /* JavaBlockID */ "carpet", /* JavaBlockMetadata */ 10, /* PocketBlockID */ 171, /* PocketBlockMetadata */ 10);
		carpet->registerVariant(/* VariantName */ "blue", /* JavaBlockID */ "carpet", /* JavaBlockMetadata */ 11, /* PocketBlockID */ 171, /* PocketBlockMetadata */ 11);
		carpet->registerVariant(/* VariantName */ "brown", /* JavaBlockID */ "carpet", /* JavaBlockMetadata */ 12, /* PocketBlockID */ 171, /* PocketBlockMetadata */ 12);
		carpet->registerVariant(/* VariantName */ "green", /* JavaBlockID */ "carpet", /* JavaBlockMetadata */ 13, /* PocketBlockID */ 171, /* PocketBlockMetadata */ 13);
		carpet->registerVariant(/* VariantName */ "red", /* JavaBlockID */ "carpet", /* JavaBlockMetadata */ 14, /* PocketBlockID */ 171, /* PocketBlockMetadata */ 14);
		carpet->registerVariant(/* VariantName */ "black", /* JavaBlockID */ "carpet", /* JavaBlockMetadata */ 15, /* PocketBlockID */ 171, /* PocketBlockMetadata */ 15);
		hardened_clay->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "hardened_clay", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 172, /* PocketBlockMetadata */ 0);
		coal_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "coal_block", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 173, /* PocketBlockMetadata */ 0);
		packed_ice->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "packed_ice", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 174, /* PocketBlockMetadata */ 0);
		double_plant->registerVariant(/* VariantName */ "sunflower", /* JavaBlockID */ "double_plant", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 175, /* PocketBlockMetadata */ 0);
		double_plant->registerVariant(/* VariantName */ "syringa", /* JavaBlockID */ "double_plant", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 175, /* PocketBlockMetadata */ 1);
		double_plant->registerVariant(/* VariantName */ "grass", /* JavaBlockID */ "double_plant", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 175, /* PocketBlockMetadata */ 2);
		double_plant->registerVariant(/* VariantName */ "fern", /* JavaBlockID */ "double_plant", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 175, /* PocketBlockMetadata */ 3);
		double_plant->registerVariant(/* VariantName */ "rose", /* JavaBlockID */ "double_plant", /* JavaBlockMetadata */ 4, /* PocketBlockID */ 175, /* PocketBlockMetadata */ 4);
		double_plant->registerVariant(/* VariantName */ "paeonia", /* JavaBlockID */ "double_plant", /* JavaBlockMetadata */ 5, /* PocketBlockID */ 175, /* PocketBlockMetadata */ 5);
		standing_banner->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "standing_banner", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 255, /* PocketBlockMetadata */ 0);
		wall_banner->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "wall_banner", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 255, /* PocketBlockMetadata */ 0);
		daylight_detector_inverted->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "daylight_detector_inverted", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 178, /* PocketBlockMetadata */ 0);
		red_sandstone->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "red_sandstone", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 179, /* PocketBlockMetadata */ 0);
		red_sandstone->registerVariant(/* VariantName */ "chiseled", /* JavaBlockID */ "red_sandstone", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 179, /* PocketBlockMetadata */ 1);
		red_sandstone->registerVariant(/* VariantName */ "smooth", /* JavaBlockID */ "red_sandstone", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 179, /* PocketBlockMetadata */ 2);
		red_sandstone_stairs->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "red_sandstone_stairs", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 180, /* PocketBlockMetadata */ 0);
		double_stone_slab2->registerVariant(/* VariantName */ "red_sandstone", /* JavaBlockID */ "double_stone_slab2", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 181, /* PocketBlockMetadata */ 0);
		stone_slab2->registerVariant(/* VariantName */ "red_sandstone", /* JavaBlockID */ "stone_slab2", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 182, /* PocketBlockMetadata */ 0);
		spruce_fence_gate->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "spruce_fence_gate", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 183, /* PocketBlockMetadata */ 0);
		birch_fence_gate->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "birch_fence_gate", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 184, /* PocketBlockMetadata */ 0);
		jungle_fence_gate->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "jungle_fence_gate", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 185, /* PocketBlockMetadata */ 0);
		dark_oak_fence_gate->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "dark_oak_fence_gate", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 186, /* PocketBlockMetadata */ 0);
		acacia_fence_gate->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "acacia_fence_gate", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 187, /* PocketBlockMetadata */ 0);
		fence->registerVariant(/* VariantName */ "spruce_fence", /* JavaBlockID */ "spruce_fence", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 85, /* PocketBlockMetadata */ 1);
		fence->registerVariant(/* VariantName */ "birch_fence", /* JavaBlockID */ "birch_fence", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 85, /* PocketBlockMetadata */ 2);
		fence->registerVariant(/* VariantName */ "jungle_fence", /* JavaBlockID */ "jungle_fence", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 85, /* PocketBlockMetadata */ 3);
		fence->registerVariant(/* VariantName */ "dark_oak_fence", /* JavaBlockID */ "dark_oak_fence", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 85, /* PocketBlockMetadata */ 4);
		fence->registerVariant(/* VariantName */ "acacia_fence", /* JavaBlockID */ "acacia_fence", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 85, /* PocketBlockMetadata */ 5);
		door->registerVariant(/* VariantName */ "spruce_door", /* JavaBlockID */ "spruce_door", /* JavaBlockMetadata */ -1, /* PocketBlockID */ 193, /* PocketBlockMetadata */ -1);
		door->registerVariant(/* VariantName */ "birch_door", /* JavaBlockID */ "birch_door", /* JavaBlockMetadata */ -1, /* PocketBlockID */ 194, /* PocketBlockMetadata */ -1);
		door->registerVariant(/* VariantName */ "jungle_door", /* JavaBlockID */ "jungle_door", /* JavaBlockMetadata */ -1, /* PocketBlockID */ 195, /* PocketBlockMetadata */ -1);
		door->registerVariant(/* VariantName */ "acacia_door", /* JavaBlockID */ "acacia_door", /* JavaBlockMetadata */ -1, /* PocketBlockID */ 196, /* PocketBlockMetadata */ -1);
		door->registerVariant(/* VariantName */ "dark_oak_door", /* JavaBlockID */ "dark_oak_door", /* JavaBlockMetadata */ -1, /* PocketBlockID */ 197, /* PocketBlockMetadata */ -1);
		end_rod->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "end_rod", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 208, /* PocketBlockMetadata */ 0);
		chorus_plant->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "chorus_plant", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 240, /* PocketBlockMetadata */ 0);
		chorus_flower->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "chorus_flower", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 200, /* PocketBlockMetadata */ 0);
		purpur_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "purpur_block", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 201, /* PocketBlockMetadata */ 0);
		purpur_block->registerVariant(/* VariantName */ "purpur_pillar", /* JavaBlockID */ "purpur_pillar", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 201, /* PocketBlockMetadata */ 2);
		purpur_stairs->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "purpur_stairs", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 203, /* PocketBlockMetadata */ 0);
		double_stone_slab2->registerVariant(/* VariantName */ "purpur_double_slab", /* JavaBlockID */ "purpur_double_slab", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 181, /* PocketBlockMetadata */ 1);
		stone_slab2->registerVariant(/* VariantName */ "purpur_slab", /* JavaBlockID */ "purpur_slab", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 182, /* PocketBlockMetadata */ 1);
		end_bricks->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "end_bricks", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 206, /* PocketBlockMetadata */ 0);
		beetroots->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "beetroots", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 244, /* PocketBlockMetadata */ 0);
		grass_path->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "grass_path", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 198, /* PocketBlockMetadata */ 0);
		end_gateway->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "end_gateway", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 209, /* PocketBlockMetadata */ 0);
		repeating_command_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "repeating_command_block", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 188, /* PocketBlockMetadata */ 0);
		chain_command_block->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "chain_command_block", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 189, /* PocketBlockMetadata */ 0);
		frosted_ice->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "frosted_ice", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 207, /* PocketBlockMetadata */ 0);
		magma->registerVariant(/* VariantName */ "", /* JavaBlockID */ "magma", /* JavaBlockMetadata */ , /* PocketBlockID */ 255, /* PocketBlockMetadata */ );
		nether_wart_block->registerVariant(/* VariantName */ "", /* JavaBlockID */ "nether_wart_block", /* JavaBlockMetadata */ , /* PocketBlockID */ 255, /* PocketBlockMetadata */ );
		red_nether_brick->registerVariant(/* VariantName */ "", /* JavaBlockID */ "red_nether_brick", /* JavaBlockMetadata */ , /* PocketBlockID */ 255, /* PocketBlockMetadata */ );
		bone_block->registerVariant(/* VariantName */ "", /* JavaBlockID */ "bone_block", /* JavaBlockMetadata */ , /* PocketBlockID */ 255, /* PocketBlockMetadata */ );
		structure_void->registerVariant(/* VariantName */ "", /* JavaBlockID */ "structure_void", /* JavaBlockMetadata */ , /* PocketBlockID */ 255, /* PocketBlockMetadata */ );
		observer->registerVariant(/* VariantName */ "default", /* JavaBlockID */ "observer", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 251, /* PocketBlockMetadata */ 0);
		shulker_box->registerVariant(/* VariantName */ "white", /* JavaBlockID */ "white_shulker_box", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 218, /* PocketBlockMetadata */ 0);
		shulker_box->registerVariant(/* VariantName */ "orange", /* JavaBlockID */ "orange_shulker_box", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 218, /* PocketBlockMetadata */ 1);
		shulker_box->registerVariant(/* VariantName */ "magenta", /* JavaBlockID */ "magenta_shulker_box", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 218, /* PocketBlockMetadata */ 2);
		shulker_box->registerVariant(/* VariantName */ "lightBlue", /* JavaBlockID */ "light_blue_shulker_box", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 218, /* PocketBlockMetadata */ 3);
		shulker_box->registerVariant(/* VariantName */ "yellow", /* JavaBlockID */ "yellow_shulker_box", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 218, /* PocketBlockMetadata */ 4);
		shulker_box->registerVariant(/* VariantName */ "lime", /* JavaBlockID */ "lime_shulker_box", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 218, /* PocketBlockMetadata */ 5);
		shulker_box->registerVariant(/* VariantName */ "pink", /* JavaBlockID */ "pink_shulker_box", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 218, /* PocketBlockMetadata */ 6);
		shulker_box->registerVariant(/* VariantName */ "gray", /* JavaBlockID */ "gray_shulker_box", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 218, /* PocketBlockMetadata */ 7);
		shulker_box->registerVariant(/* VariantName */ "silver", /* JavaBlockID */ "silver_shulker_box", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 218, /* PocketBlockMetadata */ 8);
		shulker_box->registerVariant(/* VariantName */ "cyan", /* JavaBlockID */ "cyan_shulker_box", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 218, /* PocketBlockMetadata */ 9);
		shulker_box->registerVariant(/* VariantName */ "purple", /* JavaBlockID */ "purple_shulker_box", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 218, /* PocketBlockMetadata */ 10);
		shulker_box->registerVariant(/* VariantName */ "blue", /* JavaBlockID */ "blue_shulker_box", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 218, /* PocketBlockMetadata */ 11);
		shulker_box->registerVariant(/* VariantName */ "brown", /* JavaBlockID */ "brown_shulker_box", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 218, /* PocketBlockMetadata */ 12);
		shulker_box->registerVariant(/* VariantName */ "green", /* JavaBlockID */ "green_shulker_box", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 218, /* PocketBlockMetadata */ 13);
		shulker_box->registerVariant(/* VariantName */ "red", /* JavaBlockID */ "red_shulker_box", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 218, /* PocketBlockMetadata */ 14);
		shulker_box->registerVariant(/* VariantName */ "black", /* JavaBlockID */ "black_shulker_box", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 218, /* PocketBlockMetadata */ 15);
		glazed_terracotta->registerVariant(/* VariantName */ "white", /* JavaBlockID */ "white_glazed_terracotta", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 220, /* PocketBlockMetadata */ 0);
		glazed_terracotta->registerVariant(/* VariantName */ "orange", /* JavaBlockID */ "orange_glazed_terracotta", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 221, /* PocketBlockMetadata */ 0);
		glazed_terracotta->registerVariant(/* VariantName */ "magenta", /* JavaBlockID */ "magenta_glazed_terracotta", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 222, /* PocketBlockMetadata */ 0);
		glazed_terracotta->registerVariant(/* VariantName */ "lightBlue", /* JavaBlockID */ "light_blue_glazed_terracotta", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 223, /* PocketBlockMetadata */ 0);
		glazed_terracotta->registerVariant(/* VariantName */ "yellow", /* JavaBlockID */ "yellow_glazed_terracotta", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 224, /* PocketBlockMetadata */ 0);
		glazed_terracotta->registerVariant(/* VariantName */ "lime", /* JavaBlockID */ "lime_glazed_terracotta", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 225, /* PocketBlockMetadata */ 0);
		glazed_terracotta->registerVariant(/* VariantName */ "pink", /* JavaBlockID */ "pink_glazed_terracotta", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 226, /* PocketBlockMetadata */ 0);
		glazed_terracotta->registerVariant(/* VariantName */ "gray", /* JavaBlockID */ "gray_glazed_terracotta", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 227, /* PocketBlockMetadata */ 0);
		glazed_terracotta->registerVariant(/* VariantName */ "silver", /* JavaBlockID */ "light_gray_glazed_terracotta", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 228, /* PocketBlockMetadata */ 0);
		glazed_terracotta->registerVariant(/* VariantName */ "cyan", /* JavaBlockID */ "cyan_glazed_terracotta", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 229, /* PocketBlockMetadata */ 0);
		glazed_terracotta->registerVariant(/* VariantName */ "purple", /* JavaBlockID */ "purple_glazed_terracotta", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 219, /* PocketBlockMetadata */ 0);
		glazed_terracotta->registerVariant(/* VariantName */ "blue", /* JavaBlockID */ "blue_glazed_terracotta", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 231, /* PocketBlockMetadata */ 0);
		glazed_terracotta->registerVariant(/* VariantName */ "brown", /* JavaBlockID */ "brown_glazed_terracotta", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 232, /* PocketBlockMetadata */ 0);
		glazed_terracotta->registerVariant(/* VariantName */ "green", /* JavaBlockID */ "green_glazed_terracotta", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 233, /* PocketBlockMetadata */ 0);
		glazed_terracotta->registerVariant(/* VariantName */ "red", /* JavaBlockID */ "red_glazed_terracotta", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 234, /* PocketBlockMetadata */ 0);
		glazed_terracotta->registerVariant(/* VariantName */ "black", /* JavaBlockID */ "black_glazed_terracotta", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 235, /* PocketBlockMetadata */ 0);
		concrete->registerVariant(/* VariantName */ "white", /* JavaBlockID */ "concrete", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 236, /* PocketBlockMetadata */ 0);
		concrete->registerVariant(/* VariantName */ "orange", /* JavaBlockID */ "concrete", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 236, /* PocketBlockMetadata */ 1);
		concrete->registerVariant(/* VariantName */ "magenta", /* JavaBlockID */ "concrete", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 236, /* PocketBlockMetadata */ 2);
		concrete->registerVariant(/* VariantName */ "lightBlue", /* JavaBlockID */ "concrete", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 236, /* PocketBlockMetadata */ 3);
		concrete->registerVariant(/* VariantName */ "yellow", /* JavaBlockID */ "concrete", /* JavaBlockMetadata */ 4, /* PocketBlockID */ 236, /* PocketBlockMetadata */ 4);
		concrete->registerVariant(/* VariantName */ "lime", /* JavaBlockID */ "concrete", /* JavaBlockMetadata */ 5, /* PocketBlockID */ 236, /* PocketBlockMetadata */ 5);
		concrete->registerVariant(/* VariantName */ "pink", /* JavaBlockID */ "concrete", /* JavaBlockMetadata */ 6, /* PocketBlockID */ 236, /* PocketBlockMetadata */ 6);
		concrete->registerVariant(/* VariantName */ "gray", /* JavaBlockID */ "concrete", /* JavaBlockMetadata */ 7, /* PocketBlockID */ 236, /* PocketBlockMetadata */ 7);
		concrete->registerVariant(/* VariantName */ "silver", /* JavaBlockID */ "concrete", /* JavaBlockMetadata */ 8, /* PocketBlockID */ 236, /* PocketBlockMetadata */ 8);
		concrete->registerVariant(/* VariantName */ "cyan", /* JavaBlockID */ "concrete", /* JavaBlockMetadata */ 9, /* PocketBlockID */ 236, /* PocketBlockMetadata */ 9);
		concrete->registerVariant(/* VariantName */ "purple", /* JavaBlockID */ "concrete", /* JavaBlockMetadata */ 10, /* PocketBlockID */ 236, /* PocketBlockMetadata */ 10);
		concrete->registerVariant(/* VariantName */ "blue", /* JavaBlockID */ "concrete", /* JavaBlockMetadata */ 11, /* PocketBlockID */ 236, /* PocketBlockMetadata */ 11);
		concrete->registerVariant(/* VariantName */ "brown", /* JavaBlockID */ "concrete", /* JavaBlockMetadata */ 12, /* PocketBlockID */ 236, /* PocketBlockMetadata */ 12);
		concrete->registerVariant(/* VariantName */ "green", /* JavaBlockID */ "concrete", /* JavaBlockMetadata */ 13, /* PocketBlockID */ 236, /* PocketBlockMetadata */ 13);
		concrete->registerVariant(/* VariantName */ "red", /* JavaBlockID */ "concrete", /* JavaBlockMetadata */ 14, /* PocketBlockID */ 236, /* PocketBlockMetadata */ 14);
		concrete->registerVariant(/* VariantName */ "black", /* JavaBlockID */ "concrete", /* JavaBlockMetadata */ 15, /* PocketBlockID */ 236, /* PocketBlockMetadata */ 15);
		concrete_powder->registerVariant(/* VariantName */ "white", /* JavaBlockID */ "concrete_powder", /* JavaBlockMetadata */ 0, /* PocketBlockID */ 237, /* PocketBlockMetadata */ 0);
		concrete_powder->registerVariant(/* VariantName */ "orange", /* JavaBlockID */ "concrete_powder", /* JavaBlockMetadata */ 1, /* PocketBlockID */ 237, /* PocketBlockMetadata */ 1);
		concrete_powder->registerVariant(/* VariantName */ "magenta", /* JavaBlockID */ "concrete_powder", /* JavaBlockMetadata */ 2, /* PocketBlockID */ 237, /* PocketBlockMetadata */ 2);
		concrete_powder->registerVariant(/* VariantName */ "lightBlue", /* JavaBlockID */ "concrete_powder", /* JavaBlockMetadata */ 3, /* PocketBlockID */ 237, /* PocketBlockMetadata */ 3);
		concrete_powder->registerVariant(/* VariantName */ "yellow", /* JavaBlockID */ "concrete_powder", /* JavaBlockMetadata */ 4, /* PocketBlockID */ 237, /* PocketBlockMetadata */ 4);
		concrete_powder->registerVariant(/* VariantName */ "lime", /* JavaBlockID */ "concrete_powder", /* JavaBlockMetadata */ 5, /* PocketBlockID */ 237, /* PocketBlockMetadata */ 5);
		concrete_powder->registerVariant(/* VariantName */ "pink", /* JavaBlockID */ "concrete_powder", /* JavaBlockMetadata */ 6, /* PocketBlockID */ 237, /* PocketBlockMetadata */ 6);
		concrete_powder->registerVariant(/* VariantName */ "gray", /* JavaBlockID */ "concrete_powder", /* JavaBlockMetadata */ 7, /* PocketBlockID */ 237, /* PocketBlockMetadata */ 7);
		concrete_powder->registerVariant(/* VariantName */ "silver", /* JavaBlockID */ "concrete_powder", /* JavaBlockMetadata */ 8, /* PocketBlockID */ 237, /* PocketBlockMetadata */ 8);
		concrete_powder->registerVariant(/* VariantName */ "cyan", /* JavaBlockID */ "concrete_powder", /* JavaBlockMetadata */ 9, /* PocketBlockID */ 237, /* PocketBlockMetadata */ 9);
		concrete_powder->registerVariant(/* VariantName */ "purple", /* JavaBlockID */ "concrete_powder", /* JavaBlockMetadata */ 10, /* PocketBlockID */ 237, /* PocketBlockMetadata */ 10);
		concrete_powder->registerVariant(/* VariantName */ "blue", /* JavaBlockID */ "concrete_powder", /* JavaBlockMetadata */ 11, /* PocketBlockID */ 237, /* PocketBlockMetadata */ 11);
		concrete_powder->registerVariant(/* VariantName */ "brown", /* JavaBlockID */ "concrete_powder", /* JavaBlockMetadata */ 12, /* PocketBlockID */ 237, /* PocketBlockMetadata */ 12);
		concrete_powder->registerVariant(/* VariantName */ "green", /* JavaBlockID */ "concrete_powder", /* JavaBlockMetadata */ 13, /* PocketBlockID */ 237, /* PocketBlockMetadata */ 13);
		concrete_powder->registerVariant(/* VariantName */ "red", /* JavaBlockID */ "concrete_powder", /* JavaBlockMetadata */ 14, /* PocketBlockID */ 237, /* PocketBlockMetadata */ 14);
		concrete_powder->registerVariant(/* VariantName */ "black", /* JavaBlockID */ "concrete_powder", /* JavaBlockMetadata */ 15, /* PocketBlockID */ 237, /* PocketBlockMetadata */ 15);
		structure_block->registerVariant(/* VariantName */ "", /* JavaBlockID */ "structure_block", /* JavaBlockMetadata */ , /* PocketBlockID */ 255, /* PocketBlockMetadata */ );
		->registerVariant(/* VariantName */ "", /* JavaBlockID */ "", /* JavaBlockMetadata */ frame, /* PocketBlockID */ 0, /* PocketBlockMetadata */ 199);
		std::map<std::string, Block*> Block::pocketBlockMap = {
		        { std::make_tuple(0, 0), new ABlockID("air", "default") }, ,
		        { std::make_tuple(1, 0), new ABlockID("stone", "default") }, ,
		        { std::make_tuple(1, 0), new ABlockID("stone", "default") }, ,
		        { std::make_tuple(1, 1), new ABlockID("stone", "granite") }, ,
		        { std::make_tuple(1, 2), new ABlockID("stone", "graniteSmooth") }, ,
		        { std::make_tuple(1, 3), new ABlockID("stone", "diorite") }, ,
		        { std::make_tuple(1, 4), new ABlockID("stone", "dioriteSmooth") }, ,
		        { std::make_tuple(1, 5), new ABlockID("stone", "andesite") }, ,
		        { std::make_tuple(1, 6), new ABlockID("stone", "andesiteSmooth") }, ,
		        { std::make_tuple(2, 0), new ABlockID("grass", "default") }, ,
		        { std::make_tuple(3, 0), new ABlockID("dirt", "default") }, ,
		        { std::make_tuple(4, 0), new ABlockID("cobblestone", "default") }, ,
		        { std::make_tuple(5, 0), new ABlockID("planks", "oak") }, ,
		        { std::make_tuple(5, 1), new ABlockID("planks", "spruce") }, ,
		        { std::make_tuple(5, 2), new ABlockID("planks", "birch") }, ,
		        { std::make_tuple(5, 3), new ABlockID("planks", "jungle") }, ,
		        { std::make_tuple(5, 4), new ABlockID("planks", "acacia") }, ,
		        { std::make_tuple(5, 5), new ABlockID("planks", "big_oak") }, ,
		        { std::make_tuple(6, 0), new ABlockID("sapling", "oak") }, ,
		        { std::make_tuple(6, 1), new ABlockID("sapling", "spruce") }, ,
		        { std::make_tuple(6, 2), new ABlockID("sapling", "birch") }, ,
		        { std::make_tuple(6, 3), new ABlockID("sapling", "jungle") }, ,
		        { std::make_tuple(6, 4), new ABlockID("sapling", "acacia") }, ,
		        { std::make_tuple(6, 5), new ABlockID("sapling", "big_oak") }, ,
		        { std::make_tuple(7, 0), new ABlockID("bedrock", "default") }, ,
		        { std::make_tuple(8, 0), new ABlockID("flowing_water", "default") }, ,
		        { std::make_tuple(9, 0), new ABlockID("water", "default") }, ,
		        { std::make_tuple(10, 0), new ABlockID("flowing_lava", "default") }, ,
		        { std::make_tuple(11, 0), new ABlockID("lava", "default") }, ,
		        { std::make_tuple(12, 0), new ABlockID("sand", "default") }, ,
		        { std::make_tuple(12, 1), new ABlockID("sand", "red") }, ,
		        { std::make_tuple(13, 0), new ABlockID("gravel", "default") }, ,
		        { std::make_tuple(14, 0), new ABlockID("gold_ore", "default") }, ,
		        { std::make_tuple(15, 0), new ABlockID("iron_ore", "default") }, ,
		        { std::make_tuple(16, 0), new ABlockID("coal_ore", "default") }, ,
		        { std::make_tuple(17, 0), new ABlockID("log", "oak") }, ,
		        { std::make_tuple(17, 1), new ABlockID("log", "spruce") }, ,
		        { std::make_tuple(17, 2), new ABlockID("log", "birch") }, ,
		        { std::make_tuple(17, 3), new ABlockID("log", "jungle") }, ,
		        { std::make_tuple(18, 0), new ABlockID("leaves", "oak") }, ,
		        { std::make_tuple(18, 1), new ABlockID("leaves", "spruce") }, ,
		        { std::make_tuple(18, 2), new ABlockID("leaves", "birch") }, ,
		        { std::make_tuple(18, 3), new ABlockID("leaves", "jungle") }, ,
		        { std::make_tuple(19, 0), new ABlockID("sponge", "dry") }, ,
		        { std::make_tuple(19, 1), new ABlockID("sponge", "wet") }, ,
		        { std::make_tuple(20, 0), new ABlockID("glass", "default") }, ,
		        { std::make_tuple(21, 0), new ABlockID("lapis_ore", "default") }, ,
		        { std::make_tuple(22, 0), new ABlockID("lapis_block", "default") }, ,
		        { std::make_tuple(23, 0), new ABlockID("dispenser", "default") }, ,
		        { std::make_tuple(24, 0), new ABlockID("sandstone", "default") }, ,
		        { std::make_tuple(24, 1), new ABlockID("sandstone", "chiseled") }, ,
		        { std::make_tuple(24, 2), new ABlockID("sandstone", "smooth") }, ,
		        { std::make_tuple(25, 0), new ABlockID("noteblock", "default") }, ,
		        { std::make_tuple(26, 0), new ABlockID("bed", "default") }, ,
		        { std::make_tuple(27, 0), new ABlockID("golden_rail", "default") }, ,
		        { std::make_tuple(28, 0), new ABlockID("detector_rail", "default") }, ,
		        { std::make_tuple(29, 0), new ABlockID("sticky_piston", "default") }, ,
		        { std::make_tuple(30, 0), new ABlockID("web", "default") }, ,
		        { std::make_tuple(31, 0), new ABlockID("tallgrass", "shrub") }, ,
		        { std::make_tuple(31, 1), new ABlockID("tallgrass", "tallgrass") }, ,
		        { std::make_tuple(31, 2), new ABlockID("tallgrass", "fern") }, ,
		        { std::make_tuple(32, 0), new ABlockID("deadbush", "default") }, ,
		        { std::make_tuple(33, 0), new ABlockID("piston", "default") }, ,
		        { std::make_tuple(34, 0), new ABlockID("piston_head", "default") }, ,
		        { std::make_tuple(35, 0), new ABlockID("wool", "white") }, ,
		        { std::make_tuple(35, 1), new ABlockID("wool", "orange") }, ,
		        { std::make_tuple(35, 2), new ABlockID("wool", "magenta") }, ,
		        { std::make_tuple(35, 3), new ABlockID("wool", "lightBlue") }, ,
		        { std::make_tuple(35, 4), new ABlockID("wool", "yellow") }, ,
		        { std::make_tuple(35, 5), new ABlockID("wool", "lime") }, ,
		        { std::make_tuple(35, 6), new ABlockID("wool", "pink") }, ,
		        { std::make_tuple(35, 7), new ABlockID("wool", "gray") }, ,
		        { std::make_tuple(35, 8), new ABlockID("wool", "silver") }, ,
		        { std::make_tuple(35, 9), new ABlockID("wool", "cyan") }, ,
		        { std::make_tuple(35, 10), new ABlockID("wool", "purple") }, ,
		        { std::make_tuple(35, 11), new ABlockID("wool", "blue") }, ,
		        { std::make_tuple(35, 12), new ABlockID("wool", "brown") }, ,
		        { std::make_tuple(35, 13), new ABlockID("wool", "green") }, ,
		        { std::make_tuple(35, 14), new ABlockID("wool", "red") }, ,
		        { std::make_tuple(35, 15), new ABlockID("wool", "black") }, ,
		        { std::make_tuple(250, 0), new ABlockID("piston_moving_piece", "default") }, ,
		        { std::make_tuple(37, 0), new ABlockID("yellow_flower", "default") }, ,
		        { std::make_tuple(38, 0), new ABlockID("red_flower", "poppy") }, ,
		        { std::make_tuple(38, 1), new ABlockID("red_flower", "blueOrchid") }, ,
		        { std::make_tuple(38, 2), new ABlockID("red_flower", "allium") }, ,
		        { std::make_tuple(38, 3), new ABlockID("red_flower", "houstonia") }, ,
		        { std::make_tuple(38, 4), new ABlockID("red_flower", "tulipRed") }, ,
		        { std::make_tuple(38, 5), new ABlockID("red_flower", "tulipOrange") }, ,
		        { std::make_tuple(38, 6), new ABlockID("red_flower", "tulipWhite") }, ,
		        { std::make_tuple(38, 7), new ABlockID("red_flower", "tulipPink") }, ,
		        { std::make_tuple(38, 8), new ABlockID("red_flower", "oxeyeDaisy") }, ,
		        { std::make_tuple(39, 0), new ABlockID("brown_mushroom", "default") }, ,
		        { std::make_tuple(40, 0), new ABlockID("red_mushroom", "default") }, ,
		        { std::make_tuple(41, 0), new ABlockID("gold_block", "default") }, ,
		        { std::make_tuple(42, 0), new ABlockID("iron_block", "default") }, ,
		        { std::make_tuple(43, 0), new ABlockID("double_stone_slab", "stone") }, ,
		        { std::make_tuple(43, 1), new ABlockID("double_stone_slab", "sand") }, ,
		        { std::make_tuple(43, 2), new ABlockID("double_stone_slab", "wood") }, ,
		        { std::make_tuple(43, 3), new ABlockID("double_stone_slab", "cobble") }, ,
		        { std::make_tuple(43, 4), new ABlockID("double_stone_slab", "brick") }, ,
		        { std::make_tuple(43, 5), new ABlockID("double_stone_slab", "smoothStoneBrick") }, ,
		        { std::make_tuple(43, 6), new ABlockID("double_stone_slab", "quartz") }, ,
		        { std::make_tuple(43, 7), new ABlockID("double_stone_slab", "netherBrick") }, ,
		        { std::make_tuple(44, 0), new ABlockID("stone_slab", "stone") }, ,
		        { std::make_tuple(44, 1), new ABlockID("stone_slab", "sand") }, ,
		        { std::make_tuple(44, 2), new ABlockID("stone_slab", "wood") }, ,
		        { std::make_tuple(44, 3), new ABlockID("stone_slab", "cobble") }, ,
		        { std::make_tuple(44, 4), new ABlockID("stone_slab", "brick") }, ,
		        { std::make_tuple(44, 5), new ABlockID("stone_slab", "smoothStoneBrick") }, ,
		        { std::make_tuple(44, 6), new ABlockID("stone_slab", "quartz") }, ,
		        { std::make_tuple(44, 7), new ABlockID("stone_slab", "netherBrick") }, ,
		        { std::make_tuple(45, 0), new ABlockID("brick_block", "default") }, ,
		        { std::make_tuple(46, 0), new ABlockID("tnt", "default") }, ,
		        { std::make_tuple(47, 0), new ABlockID("bookshelf", "default") }, ,
		        { std::make_tuple(48, 0), new ABlockID("mossy_cobblestone", "default") }, ,
		        { std::make_tuple(49, 0), new ABlockID("obsidian", "default") }, ,
		        { std::make_tuple(50, 0), new ABlockID("torch", "default") }, ,
		        { std::make_tuple(51, 0), new ABlockID("fire", "default") }, ,
		        { std::make_tuple(52, 0), new ABlockID("mob_spawner", "default") }, ,
		        { std::make_tuple(53, 0), new ABlockID("oak_stairs", "default") }, ,
		        { std::make_tuple(54, 0), new ABlockID("chest", "default") }, ,
		        { std::make_tuple(55, 0), new ABlockID("redstone_wire", "default") }, ,
		        { std::make_tuple(56, 0), new ABlockID("diamond_ore", "default") }, ,
		        { std::make_tuple(57, 0), new ABlockID("diamond_block", "default") }, ,
		        { std::make_tuple(58, 0), new ABlockID("crafting_table", "default") }, ,
		        { std::make_tuple(59, 0), new ABlockID("wheat", "default") }, ,
		        { std::make_tuple(60, 0), new ABlockID("farmland", "default") }, ,
		        { std::make_tuple(61, 0), new ABlockID("furnace", "default") }, ,
		        { std::make_tuple(62, 0), new ABlockID("burning_furnace", "default") }, ,
		        { std::make_tuple(63, 0), new ABlockID("sign_post", "default") }, ,
		        { std::make_tuple(64, 0), new ABlockID("door", "wooden_door") }, ,
		        { std::make_tuple(65, 0), new ABlockID("ladder", "default") }, ,
		        { std::make_tuple(66, 0), new ABlockID("rail", "default") }, ,
		        { std::make_tuple(67, 0), new ABlockID("cobblestone_stairs", "default") }, ,
		        { std::make_tuple(68, 0), new ABlockID("wall_sign", "default") }, ,
		        { std::make_tuple(69, 0), new ABlockID("lever", "default") }, ,
		        { std::make_tuple(70, 0), new ABlockID("stone_pressure_plate", "default") }, ,
		        { std::make_tuple(71, 0), new ABlockID("iron_door", "default") }, ,
		        { std::make_tuple(72, 0), new ABlockID("wooden_pressure_plate", "default") }, ,
		        { std::make_tuple(73, 0), new ABlockID("redstone_ore", "default") }, ,
		        { std::make_tuple(74, 0), new ABlockID("glowing_redstone_ore", "default") }, ,
		        { std::make_tuple(75, 0), new ABlockID("redstone_torch_off", "default") }, ,
		        { std::make_tuple(76, 0), new ABlockID("redstone_torch_on", "default") }, ,
		        { std::make_tuple(77, 0), new ABlockID("stone_button", "default") }, ,
		        { std::make_tuple(78, 0), new ABlockID("snow_layer", "default") }, ,
		        { std::make_tuple(79, 0), new ABlockID("ice", "default") }, ,
		        { std::make_tuple(80, 0), new ABlockID("snow", "default") }, ,
		        { std::make_tuple(81, 0), new ABlockID("cactus", "default") }, ,
		        { std::make_tuple(82, 0), new ABlockID("clay", "default") }, ,
		        { std::make_tuple(83, 0), new ABlockID("sugar_cane_block", "default") }, ,
		        { std::make_tuple(255, ), new ABlockID("jukebox", "") }, ,
		        { std::make_tuple(85, 0), new ABlockID("fence", "oak_fence") }, ,
		        { std::make_tuple(86, 0), new ABlockID("pumpkin", "default") }, ,
		        { std::make_tuple(87, 0), new ABlockID("netherrack", "default") }, ,
		        { std::make_tuple(88, 0), new ABlockID("soul_sand", "default") }, ,
		        { std::make_tuple(89, 0), new ABlockID("glowstone", "default") }, ,
		        { std::make_tuple(90, 0), new ABlockID("portal", "default") }, ,
		        { std::make_tuple(91, 0), new ABlockID("jack_o_lantern", "default") }, ,
		        { std::make_tuple(92, 0), new ABlockID("cake", "default") }, ,
		        { std::make_tuple(93, 0), new ABlockID("unpowered_repeater", "default") }, ,
		        { std::make_tuple(94, 0), new ABlockID("powered_repeater", "default") }, ,
		        { std::make_tuple(241, 0), new ABlockID("stained_glass", "white") }, ,
		        { std::make_tuple(241, 1), new ABlockID("stained_glass", "orange") }, ,
		        { std::make_tuple(241, 2), new ABlockID("stained_glass", "magenta") }, ,
		        { std::make_tuple(241, 3), new ABlockID("stained_glass", "lightBlue") }, ,
		        { std::make_tuple(241, 4), new ABlockID("stained_glass", "yellow") }, ,
		        { std::make_tuple(241, 5), new ABlockID("stained_glass", "lime") }, ,
		        { std::make_tuple(241, 6), new ABlockID("stained_glass", "pink") }, ,
		        { std::make_tuple(241, 7), new ABlockID("stained_glass", "gray") }, ,
		        { std::make_tuple(241, 8), new ABlockID("stained_glass", "silver") }, ,
		        { std::make_tuple(241, 9), new ABlockID("stained_glass", "cyan") }, ,
		        { std::make_tuple(241, 10), new ABlockID("stained_glass", "purple") }, ,
		        { std::make_tuple(241, 11), new ABlockID("stained_glass", "blue") }, ,
		        { std::make_tuple(241, 12), new ABlockID("stained_glass", "brown") }, ,
		        { std::make_tuple(241, 13), new ABlockID("stained_glass", "green") }, ,
		        { std::make_tuple(241, 14), new ABlockID("stained_glass", "red") }, ,
		        { std::make_tuple(241, 15), new ABlockID("stained_glass", "black") }, ,
		        { std::make_tuple(96, 0), new ABlockID("trap_door", "default") }, ,
		        { std::make_tuple(97, 0), new ABlockID("monster_egg", "stone") }, ,
		        { std::make_tuple(97, 1), new ABlockID("monster_egg", "cobble") }, ,
		        { std::make_tuple(97, 2), new ABlockID("monster_egg", "brick") }, ,
		        { std::make_tuple(97, 3), new ABlockID("monster_egg", "mossybrick") }, ,
		        { std::make_tuple(97, 4), new ABlockID("monster_egg", "crackedbrick") }, ,
		        { std::make_tuple(97, 5), new ABlockID("monster_egg", "chiseledbrick") }, ,
		        { std::make_tuple(98, 0), new ABlockID("stonebrick", "default") }, ,
		        { std::make_tuple(98, 1), new ABlockID("stonebrick", "mossy") }, ,
		        { std::make_tuple(98, 2), new ABlockID("stonebrick", "cracked") }, ,
		        { std::make_tuple(98, 3), new ABlockID("stonebrick", "chiseled") }, ,
		        { std::make_tuple(99, 0), new ABlockID("brown_mushroom_block", "default") }, ,
		        { std::make_tuple(100, 0), new ABlockID("red_mushroom_block", "default") }, ,
		        { std::make_tuple(101, 0), new ABlockID("iron_bars", "default") }, ,
		        { std::make_tuple(102, 0), new ABlockID("glass_pane", "default") }, ,
		        { std::make_tuple(103, 0), new ABlockID("melon_block", "default") }, ,
		        { std::make_tuple(104, 0), new ABlockID("pumpkin_stem", "default") }, ,
		        { std::make_tuple(105, 0), new ABlockID("melon_stem", "default") }, ,
		        { std::make_tuple(106, 0), new ABlockID("vine", "default") }, ,
		        { std::make_tuple(107, 0), new ABlockID("fence_gate", "default") }, ,
		        { std::make_tuple(108, 0), new ABlockID("brick_stairs", "default") }, ,
		        { std::make_tuple(109, 0), new ABlockID("stone_brick_stairs", "default") }, ,
		        { std::make_tuple(110, 0), new ABlockID("mycelium", "default") }, ,
		        { std::make_tuple(111, 0), new ABlockID("waterlily", "default") }, ,
		        { std::make_tuple(112, 0), new ABlockID("nether_brick", "default") }, ,
		        { std::make_tuple(113, 0), new ABlockID("nether_brick_fence", "default") }, ,
		        { std::make_tuple(114, 0), new ABlockID("nether_brick_stairs", "default") }, ,
		        { std::make_tuple(115, 0), new ABlockID("nether_wart", "default") }, ,
		        { std::make_tuple(116, 0), new ABlockID("enchanting_table", "default") }, ,
		        { std::make_tuple(117, 0), new ABlockID("brewing_stand", "default") }, ,
		        { std::make_tuple(118, 0), new ABlockID("cauldron", "default") }, ,
		        { std::make_tuple(119, 0), new ABlockID("end_portal", "default") }, ,
		        { std::make_tuple(120, 0), new ABlockID("end_portal_frame", "default") }, ,
		        { std::make_tuple(121, 0), new ABlockID("end_stone", "default") }, ,
		        { std::make_tuple(122, 0), new ABlockID("dragon_egg", "default") }, ,
		        { std::make_tuple(123, 0), new ABlockID("redstone_lamp_off", "default") }, ,
		        { std::make_tuple(124, 0), new ABlockID("redstone_lamp_on", "default") }, ,
		        { std::make_tuple(157, 0), new ABlockID("double_wooden_slab", "oak") }, ,
		        { std::make_tuple(157, 1), new ABlockID("double_wooden_slab", "spruce") }, ,
		        { std::make_tuple(157, 2), new ABlockID("double_wooden_slab", "birch") }, ,
		        { std::make_tuple(157, 3), new ABlockID("double_wooden_slab", "jungle") }, ,
		        { std::make_tuple(157, 4), new ABlockID("double_wooden_slab", "acacia") }, ,
		        { std::make_tuple(157, 5), new ABlockID("double_wooden_slab", "big_oak") }, ,
		        { std::make_tuple(158, 0), new ABlockID("wooden_slab", "oak") }, ,
		        { std::make_tuple(158, 1), new ABlockID("wooden_slab", "spruce") }, ,
		        { std::make_tuple(158, 2), new ABlockID("wooden_slab", "birch") }, ,
		        { std::make_tuple(158, 3), new ABlockID("wooden_slab", "jungle") }, ,
		        { std::make_tuple(158, 4), new ABlockID("wooden_slab", "acacia") }, ,
		        { std::make_tuple(158, 5), new ABlockID("wooden_slab", "big_oak") }, ,
		        { std::make_tuple(127, 0), new ABlockID("cocoa", "default") }, ,
		        { std::make_tuple(128, 0), new ABlockID("sandstone_stairs", "default") }, ,
		        { std::make_tuple(129, 0), new ABlockID("emerald_ore", "default") }, ,
		        { std::make_tuple(130, 0), new ABlockID("ender_chest", "default") }, ,
		        { std::make_tuple(131, 0), new ABlockID("tripwire_hook", "default") }, ,
		        { std::make_tuple(132, 0), new ABlockID("tripwire", "default") }, ,
		        { std::make_tuple(133, 0), new ABlockID("emerald_block", "default") }, ,
		        { std::make_tuple(134, 0), new ABlockID("spruce_stairs", "default") }, ,
		        { std::make_tuple(135, 0), new ABlockID("birch_stairs", "default") }, ,
		        { std::make_tuple(136, 0), new ABlockID("jungle_stairs", "default") }, ,
		        { std::make_tuple(137, 0), new ABlockID("command_block", "default") }, ,
		        { std::make_tuple(138, 0), new ABlockID("beacon", "default") }, ,
		        { std::make_tuple(139, 0), new ABlockID("cobblestone_wall", "normal") }, ,
		        { std::make_tuple(139, 1), new ABlockID("cobblestone_wall", "mossy") }, ,
		        { std::make_tuple(140, 0), new ABlockID("flower_pot", "default") }, ,
		        { std::make_tuple(141, 0), new ABlockID("carrots", "default") }, ,
		        { std::make_tuple(142, 0), new ABlockID("potatos", "default") }, ,
		        { std::make_tuple(143, 0), new ABlockID("wooden_button", "default") }, ,
		        { std::make_tuple(144, 0), new ABlockID("skull", "default") }, ,
		        { std::make_tuple(145, 0), new ABlockID("anvil", "default") }, ,
		        { std::make_tuple(145, 4), new ABlockID("anvil", "slightlyDamaged") }, ,
		        { std::make_tuple(145, 8), new ABlockID("anvil", "veryDamaged") }, ,
		        { std::make_tuple(146, 0), new ABlockID("trapped_chest", "default") }, ,
		        { std::make_tuple(147, 0), new ABlockID("gold_pressure_plate", "default") }, ,
		        { std::make_tuple(148, 0), new ABlockID("iron_pressure_plate", "default") }, ,
		        { std::make_tuple(149, 0), new ABlockID("redstone_comparator_off", "default") }, ,
		        { std::make_tuple(150, 0), new ABlockID("redstone_comparator_on", "default") }, ,
		        { std::make_tuple(151, 0), new ABlockID("daylight_detector", "default") }, ,
		        { std::make_tuple(152, 0), new ABlockID("redstone_block", "default") }, ,
		        { std::make_tuple(153, 0), new ABlockID("quartz_ore", "default") }, ,
		        { std::make_tuple(154, 0), new ABlockID("hopper", "default") }, ,
		        { std::make_tuple(155, 0), new ABlockID("quartz_block", "default") }, ,
		        { std::make_tuple(155, 1), new ABlockID("quartz_block", "chiseled") }, ,
		        { std::make_tuple(155, 2), new ABlockID("quartz_block", "vertical") }, ,
		        { std::make_tuple(156, 0), new ABlockID("quartz_stairs", "default") }, ,
		        { std::make_tuple(126, 0), new ABlockID("activator_rail", "default") }, ,
		        { std::make_tuple(125, 0), new ABlockID("dropper", "default") }, ,
		        { std::make_tuple(159, 0), new ABlockID("stained_hardened_clay", "default") }, ,
		        { std::make_tuple(160, 0), new ABlockID("stained_glass_pane", "white") }, ,
		        { std::make_tuple(160, 1), new ABlockID("stained_glass_pane", "orange") }, ,
		        { std::make_tuple(160, 2), new ABlockID("stained_glass_pane", "magenta") }, ,
		        { std::make_tuple(160, 3), new ABlockID("stained_glass_pane", "lightBlue") }, ,
		        { std::make_tuple(160, 4), new ABlockID("stained_glass_pane", "yellow") }, ,
		        { std::make_tuple(160, 5), new ABlockID("stained_glass_pane", "lime") }, ,
		        { std::make_tuple(160, 6), new ABlockID("stained_glass_pane", "pink") }, ,
		        { std::make_tuple(160, 7), new ABlockID("stained_glass_pane", "gray") }, ,
		        { std::make_tuple(160, 8), new ABlockID("stained_glass_pane", "silver") }, ,
		        { std::make_tuple(160, 9), new ABlockID("stained_glass_pane", "cyan") }, ,
		        { std::make_tuple(160, 10), new ABlockID("stained_glass_pane", "purple") }, ,
		        { std::make_tuple(160, 11), new ABlockID("stained_glass_pane", "blue") }, ,
		        { std::make_tuple(160, 12), new ABlockID("stained_glass_pane", "brown") }, ,
		        { std::make_tuple(160, 13), new ABlockID("stained_glass_pane", "green") }, ,
		        { std::make_tuple(160, 14), new ABlockID("stained_glass_pane", "red") }, ,
		        { std::make_tuple(160, 15), new ABlockID("stained_glass_pane", "black") }, ,
		        { std::make_tuple(161, 0), new ABlockID("leaves2", "acacia") }, ,
		        { std::make_tuple(161, 1), new ABlockID("leaves2", "big_oak") }, ,
		        { std::make_tuple(162, 0), new ABlockID("log2", "acacia") }, ,
		        { std::make_tuple(162, 1), new ABlockID("log2", "big_oak") }, ,
		        { std::make_tuple(163, 0), new ABlockID("acacia_stairs", "default") }, ,
		        { std::make_tuple(164, 0), new ABlockID("dark_oak_stairs", "default") }, ,
		        { std::make_tuple(165, 0), new ABlockID("slime", "default") }, ,
		        { std::make_tuple(75, 0), new ABlockID("barrier", "default") }, ,
		        { std::make_tuple(167, 0), new ABlockID("iron_trapdoor", "default") }, ,
		        { std::make_tuple(168, 0), new ABlockID("prismarine", "rough") }, ,
		        { std::make_tuple(168, 1), new ABlockID("prismarine", "dark") }, ,
		        { std::make_tuple(168, 2), new ABlockID("prismarine", "bricks") }, ,
		        { std::make_tuple(169, 0), new ABlockID("sea_lantern", "default") }, ,
		        { std::make_tuple(170, 0), new ABlockID("hay_block", "default") }, ,
		        { std::make_tuple(171, 0), new ABlockID("carpet", "white") }, ,
		        { std::make_tuple(171, 1), new ABlockID("carpet", "orange") }, ,
		        { std::make_tuple(171, 2), new ABlockID("carpet", "magenta") }, ,
		        { std::make_tuple(171, 3), new ABlockID("carpet", "lightBlue") }, ,
		        { std::make_tuple(171, 4), new ABlockID("carpet", "yellow") }, ,
		        { std::make_tuple(171, 5), new ABlockID("carpet", "lime") }, ,
		        { std::make_tuple(171, 6), new ABlockID("carpet", "pink") }, ,
		        { std::make_tuple(171, 7), new ABlockID("carpet", "gray") }, ,
		        { std::make_tuple(171, 8), new ABlockID("carpet", "silver") }, ,
		        { std::make_tuple(171, 9), new ABlockID("carpet", "cyan") }, ,
		        { std::make_tuple(171, 10), new ABlockID("carpet", "purple") }, ,
		        { std::make_tuple(171, 11), new ABlockID("carpet", "blue") }, ,
		        { std::make_tuple(171, 12), new ABlockID("carpet", "brown") }, ,
		        { std::make_tuple(171, 13), new ABlockID("carpet", "green") }, ,
		        { std::make_tuple(171, 14), new ABlockID("carpet", "red") }, ,
		        { std::make_tuple(171, 15), new ABlockID("carpet", "black") }, ,
		        { std::make_tuple(172, 0), new ABlockID("hardened_clay", "default") }, ,
		        { std::make_tuple(173, 0), new ABlockID("coal_block", "default") }, ,
		        { std::make_tuple(174, 0), new ABlockID("packed_ice", "default") }, ,
		        { std::make_tuple(175, 0), new ABlockID("double_plant", "sunflower") }, ,
		        { std::make_tuple(175, 1), new ABlockID("double_plant", "syringa") }, ,
		        { std::make_tuple(175, 2), new ABlockID("double_plant", "grass") }, ,
		        { std::make_tuple(175, 3), new ABlockID("double_plant", "fern") }, ,
		        { std::make_tuple(175, 4), new ABlockID("double_plant", "rose") }, ,
		        { std::make_tuple(175, 5), new ABlockID("double_plant", "paeonia") }, ,
		        { std::make_tuple(255, 0), new ABlockID("standing_banner", "default") }, ,
		        { std::make_tuple(255, 0), new ABlockID("wall_banner", "default") }, ,
		        { std::make_tuple(178, 0), new ABlockID("daylight_detector_inverted", "default") }, ,
		        { std::make_tuple(179, 0), new ABlockID("red_sandstone", "default") }, ,
		        { std::make_tuple(179, 1), new ABlockID("red_sandstone", "chiseled") }, ,
		        { std::make_tuple(179, 2), new ABlockID("red_sandstone", "smooth") }, ,
		        { std::make_tuple(180, 0), new ABlockID("red_sandstone_stairs", "default") }, ,
		        { std::make_tuple(181, 0), new ABlockID("double_stone_slab2", "red_sandstone") }, ,
		        { std::make_tuple(182, 0), new ABlockID("stone_slab2", "red_sandstone") }, ,
		        { std::make_tuple(183, 0), new ABlockID("spruce_fence_gate", "default") }, ,
		        { std::make_tuple(184, 0), new ABlockID("birch_fence_gate", "default") }, ,
		        { std::make_tuple(185, 0), new ABlockID("jungle_fence_gate", "default") }, ,
		        { std::make_tuple(186, 0), new ABlockID("dark_oak_fence_gate", "default") }, ,
		        { std::make_tuple(187, 0), new ABlockID("acacia_fence_gate", "default") }, ,
		        { std::make_tuple(85, 1), new ABlockID("fence", "spruce_fence") }, ,
		        { std::make_tuple(85, 2), new ABlockID("fence", "birch_fence") }, ,
		        { std::make_tuple(85, 3), new ABlockID("fence", "jungle_fence") }, ,
		        { std::make_tuple(85, 4), new ABlockID("fence", "dark_oak_fence") }, ,
		        { std::make_tuple(85, 5), new ABlockID("fence", "acacia_fence") }, ,
		        { std::make_tuple(193, -1), new ABlockID("door", "spruce_door") }, ,
		        { std::make_tuple(194, -1), new ABlockID("door", "birch_door") }, ,
		        { std::make_tuple(195, -1), new ABlockID("door", "jungle_door") }, ,
		        { std::make_tuple(196, -1), new ABlockID("door", "acacia_door") }, ,
		        { std::make_tuple(197, -1), new ABlockID("door", "dark_oak_door") }, ,
		        { std::make_tuple(208, 0), new ABlockID("end_rod", "default") }, ,
		        { std::make_tuple(240, 0), new ABlockID("chorus_plant", "default") }, ,
		        { std::make_tuple(200, 0), new ABlockID("chorus_flower", "default") }, ,
		        { std::make_tuple(201, 0), new ABlockID("purpur_block", "default") }, ,
		        { std::make_tuple(201, 2), new ABlockID("purpur_block", "purpur_pillar") }, ,
		        { std::make_tuple(203, 0), new ABlockID("purpur_stairs", "default") }, ,
		        { std::make_tuple(181, 1), new ABlockID("double_stone_slab2", "purpur_double_slab") }, ,
		        { std::make_tuple(182, 1), new ABlockID("stone_slab2", "purpur_slab") }, ,
		        { std::make_tuple(206, 0), new ABlockID("end_bricks", "default") }, ,
		        { std::make_tuple(244, 0), new ABlockID("beetroots", "default") }, ,
		        { std::make_tuple(198, 0), new ABlockID("grass_path", "default") }, ,
		        { std::make_tuple(209, 0), new ABlockID("end_gateway", "default") }, ,
		        { std::make_tuple(188, 0), new ABlockID("repeating_command_block", "default") }, ,
		        { std::make_tuple(189, 0), new ABlockID("chain_command_block", "default") }, ,
		        { std::make_tuple(207, 0), new ABlockID("frosted_ice", "default") }, ,
		        { std::make_tuple(255, ), new ABlockID("magma", "") }, ,
		        { std::make_tuple(255, ), new ABlockID("nether_wart_block", "") }, ,
		        { std::make_tuple(255, ), new ABlockID("red_nether_brick", "") }, ,
		        { std::make_tuple(255, ), new ABlockID("bone_block", "") }, ,
		        { std::make_tuple(255, ), new ABlockID("structure_void", "") }, ,
		        { std::make_tuple(251, 0), new ABlockID("observer", "default") }, ,
		        { std::make_tuple(218, 0), new ABlockID("shulker_box", "white") }, ,
		        { std::make_tuple(218, 1), new ABlockID("shulker_box", "orange") }, ,
		        { std::make_tuple(218, 2), new ABlockID("shulker_box", "magenta") }, ,
		        { std::make_tuple(218, 3), new ABlockID("shulker_box", "lightBlue") }, ,
		        { std::make_tuple(218, 4), new ABlockID("shulker_box", "yellow") }, ,
		        { std::make_tuple(218, 5), new ABlockID("shulker_box", "lime") }, ,
		        { std::make_tuple(218, 6), new ABlockID("shulker_box", "pink") }, ,
		        { std::make_tuple(218, 7), new ABlockID("shulker_box", "gray") }, ,
		        { std::make_tuple(218, 8), new ABlockID("shulker_box", "silver") }, ,
		        { std::make_tuple(218, 9), new ABlockID("shulker_box", "cyan") }, ,
		        { std::make_tuple(218, 10), new ABlockID("shulker_box", "purple") }, ,
		        { std::make_tuple(218, 11), new ABlockID("shulker_box", "blue") }, ,
		        { std::make_tuple(218, 12), new ABlockID("shulker_box", "brown") }, ,
		        { std::make_tuple(218, 13), new ABlockID("shulker_box", "green") }, ,
		        { std::make_tuple(218, 14), new ABlockID("shulker_box", "red") }, ,
		        { std::make_tuple(218, 15), new ABlockID("shulker_box", "black") }, ,
		        { std::make_tuple(220, 0), new ABlockID("glazed_terracotta", "white") }, ,
		        { std::make_tuple(221, 0), new ABlockID("glazed_terracotta", "orange") }, ,
		        { std::make_tuple(222, 0), new ABlockID("glazed_terracotta", "magenta") }, ,
		        { std::make_tuple(223, 0), new ABlockID("glazed_terracotta", "lightBlue") }, ,
		        { std::make_tuple(224, 0), new ABlockID("glazed_terracotta", "yellow") }, ,
		        { std::make_tuple(225, 0), new ABlockID("glazed_terracotta", "lime") }, ,
		        { std::make_tuple(226, 0), new ABlockID("glazed_terracotta", "pink") }, ,
		        { std::make_tuple(227, 0), new ABlockID("glazed_terracotta", "gray") }, ,
		        { std::make_tuple(228, 0), new ABlockID("glazed_terracotta", "silver") }, ,
		        { std::make_tuple(229, 0), new ABlockID("glazed_terracotta", "cyan") }, ,
		        { std::make_tuple(219, 0), new ABlockID("glazed_terracotta", "purple") }, ,
		        { std::make_tuple(231, 0), new ABlockID("glazed_terracotta", "blue") }, ,
		        { std::make_tuple(232, 0), new ABlockID("glazed_terracotta", "brown") }, ,
		        { std::make_tuple(233, 0), new ABlockID("glazed_terracotta", "green") }, ,
		        { std::make_tuple(234, 0), new ABlockID("glazed_terracotta", "red") }, ,
		        { std::make_tuple(235, 0), new ABlockID("glazed_terracotta", "black") }, ,
		        { std::make_tuple(236, 0), new ABlockID("concrete", "white") }, ,
		        { std::make_tuple(236, 1), new ABlockID("concrete", "orange") }, ,
		        { std::make_tuple(236, 2), new ABlockID("concrete", "magenta") }, ,
		        { std::make_tuple(236, 3), new ABlockID("concrete", "lightBlue") }, ,
		        { std::make_tuple(236, 4), new ABlockID("concrete", "yellow") }, ,
		        { std::make_tuple(236, 5), new ABlockID("concrete", "lime") }, ,
		        { std::make_tuple(236, 6), new ABlockID("concrete", "pink") }, ,
		        { std::make_tuple(236, 7), new ABlockID("concrete", "gray") }, ,
		        { std::make_tuple(236, 8), new ABlockID("concrete", "silver") }, ,
		        { std::make_tuple(236, 9), new ABlockID("concrete", "cyan") }, ,
		        { std::make_tuple(236, 10), new ABlockID("concrete", "purple") }, ,
		        { std::make_tuple(236, 11), new ABlockID("concrete", "blue") }, ,
		        { std::make_tuple(236, 12), new ABlockID("concrete", "brown") }, ,
		        { std::make_tuple(236, 13), new ABlockID("concrete", "green") }, ,
		        { std::make_tuple(236, 14), new ABlockID("concrete", "red") }, ,
		        { std::make_tuple(236, 15), new ABlockID("concrete", "black") }, ,
		        { std::make_tuple(237, 0), new ABlockID("concrete_powder", "white") }, ,
		        { std::make_tuple(237, 1), new ABlockID("concrete_powder", "orange") }, ,
		        { std::make_tuple(237, 2), new ABlockID("concrete_powder", "magenta") }, ,
		        { std::make_tuple(237, 3), new ABlockID("concrete_powder", "lightBlue") }, ,
		        { std::make_tuple(237, 4), new ABlockID("concrete_powder", "yellow") }, ,
		        { std::make_tuple(237, 5), new ABlockID("concrete_powder", "lime") }, ,
		        { std::make_tuple(237, 6), new ABlockID("concrete_powder", "pink") }, ,
		        { std::make_tuple(237, 7), new ABlockID("concrete_powder", "gray") }, ,
		        { std::make_tuple(237, 8), new ABlockID("concrete_powder", "silver") }, ,
		        { std::make_tuple(237, 9), new ABlockID("concrete_powder", "cyan") }, ,
		        { std::make_tuple(237, 10), new ABlockID("concrete_powder", "purple") }, ,
		        { std::make_tuple(237, 11), new ABlockID("concrete_powder", "blue") }, ,
		        { std::make_tuple(237, 12), new ABlockID("concrete_powder", "brown") }, ,
		        { std::make_tuple(237, 13), new ABlockID("concrete_powder", "green") }, ,
		        { std::make_tuple(237, 14), new ABlockID("concrete_powder", "red") }, ,
		        { std::make_tuple(237, 15), new ABlockID("concrete_powder", "black") }, ,
		        { std::make_tuple(255, ), new ABlockID("structure_block", "") }, ,
		        { std::make_tuple(0, 199), new ABlockID("", "") }, ,
		};
		std::map<std::string, Block*> Block::javaBlockMap = {
		        { std::make_tuple("air", 0), new ABlockID("air", "default") }, ,
		        { std::make_tuple("stone", 0), new ABlockID("stone", "default") }, ,
		        { std::make_tuple("stone", 0), new ABlockID("stone", "default") }, ,
		        { std::make_tuple("stone", 1), new ABlockID("stone", "granite") }, ,
		        { std::make_tuple("stone", 2), new ABlockID("stone", "graniteSmooth") }, ,
		        { std::make_tuple("stone", 3), new ABlockID("stone", "diorite") }, ,
		        { std::make_tuple("stone", 4), new ABlockID("stone", "dioriteSmooth") }, ,
		        { std::make_tuple("stone", 5), new ABlockID("stone", "andesite") }, ,
		        { std::make_tuple("stone", 6), new ABlockID("stone", "andesiteSmooth") }, ,
		        { std::make_tuple("grass", 0), new ABlockID("grass", "default") }, ,
		        { std::make_tuple("dirt", 0), new ABlockID("dirt", "default") }, ,
		        { std::make_tuple("cobblestone", 0), new ABlockID("cobblestone", "default") }, ,
		        { std::make_tuple("wood", 0), new ABlockID("planks", "oak") }, ,
		        { std::make_tuple("wood", 1), new ABlockID("planks", "spruce") }, ,
		        { std::make_tuple("wood", 2), new ABlockID("planks", "birch") }, ,
		        { std::make_tuple("wood", 3), new ABlockID("planks", "jungle") }, ,
		        { std::make_tuple("wood", 4), new ABlockID("planks", "acacia") }, ,
		        { std::make_tuple("wood", 5), new ABlockID("planks", "big_oak") }, ,
		        { std::make_tuple("sapling", 0), new ABlockID("sapling", "oak") }, ,
		        { std::make_tuple("sapling", 1), new ABlockID("sapling", "spruce") }, ,
		        { std::make_tuple("sapling", 2), new ABlockID("sapling", "birch") }, ,
		        { std::make_tuple("sapling", 3), new ABlockID("sapling", "jungle") }, ,
		        { std::make_tuple("sapling", 4), new ABlockID("sapling", "acacia") }, ,
		        { std::make_tuple("sapling", 5), new ABlockID("sapling", "big_oak") }, ,
		        { std::make_tuple("bedrock", 0), new ABlockID("bedrock", "default") }, ,
		        { std::make_tuple("flowing_water", 0), new ABlockID("flowing_water", "default") }, ,
		        { std::make_tuple("water", 0), new ABlockID("water", "default") }, ,
		        { std::make_tuple("flowing_lava", 0), new ABlockID("flowing_lava", "default") }, ,
		        { std::make_tuple("lava", 0), new ABlockID("lava", "default") }, ,
		        { std::make_tuple("sand", 0), new ABlockID("sand", "default") }, ,
		        { std::make_tuple("sand", 1), new ABlockID("sand", "red") }, ,
		        { std::make_tuple("gravel", 0), new ABlockID("gravel", "default") }, ,
		        { std::make_tuple("gold_ore", 0), new ABlockID("gold_ore", "default") }, ,
		        { std::make_tuple("iron_ore", 0), new ABlockID("iron_ore", "default") }, ,
		        { std::make_tuple("coal_ore", 0), new ABlockID("coal_ore", "default") }, ,
		        { std::make_tuple("log", 0), new ABlockID("log", "oak") }, ,
		        { std::make_tuple("log", 1), new ABlockID("log", "spruce") }, ,
		        { std::make_tuple("log", 2), new ABlockID("log", "birch") }, ,
		        { std::make_tuple("log", 3), new ABlockID("log", "jungle") }, ,
		        { std::make_tuple("leaves", 0), new ABlockID("leaves", "oak") }, ,
		        { std::make_tuple("leaves", 1), new ABlockID("leaves", "spruce") }, ,
		        { std::make_tuple("leaves", 2), new ABlockID("leaves", "birch") }, ,
		        { std::make_tuple("leaves", 3), new ABlockID("leaves", "jungle") }, ,
		        { std::make_tuple("sponge", 0), new ABlockID("sponge", "dry") }, ,
		        { std::make_tuple("sponge", 1), new ABlockID("sponge", "wet") }, ,
		        { std::make_tuple("glass", 0), new ABlockID("glass", "default") }, ,
		        { std::make_tuple("lapis_ore", 0), new ABlockID("lapis_ore", "default") }, ,
		        { std::make_tuple("lapis_block", 0), new ABlockID("lapis_block", "default") }, ,
		        { std::make_tuple("dispenser", 0), new ABlockID("dispenser", "default") }, ,
		        { std::make_tuple("sandstone", 0), new ABlockID("sandstone", "default") }, ,
		        { std::make_tuple("sandstone", 1), new ABlockID("sandstone", "chiseled") }, ,
		        { std::make_tuple("sandstone", 2), new ABlockID("sandstone", "smooth") }, ,
		        { std::make_tuple("noteblock", 0), new ABlockID("noteblock", "default") }, ,
		        { std::make_tuple("bed", 0), new ABlockID("bed", "default") }, ,
		        { std::make_tuple("golden_rail", 0), new ABlockID("golden_rail", "default") }, ,
		        { std::make_tuple("detector_rail", 0), new ABlockID("detector_rail", "default") }, ,
		        { std::make_tuple("sticky_piston", 0), new ABlockID("sticky_piston", "default") }, ,
		        { std::make_tuple("web", 0), new ABlockID("web", "default") }, ,
		        { std::make_tuple("tallgrass", 0), new ABlockID("tallgrass", "shrub") }, ,
		        { std::make_tuple("tallgrass", 1), new ABlockID("tallgrass", "tallgrass") }, ,
		        { std::make_tuple("tallgrass", 2), new ABlockID("tallgrass", "fern") }, ,
		        { std::make_tuple("deadbush", 0), new ABlockID("deadbush", "default") }, ,
		        { std::make_tuple("piston", 0), new ABlockID("piston", "default") }, ,
		        { std::make_tuple("piston_head", 0), new ABlockID("piston_head", "default") }, ,
		        { std::make_tuple("wool", 0), new ABlockID("wool", "white") }, ,
		        { std::make_tuple("wool", 1), new ABlockID("wool", "orange") }, ,
		        { std::make_tuple("wool", 2), new ABlockID("wool", "magenta") }, ,
		        { std::make_tuple("wool", 3), new ABlockID("wool", "lightBlue") }, ,
		        { std::make_tuple("wool", 4), new ABlockID("wool", "yellow") }, ,
		        { std::make_tuple("wool", 5), new ABlockID("wool", "lime") }, ,
		        { std::make_tuple("wool", 6), new ABlockID("wool", "pink") }, ,
		        { std::make_tuple("wool", 7), new ABlockID("wool", "gray") }, ,
		        { std::make_tuple("wool", 8), new ABlockID("wool", "silver") }, ,
		        { std::make_tuple("wool", 9), new ABlockID("wool", "cyan") }, ,
		        { std::make_tuple("wool", 10), new ABlockID("wool", "purple") }, ,
		        { std::make_tuple("wool", 11), new ABlockID("wool", "blue") }, ,
		        { std::make_tuple("wool", 12), new ABlockID("wool", "brown") }, ,
		        { std::make_tuple("wool", 13), new ABlockID("wool", "green") }, ,
		        { std::make_tuple("wool", 14), new ABlockID("wool", "red") }, ,
		        { std::make_tuple("wool", 15), new ABlockID("wool", "black") }, ,
		        { std::make_tuple("piston_extension", 0), new ABlockID("piston_moving_piece", "default") }, ,
		        { std::make_tuple("yellow_flower", 0), new ABlockID("yellow_flower", "default") }, ,
		        { std::make_tuple("red_flower", 0), new ABlockID("red_flower", "poppy") }, ,
		        { std::make_tuple("red_flower", 1), new ABlockID("red_flower", "blueOrchid") }, ,
		        { std::make_tuple("red_flower", 2), new ABlockID("red_flower", "allium") }, ,
		        { std::make_tuple("red_flower", 3), new ABlockID("red_flower", "houstonia") }, ,
		        { std::make_tuple("red_flower", 4), new ABlockID("red_flower", "tulipRed") }, ,
		        { std::make_tuple("red_flower", 5), new ABlockID("red_flower", "tulipOrange") }, ,
		        { std::make_tuple("red_flower", 6), new ABlockID("red_flower", "tulipWhite") }, ,
		        { std::make_tuple("red_flower", 7), new ABlockID("red_flower", "tulipPink") }, ,
		        { std::make_tuple("red_flower", 8), new ABlockID("red_flower", "oxeyeDaisy") }, ,
		        { std::make_tuple("brown_mushroom", 0), new ABlockID("brown_mushroom", "default") }, ,
		        { std::make_tuple("red_mushroom", 0), new ABlockID("red_mushroom", "default") }, ,
		        { std::make_tuple("gold_block", 0), new ABlockID("gold_block", "default") }, ,
		        { std::make_tuple("iron_block", 0), new ABlockID("iron_block", "default") }, ,
		        { std::make_tuple("double_stone_slab", 0), new ABlockID("double_stone_slab", "stone") }, ,
		        { std::make_tuple("double_stone_slab", 1), new ABlockID("double_stone_slab", "sand") }, ,
		        { std::make_tuple("double_stone_slab", 2), new ABlockID("double_stone_slab", "wood") }, ,
		        { std::make_tuple("double_stone_slab", 3), new ABlockID("double_stone_slab", "cobble") }, ,
		        { std::make_tuple("double_stone_slab", 4), new ABlockID("double_stone_slab", "brick") }, ,
		        { std::make_tuple("double_stone_slab", 5), new ABlockID("double_stone_slab", "smoothStoneBrick") }, ,
		        { std::make_tuple("double_stone_slab", 7), new ABlockID("double_stone_slab", "quartz") }, ,
		        { std::make_tuple("double_stone_slab", 6), new ABlockID("double_stone_slab", "netherBrick") }, ,
		        { std::make_tuple("stone_slab", 0), new ABlockID("stone_slab", "stone") }, ,
		        { std::make_tuple("stone_slab", 1), new ABlockID("stone_slab", "sand") }, ,
		        { std::make_tuple("stone_slab", 2), new ABlockID("stone_slab", "wood") }, ,
		        { std::make_tuple("stone_slab", 3), new ABlockID("stone_slab", "cobble") }, ,
		        { std::make_tuple("stone_slab", 4), new ABlockID("stone_slab", "brick") }, ,
		        { std::make_tuple("stone_slab", 5), new ABlockID("stone_slab", "smoothStoneBrick") }, ,
		        { std::make_tuple("stone_slab", 7), new ABlockID("stone_slab", "quartz") }, ,
		        { std::make_tuple("stone_slab", 6), new ABlockID("stone_slab", "netherBrick") }, ,
		        { std::make_tuple("brick_block", 0), new ABlockID("brick_block", "default") }, ,
		        { std::make_tuple("tnt", 0), new ABlockID("tnt", "default") }, ,
		        { std::make_tuple("bookshelf", 0), new ABlockID("bookshelf", "default") }, ,
		        { std::make_tuple("mossy_cobblestone", 0), new ABlockID("mossy_cobblestone", "default") }, ,
		        { std::make_tuple("obsidian", 0), new ABlockID("obsidian", "default") }, ,
		        { std::make_tuple("torch", 0), new ABlockID("torch", "default") }, ,
		        { std::make_tuple("fire", 0), new ABlockID("fire", "default") }, ,
		        { std::make_tuple("mob_spawner", 0), new ABlockID("mob_spawner", "default") }, ,
		        { std::make_tuple("oak_stairs", 0), new ABlockID("oak_stairs", "default") }, ,
		        { std::make_tuple("chest", 0), new ABlockID("chest", "default") }, ,
		        { std::make_tuple("redstone_wire", 0), new ABlockID("redstone_wire", "default") }, ,
		        { std::make_tuple("diamond_ore", 0), new ABlockID("diamond_ore", "default") }, ,
		        { std::make_tuple("diamond_block", 0), new ABlockID("diamond_block", "default") }, ,
		        { std::make_tuple("crafting_table", 0), new ABlockID("crafting_table", "default") }, ,
		        { std::make_tuple("wheat", 0), new ABlockID("wheat", "default") }, ,
		        { std::make_tuple("farmland", 0), new ABlockID("farmland", "default") }, ,
		        { std::make_tuple("furnace", 0), new ABlockID("furnace", "default") }, ,
		        { std::make_tuple("lit_furnace", 0), new ABlockID("burning_furnace", "default") }, ,
		        { std::make_tuple("standing_sign", 0), new ABlockID("sign_post", "default") }, ,
		        { std::make_tuple("wooden_door", 0), new ABlockID("door", "wooden_door") }, ,
		        { std::make_tuple("ladder", 0), new ABlockID("ladder", "default") }, ,
		        { std::make_tuple("rail", 0), new ABlockID("rail", "default") }, ,
		        { std::make_tuple("stone_stairs", 0), new ABlockID("cobblestone_stairs", "default") }, ,
		        { std::make_tuple("wall_sign", 0), new ABlockID("wall_sign", "default") }, ,
		        { std::make_tuple("lever", 0), new ABlockID("lever", "default") }, ,
		        { std::make_tuple("stone_pressure_plate", 0), new ABlockID("stone_pressure_plate", "default") }, ,
		        { std::make_tuple("iron_door", 0), new ABlockID("iron_door", "default") }, ,
		        { std::make_tuple("wooden_pressure_plate", 0), new ABlockID("wooden_pressure_plate", "default") }, ,
		        { std::make_tuple("redstone_ore", 0), new ABlockID("redstone_ore", "default") }, ,
		        { std::make_tuple("lit_redstone_ore", 0), new ABlockID("glowing_redstone_ore", "default") }, ,
		        { std::make_tuple("unlit_redstone_torch", 0), new ABlockID("redstone_torch_off", "default") }, ,
		        { std::make_tuple("redstone_torch", 0), new ABlockID("redstone_torch_on", "default") }, ,
		        { std::make_tuple("stone_button", 0), new ABlockID("stone_button", "default") }, ,
		        { std::make_tuple("snow_layer", 0), new ABlockID("snow_layer", "default") }, ,
		        { std::make_tuple("ice", 0), new ABlockID("ice", "default") }, ,
		        { std::make_tuple("snow", 0), new ABlockID("snow", "default") }, ,
		        { std::make_tuple("cactus", 0), new ABlockID("cactus", "default") }, ,
		        { std::make_tuple("clay", 0), new ABlockID("clay", "default") }, ,
		        { std::make_tuple("reeds", 0), new ABlockID("sugar_cane_block", "default") }, ,
		        { std::make_tuple("jukebox", ), new ABlockID("jukebox", "") }, ,
		        { std::make_tuple("fence", 0), new ABlockID("fence", "oak_fence") }, ,
		        { std::make_tuple("pumpkin", 0), new ABlockID("pumpkin", "default") }, ,
		        { std::make_tuple("netherrack", 0), new ABlockID("netherrack", "default") }, ,
		        { std::make_tuple("soul_sand", 0), new ABlockID("soul_sand", "default") }, ,
		        { std::make_tuple("glowstone", 0), new ABlockID("glowstone", "default") }, ,
		        { std::make_tuple("portal", 0), new ABlockID("portal", "default") }, ,
		        { std::make_tuple("lit_pumpkin", 0), new ABlockID("jack_o_lantern", "default") }, ,
		        { std::make_tuple("cake", 0), new ABlockID("cake", "default") }, ,
		        { std::make_tuple("unpowered_repeater", 0), new ABlockID("unpowered_repeater", "default") }, ,
		        { std::make_tuple("powered_repeater", 0), new ABlockID("powered_repeater", "default") }, ,
		        { std::make_tuple("stained_glass", 0), new ABlockID("stained_glass", "white") }, ,
		        { std::make_tuple("stained_glass", 1), new ABlockID("stained_glass", "orange") }, ,
		        { std::make_tuple("stained_glass", 2), new ABlockID("stained_glass", "magenta") }, ,
		        { std::make_tuple("stained_glass", 3), new ABlockID("stained_glass", "lightBlue") }, ,
		        { std::make_tuple("stained_glass", 4), new ABlockID("stained_glass", "yellow") }, ,
		        { std::make_tuple("stained_glass", 5), new ABlockID("stained_glass", "lime") }, ,
		        { std::make_tuple("stained_glass", 6), new ABlockID("stained_glass", "pink") }, ,
		        { std::make_tuple("stained_glass", 7), new ABlockID("stained_glass", "gray") }, ,
		        { std::make_tuple("stained_glass", 8), new ABlockID("stained_glass", "silver") }, ,
		        { std::make_tuple("stained_glass", 9), new ABlockID("stained_glass", "cyan") }, ,
		        { std::make_tuple("stained_glass", 10), new ABlockID("stained_glass", "purple") }, ,
		        { std::make_tuple("stained_glass", 11), new ABlockID("stained_glass", "blue") }, ,
		        { std::make_tuple("stained_glass", 12), new ABlockID("stained_glass", "brown") }, ,
		        { std::make_tuple("stained_glass", 13), new ABlockID("stained_glass", "green") }, ,
		        { std::make_tuple("stained_glass", 14), new ABlockID("stained_glass", "red") }, ,
		        { std::make_tuple("stained_glass", 15), new ABlockID("stained_glass", "black") }, ,
		        { std::make_tuple("trap_door", 0), new ABlockID("trap_door", "default") }, ,
		        { std::make_tuple("monster_egg", 0), new ABlockID("monster_egg", "stone") }, ,
		        { std::make_tuple("monster_egg", 1), new ABlockID("monster_egg", "cobble") }, ,
		        { std::make_tuple("monster_egg", 2), new ABlockID("monster_egg", "brick") }, ,
		        { std::make_tuple("monster_egg", 3), new ABlockID("monster_egg", "mossybrick") }, ,
		        { std::make_tuple("monster_egg", 4), new ABlockID("monster_egg", "crackedbrick") }, ,
		        { std::make_tuple("monster_egg", 5), new ABlockID("monster_egg", "chiseledbrick") }, ,
		        { std::make_tuple("stonebrick", 0), new ABlockID("stonebrick", "default") }, ,
		        { std::make_tuple("stonebrick", 1), new ABlockID("stonebrick", "mossy") }, ,
		        { std::make_tuple("stonebrick", 2), new ABlockID("stonebrick", "cracked") }, ,
		        { std::make_tuple("stonebrick", 3), new ABlockID("stonebrick", "chiseled") }, ,
		        { std::make_tuple("brown_mushroom_block", 0), new ABlockID("brown_mushroom_block", "default") }, ,
		        { std::make_tuple("red_mushroom_block", 0), new ABlockID("red_mushroom_block", "default") }, ,
		        { std::make_tuple("iron_bars", 0), new ABlockID("iron_bars", "default") }, ,
		        { std::make_tuple("glass_pane", 0), new ABlockID("glass_pane", "default") }, ,
		        { std::make_tuple("melon_block", 0), new ABlockID("melon_block", "default") }, ,
		        { std::make_tuple("pumpkin_stem", 0), new ABlockID("pumpkin_stem", "default") }, ,
		        { std::make_tuple("melon_stem", 0), new ABlockID("melon_stem", "default") }, ,
		        { std::make_tuple("vine", 0), new ABlockID("vine", "default") }, ,
		        { std::make_tuple("fence_gate", 0), new ABlockID("fence_gate", "default") }, ,
		        { std::make_tuple("brick_stairs", 0), new ABlockID("brick_stairs", "default") }, ,
		        { std::make_tuple("stone_brick_stairs", 0), new ABlockID("stone_brick_stairs", "default") }, ,
		        { std::make_tuple("mycelium", 0), new ABlockID("mycelium", "default") }, ,
		        { std::make_tuple("waterlily", 0), new ABlockID("waterlily", "default") }, ,
		        { std::make_tuple("nether_brick", 0), new ABlockID("nether_brick", "default") }, ,
		        { std::make_tuple("nether_brick_fence", 0), new ABlockID("nether_brick_fence", "default") }, ,
		        { std::make_tuple("nether_brick_stairs", 0), new ABlockID("nether_brick_stairs", "default") }, ,
		        { std::make_tuple("nether_wart", 0), new ABlockID("nether_wart", "default") }, ,
		        { std::make_tuple("enchanting_table", 0), new ABlockID("enchanting_table", "default") }, ,
		        { std::make_tuple("brewing_stand", 0), new ABlockID("brewing_stand", "default") }, ,
		        { std::make_tuple("cauldron", 0), new ABlockID("cauldron", "default") }, ,
		        { std::make_tuple("end_portal", 0), new ABlockID("end_portal", "default") }, ,
		        { std::make_tuple("end_portal_frame", 0), new ABlockID("end_portal_frame", "default") }, ,
		        { std::make_tuple("end_stone", 0), new ABlockID("end_stone", "default") }, ,
		        { std::make_tuple("dragon_egg", 0), new ABlockID("dragon_egg", "default") }, ,
		        { std::make_tuple("redstone_lamp", 0), new ABlockID("redstone_lamp_off", "default") }, ,
		        { std::make_tuple("lit_redstone_lamp", 0), new ABlockID("redstone_lamp_on", "default") }, ,
		        { std::make_tuple("double_wooden_slab", 0), new ABlockID("double_wooden_slab", "oak") }, ,
		        { std::make_tuple("double_wooden_slab", 1), new ABlockID("double_wooden_slab", "spruce") }, ,
		        { std::make_tuple("double_wooden_slab", 2), new ABlockID("double_wooden_slab", "birch") }, ,
		        { std::make_tuple("double_wooden_slab", 3), new ABlockID("double_wooden_slab", "jungle") }, ,
		        { std::make_tuple("double_wooden_slab", 4), new ABlockID("double_wooden_slab", "acacia") }, ,
		        { std::make_tuple("double_wooden_slab", 5), new ABlockID("double_wooden_slab", "big_oak") }, ,
		        { std::make_tuple("wooden_slab", 0), new ABlockID("wooden_slab", "oak") }, ,
		        { std::make_tuple("wooden_slab", 1), new ABlockID("wooden_slab", "spruce") }, ,
		        { std::make_tuple("wooden_slab", 2), new ABlockID("wooden_slab", "birch") }, ,
		        { std::make_tuple("wooden_slab", 3), new ABlockID("wooden_slab", "jungle") }, ,
		        { std::make_tuple("wooden_slab", 4), new ABlockID("wooden_slab", "acacia") }, ,
		        { std::make_tuple("wooden_slab", 5), new ABlockID("wooden_slab", "big_oak") }, ,
		        { std::make_tuple("cocoa", 0), new ABlockID("cocoa", "default") }, ,
		        { std::make_tuple("sandstone_stairs", 0), new ABlockID("sandstone_stairs", "default") }, ,
		        { std::make_tuple("emerald_ore", 0), new ABlockID("emerald_ore", "default") }, ,
		        { std::make_tuple("ender_chest", 0), new ABlockID("ender_chest", "default") }, ,
		        { std::make_tuple("tripwire_hook", 0), new ABlockID("tripwire_hook", "default") }, ,
		        { std::make_tuple("tripwire", 0), new ABlockID("tripwire", "default") }, ,
		        { std::make_tuple("emerald_block", 0), new ABlockID("emerald_block", "default") }, ,
		        { std::make_tuple("spruce_stairs", 0), new ABlockID("spruce_stairs", "default") }, ,
		        { std::make_tuple("birch_stairs", 0), new ABlockID("birch_stairs", "default") }, ,
		        { std::make_tuple("jungle_stairs", 0), new ABlockID("jungle_stairs", "default") }, ,
		        { std::make_tuple("command_block", 0), new ABlockID("command_block", "default") }, ,
		        { std::make_tuple("beacon", 0), new ABlockID("beacon", "default") }, ,
		        { std::make_tuple("cobblestone_wall", 0), new ABlockID("cobblestone_wall", "normal") }, ,
		        { std::make_tuple("cobblestone_wall", 1), new ABlockID("cobblestone_wall", "mossy") }, ,
		        { std::make_tuple("flower_pot", 0), new ABlockID("flower_pot", "default") }, ,
		        { std::make_tuple("carrots", 0), new ABlockID("carrots", "default") }, ,
		        { std::make_tuple("potatos", 0), new ABlockID("potatos", "default") }, ,
		        { std::make_tuple("wooden_button", 0), new ABlockID("wooden_button", "default") }, ,
		        { std::make_tuple("skull", 0), new ABlockID("skull", "default") }, ,
		        { std::make_tuple("anvil", 0), new ABlockID("anvil", "default") }, ,
		        { std::make_tuple("anvil", -1), new ABlockID("anvil", "slightlyDamaged") }, ,
		        { std::make_tuple("anvil", -1), new ABlockID("anvil", "veryDamaged") }, ,
		        { std::make_tuple("trapped_chest", 0), new ABlockID("trapped_chest", "default") }, ,
		        { std::make_tuple("light_weighted_pressure_plate", 0), new ABlockID("gold_pressure_plate", "default") }, ,
		        { std::make_tuple("heavy_weighted_pressure_plate", 0), new ABlockID("iron_pressure_plate", "default") }, ,
		        { std::make_tuple("unpowered_comparator", 0), new ABlockID("redstone_comparator_off", "default") }, ,
		        { std::make_tuple("powered_comparator", 0), new ABlockID("redstone_comparator_on", "default") }, ,
		        { std::make_tuple("daylight_detector", 0), new ABlockID("daylight_detector", "default") }, ,
		        { std::make_tuple("redstone_block", 0), new ABlockID("redstone_block", "default") }, ,
		        { std::make_tuple("quartz_ore", 0), new ABlockID("quartz_ore", "default") }, ,
		        { std::make_tuple("hopper", 0), new ABlockID("hopper", "default") }, ,
		        { std::make_tuple("quartz_block", 0), new ABlockID("quartz_block", "default") }, ,
		        { std::make_tuple("quartz_block", 1), new ABlockID("quartz_block", "chiseled") }, ,
		        { std::make_tuple("quartz_block", 2), new ABlockID("quartz_block", "vertical") }, ,
		        { std::make_tuple("quartz_stairs", 0), new ABlockID("quartz_stairs", "default") }, ,
		        { std::make_tuple("activator_rail", 0), new ABlockID("activator_rail", "default") }, ,
		        { std::make_tuple("dropper", 0), new ABlockID("dropper", "default") }, ,
		        { std::make_tuple("stained_hardened_clay", 0), new ABlockID("stained_hardened_clay", "default") }, ,
		        { std::make_tuple("stained_glass_pane", 0), new ABlockID("stained_glass_pane", "white") }, ,
		        { std::make_tuple("stained_glass_pane", 1), new ABlockID("stained_glass_pane", "orange") }, ,
		        { std::make_tuple("stained_glass_pane", 2), new ABlockID("stained_glass_pane", "magenta") }, ,
		        { std::make_tuple("stained_glass_pane", 3), new ABlockID("stained_glass_pane", "lightBlue") }, ,
		        { std::make_tuple("stained_glass_pane", 4), new ABlockID("stained_glass_pane", "yellow") }, ,
		        { std::make_tuple("stained_glass_pane", 5), new ABlockID("stained_glass_pane", "lime") }, ,
		        { std::make_tuple("stained_glass_pane", 6), new ABlockID("stained_glass_pane", "pink") }, ,
		        { std::make_tuple("stained_glass_pane", 7), new ABlockID("stained_glass_pane", "gray") }, ,
		        { std::make_tuple("stained_glass_pane", 8), new ABlockID("stained_glass_pane", "silver") }, ,
		        { std::make_tuple("stained_glass_pane", 9), new ABlockID("stained_glass_pane", "cyan") }, ,
		        { std::make_tuple("stained_glass_pane", 10), new ABlockID("stained_glass_pane", "purple") }, ,
		        { std::make_tuple("stained_glass_pane", 11), new ABlockID("stained_glass_pane", "blue") }, ,
		        { std::make_tuple("stained_glass_pane", 12), new ABlockID("stained_glass_pane", "brown") }, ,
		        { std::make_tuple("stained_glass_pane", 13), new ABlockID("stained_glass_pane", "green") }, ,
		        { std::make_tuple("stained_glass_pane", 14), new ABlockID("stained_glass_pane", "red") }, ,
		        { std::make_tuple("stained_glass_pane", 15), new ABlockID("stained_glass_pane", "black") }, ,
		        { std::make_tuple("leaves2", 0), new ABlockID("leaves2", "acacia") }, ,
		        { std::make_tuple("leaves2", 1), new ABlockID("leaves2", "big_oak") }, ,
		        { std::make_tuple("log2", 0), new ABlockID("log2", "acacia") }, ,
		        { std::make_tuple("log2", 1), new ABlockID("log2", "big_oak") }, ,
		        { std::make_tuple("acacia_stairs", 0), new ABlockID("acacia_stairs", "default") }, ,
		        { std::make_tuple("dark_oak_stairs", 0), new ABlockID("dark_oak_stairs", "default") }, ,
		        { std::make_tuple("slime", 0), new ABlockID("slime", "default") }, ,
		        { std::make_tuple("barrier", 0), new ABlockID("barrier", "default") }, ,
		        { std::make_tuple("iron_trapdoor", 0), new ABlockID("iron_trapdoor", "default") }, ,
		        { std::make_tuple("prismarine", 0), new ABlockID("prismarine", "rough") }, ,
		        { std::make_tuple("prismarine", 2), new ABlockID("prismarine", "dark") }, ,
		        { std::make_tuple("prismarine", 1), new ABlockID("prismarine", "bricks") }, ,
		        { std::make_tuple("sea_lantern", 0), new ABlockID("sea_lantern", "default") }, ,
		        { std::make_tuple("hay_block", 0), new ABlockID("hay_block", "default") }, ,
		        { std::make_tuple("carpet", 0), new ABlockID("carpet", "white") }, ,
		        { std::make_tuple("carpet", 1), new ABlockID("carpet", "orange") }, ,
		        { std::make_tuple("carpet", 2), new ABlockID("carpet", "magenta") }, ,
		        { std::make_tuple("carpet", 3), new ABlockID("carpet", "lightBlue") }, ,
		        { std::make_tuple("carpet", 4), new ABlockID("carpet", "yellow") }, ,
		        { std::make_tuple("carpet", 5), new ABlockID("carpet", "lime") }, ,
		        { std::make_tuple("carpet", 6), new ABlockID("carpet", "pink") }, ,
		        { std::make_tuple("carpet", 7), new ABlockID("carpet", "gray") }, ,
		        { std::make_tuple("carpet", 8), new ABlockID("carpet", "silver") }, ,
		        { std::make_tuple("carpet", 9), new ABlockID("carpet", "cyan") }, ,
		        { std::make_tuple("carpet", 10), new ABlockID("carpet", "purple") }, ,
		        { std::make_tuple("carpet", 11), new ABlockID("carpet", "blue") }, ,
		        { std::make_tuple("carpet", 12), new ABlockID("carpet", "brown") }, ,
		        { std::make_tuple("carpet", 13), new ABlockID("carpet", "green") }, ,
		        { std::make_tuple("carpet", 14), new ABlockID("carpet", "red") }, ,
		        { std::make_tuple("carpet", 15), new ABlockID("carpet", "black") }, ,
		        { std::make_tuple("hardened_clay", 0), new ABlockID("hardened_clay", "default") }, ,
		        { std::make_tuple("coal_block", 0), new ABlockID("coal_block", "default") }, ,
		        { std::make_tuple("packed_ice", 0), new ABlockID("packed_ice", "default") }, ,
		        { std::make_tuple("double_plant", 0), new ABlockID("double_plant", "sunflower") }, ,
		        { std::make_tuple("double_plant", 1), new ABlockID("double_plant", "syringa") }, ,
		        { std::make_tuple("double_plant", 2), new ABlockID("double_plant", "grass") }, ,
		        { std::make_tuple("double_plant", 3), new ABlockID("double_plant", "fern") }, ,
		        { std::make_tuple("double_plant", 4), new ABlockID("double_plant", "rose") }, ,
		        { std::make_tuple("double_plant", 5), new ABlockID("double_plant", "paeonia") }, ,
		        { std::make_tuple("standing_banner", 0), new ABlockID("standing_banner", "default") }, ,
		        { std::make_tuple("wall_banner", 0), new ABlockID("wall_banner", "default") }, ,
		        { std::make_tuple("daylight_detector_inverted", 0), new ABlockID("daylight_detector_inverted", "default") }, ,
		        { std::make_tuple("red_sandstone", 0), new ABlockID("red_sandstone", "default") }, ,
		        { std::make_tuple("red_sandstone", 1), new ABlockID("red_sandstone", "chiseled") }, ,
		        { std::make_tuple("red_sandstone", 2), new ABlockID("red_sandstone", "smooth") }, ,
		        { std::make_tuple("red_sandstone_stairs", 0), new ABlockID("red_sandstone_stairs", "default") }, ,
		        { std::make_tuple("double_stone_slab2", 0), new ABlockID("double_stone_slab2", "red_sandstone") }, ,
		        { std::make_tuple("stone_slab2", 0), new ABlockID("stone_slab2", "red_sandstone") }, ,
		        { std::make_tuple("spruce_fence_gate", 0), new ABlockID("spruce_fence_gate", "default") }, ,
		        { std::make_tuple("birch_fence_gate", 0), new ABlockID("birch_fence_gate", "default") }, ,
		        { std::make_tuple("jungle_fence_gate", 0), new ABlockID("jungle_fence_gate", "default") }, ,
		        { std::make_tuple("dark_oak_fence_gate", 0), new ABlockID("dark_oak_fence_gate", "default") }, ,
		        { std::make_tuple("acacia_fence_gate", 0), new ABlockID("acacia_fence_gate", "default") }, ,
		        { std::make_tuple("spruce_fence", 0), new ABlockID("fence", "spruce_fence") }, ,
		        { std::make_tuple("birch_fence", 0), new ABlockID("fence", "birch_fence") }, ,
		        { std::make_tuple("jungle_fence", 0), new ABlockID("fence", "jungle_fence") }, ,
		        { std::make_tuple("dark_oak_fence", 0), new ABlockID("fence", "dark_oak_fence") }, ,
		        { std::make_tuple("acacia_fence", 0), new ABlockID("fence", "acacia_fence") }, ,
		        { std::make_tuple("spruce_door", -1), new ABlockID("door", "spruce_door") }, ,
		        { std::make_tuple("birch_door", -1), new ABlockID("door", "birch_door") }, ,
		        { std::make_tuple("jungle_door", -1), new ABlockID("door", "jungle_door") }, ,
		        { std::make_tuple("acacia_door", -1), new ABlockID("door", "acacia_door") }, ,
		        { std::make_tuple("dark_oak_door", -1), new ABlockID("door", "dark_oak_door") }, ,
		        { std::make_tuple("end_rod", 0), new ABlockID("end_rod", "default") }, ,
		        { std::make_tuple("chorus_plant", 0), new ABlockID("chorus_plant", "default") }, ,
		        { std::make_tuple("chorus_flower", 0), new ABlockID("chorus_flower", "default") }, ,
		        { std::make_tuple("purpur_block", 0), new ABlockID("purpur_block", "default") }, ,
		        { std::make_tuple("purpur_pillar", 0), new ABlockID("purpur_block", "purpur_pillar") }, ,
		        { std::make_tuple("purpur_stairs", 0), new ABlockID("purpur_stairs", "default") }, ,
		        { std::make_tuple("purpur_double_slab", 0), new ABlockID("double_stone_slab2", "purpur_double_slab") }, ,
		        { std::make_tuple("purpur_slab", 0), new ABlockID("stone_slab2", "purpur_slab") }, ,
		        { std::make_tuple("end_bricks", 0), new ABlockID("end_bricks", "default") }, ,
		        { std::make_tuple("beetroots", 0), new ABlockID("beetroots", "default") }, ,
		        { std::make_tuple("grass_path", 0), new ABlockID("grass_path", "default") }, ,
		        { std::make_tuple("end_gateway", 0), new ABlockID("end_gateway", "default") }, ,
		        { std::make_tuple("repeating_command_block", 0), new ABlockID("repeating_command_block", "default") }, ,
		        { std::make_tuple("chain_command_block", 0), new ABlockID("chain_command_block", "default") }, ,
		        { std::make_tuple("frosted_ice", 0), new ABlockID("frosted_ice", "default") }, ,
		        { std::make_tuple("magma", ), new ABlockID("magma", "") }, ,
		        { std::make_tuple("nether_wart_block", ), new ABlockID("nether_wart_block", "") }, ,
		        { std::make_tuple("red_nether_brick", ), new ABlockID("red_nether_brick", "") }, ,
		        { std::make_tuple("bone_block", ), new ABlockID("bone_block", "") }, ,
		        { std::make_tuple("structure_void", ), new ABlockID("structure_void", "") }, ,
		        { std::make_tuple("observer", 0), new ABlockID("observer", "default") }, ,
		        { std::make_tuple("white_shulker_box", 0), new ABlockID("shulker_box", "white") }, ,
		        { std::make_tuple("orange_shulker_box", 0), new ABlockID("shulker_box", "orange") }, ,
		        { std::make_tuple("magenta_shulker_box", 0), new ABlockID("shulker_box", "magenta") }, ,
		        { std::make_tuple("light_blue_shulker_box", 0), new ABlockID("shulker_box", "lightBlue") }, ,
		        { std::make_tuple("yellow_shulker_box", 0), new ABlockID("shulker_box", "yellow") }, ,
		        { std::make_tuple("lime_shulker_box", 0), new ABlockID("shulker_box", "lime") }, ,
		        { std::make_tuple("pink_shulker_box", 0), new ABlockID("shulker_box", "pink") }, ,
		        { std::make_tuple("gray_shulker_box", 0), new ABlockID("shulker_box", "gray") }, ,
		        { std::make_tuple("silver_shulker_box", 0), new ABlockID("shulker_box", "silver") }, ,
		        { std::make_tuple("cyan_shulker_box", 0), new ABlockID("shulker_box", "cyan") }, ,
		        { std::make_tuple("purple_shulker_box", 0), new ABlockID("shulker_box", "purple") }, ,
		        { std::make_tuple("blue_shulker_box", 0), new ABlockID("shulker_box", "blue") }, ,
		        { std::make_tuple("brown_shulker_box", 0), new ABlockID("shulker_box", "brown") }, ,
		        { std::make_tuple("green_shulker_box", 0), new ABlockID("shulker_box", "green") }, ,
		        { std::make_tuple("red_shulker_box", 0), new ABlockID("shulker_box", "red") }, ,
		        { std::make_tuple("black_shulker_box", 0), new ABlockID("shulker_box", "black") }, ,
		        { std::make_tuple("white_glazed_terracotta", 0), new ABlockID("glazed_terracotta", "white") }, ,
		        { std::make_tuple("orange_glazed_terracotta", 0), new ABlockID("glazed_terracotta", "orange") }, ,
		        { std::make_tuple("magenta_glazed_terracotta", 0), new ABlockID("glazed_terracotta", "magenta") }, ,
		        { std::make_tuple("light_blue_glazed_terracotta", 0), new ABlockID("glazed_terracotta", "lightBlue") }, ,
		        { std::make_tuple("yellow_glazed_terracotta", 0), new ABlockID("glazed_terracotta", "yellow") }, ,
		        { std::make_tuple("lime_glazed_terracotta", 0), new ABlockID("glazed_terracotta", "lime") }, ,
		        { std::make_tuple("pink_glazed_terracotta", 0), new ABlockID("glazed_terracotta", "pink") }, ,
		        { std::make_tuple("gray_glazed_terracotta", 0), new ABlockID("glazed_terracotta", "gray") }, ,
		        { std::make_tuple("light_gray_glazed_terracotta", 0), new ABlockID("glazed_terracotta", "silver") }, ,
		        { std::make_tuple("cyan_glazed_terracotta", 0), new ABlockID("glazed_terracotta", "cyan") }, ,
		        { std::make_tuple("purple_glazed_terracotta", 0), new ABlockID("glazed_terracotta", "purple") }, ,
		        { std::make_tuple("blue_glazed_terracotta", 0), new ABlockID("glazed_terracotta", "blue") }, ,
		        { std::make_tuple("brown_glazed_terracotta", 0), new ABlockID("glazed_terracotta", "brown") }, ,
		        { std::make_tuple("green_glazed_terracotta", 0), new ABlockID("glazed_terracotta", "green") }, ,
		        { std::make_tuple("red_glazed_terracotta", 0), new ABlockID("glazed_terracotta", "red") }, ,
		        { std::make_tuple("black_glazed_terracotta", 0), new ABlockID("glazed_terracotta", "black") }, ,
		        { std::make_tuple("concrete", 0), new ABlockID("concrete", "white") }, ,
		        { std::make_tuple("concrete", 1), new ABlockID("concrete", "orange") }, ,
		        { std::make_tuple("concrete", 2), new ABlockID("concrete", "magenta") }, ,
		        { std::make_tuple("concrete", 3), new ABlockID("concrete", "lightBlue") }, ,
		        { std::make_tuple("concrete", 4), new ABlockID("concrete", "yellow") }, ,
		        { std::make_tuple("concrete", 5), new ABlockID("concrete", "lime") }, ,
		        { std::make_tuple("concrete", 6), new ABlockID("concrete", "pink") }, ,
		        { std::make_tuple("concrete", 7), new ABlockID("concrete", "gray") }, ,
		        { std::make_tuple("concrete", 8), new ABlockID("concrete", "silver") }, ,
		        { std::make_tuple("concrete", 9), new ABlockID("concrete", "cyan") }, ,
		        { std::make_tuple("concrete", 10), new ABlockID("concrete", "purple") }, ,
		        { std::make_tuple("concrete", 11), new ABlockID("concrete", "blue") }, ,
		        { std::make_tuple("concrete", 12), new ABlockID("concrete", "brown") }, ,
		        { std::make_tuple("concrete", 13), new ABlockID("concrete", "green") }, ,
		        { std::make_tuple("concrete", 14), new ABlockID("concrete", "red") }, ,
		        { std::make_tuple("concrete", 15), new ABlockID("concrete", "black") }, ,
		        { std::make_tuple("concrete_powder", 0), new ABlockID("concrete_powder", "white") }, ,
		        { std::make_tuple("concrete_powder", 1), new ABlockID("concrete_powder", "orange") }, ,
		        { std::make_tuple("concrete_powder", 2), new ABlockID("concrete_powder", "magenta") }, ,
		        { std::make_tuple("concrete_powder", 3), new ABlockID("concrete_powder", "lightBlue") }, ,
		        { std::make_tuple("concrete_powder", 4), new ABlockID("concrete_powder", "yellow") }, ,
		        { std::make_tuple("concrete_powder", 5), new ABlockID("concrete_powder", "lime") }, ,
		        { std::make_tuple("concrete_powder", 6), new ABlockID("concrete_powder", "pink") }, ,
		        { std::make_tuple("concrete_powder", 7), new ABlockID("concrete_powder", "gray") }, ,
		        { std::make_tuple("concrete_powder", 8), new ABlockID("concrete_powder", "silver") }, ,
		        { std::make_tuple("concrete_powder", 9), new ABlockID("concrete_powder", "cyan") }, ,
		        { std::make_tuple("concrete_powder", 10), new ABlockID("concrete_powder", "purple") }, ,
		        { std::make_tuple("concrete_powder", 11), new ABlockID("concrete_powder", "blue") }, ,
		        { std::make_tuple("concrete_powder", 12), new ABlockID("concrete_powder", "brown") }, ,
		        { std::make_tuple("concrete_powder", 13), new ABlockID("concrete_powder", "green") }, ,
		        { std::make_tuple("concrete_powder", 14), new ABlockID("concrete_powder", "red") }, ,
		        { std::make_tuple("concrete_powder", 15), new ABlockID("concrete_powder", "black") }, ,
		        { std::make_tuple("structure_block", ), new ABlockID("structure_block", "") }, ,
		        { std::make_tuple("", frame), new ABlockID("", "") }, ,
		};
	}
}