namespace Androemda {
	class Block {

	private:
		/*
		Misc stuffs that get written to when we initialize the block instance
		bool randomlyTicks;
		Material *material;
		*/

		std::string id;

	public:

		// Static Stuff		
		static std::map</* AndromedaBlockID */ std::string, Block*> blockMap;

		static std::map<std::pair<PocketBlockID, PocketBlockMetadata>, ABlockID*> pocketBlockMap;
		static std::map<std::pair<JavaBlockID, JavaBlockMetadata>, ABlockID*> javaBlockMap;


		/**
		* Method of obtaining block
		* 
		* - in initblock.h we register Andromeda blocks and their 'variants'
		* - to obtain a block, use BlockFactory.cpp
		*/

		// ----------

		std::map<std::string, BlockVariant> block_variants;

		Block(std::string aBlockID, Material *block_material, float hardness, float blast_resistance, char light_level, int color = 0);

		// Block variant-related functions
		void registerVariant(std::string aBlockVariantID, JavaBlockID java_block_id, JavaBlockMetadata java_block_metadata, PocketBlockID pocket_block_id, PocketBlockMetadata pocket_block_metadata);

		BlockVariant* getVariantFromPocket(PocketBlockID block_id, PocketBlockMetadata block_metadata);

		BlockVariant* getVariantFromJava(JavaBlockID block_id, JavaBlockMetadata block_metatadata);

		void init();

		// Block Properties
		inline bool isSolid() { return this->material->IsSolid; }
		inline bool isTransparent() { return this->material->IsTransparent;	}
		inline bool isFlamable() { return this->material->IsFlamable; }
		inline bool isCollidable() { return this->material->BlocksMovement; }
		inline bool isReplacable() { return this->material->IsReplaceable; }

		virtual Color getMapColor();

		// Block Event Handlers

		virtual void onPlace(BlockVariant variant, BlockCoordinates *coords);
		inline void onPlace(std::string variant_name, BlockCoordinates *coords) {
			return this->onPlace(block_variants.at(variant_name), coords);
		}

		virtual void onBreak(BlockVariant variant, BlockCoordinates *coords);
		inline void onBreak(std::string variant_name, BlockCoordinates *coords) {
			return this->onBreak(block_variants.at(variant_name), coords);
		}

		virtual void onExploded(BlockVariant variant, BlockCoordinates *coords);
		inline void onExploded(std::string variant_name, BlockCoordinates *coords) {
			return this->onExploded(block_variants.at(variant_name), coords);
		}

		virtual void onMove(BlockVariant variant, BlockCoordinates *coords);
		inline void onMove(std::string variant_name, BlockCoordinates *coords) {
			return this->onMove(block_variants.at(variant_name), coords);
		}

		// Block Conditional Tests

		virtual bool canPlaceAt(BlockVariant variant, BlockCoordinates *coords);
		inline void canPlaceAt(std::string variant_name, BlockCoordinates *coords) {
			return this->canPlaceAt(block_variants.at(variant_name), coords);
		}

		virtual bool canPlaceOn(BlockVariant variant, Block *block);
		inline void canPlaceOn(std::string variant_name, BlockCoordinates *coords) {
			return this->canPlaceOn(block_variants.at(variant_name), coords);
		}

		virtual bool canBuildOver(BlockVariant variant, Block *block);
		inline void canBuildOver(std::string variant_name, BlockCoordinates *coords) {
			return this->canBuildOver(block_variants.at(variant_name), coords);
		}

		// Block Actions

		// Other Getters

		virtual float getGroundFriction();

		// Exporting Actions

		// Item NBT
		static bool toJavaNBT(Utils::BlockCoordinates*, nbt::tag_compound*);
		static bool toPocketNBT(Utils::BlockCoordinates*, nbt::tag_compound*);

		virtual bool toJavaNBT(std::string variant_id, nbt::tag_compound *tag);
		virtual bool toPocketNBT(std::string variant_id, nbt::tag_compound *tag);

		// char = blockdata mapped to a variant
		virtual bool toJavaNBTFromPocket(char pocket_meta, nbt::tag_compound* tag);
		virtual bool toJavaNBTFromJava(char java_meta, nbt::tag_compound* tag);
		virtual bool toPocketNBTFromPocket(char pocket_meta, nbt::tag_compound* tag);
		virtual bool toPocketNBTFromJava(char java_meta, nbt::tag_compound* tag);

		//virtual  std::pair<unsigned char /* id */, char /* meta */> getPocketBlock();
		//virtual  std::pair<std::string /* id */, char /* meta */> getJavaBlock();
		// getAnvilBlock();
	};
}