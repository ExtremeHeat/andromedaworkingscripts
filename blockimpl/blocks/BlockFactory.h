namespace Andromeda {
	class BlockFactory {

		static ABlock* getBlock(ABlockID andromeda_block_id);

		static ABlock* getBlockFromPocket(PocketBlockID block_id, PocketBlockMetadata block_metadata);

		static ABlock* getBlockFromJava(JavaBlockID block_id, JavaBlockMetadata block_metadata);
	}
}