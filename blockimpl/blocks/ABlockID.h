namespace Andromeda {
	struct ABlockID {
		std::string block_name;
		std::string block_variant;
	}
}