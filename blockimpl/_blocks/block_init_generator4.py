import datetime
import re

BLOCK_LIST_CSV = "./block_list_and_data2.txt"
BLOCK_VARIANT_CSV = "./blocks_with113.tsv"

def read_file(path):
	with open(path) as f:
   		data = f.read()
   	return data

andromeda_fq_blocks = []
header_block_defs = []
factory_defs = []
block_defs = []

def create_line(block_id, block_material, hardness, blast_resistance, light_level, block_type):
	fbid = block_id.replace("_", " ").title()

	if "," in block_type or "*" in block_type or "SPECIAL" in block_type:
		blocktype = "Block" + fbid.replace(" ", "")
	elif "DIRECTIONAL" in block_type:
		blocktype = "DirectionalBlock"
	elif "DOUBLE" in block_type:
		blocktype = "DoubleBlock"
	elif "PARTIAL" in block_type:
		blocktype = "PartialBlock"
	else:
		blocktype = 'Block'

	if str(light_level) == "":
		light_level = '0'

	if str(blast_resistance) == "":
		blast_resistance = "-1"

	if "." not in hardness:
		hardness += ".0"

	if "." not in blast_resistance:
		blast_resistance += ".0"

	header_block_defs.append('static {blocktype}* {blockid};'.format(blocktype = blocktype, blockid = block_id))
	factory_defs.append('{blocktype}* BlockFactory::{blockid};'.format(blocktype=blocktype, blockid=block_id))
	block_defs.append('{blocktype}* {blockid} = new {blocktype}("{blockid}", Materials::{material}, {hardness}f, {blastresistance}f, {lightlevel});'.format(blocktype = blocktype, blockid = block_id, material=block_material, hardness=hardness, blastresistance=blast_resistance, lightlevel=light_level))

block_list = []
def register_to_blockh(block_id):
	block_list.append('{"%s", %s},' % (block_id, block_id))

factory_list = []
factory_free_list = []
def register_to_factory(block_id):
	factory_list.append("BlockFactory::{block_id} = {block_id};".format(block_id=block_id))
	factory_free_list.append("BlockFactory::{block_id} = nullptr;".format(block_id=block_id))

variant_list = []
pocket_list_c = []
pocket_list = []
pocket_named_list_c = []
pocket_named_list = []
java_list_c = []
java_list = []
java_nominal_list = []
java_nstate_map = {}

def create_variant(andromeda_id, andromeda_variant, java_numerical_id, java_id, java_meta, java_nominal_id, pocket_name, pocket_id, pocket_meta):
	if "-" in java_meta:
		java_meta = "/*%s*/0" % java_meta
	if "-" in pocket_meta:
		pocket_meta = "/*%s*/0" % pocket_meta

	andromeda_block_index_id = len(andromeda_fq_blocks)
	andromeda_fq_blocks.append([andromeda_id, andromeda_variant, andromeda_block_index_id])
	variant_list.append('{andromedaid}->registerVariant(/* Andromeda Block ID Index */ {aBlockID}, /* VariantName */ "{aBlockVariantID}", /* JavaLegacyNumericalID */ {java_block_legacy_id}, /* JavaBlockID */ "{java_block_id}", /* JavaBlockMetadata */ {java_block_metadata}, /* JavaBlockNominalID */ "{java_block_nominal_id}", /* PocketBlockID */ {pocket_block_id}, /* PocketBlockName */ "{pocket_block_name}", /* PocketBlockMetadata */ {pocket_block_metadata});'.format(
		andromedaid = andromeda_id,
		aBlockID = andromeda_block_index_id,
		aBlockVariantID = andromeda_variant,
		java_block_legacy_id = java_numerical_id,
		java_block_id = java_id,
		java_block_metadata = java_meta,
		java_block_nominal_id = java_nominal_id.replace("*", ""),
		pocket_block_id = pocket_id,
		pocket_block_name = pocket_name,
		pocket_block_metadata = pocket_meta
	))
	#print ("%s,%s" % (pocket_id, pocket_meta.split("/")[-1]))
	if ("%s,%s" % (pocket_id, pocket_meta.split("/")[-1])) not in pocket_list_c:
		#print 'not in'
		pocket_list.append('{ std::make_pair(%s, %s), %d },' % (pocket_id, pocket_meta, andromeda_block_index_id))
	if ("%s,%s" % (pocket_name, pocket_meta.split("/")[-1])) not in pocket_named_list_c:
		pocket_named_list.append('{ std::make_pair("%s", %s), %d },' % (pocket_name, pocket_meta, andromeda_block_index_id))
	if ("%s,%s" % (java_id, java_meta.split("/")[-1])) not in java_list_c:
		java_list.append('{ std::make_pair("%s", %s), %d },' % (java_id, java_meta, andromeda_block_index_id))
	java_nominal_list.append('{ "%s", %d },' % (java_nominal_id, andromeda_block_index_id))
	
	s = java_nominal_id.split("[")
	statename = s[0]
	if len(s) > 1:
		p = s[1][:-1]
	else:
		p = None
		#print statename, andromeda_block_index_id
	if statename not in java_nstate_map:
		java_nstate_map[statename] = [[p, andromeda_block_index_id]]
	else:
		java_nstate_map[statename].append([p, andromeda_block_index_id])

	pocket_list_c.append("%s,%s" % (pocket_id, pocket_meta.split("/")[-1]))
	pocket_named_list_c.append("%s,%s" % (pocket_name, pocket_meta.split("/")[-1]))
	java_list_c.append("%s,%s" % (java_id, java_meta.split("/")[-1]))

with open(BLOCK_LIST_CSV) as f:
    blocklistcontent = f.readlines()

with open(BLOCK_VARIANT_CSV) as f:
    blockvariantcontent = f.readlines()

for line in blocklistcontent:
	if line.startswith("#"):
		continue
	s = line.split("\t")
	bID = s[0]
	bMaterial = s[1]
	bHardness = s[2]
	bBlastResistance = s[3]
	bLightLevel = s[4]
	bBlockType = s[5]

	create_line(bID, bMaterial, bHardness, bBlastResistance, bLightLevel, bBlockType)
	register_to_blockh(bID)
	register_to_factory(bID)

for line in blockvariantcontent:
	if line.startswith("#") or line.startswith("\t"):
		continue
	s = line.split("\t")
	andromedaId = s[0].replace("@", "").replace("&", "")
	andromedaVariant = s[1]
	javaBlockName = s[2].replace("*", "")
	javaBlockMetadata = s[3]
	pocketBlockId = s[4]
	pocketBlockMetadata = s[5] or "0"
	javaLegacyNumericalId = s[6]
	pocketBlockName = re.match('(\w+)', s[7]).group(1)
	javaNominalId = s[9].strip().replace("*", "").replace('"', '')

	create_variant(andromedaId, andromedaVariant, javaLegacyNumericalId, javaBlockName, javaBlockMetadata, javaNominalId, pocketBlockName, pocketBlockId, pocketBlockMetadata)

def do():
	tf = read_file("Templ_InitBlocks.cpp")

	generatedOn = datetime.datetime.utcnow()
	blockFactoryFwdDefs = ""
	blockInits = ""
	blockMap = ""
	blockVariantRegistration = ""
	blockFactoryDefs = ""
	blockFactoryFrees = ""
	javaBlockMap = ""
	pocketBlockMap = ""
	pocketNamedBlockMap = ""
	andromedaFqBlocks = ""
	javaBlockStatesMap = ""

	for factory_def in factory_defs:
		blockFactoryFwdDefs += (factory_def + "\n\t")

	for block_def in block_defs:
		blockInits += block_def + "\n\t"

	for block in block_list:
		blockMap += block + "\n\t"

	for variant in variant_list:
		blockVariantRegistration += variant + "\n\t"

	for factory in factory_list:
		blockFactoryDefs += factory + "\n\t"

	for factory in factory_free_list:
		blockFactoryFrees += factory + "\n\t"

	for java in java_list:
		javaBlockMap += java + "\n\t\t\t"

	for pocket in pocket_list:
		pocketBlockMap += pocket + "\n\t\t\t"

	for pocket in pocket_named_list:
		pocketNamedBlockMap += pocket + "\n\t\t\t"

	for andromeda_fq_block in andromeda_fq_blocks:
		andromedaFqBlocks += '\n\t\tmap[%d] = new ABlockID{"%s", "%s"};' % (andromeda_fq_block[2], andromeda_fq_block[0], andromeda_fq_block[1])

	for statename in java_nstate_map:
		javaBlockStatesMap += '\n\t\t{ "%s", {' % statename
		states = java_nstate_map[statename]
		for state in states:
			javaBlockStatesMap += ' { "%s", %s }, ' % (state[0] or "", state[1])
		javaBlockStatesMap += ' } },'

	print tf % {"GeneratedOn":generatedOn,"BlockFactoryFwdDefs":blockFactoryFwdDefs,
	"BlockInits": blockInits,"BlockMap": blockMap,"BlockVariantRegistration": blockVariantRegistration,
	"BlockFactoryDefs": blockFactoryDefs,"JavaBlockMap":javaBlockMap,"PocketBlockMap":pocketBlockMap,
	"PocketNamedBlockMap": pocketNamedBlockMap, "JavaNominalStateMap": javaBlockStatesMap,
	"BlockIdMap": andromedaFqBlocks, "BlockIdMapSize": len(andromeda_fq_blocks), "BlockFactoryFrees": blockFactoryFrees}

do()

print "/*HEADER DEFS for BlockFactory.h:"
for header_block_def in header_block_defs:
	print header_block_def
print "*/"