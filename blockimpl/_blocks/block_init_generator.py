BLOCK_LIST_CSV = "./block_list_and_data2.csv"
BLOCK_VARIANT_CSV = "./blocks.csv"

header_block_defs = []
factory_defs = []
def create_line(block_id, block_material, hardness, blast_resistance, light_level, block_type):


	fbid = block_id.replace("_", " ").title()

	if "," in block_type or "*" in block_type or "SPECIAL" in block_type:
		blocktype = "Blocks::Block" + fbid.replace(" ", "")
	elif "DIRECTIONAL" in block_type:
		blocktype = "DirectionalBlock"
	elif "DOUBLE" in block_type:
		blocktype = "DoubleBlock"
	elif "PARTIAL" in block_type:
		blocktype = "PartialBlock"
	else:
		blocktype = 'Block'

	if str(light_level) == "":
		light_level = '0'

	header_block_defs.append('static {blocktype}* {blockid} = nullptr;'.format(blocktype = blocktype, blockid = block_id))
	factory_defs.append('{blocktype}* BlockFactory::{blockid};'.format(blocktype=blocktype, blockid=block_id))
	print '{blocktype}* {blockid} = new {blocktype}("{blockid}", Materials::{material}, {hardness}, {blastresistance}, {lightlevel});'.format(blocktype = blocktype, blockid = block_id, material=block_material, hardness=hardness, blastresistance=blast_resistance, lightlevel=light_level)


block_list = []
def register_to_blockh(block_id):
	block_list.append('{"%s", %s}' % (block_id, block_id))

factory_list = []
def register_to_factory(block_id):
	factory_list.append("BlockFactory::{block_id} = {block_id};".format(block_id=block_id))

pocket_list = []
java_list = []
java_nominal_list = []

def create_variant(andromeda_id, andromeda_variant, java_id, java_meta, java_nominal_id, pocket_id, pocket_meta):
	print '{andromedaid}->registerVariant(/* VariantName */ "{aBlockVariantID}", /* JavaBlockID */ "{java_block_id}", /* JavaBlockMetadata */ {java_block_metadata}, /* JavaBlockNominalID */ "{java_block_nominal_id}",/* PocketBlockID */ {pocket_block_id}, /* PocketBlockMetadata */ {pocket_block_metadata});'.format(
		andromedaid = andromeda_id,
		aBlockVariantID = andromeda_variant,
		java_block_id = java_id,
		java_block_metadata = java_meta,
		java_block_nominal_id = java_nominal_id,
		pocket_block_id = pocket_id,
		pocket_block_metadata = pocket_meta
	)
	pocket_list.append('{ std::make_pair(%s, %s), new ABlockID{"%s", "%s"} },' % (pocket_id, pocket_meta, andromeda_id, andromeda_variant))
	java_list.append('{ std::make_pair("%s", %s), new ABlockID{"%s", "%s"} },' % (java_id, java_meta, andromeda_id, andromeda_variant))
	java_nominal_list.append('{ "%s", new ABlockID{"%s", "%s"}}' % (java_nominal_id, andromeda_id, andromeda_variant))

#def add_to_pocket_list(pocket_id, ):


with open(BLOCK_LIST_CSV) as f:
    blocklistcontent = f.readlines()

with open(BLOCK_VARIANT_CSV) as f:
    blockvariantcontent = f.readlines()

for line in blocklistcontent:
	if line.startswith("#"):
		continue
	s = line.split(",")
	bID = s[0]
	bMaterial = s[1]
	bHardness = s[2]
	bBlastResistance = s[3]
	bLightLevel = s[4]
	bBlockType = s[5]

	create_line(bID, bMaterial, bHardness, bBlastResistance, bLightLevel, bBlockType)
	register_to_blockh(bID)
	register_to_factory(bID)

print "std::map<std::string, Block*> lBlockMap = {"

for block_line in block_list:
	print "\t", block_line, ","

print "};"

for i in factory_list:
	print "\t", i

for line in blockvariantcontent:
	if line.startswith("#"):
		continue
	s = line.split(",")
	andromedaId = s[0]
	andromedaVariant = s[1]
	javaBlockName = s[2]
	javaBlockMetadata = s[3]
	pocketBlockId = s[4]
	pocketBlockMetadata = s[5]
	javaNominalId = s[-1].strip()

	create_variant(andromedaId, andromedaVariant, javaBlockName, javaBlockMetadata, javaNominalId, pocketBlockId, pocketBlockMetadata)

	#add_to_pocket_list(pocketBlockId, andromedaId)

print "std::map<std::pair<PocketBlockID, PocketBlockMetadata>, ABlockID*> lPocketBlockMap = {"

for block_line in pocket_list:
	print "\t", block_line

print "};"

print "std::map<std::pair<JavaBlockID, JavaBlockMetadata>, ABlockID*> lJavaBlockMap = {"

for block_line in java_list:
	print "\t", block_line

print "};"

print "std::map<std::pair<JavaBlockID, JavaBlockMetadata>, ABlockID*> lJavaNominalBlockMap = {"
for block_line in java_nominal_list:
	print "\t", block_line

print "};"

for header_block_def in header_block_defs:
	print header_block_def

for factory_def in factory_defs:
	print factory_def