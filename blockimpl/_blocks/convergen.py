s = """white_shulker_box	0	218	0	219	shulker_box	SPECIAL
orange_shulker_box	0	218	1	220	shulker_box	SPECIAL
magenta_shulker_box	0	218	2	221	shulker_box	SPECIAL
light_blue_shulker_box	0	218	3	222	shulker_box	SPECIAL
yellow_shulker_box	0	218	4	223	shulker_box	SPECIAL
lime_shulker_box	0	218	5	224	shulker_box	SPECIAL
pink_shulker_box	0	218	6	225	shulker_box	SPECIAL
gray_shulker_box	0	218	7	226	shulker_box	SPECIAL
silver_shulker_box	0	218	8	227	shulker_box	SPECIAL
cyan_shulker_box	0	218	9	228	shulker_box	SPECIAL
purple_shulker_box	0	218	10	229	shulker_box	SPECIAL
blue_shulker_box	0	218	11	230	shulker_box	SPECIAL
brown_shulker_box	0	218	12	231	shulker_box	SPECIAL
green_shulker_box	0	218	13	232	shulker_box	SPECIAL
red_shulker_box	0	218	14	233	shulker_box	SPECIAL
black_shulker_box	0	218	15	234	shulker_box	SPECIAL"""

s2 = """whiteS	white_glazed_terracotta	0	220	3	235	white_glazed_terracotta         	DIRECTIONAL
whiteW	white_glazed_terracotta	1	220	4	235	white_glazed_terracotta         	DIRECTIONAL
whiteN	white_glazed_terracotta	2	220	2	235	white_glazed_terracotta         	DIRECTIONAL
whiteE	white_glazed_terracotta	3	220	5	235	white_glazed_terracotta         	DIRECTIONAL
orangeS	orange_glazed_terracotta	0	221	3	236	orange_glazed_terracotta        	DIRECTIONAL
orangeW	orange_glazed_terracotta	1	221	4	236	orange_glazed_terracotta        	DIRECTIONAL
orangeN	orange_glazed_terracotta	2	221	2	236	orange_glazed_terracotta        	DIRECTIONAL
orangeE	orange_glazed_terracotta	3	221	5	236	orange_glazed_terracotta        	DIRECTIONAL
magentaS	magenta_glazed_terracotta	0	222	3	237	magenta_glazed_terracotta       	DIRECTIONAL
magentaW	magenta_glazed_terracotta	1	222	4	237	magenta_glazed_terracotta       	DIRECTIONAL
magentaN	magenta_glazed_terracotta	2	222	2	237	magenta_glazed_terracotta       	DIRECTIONAL
magentaE	magenta_glazed_terracotta	3	222	5	237	magenta_glazed_terracotta       	DIRECTIONAL
lightBlueS	light_blue_glazed_terracotta	0	223	3	238	light_blue_glazed_terracotta    	DIRECTIONAL
lightBlueW	light_blue_glazed_terracotta	1	223	4	238	light_blue_glazed_terracotta    	DIRECTIONAL
lightBlueN	light_blue_glazed_terracotta	2	223	2	238	light_blue_glazed_terracotta    	DIRECTIONAL
lightBlueE	light_blue_glazed_terracotta	3	223	5	238	light_blue_glazed_terracotta    	DIRECTIONAL
yellowS	yellow_glazed_terracotta	0	224	3	239	yellow_glazed_terracotta        	DIRECTIONAL
yellowW	yellow_glazed_terracotta	1	224	4	239	yellow_glazed_terracotta        	DIRECTIONAL
yellowN	yellow_glazed_terracotta	2	224	2	239	yellow_glazed_terracotta        	DIRECTIONAL
yellowE	yellow_glazed_terracotta	3	224	5	239	yellow_glazed_terracotta        	DIRECTIONAL
limeS	lime_glazed_terracotta	0	225	3	240	lime_glazed_terracotta          	DIRECTIONAL
limeW	lime_glazed_terracotta	1	225	4	240	lime_glazed_terracotta          	DIRECTIONAL
limeN	lime_glazed_terracotta	2	225	2	240	lime_glazed_terracotta          	DIRECTIONAL
limeE	lime_glazed_terracotta	3	225	5	240	lime_glazed_terracotta          	DIRECTIONAL
pinkS	pink_glazed_terracotta	0	226	3	241	pink_glazed_terracotta          	DIRECTIONAL
pinkW	pink_glazed_terracotta	1	226	4	241	pink_glazed_terracotta          	DIRECTIONAL
pinkN	pink_glazed_terracotta	2	226	2	241	pink_glazed_terracotta          	DIRECTIONAL
pinkE	pink_glazed_terracotta	3	226	5	241	pink_glazed_terracotta          	DIRECTIONAL
grayS	gray_glazed_terracotta	0	227	3	242	gray_glazed_terracotta          	DIRECTIONAL
grayW	gray_glazed_terracotta	1	227	4	242	gray_glazed_terracotta          	DIRECTIONAL
grayN	gray_glazed_terracotta	2	227	2	242	gray_glazed_terracotta          	DIRECTIONAL
grayE	gray_glazed_terracotta	3	227	5	242	gray_glazed_terracotta          	DIRECTIONAL
silverS	light_gray_glazed_terracotta	0	228	3	243	silver_glazed_terracotta        	DIRECTIONAL
silverW	light_gray_glazed_terracotta	1	228	4	243	silver_glazed_terracotta        	DIRECTIONAL
silverN	light_gray_glazed_terracotta	2	228	2	243	silver_glazed_terracotta        	DIRECTIONAL
silverE	light_gray_glazed_terracotta	3	228	5	243	silver_glazed_terracotta        	DIRECTIONAL
cyanS	cyan_glazed_terracotta	0	229	3	244	cyan_glazed_terracotta          	DIRECTIONAL
cyanW	cyan_glazed_terracotta	1	229	4	244	cyan_glazed_terracotta          	DIRECTIONAL
cyanN	cyan_glazed_terracotta	2	229	2	244	cyan_glazed_terracotta          	DIRECTIONAL
cyanE	cyan_glazed_terracotta	3	229	5	244	cyan_glazed_terracotta          	DIRECTIONAL
purpleS	purple_glazed_terracotta	0	219	3	245	purple_glazed_terracotta        	DIRECTIONAL
purpleW	purple_glazed_terracotta	1	219	4	245	purple_glazed_terracotta        	DIRECTIONAL
purpleN	purple_glazed_terracotta	2	219	2	245	purple_glazed_terracotta        	DIRECTIONAL
purpleE	purple_glazed_terracotta	3	219	5	245	purple_glazed_terracotta        	DIRECTIONAL
blueS	blue_glazed_terracotta	0	231	3	246	blue_glazed_terracotta          	DIRECTIONAL
blueW	blue_glazed_terracotta	1	231	4	246	blue_glazed_terracotta          	DIRECTIONAL
blueN	blue_glazed_terracotta	2	231	2	246	blue_glazed_terracotta          	DIRECTIONAL
blueE	blue_glazed_terracotta	3	231	5	246	blue_glazed_terracotta          	DIRECTIONAL
brownS	brown_glazed_terracotta	0	232	3	247	brown_glazed_terracotta         	DIRECTIONAL
brownW	brown_glazed_terracotta	1	232	4	247	brown_glazed_terracotta         	DIRECTIONAL
brownN	brown_glazed_terracotta	2	232	2	247	brown_glazed_terracotta         	DIRECTIONAL
brownE	brown_glazed_terracotta	3	232	5	247	brown_glazed_terracotta         	DIRECTIONAL
greenS	green_glazed_terracotta	0	233	3	248	green_glazed_terracotta         	DIRECTIONAL
greenW	green_glazed_terracotta	1	233	4	248	green_glazed_terracotta         	DIRECTIONAL
greenN	green_glazed_terracotta	2	233	2	248	green_glazed_terracotta         	DIRECTIONAL
greenE	green_glazed_terracotta	3	233	5	248	green_glazed_terracotta         	DIRECTIONAL
redS	red_glazed_terracotta	0	234	3	249	red_glazed_terracotta           	DIRECTIONAL
redW	red_glazed_terracotta	1	234	4	249	red_glazed_terracotta           	DIRECTIONAL
redN	red_glazed_terracotta	2	234	2	249	red_glazed_terracotta           	DIRECTIONAL
redE	red_glazed_terracotta	3	234	5	249	red_glazed_terracotta           	DIRECTIONAL
blackS	black_glazed_terracotta	0	235	3	250	black_glazed_terracotta         	DIRECTIONAL
blackW	black_glazed_terracotta	1	235	4	250	black_glazed_terracotta         	DIRECTIONAL
blackN	black_glazed_terracotta	2	235	2	250	black_glazed_terracotta         	DIRECTIONAL
blackE	black_glazed_terracotta	3	235	5	250	black_glazed_terracotta         	DIRECTIONAL"""

s3 = """white	concrete	0	236	0	251	concrete	
orange	concrete	1	236	1	251	concrete	
magenta	concrete	2	236	2	251	concrete	
lightBlue	concrete	3	236	3	251	concrete	
yellow	concrete	4	236	4	251	concrete	
lime	concrete	5	236	5	251	concrete	
pink	concrete	6	236	6	251	concrete	
gray	concrete	7	236	7	251	concrete	
silver	concrete	8	236	8	251	concrete	
cyan	concrete	9	236	9	251	concrete	
purple	concrete	10	236	10	251	concrete	
blue	concrete	11	236	11	251	concrete	
brown	concrete	12	236	12	251	concrete	
green	concrete	13	236	13	251	concrete	
red	concrete	14	236	14	251	concrete	
black	concrete	15	236	15	251	concrete	
white	concrete_powder	0	237	0	252	concretePowder	
orange	concrete_powder	1	237	1	252	concretePowder	
magenta	concrete_powder	2	237	2	252	concretePowder	
lightBlue	concrete_powder	3	237	3	252	concretePowder	
yellow	concrete_powder	4	237	4	252	concretePowder	
lime	concrete_powder	5	237	5	252	concretePowder	
pink	concrete_powder	6	237	6	252	concretePowder	
gray	concrete_powder	7	237	7	252	concretePowder	
silver	concrete_powder	8	237	8	252	concretePowder	
cyan	concrete_powder	9	237	9	252	concretePowder	
purple	concrete_powder	10	237	10	252	concretePowder	
blue	concrete_powder	11	237	11	252	concretePowder	
brown	concrete_powder	12	237	12	252	concretePowder	
green	concrete_powder	13	237	13	252	concretePowder	
red	concrete_powder	14	237	14	252	concretePowder	
black	concrete_powder	15	237	15	252	concretePowder	"""

convlJavaToPocketBlockIdAndMeta = "case %s: // %s => %s$%s\n\toutBlock = %s;\n\toutBlockData.set(XYZ, %s);\n\tbreak;"
convlJavaToPocketSwitchBlockIdOnlyNoBreak = "case %s: // %s\n\toutBlock = %s;"
convlJavaToPocketSwitchBlockIdAndMeta = "case %s:\n\toutBlock = %s;\n\toutBlockData.set(XYZ, %s); // %s\n\tbreak;"
convlJavaToPocketSwitchMetaOnly = "case %s:\n\toutBlockData.set(XYZ, %s); // %s\n\tbreak;"

convlPocketToJava = ""
convlPocketToJavaSwitchBlockIdOnly = "case %s:\n\toutBlock = %s; // %s\n\tbreak;"
convlPocketToJavaSwitchMetaOnly = "case %s:\n\toutBlockData.set(XYZ, %s); // %s\n\tbreak;"
convlPocketToJavaSwitchBlockIdAndMeta = "case %s:\n\toutBlock = %s;\n\toutBlockData.set(XYZ, %s); // %s\n\tbreak;"

lSwitchMetaOnly = "case %s:\n\toutBlockData.set(XYZ, %s); // %s\n\tbreak;"
lSwitchBlockIdOnly = "case %s: // %s\n\toutBlock = %s; // %s\n\tbreak;"
lSwitchBlockIdOnlyNoBreak = "case %s: // %s\n\toutBlock = %s;"


def do1():
	for line in s.split("\n"):
		#print line.split("\t")
		java_name, java_meta, pocket_id, pocket_meta, java_id, pocket_name, notes = line.split("\t")

		#print convlJavaToPocket % (java_id, java_name, pocket_name, pocket_meta, pocket_id, pocket_meta)

		print convlPocketToJavaSwitchBlockIdOnly % (pocket_meta, java_id, java_name)

def do2_JavaToPocket(): # Java To Pocket
	for line in s2.split("\n"):
		aname, java_name, java_meta, pocket_id, pocket_meta, java_id, pocket_name, notes = line.split("\t")

		if java_meta == '0':
			print lSwitchBlockIdOnlyNoBreak % (java_id, java_name, pocket_id)
			print "\tswitch (blockData.get(i)) {"
		
		print "\t", lSwitchMetaOnly % (java_meta, pocket_meta, aname)

		if java_meta == '3':
			print "\t}\n\tbreak;"
		
def do2_PocketToJava():
	for line in s2.split("\n"):
		aname, java_name, java_meta, pocket_id, pocket_meta, java_id, pocket_name, notes = line.split("\t")

		if java_meta == '0':
			print lSwitchBlockIdOnlyNoBreak % (pocket_id, pocket_name, java_id)
			print "\tswitch (blockData.get(i)) {"

		print "\t", lSwitchMetaOnly % (pocket_meta, java_meta, aname)

		if java_meta == '3':
			print "\t}\n\tbreak;"

def do3():
	for line in s3.split("\n"):
		aname, java_name, java_meta, pocket_id, pocket_meta, java_id, pocket_name, notes = line.split("\t")

		if java_meta == '0':
			# java to pocket
			#print lSwitchBlockIdOnly % (java_id, java_name, pocket_id, pocket_name)

			# pocket to java
			print lSwitchBlockIdOnly % (pocket_id, pocket_name, java_id, java_name)

do3()
