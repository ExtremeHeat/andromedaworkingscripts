// Copyright (c) 2018 extremeheat. All rights reserved.

#include "stdafx.h"

#include "InitBlocks.h"

// This file was automatically generated. Do not make substantial manual modifications without updating the script.
// File generated on %(GeneratedOn)s

namespace Andromeda {

	using namespace Blocks;

#pragma region "Block Factory Forward Defs"
	%(BlockFactoryFwdDefs)s
#pragma endregion

	void createBlockMap(std::map<std::string /* Andromeda Block Name */, Block*> &map) {
#pragma	region "Block Inits"
	%(BlockInits)s
#pragma	endregion
#pragma	region "Block Map"
	map = {
	%(BlockMap)s
	};
#pragma	endregion
#pragma region "Variant Registration"
	%(BlockVariantRegistration)s
#pragma endregion
#pragma region "Block Factory Definitions"
	%(BlockFactoryDefs)s
#pragma endregion
		return;
	}

	void destroyBlockMap(std::map<std::string /* Andromeda Block Name */, Block*> &map) {
		for (auto i : map) {
			delete i.second;
			i.second = nullptr;
		}
	%(BlockFactoryFrees)s
		return;
	}

	void createBlockIdMap(ABlockID *map[], int len) {
	%(BlockIdMap)s
		len = %(BlockIdMapSize)d;
	}

	void destroyBlockIdMap(ABlockID* map[], int len) {
		for (int i = 0; i < len; i++) {
			delete map[i];
			map[i] = nullptr;
		}
	}

	void createJavaBlockMap(std::map<std::pair<JavaBlockID, JavaBlockMetadata>, ABlockIDIndex> &map) {
		map = {
			%(JavaBlockMap)s
		};
		return;
	}

	void createJavaBlockMap(std::map<std::string /* JavaNominalIDName */, std::vector<std::pair<std::string /* BlockState */, ABlockIDIndex>>> &map) {
		map = {
			%(JavaNominalStateMap)s
		};
	}

	void createPocketBlockMap(std::map<std::pair<PocketBlockID, PocketBlockMetadata>, ABlockIDIndex> &map) {
		map = {
			%(PocketBlockMap)s
		};
		return;
	}

	void createPocketBlockMap(std::map<std::pair<PocketBlockName, PocketBlockMetadata>, ABlockIDIndex> &map) {
		map = {
			%(PocketNamedBlockMap)s
		};
		return;
	}
}