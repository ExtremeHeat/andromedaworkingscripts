#pragma once

#include <map>
#include <tuple>

#include "Types.h"

// This file was automatically generated

namespace Andromeda {
namespace Items {
const UItemName IronShovel = "iron_shovel";
const UItemName IronPickaxe = "iron_pickaxe";
const UItemName IronAxe = "iron_axe";
const UItemName FlintAndSteel = "flint_and_steel";
const UItemName Apple = "apple";
const UItemName Bow = "bow";
const UItemName Arrow = "arrow";
const UItemName Coal_ = "coal" USES METADATA;
const UItemName Diamond = "diamond";
const UItemName IronIngot = "iron_ingot";
const UItemName GoldIngot = "gold_ingot";
const UItemName IronSword = "iron_sword";
const UItemName WoodenSword = "wooden_sword";
const UItemName WoodenShovel = "wooden_shovel";
const UItemName WoodenPickaxe = "wooden_pickaxe";
const UItemName WoodenAxe = "wooden_axe";
const UItemName StoneSword = "stone_sword";
const UItemName StoneShovel = "stone_shovel";
const UItemName StonePickaxe = "stone_pickaxe";
const UItemName StoneAxe = "stone_axe";
const UItemName DiamondSword = "diamond_sword";
const UItemName DiamondShovel = "diamond_shovel";
const UItemName DiamondPickaxe = "diamond_pickaxe";
const UItemName DiamondAxe = "diamond_axe";
const UItemName Stick = "stick";
const UItemName Bowl = "bowl";
const UItemName MushroomStew = "mushroom_stew";
const UItemName GoldenSword = "golden_sword";
const UItemName GoldenShovel = "golden_shovel";
const UItemName GoldenPickaxe = "golden_pickaxe";
const UItemName GoldenAxe = "golden_axe";
const UItemName String = "string";
const UItemName Feather = "feather";
const UItemName Gunpowder = "gunpowder";
const UItemName WoodenHoe = "wooden_hoe";
const UItemName StoneHoe = "stone_hoe";
const UItemName IronHoe = "iron_hoe";
const UItemName DiamondHoe = "diamond_hoe";
const UItemName GoldenHoe = "golden_hoe";
const UItemName WheatSeeds = "wheat_seeds";
const UItemName Wheat = "wheat";
const UItemName Bread = "bread";
const UItemName LeatherHelmet = "leather_helmet";
const UItemName LeatherChestplate = "leather_chestplate";
const UItemName LeatherLeggings = "leather_leggings";
const UItemName LeatherBoots = "leather_boots";
const UItemName ChainmailHelmet = "chainmail_helmet";
const UItemName ChainmailChestplate = "chainmail_chestplate";
const UItemName ChainmailLeggings = "chainmail_leggings";
const UItemName ChainmailBoots = "chainmail_boots";
const UItemName IronHelmet = "iron_helmet";
const UItemName IronChestplate = "iron_chestplate";
const UItemName IronLeggings = "iron_leggings";
const UItemName IronBoots = "iron_boots";
const UItemName DiamondHelmet = "diamond_helmet";
const UItemName DiamondChestplate = "diamond_chestplate";
const UItemName DiamondLeggings = "diamond_leggings";
const UItemName DiamondBoots = "diamond_boots";
const UItemName GoldenHelmet = "golden_helmet";
const UItemName GoldenChestplate = "golden_chestplate";
const UItemName GoldenLeggings = "golden_leggings";
const UItemName GoldenBoots = "golden_boots";
const UItemName Flint = "flint";
const UItemName Porkchop = "porkchop";
const UItemName CookedPorkchop = "cooked_porkchop";
const UItemName Painting = "painting";
const UItemName GoldenApple_ = "golden_apple" USES METADATA;
const UItemName Sign = "sign";
const UItemName WoodenDoor = "wooden_door";
const UItemName Bucket = "bucket";
const UItemName WaterBucket = DO THIS MANUALLY;
const UItemName LavaBucket = DO THIS MANUALLY;
const UItemName Minecart = "minecart";
const UItemName Saddle = "saddle";
const UItemName IronDoor = "iron_door";
const UItemName Redstone = "redstone";
const UItemName Snowball = "snowball";
const UItemName Boat = "boat";
const UItemName Leather = "leather";
const UItemName MilkBucket = DO THIS MANUALLY;
const UItemName Brick = "brick";
const UItemName ClayBall = "clay_ball";
const UItemName Reeds = "reeds";
const UItemName Paper = "paper";
const UItemName Book = "book";
const UItemName SlimeBall = "slime_ball";
const UItemName ChestMinecart = "chest_minecart";
const UItemName FurnaceMinecart = DO THIS MANUALLY;
const UItemName Egg = "egg";
const UItemName Compass = "compass";
const UItemName FishingRod = "fishing_rod";
const UItemName Clock = "clock";
const UItemName GlowstoneDust = "glowstone_dust";
const UItemName Fish_ = "fish" USES METADATA;
const UItemName CookedFish_ = "cooked_fish" USES METADATA;
const UItemName Dye_ = "dye" USES METADATA;
const UItemName Bone = "bone";
const UItemName Sugar = "sugar";
const UItemName Cake = "cake";
const UItemName Bed = "bed";
const UItemName Repeater = "repeater";
const UItemName Cookie = "cookie";
const UItemName FilledMap_ = USES METADATA;
const UItemName Shears = "shears";
const UItemName Melon = "melon";
const UItemName PumpkinSeeds = "pumpkin_seeds";
const UItemName MelonSeeds = "melon_seeds";
const UItemName Beef = "beef";
const UItemName CookedBeef = "cooked_beef";
const UItemName Chicken = "chicken";
const UItemName CookedChicken = "cooked_chicken";
const UItemName RottenFlesh = "rotten_flesh";
const UItemName EnderPearl = "ender_pearl";
const UItemName BlazeRod = "blaze_rod";
const UItemName GhastTear = "ghast_tear";
const UItemName GoldNugget = "gold_nugget";
const UItemName NetherWart = "nether_wart";
const UItemName Potion_ = "potion" USES METADATA;
const UItemName GlassBottle = "glass_bottle";
const UItemName SpiderEye = "spider_eye";
const UItemName FermentedSpiderEye = "fermented_spider_eye";
const UItemName BlazePowder = "blaze_powder";
const UItemName MagmaCream = "magma_cream";
const UItemName BrewingStand = "brewing_stand";
const UItemName Cauldron = "cauldron";
const UItemName EnderEye = "ender_eye";
const UItemName SpeckledMelon = "speckled_melon";
const UItemName SpawnEgg_ = "spawn_egg" USES METADATA;
const UItemName ExperienceBottle = "experience_bottle";
const UItemName FireCharge = DO THIS MANUALLY;
const UItemName WritableBook = DO THIS MANUALLY;
const UItemName WrittenBook = DO THIS MANUALLY;
const UItemName Emerald = "emerald";
const UItemName ItemFrame = DO THIS MANUALLY;
const UItemName FlowerPot = "flower_pot";
const UItemName Carrot = "carrot";
const UItemName Potato = "potato";
const UItemName BakedPotato = "baked_potato";
const UItemName PoisonousPotato = "poisonous_potato";
const UItemName Map = DO THIS MANUALLY;
const UItemName GoldenCarrot = "golden_carrot";
const UItemName Skull_ = "skull" USES METADATA;
const UItemName CarrotOnAStick = DO THIS MANUALLY;
const UItemName NetherStar = DO THIS MANUALLY;
const UItemName PumpkinPie = "pumpkin_pie";
const UItemName Fireworks = DO THIS MANUALLY;
const UItemName FireworkCharge = DO THIS MANUALLY;
const UItemName EnchantedBook = "enchanted_book";
const UItemName Comparator = "comparator";
const UItemName Netherbrick = "netherbrick";
const UItemName Quartz = "quartz";
const UItemName TntMinecart = "tnt_minecart";
const UItemName HopperMinecart = "hopper_minecart";
const UItemName PrismarineShard = "prismarine_shard";
const UItemName PrismarineCrystals = "prismarine_crystals";
const UItemName Rabbit = "rabbit";
const UItemName CookedRabbit = "cooked_rabbit";
const UItemName RabbitStew = "rabbit_stew";
const UItemName RabbitFoot = "rabbit_foot";
const UItemName RabbitHide = "rabbit_hide";
const UItemName ArmorStand = DO THIS MANUALLY;
const UItemName IronHorseArmor = DO THIS MANUALLY;
const UItemName GoldenHorseArmor = DO THIS MANUALLY;
const UItemName DiamondHorseArmor = DO THIS MANUALLY;
const UItemName Lead = "lead";
const UItemName NameTag = DO THIS MANUALLY;
const UItemName CommandBlockMinecart = DO THIS MANUALLY;
const UItemName Mutton = DO THIS MANUALLY;
const UItemName CookedMutton = DO THIS MANUALLY;
const UItemName Banner = DO THIS MANUALLY;
const UItemName EndCrystal = "end_crystal";
const UItemName SpruceDoor = "spruce_door";
const UItemName BirchDoor = "birch_door";
const UItemName JungleDoor = "jungle_door";
const UItemName AcaciaDoor = "acacia_door";
const UItemName DarkOakDoor = "dark_oak_door";
const UItemName ChorusFruit = "chorus_fruit";
const UItemName ChorusFruitPopped = "chorus_fruit_popped";
const UItemName Beetroot = "beetroot";
const UItemName BeetrootSeeds = "beetroot_seeds";
const UItemName BeetrootSoup = "beetroot_soup";
const UItemName DragonBreath = "dragon_breath";
const UItemName SplashPotion = "splash_potion";
const UItemName SpectralArrow = DO THIS MANUALLY;
const UItemName TippedArrow = DO THIS MANUALLY;
const UItemName LingeringPotion = "lingering_potion";
const UItemName Shield = DO THIS MANUALLY;
const UItemName Elytra = "elytra";
const UItemName SpruceBoat = DO THIS MANUALLY;
const UItemName BirchBoat = DO THIS MANUALLY;
const UItemName JungleBoat = DO THIS MANUALLY;
const UItemName AcaciaBoat = DO THIS MANUALLY;
const UItemName DarkOakBoat = DO THIS MANUALLY;
const UItemName TotemOfUndying = DO THIS MANUALLY;
const UItemName ShulkerShell = "shulker_shell";
// skipped  451	1C3		(unused)
const UItemName IronNugget = DO THIS MANUALLY;
const UItemName Record13 = DO THIS MANUALLY;
const UItemName RecordCat = DO THIS MANUALLY;
const UItemName RecordBlocks = DO THIS MANUALLY;
const UItemName RecordChirp = DO THIS MANUALLY;
const UItemName RecordFar = DO THIS MANUALLY;
const UItemName RecordMall = DO THIS MANUALLY;
const UItemName RecordMellohi = DO THIS MANUALLY;
const UItemName RecordStal = DO THIS MANUALLY;
const UItemName RecordStrad = DO THIS MANUALLY;
const UItemName RecordWard = DO THIS MANUALLY;
const UItemName Record11 = DO THIS MANUALLY;
const UItemName RecordWait = DO THIS MANUALLY;
} // NS Andromeda::Items
const std::map<UItemName, UItemID> = {
     {Items::IronShovel, (16777472 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::IronPickaxe, (16843009 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::IronAxe, (16908546 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::FlintAndSteel, (16974083 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Apple, (17039620 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Bow, (17105157 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Arrow, (17170694 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Coal_, (17236231 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
     {Items::Diamond, (17301768 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::IronIngot, (17367305 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::GoldIngot, (17432842 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::IronSword, (17498379 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::WoodenSword, (17563916 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::WoodenShovel, (17629453 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::WoodenPickaxe, (17694990 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::WoodenAxe, (17760527 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::StoneSword, (17826064 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::StoneShovel, (17891601 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::StonePickaxe, (17957138 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::StoneAxe, (18022675 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::DiamondSword, (18088212 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::DiamondShovel, (18153749 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::DiamondPickaxe, (18219286 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::DiamondAxe, (18284823 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Stick, (18350360 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Bowl, (18415897 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::MushroomStew, (18481434 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::GoldenSword, (18546971 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::GoldenShovel, (18612508 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::GoldenPickaxe, (18678045 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::GoldenAxe, (18743582 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::String, (18809119 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Feather, (18874656 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Gunpowder, (18940193 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::WoodenHoe, (19005730 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::StoneHoe, (19071267 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::IronHoe, (19136804 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::DiamondHoe, (19202341 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::GoldenHoe, (19267878 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::WheatSeeds, (19333415 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Wheat, (19398952 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Bread, (19464489 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::LeatherHelmet, (19530026 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::LeatherChestplate, (19595563 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::LeatherLeggings, (19661100 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::LeatherBoots, (19726637 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::ChainmailHelmet, (19792174 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::ChainmailChestplate, (19857711 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::ChainmailLeggings, (19923248 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::ChainmailBoots, (19988785 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::IronHelmet, (20054322 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::IronChestplate, (20119859 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::IronLeggings, (20185396 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::IronBoots, (20250933 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::DiamondHelmet, (20316470 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::DiamondChestplate, (20382007 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::DiamondLeggings, (20447544 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::DiamondBoots, (20513081 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::GoldenHelmet, (20578618 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::GoldenChestplate, (20644155 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::GoldenLeggings, (20709692 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::GoldenBoots, (20775229 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Flint, (20840766 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Porkchop, (20906303 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::CookedPorkchop, (20971840 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Painting, (21037377 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::GoldenApple_, (21102914 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
     {Items::Sign, (21168451 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::WoodenDoor, (21233988 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Bucket, (21299525 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Minecart, (21496136 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Saddle, (21561673 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::IronDoor, (21627210 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Redstone, (21692747 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Snowball, (21758284 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Boat, (21823821 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Leather, (21889358 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Brick, (22020432 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::ClayBall, (22085969 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Reeds, (22151506 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Paper, (22217043 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Book, (22282580 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::SlimeBall, (22348117 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::ChestMinecart, (22413654 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Egg, (22544728 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Compass, (22610265 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::FishingRod, (22675802 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Clock, (22741339 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::GlowstoneDust, (22806876 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Fish_, (22872413 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
     {Items::CookedFish_, (22937950 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
     {Items::Dye_, (23003487 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
     {Items::Bone, (23069024 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Sugar, (23134561 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Cake, (23200098 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Bed, (23265635 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Repeater, (23331172 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Cookie, (23396709 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Shears, (23527783 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Melon, (23593320 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::PumpkinSeeds, (23658857 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::MelonSeeds, (23724394 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Beef, (23789931 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::CookedBeef, (23855468 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Chicken, (23921005 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::CookedChicken, (23986542 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::RottenFlesh, (24052079 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::EnderPearl, (24117616 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::BlazeRod, (24183153 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::GhastTear, (24248690 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::GoldNugget, (24314227 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::NetherWart, (24379764 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Potion_, (24445301 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
     {Items::GlassBottle, (24510838 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::SpiderEye, (24576375 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::FermentedSpiderEye, (24641912 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::BlazePowder, (24707449 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::MagmaCream, (24772986 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::BrewingStand, (24838523 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Cauldron, (24904060 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::EnderEye, (24969597 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::SpeckledMelon, (25035134 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::SpawnEgg_, (25100671 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
     {Items::ExperienceBottle, (25166208 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Emerald, (25428356 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::FlowerPot, (25559430 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Carrot, (25624967 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Potato, (25690504 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::BakedPotato, (25756041 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::PoisonousPotato, (25821578 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::GoldenCarrot, (25952652 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Skull_, (26018189 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
     {Items::PumpkinPie, (26214800 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::EnchantedBook, (26411411 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Comparator, (26476948 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Netherbrick, (26542485 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Quartz, (26608022 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::TntMinecart, (26673559 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::HopperMinecart, (26739096 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::PrismarineShard, (26804633 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::PrismarineCrystals, (26870182 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Rabbit, (26935707 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::CookedRabbit, (27001244 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::RabbitStew, (27066781 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::RabbitFoot, (27132318 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::RabbitHide, (27197855 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Lead, (27525540 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::EndCrystal, (27918762 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::SpruceDoor, (27984299 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::BirchDoor, (28049836 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::JungleDoor, (28115373 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::AcaciaDoor, (28180910 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::DarkOakDoor, (28246447 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::ChorusFruit, (28311984 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::ChorusFruitPopped, (28377521 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Beetroot, (28443081 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::BeetrootSeeds, (28508618 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::BeetrootSoup, (28574155 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::DragonBreath, (28639669 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::SplashPotion, (28705206 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::LingeringPotion, (28901817 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::Elytra, (29032892 << 32) | ((0 << 16) | 0 & 0xFFFF)},
     {Items::ShulkerShell, (29491645 << 32) | ((0 << 16) | 0 & 0xFFFF)},
}
const std::map<std::tuple<short, short>, UItemID> PocketItemToUItemID = {
  {std::make_tuple(256, 0), (16777472 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(257, 0), (16843009 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(258, 0), (16908546 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(259, 0), (16974083 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(260, 0), (17039620 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(261, 0), (17105157 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(262, 0), (17170694 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(263, 0), (17236231 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(264, 0), (17301768 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(265, 0), (17367305 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(266, 0), (17432842 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(267, 0), (17498379 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(268, 0), (17563916 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(269, 0), (17629453 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(270, 0), (17694990 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(271, 0), (17760527 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(272, 0), (17826064 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(273, 0), (17891601 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(274, 0), (17957138 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(275, 0), (18022675 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(276, 0), (18088212 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(277, 0), (18153749 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(278, 0), (18219286 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(279, 0), (18284823 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(280, 0), (18350360 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(281, 0), (18415897 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(282, 0), (18481434 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(283, 0), (18546971 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(284, 0), (18612508 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(285, 0), (18678045 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(286, 0), (18743582 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(287, 0), (18809119 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(288, 0), (18874656 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(289, 0), (18940193 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(290, 0), (19005730 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(291, 0), (19071267 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(292, 0), (19136804 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(293, 0), (19202341 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(294, 0), (19267878 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(295, 0), (19333415 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(296, 0), (19398952 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(297, 0), (19464489 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(298, 0), (19530026 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(299, 0), (19595563 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(300, 0), (19661100 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(301, 0), (19726637 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(302, 0), (19792174 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(303, 0), (19857711 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(304, 0), (19923248 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(305, 0), (19988785 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(306, 0), (20054322 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(307, 0), (20119859 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(308, 0), (20185396 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(309, 0), (20250933 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(310, 0), (20316470 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(311, 0), (20382007 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(312, 0), (20447544 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(313, 0), (20513081 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(314, 0), (20578618 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(315, 0), (20644155 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(316, 0), (20709692 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(317, 0), (20775229 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(318, 0), (20840766 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(319, 0), (20906303 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(320, 0), (20971840 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(321, 0), (21037377 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(322, 0), (21102914 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(323, 0), (21168451 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(324, 0), (21233988 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(325, 0), (21299525 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(328, 0), (21496136 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(329, 0), (21561673 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(330, 0), (21627210 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(331, 0), (21692747 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(332, 0), (21758284 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(333, 0), (21823821 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(334, 0), (21889358 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(336, 0), (22020432 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(337, 0), (22085969 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(338, 0), (22151506 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(339, 0), (22217043 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(340, 0), (22282580 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(341, 0), (22348117 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(342, 0), (22413654 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(344, 0), (22544728 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(345, 0), (22610265 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(346, 0), (22675802 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(347, 0), (22741339 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(348, 0), (22806876 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(349, 0), (22872413 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(350, 0), (22937950 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(351, 0), (23003487 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(352, 0), (23069024 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(353, 0), (23134561 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(354, 0), (23200098 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(355, 0), (23265635 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(356, 0), (23331172 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(357, 0), (23396709 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(359, 0), (23527783 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(360, 0), (23593320 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(361, 0), (23658857 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(362, 0), (23724394 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(363, 0), (23789931 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(364, 0), (23855468 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(365, 0), (23921005 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(366, 0), (23986542 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(367, 0), (24052079 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(368, 0), (24117616 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(369, 0), (24183153 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(370, 0), (24248690 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(371, 0), (24314227 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(372, 0), (24379764 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(373, 0), (24445301 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(374, 0), (24510838 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(375, 0), (24576375 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(376, 0), (24641912 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(377, 0), (24707449 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(378, 0), (24772986 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(379, 0), (24838523 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(380, 0), (24904060 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(381, 0), (24969597 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(382, 0), (25035134 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(383, 0), (25100671 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(384, 0), (25166208 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(388, 0), (25428356 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(390, 0), (25559430 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(391, 0), (25624967 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(392, 0), (25690504 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(393, 0), (25756041 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(394, 0), (25821578 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(396, 0), (25952652 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(397, 0), (26018189 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(400, 0), (26214800 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(403, 0), (26411411 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(404, 0), (26476948 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(405, 0), (26542485 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(406, 0), (26608022 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(407, 0), (26673559 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(408, 0), (26739096 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(409, 0), (26804633 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(422, 0), (26870182 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(411, 0), (26935707 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(412, 0), (27001244 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(413, 0), (27066781 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(414, 0), (27132318 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(415, 0), (27197855 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(420, 0), (27525540 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(426, 0), (27918762 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(427, 0), (27984299 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(428, 0), (28049836 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(429, 0), (28115373 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(430, 0), (28180910 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(431, 0), (28246447 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(432, 0), (28311984 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(433, 0), (28377521 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(457, 0), (28443081 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(458, 0), (28508618 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(459, 0), (28574155 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(437, 0), (28639669 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(438, 0), (28705206 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(441, 0), (28901817 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(444, 0), (29032892 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(445, 0), (29491645 << 32) | ((0 << 16) | 0 & 0xFFFF)},
}
const std::map<std::tuple<short, short>, UItemName> PocketItemToUItemName = {
  {std::make_tuple(256, 0), Item::IronShovel},
  {std::make_tuple(257, 0), Item::IronPickaxe},
  {std::make_tuple(258, 0), Item::IronAxe},
  {std::make_tuple(259, 0), Item::FlintAndSteel},
  {std::make_tuple(260, 0), Item::Apple},
  {std::make_tuple(261, 0), Item::Bow},
  {std::make_tuple(262, 0), Item::Arrow},
  {std::make_tuple(263, 0), Item::Coal_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(264, 0), Item::Diamond},
  {std::make_tuple(265, 0), Item::IronIngot},
  {std::make_tuple(266, 0), Item::GoldIngot},
  {std::make_tuple(267, 0), Item::IronSword},
  {std::make_tuple(268, 0), Item::WoodenSword},
  {std::make_tuple(269, 0), Item::WoodenShovel},
  {std::make_tuple(270, 0), Item::WoodenPickaxe},
  {std::make_tuple(271, 0), Item::WoodenAxe},
  {std::make_tuple(272, 0), Item::StoneSword},
  {std::make_tuple(273, 0), Item::StoneShovel},
  {std::make_tuple(274, 0), Item::StonePickaxe},
  {std::make_tuple(275, 0), Item::StoneAxe},
  {std::make_tuple(276, 0), Item::DiamondSword},
  {std::make_tuple(277, 0), Item::DiamondShovel},
  {std::make_tuple(278, 0), Item::DiamondPickaxe},
  {std::make_tuple(279, 0), Item::DiamondAxe},
  {std::make_tuple(280, 0), Item::Stick},
  {std::make_tuple(281, 0), Item::Bowl},
  {std::make_tuple(282, 0), Item::MushroomStew},
  {std::make_tuple(283, 0), Item::GoldenSword},
  {std::make_tuple(284, 0), Item::GoldenShovel},
  {std::make_tuple(285, 0), Item::GoldenPickaxe},
  {std::make_tuple(286, 0), Item::GoldenAxe},
  {std::make_tuple(287, 0), Item::String},
  {std::make_tuple(288, 0), Item::Feather},
  {std::make_tuple(289, 0), Item::Gunpowder},
  {std::make_tuple(290, 0), Item::WoodenHoe},
  {std::make_tuple(291, 0), Item::StoneHoe},
  {std::make_tuple(292, 0), Item::IronHoe},
  {std::make_tuple(293, 0), Item::DiamondHoe},
  {std::make_tuple(294, 0), Item::GoldenHoe},
  {std::make_tuple(295, 0), Item::WheatSeeds},
  {std::make_tuple(296, 0), Item::Wheat},
  {std::make_tuple(297, 0), Item::Bread},
  {std::make_tuple(298, 0), Item::LeatherHelmet},
  {std::make_tuple(299, 0), Item::LeatherChestplate},
  {std::make_tuple(300, 0), Item::LeatherLeggings},
  {std::make_tuple(301, 0), Item::LeatherBoots},
  {std::make_tuple(302, 0), Item::ChainmailHelmet},
  {std::make_tuple(303, 0), Item::ChainmailChestplate},
  {std::make_tuple(304, 0), Item::ChainmailLeggings},
  {std::make_tuple(305, 0), Item::ChainmailBoots},
  {std::make_tuple(306, 0), Item::IronHelmet},
  {std::make_tuple(307, 0), Item::IronChestplate},
  {std::make_tuple(308, 0), Item::IronLeggings},
  {std::make_tuple(309, 0), Item::IronBoots},
  {std::make_tuple(310, 0), Item::DiamondHelmet},
  {std::make_tuple(311, 0), Item::DiamondChestplate},
  {std::make_tuple(312, 0), Item::DiamondLeggings},
  {std::make_tuple(313, 0), Item::DiamondBoots},
  {std::make_tuple(314, 0), Item::GoldenHelmet},
  {std::make_tuple(315, 0), Item::GoldenChestplate},
  {std::make_tuple(316, 0), Item::GoldenLeggings},
  {std::make_tuple(317, 0), Item::GoldenBoots},
  {std::make_tuple(318, 0), Item::Flint},
  {std::make_tuple(319, 0), Item::Porkchop},
  {std::make_tuple(320, 0), Item::CookedPorkchop},
  {std::make_tuple(321, 0), Item::Painting},
  {std::make_tuple(322, 0), Item::GoldenApple_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(323, 0), Item::Sign},
  {std::make_tuple(324, 0), Item::WoodenDoor},
  {std::make_tuple(325, 0), Item::Bucket},
  {std::make_tuple(328, 0), Item::Minecart},
  {std::make_tuple(329, 0), Item::Saddle},
  {std::make_tuple(330, 0), Item::IronDoor},
  {std::make_tuple(331, 0), Item::Redstone},
  {std::make_tuple(332, 0), Item::Snowball},
  {std::make_tuple(333, 0), Item::Boat},
  {std::make_tuple(334, 0), Item::Leather},
  {std::make_tuple(336, 0), Item::Brick},
  {std::make_tuple(337, 0), Item::ClayBall},
  {std::make_tuple(338, 0), Item::Reeds},
  {std::make_tuple(339, 0), Item::Paper},
  {std::make_tuple(340, 0), Item::Book},
  {std::make_tuple(341, 0), Item::SlimeBall},
  {std::make_tuple(342, 0), Item::ChestMinecart},
  {std::make_tuple(344, 0), Item::Egg},
  {std::make_tuple(345, 0), Item::Compass},
  {std::make_tuple(346, 0), Item::FishingRod},
  {std::make_tuple(347, 0), Item::Clock},
  {std::make_tuple(348, 0), Item::GlowstoneDust},
  {std::make_tuple(349, 0), Item::Fish_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(350, 0), Item::CookedFish_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(351, 0), Item::Dye_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(352, 0), Item::Bone},
  {std::make_tuple(353, 0), Item::Sugar},
  {std::make_tuple(354, 0), Item::Cake},
  {std::make_tuple(355, 0), Item::Bed},
  {std::make_tuple(356, 0), Item::Repeater},
  {std::make_tuple(357, 0), Item::Cookie},
  {std::make_tuple(359, 0), Item::Shears},
  {std::make_tuple(360, 0), Item::Melon},
  {std::make_tuple(361, 0), Item::PumpkinSeeds},
  {std::make_tuple(362, 0), Item::MelonSeeds},
  {std::make_tuple(363, 0), Item::Beef},
  {std::make_tuple(364, 0), Item::CookedBeef},
  {std::make_tuple(365, 0), Item::Chicken},
  {std::make_tuple(366, 0), Item::CookedChicken},
  {std::make_tuple(367, 0), Item::RottenFlesh},
  {std::make_tuple(368, 0), Item::EnderPearl},
  {std::make_tuple(369, 0), Item::BlazeRod},
  {std::make_tuple(370, 0), Item::GhastTear},
  {std::make_tuple(371, 0), Item::GoldNugget},
  {std::make_tuple(372, 0), Item::NetherWart},
  {std::make_tuple(373, 0), Item::Potion_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(374, 0), Item::GlassBottle},
  {std::make_tuple(375, 0), Item::SpiderEye},
  {std::make_tuple(376, 0), Item::FermentedSpiderEye},
  {std::make_tuple(377, 0), Item::BlazePowder},
  {std::make_tuple(378, 0), Item::MagmaCream},
  {std::make_tuple(379, 0), Item::BrewingStand},
  {std::make_tuple(380, 0), Item::Cauldron},
  {std::make_tuple(381, 0), Item::EnderEye},
  {std::make_tuple(382, 0), Item::SpeckledMelon},
  {std::make_tuple(383, 0), Item::SpawnEgg_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(384, 0), Item::ExperienceBottle},
  {std::make_tuple(388, 0), Item::Emerald},
  {std::make_tuple(390, 0), Item::FlowerPot},
  {std::make_tuple(391, 0), Item::Carrot},
  {std::make_tuple(392, 0), Item::Potato},
  {std::make_tuple(393, 0), Item::BakedPotato},
  {std::make_tuple(394, 0), Item::PoisonousPotato},
  {std::make_tuple(396, 0), Item::GoldenCarrot},
  {std::make_tuple(397, 0), Item::Skull_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(400, 0), Item::PumpkinPie},
  {std::make_tuple(403, 0), Item::EnchantedBook},
  {std::make_tuple(404, 0), Item::Comparator},
  {std::make_tuple(405, 0), Item::Netherbrick},
  {std::make_tuple(406, 0), Item::Quartz},
  {std::make_tuple(407, 0), Item::TntMinecart},
  {std::make_tuple(408, 0), Item::HopperMinecart},
  {std::make_tuple(409, 0), Item::PrismarineShard},
  {std::make_tuple(422, 0), Item::PrismarineCrystals},
  {std::make_tuple(411, 0), Item::Rabbit},
  {std::make_tuple(412, 0), Item::CookedRabbit},
  {std::make_tuple(413, 0), Item::RabbitStew},
  {std::make_tuple(414, 0), Item::RabbitFoot},
  {std::make_tuple(415, 0), Item::RabbitHide},
  {std::make_tuple(420, 0), Item::Lead},
  {std::make_tuple(426, 0), Item::EndCrystal},
  {std::make_tuple(427, 0), Item::SpruceDoor},
  {std::make_tuple(428, 0), Item::BirchDoor},
  {std::make_tuple(429, 0), Item::JungleDoor},
  {std::make_tuple(430, 0), Item::AcaciaDoor},
  {std::make_tuple(431, 0), Item::DarkOakDoor},
  {std::make_tuple(432, 0), Item::ChorusFruit},
  {std::make_tuple(433, 0), Item::ChorusFruitPopped},
  {std::make_tuple(457, 0), Item::Beetroot},
  {std::make_tuple(458, 0), Item::BeetrootSeeds},
  {std::make_tuple(459, 0), Item::BeetrootSoup},
  {std::make_tuple(437, 0), Item::DragonBreath},
  {std::make_tuple(438, 0), Item::SplashPotion},
  {std::make_tuple(441, 0), Item::LingeringPotion},
  {std::make_tuple(444, 0), Item::Elytra},
  {std::make_tuple(445, 0), Item::ShulkerShell},
}
const std::map<std::tuple<short, short>, std::tuple<std::string, short>> PocketItemToJavaItem = {
    {std::make_tuple(256, 0), std::make_tuple(256, 0)},
    {std::make_tuple(257, 0), std::make_tuple(257, 0)},
    {std::make_tuple(258, 0), std::make_tuple(258, 0)},
    {std::make_tuple(259, 0), std::make_tuple(259, 0)},
    {std::make_tuple(260, 0), std::make_tuple(260, 0)},
    {std::make_tuple(261, 0), std::make_tuple(261, 0)},
    {std::make_tuple(262, 0), std::make_tuple(262, 0)},
    {std::make_tuple(263, 0), std::make_tuple(263, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(264, 0), std::make_tuple(264, 0)},
    {std::make_tuple(265, 0), std::make_tuple(265, 0)},
    {std::make_tuple(266, 0), std::make_tuple(266, 0)},
    {std::make_tuple(267, 0), std::make_tuple(267, 0)},
    {std::make_tuple(268, 0), std::make_tuple(268, 0)},
    {std::make_tuple(269, 0), std::make_tuple(269, 0)},
    {std::make_tuple(270, 0), std::make_tuple(270, 0)},
    {std::make_tuple(271, 0), std::make_tuple(271, 0)},
    {std::make_tuple(272, 0), std::make_tuple(272, 0)},
    {std::make_tuple(273, 0), std::make_tuple(273, 0)},
    {std::make_tuple(274, 0), std::make_tuple(274, 0)},
    {std::make_tuple(275, 0), std::make_tuple(275, 0)},
    {std::make_tuple(276, 0), std::make_tuple(276, 0)},
    {std::make_tuple(277, 0), std::make_tuple(277, 0)},
    {std::make_tuple(278, 0), std::make_tuple(278, 0)},
    {std::make_tuple(279, 0), std::make_tuple(279, 0)},
    {std::make_tuple(280, 0), std::make_tuple(280, 0)},
    {std::make_tuple(281, 0), std::make_tuple(281, 0)},
    {std::make_tuple(282, 0), std::make_tuple(282, 0)},
    {std::make_tuple(283, 0), std::make_tuple(283, 0)},
    {std::make_tuple(284, 0), std::make_tuple(284, 0)},
    {std::make_tuple(285, 0), std::make_tuple(285, 0)},
    {std::make_tuple(286, 0), std::make_tuple(286, 0)},
    {std::make_tuple(287, 0), std::make_tuple(287, 0)},
    {std::make_tuple(288, 0), std::make_tuple(288, 0)},
    {std::make_tuple(289, 0), std::make_tuple(289, 0)},
    {std::make_tuple(290, 0), std::make_tuple(290, 0)},
    {std::make_tuple(291, 0), std::make_tuple(291, 0)},
    {std::make_tuple(292, 0), std::make_tuple(292, 0)},
    {std::make_tuple(293, 0), std::make_tuple(293, 0)},
    {std::make_tuple(294, 0), std::make_tuple(294, 0)},
    {std::make_tuple(295, 0), std::make_tuple(295, 0)},
    {std::make_tuple(296, 0), std::make_tuple(296, 0)},
    {std::make_tuple(297, 0), std::make_tuple(297, 0)},
    {std::make_tuple(298, 0), std::make_tuple(298, 0)},
    {std::make_tuple(299, 0), std::make_tuple(299, 0)},
    {std::make_tuple(300, 0), std::make_tuple(300, 0)},
    {std::make_tuple(301, 0), std::make_tuple(301, 0)},
    {std::make_tuple(302, 0), std::make_tuple(302, 0)},
    {std::make_tuple(303, 0), std::make_tuple(303, 0)},
    {std::make_tuple(304, 0), std::make_tuple(304, 0)},
    {std::make_tuple(305, 0), std::make_tuple(305, 0)},
    {std::make_tuple(306, 0), std::make_tuple(306, 0)},
    {std::make_tuple(307, 0), std::make_tuple(307, 0)},
    {std::make_tuple(308, 0), std::make_tuple(308, 0)},
    {std::make_tuple(309, 0), std::make_tuple(309, 0)},
    {std::make_tuple(310, 0), std::make_tuple(310, 0)},
    {std::make_tuple(311, 0), std::make_tuple(311, 0)},
    {std::make_tuple(312, 0), std::make_tuple(312, 0)},
    {std::make_tuple(313, 0), std::make_tuple(313, 0)},
    {std::make_tuple(314, 0), std::make_tuple(314, 0)},
    {std::make_tuple(315, 0), std::make_tuple(315, 0)},
    {std::make_tuple(316, 0), std::make_tuple(316, 0)},
    {std::make_tuple(317, 0), std::make_tuple(317, 0)},
    {std::make_tuple(318, 0), std::make_tuple(318, 0)},
    {std::make_tuple(319, 0), std::make_tuple(319, 0)},
    {std::make_tuple(320, 0), std::make_tuple(320, 0)},
    {std::make_tuple(321, 0), std::make_tuple(321, 0)},
    {std::make_tuple(322, 0), std::make_tuple(322, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(323, 0), std::make_tuple(323, 0)},
    {std::make_tuple(324, 0), std::make_tuple(324, 0)},
    {std::make_tuple(325, 0), std::make_tuple(325, 0)},
    {std::make_tuple(328, 0), std::make_tuple(328, 0)},
    {std::make_tuple(329, 0), std::make_tuple(329, 0)},
    {std::make_tuple(330, 0), std::make_tuple(330, 0)},
    {std::make_tuple(331, 0), std::make_tuple(331, 0)},
    {std::make_tuple(332, 0), std::make_tuple(332, 0)},
    {std::make_tuple(333, 0), std::make_tuple(333, 0)},
    {std::make_tuple(334, 0), std::make_tuple(334, 0)},
    {std::make_tuple(336, 0), std::make_tuple(336, 0)},
    {std::make_tuple(337, 0), std::make_tuple(337, 0)},
    {std::make_tuple(338, 0), std::make_tuple(338, 0)},
    {std::make_tuple(339, 0), std::make_tuple(339, 0)},
    {std::make_tuple(340, 0), std::make_tuple(340, 0)},
    {std::make_tuple(341, 0), std::make_tuple(341, 0)},
    {std::make_tuple(342, 0), std::make_tuple(342, 0)},
    {std::make_tuple(344, 0), std::make_tuple(344, 0)},
    {std::make_tuple(345, 0), std::make_tuple(345, 0)},
    {std::make_tuple(346, 0), std::make_tuple(346, 0)},
    {std::make_tuple(347, 0), std::make_tuple(347, 0)},
    {std::make_tuple(348, 0), std::make_tuple(348, 0)},
    {std::make_tuple(349, 0), std::make_tuple(349, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(350, 0), std::make_tuple(350, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(351, 0), std::make_tuple(351, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(352, 0), std::make_tuple(352, 0)},
    {std::make_tuple(353, 0), std::make_tuple(353, 0)},
    {std::make_tuple(354, 0), std::make_tuple(354, 0)},
    {std::make_tuple(355, 0), std::make_tuple(355, 0)},
    {std::make_tuple(356, 0), std::make_tuple(356, 0)},
    {std::make_tuple(357, 0), std::make_tuple(357, 0)},
    {std::make_tuple(359, 0), std::make_tuple(359, 0)},
    {std::make_tuple(360, 0), std::make_tuple(360, 0)},
    {std::make_tuple(361, 0), std::make_tuple(361, 0)},
    {std::make_tuple(362, 0), std::make_tuple(362, 0)},
    {std::make_tuple(363, 0), std::make_tuple(363, 0)},
    {std::make_tuple(364, 0), std::make_tuple(364, 0)},
    {std::make_tuple(365, 0), std::make_tuple(365, 0)},
    {std::make_tuple(366, 0), std::make_tuple(366, 0)},
    {std::make_tuple(367, 0), std::make_tuple(367, 0)},
    {std::make_tuple(368, 0), std::make_tuple(368, 0)},
    {std::make_tuple(369, 0), std::make_tuple(369, 0)},
    {std::make_tuple(370, 0), std::make_tuple(370, 0)},
    {std::make_tuple(371, 0), std::make_tuple(371, 0)},
    {std::make_tuple(372, 0), std::make_tuple(372, 0)},
    {std::make_tuple(373, 0), std::make_tuple(373, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(374, 0), std::make_tuple(374, 0)},
    {std::make_tuple(375, 0), std::make_tuple(375, 0)},
    {std::make_tuple(376, 0), std::make_tuple(376, 0)},
    {std::make_tuple(377, 0), std::make_tuple(377, 0)},
    {std::make_tuple(378, 0), std::make_tuple(378, 0)},
    {std::make_tuple(379, 0), std::make_tuple(379, 0)},
    {std::make_tuple(380, 0), std::make_tuple(380, 0)},
    {std::make_tuple(381, 0), std::make_tuple(381, 0)},
    {std::make_tuple(382, 0), std::make_tuple(382, 0)},
    {std::make_tuple(383, 0), std::make_tuple(383, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(384, 0), std::make_tuple(384, 0)},
    {std::make_tuple(388, 0), std::make_tuple(388, 0)},
    {std::make_tuple(390, 0), std::make_tuple(390, 0)},
    {std::make_tuple(391, 0), std::make_tuple(391, 0)},
    {std::make_tuple(392, 0), std::make_tuple(392, 0)},
    {std::make_tuple(393, 0), std::make_tuple(393, 0)},
    {std::make_tuple(394, 0), std::make_tuple(394, 0)},
    {std::make_tuple(396, 0), std::make_tuple(396, 0)},
    {std::make_tuple(397, 0), std::make_tuple(397, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(400, 0), std::make_tuple(400, 0)},
    {std::make_tuple(403, 0), std::make_tuple(403, 0)},
    {std::make_tuple(404, 0), std::make_tuple(404, 0)},
    {std::make_tuple(405, 0), std::make_tuple(405, 0)},
    {std::make_tuple(406, 0), std::make_tuple(406, 0)},
    {std::make_tuple(407, 0), std::make_tuple(407, 0)},
    {std::make_tuple(408, 0), std::make_tuple(408, 0)},
    {std::make_tuple(409, 0), std::make_tuple(409, 0)},
    {std::make_tuple(422, 0), std::make_tuple(410, 0)},
    {std::make_tuple(411, 0), std::make_tuple(411, 0)},
    {std::make_tuple(412, 0), std::make_tuple(412, 0)},
    {std::make_tuple(413, 0), std::make_tuple(413, 0)},
    {std::make_tuple(414, 0), std::make_tuple(414, 0)},
    {std::make_tuple(415, 0), std::make_tuple(415, 0)},
    {std::make_tuple(420, 0), std::make_tuple(420, 0)},
    {std::make_tuple(426, 0), std::make_tuple(426, 0)},
    {std::make_tuple(427, 0), std::make_tuple(427, 0)},
    {std::make_tuple(428, 0), std::make_tuple(428, 0)},
    {std::make_tuple(429, 0), std::make_tuple(429, 0)},
    {std::make_tuple(430, 0), std::make_tuple(430, 0)},
    {std::make_tuple(431, 0), std::make_tuple(431, 0)},
    {std::make_tuple(432, 0), std::make_tuple(432, 0)},
    {std::make_tuple(433, 0), std::make_tuple(433, 0)},
    {std::make_tuple(457, 0), std::make_tuple(434, 0)},
    {std::make_tuple(458, 0), std::make_tuple(435, 0)},
    {std::make_tuple(459, 0), std::make_tuple(436, 0)},
    {std::make_tuple(437, 0), std::make_tuple(437, 0)},
    {std::make_tuple(438, 0), std::make_tuple(438, 0)},
    {std::make_tuple(441, 0), std::make_tuple(441, 0)},
    {std::make_tuple(444, 0), std::make_tuple(443, 0)},
    {std::make_tuple(445, 0), std::make_tuple(450, 0)},
}
const std::map<std::tuple<std::string, short>, UItemID> JavaItemToUItemID = {
  {std::make_tuple(256, 0), (16777472 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(257, 0), (16843009 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(258, 0), (16908546 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(259, 0), (16974083 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(260, 0), (17039620 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(261, 0), (17105157 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(262, 0), (17170694 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(263, 0), (17236231 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(264, 0), (17301768 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(265, 0), (17367305 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(266, 0), (17432842 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(267, 0), (17498379 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(268, 0), (17563916 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(269, 0), (17629453 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(270, 0), (17694990 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(271, 0), (17760527 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(272, 0), (17826064 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(273, 0), (17891601 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(274, 0), (17957138 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(275, 0), (18022675 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(276, 0), (18088212 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(277, 0), (18153749 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(278, 0), (18219286 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(279, 0), (18284823 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(280, 0), (18350360 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(281, 0), (18415897 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(282, 0), (18481434 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(283, 0), (18546971 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(284, 0), (18612508 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(285, 0), (18678045 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(286, 0), (18743582 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(287, 0), (18809119 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(288, 0), (18874656 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(289, 0), (18940193 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(290, 0), (19005730 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(291, 0), (19071267 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(292, 0), (19136804 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(293, 0), (19202341 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(294, 0), (19267878 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(295, 0), (19333415 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(296, 0), (19398952 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(297, 0), (19464489 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(298, 0), (19530026 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(299, 0), (19595563 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(300, 0), (19661100 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(301, 0), (19726637 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(302, 0), (19792174 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(303, 0), (19857711 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(304, 0), (19923248 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(305, 0), (19988785 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(306, 0), (20054322 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(307, 0), (20119859 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(308, 0), (20185396 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(309, 0), (20250933 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(310, 0), (20316470 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(311, 0), (20382007 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(312, 0), (20447544 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(313, 0), (20513081 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(314, 0), (20578618 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(315, 0), (20644155 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(316, 0), (20709692 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(317, 0), (20775229 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(318, 0), (20840766 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(319, 0), (20906303 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(320, 0), (20971840 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(321, 0), (21037377 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(322, 0), (21102914 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(323, 0), (21168451 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(324, 0), (21233988 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(325, 0), (21299525 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(328, 0), (21496136 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(329, 0), (21561673 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(330, 0), (21627210 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(331, 0), (21692747 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(332, 0), (21758284 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(333, 0), (21823821 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(334, 0), (21889358 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(336, 0), (22020432 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(337, 0), (22085969 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(338, 0), (22151506 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(339, 0), (22217043 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(340, 0), (22282580 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(341, 0), (22348117 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(342, 0), (22413654 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(344, 0), (22544728 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(345, 0), (22610265 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(346, 0), (22675802 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(347, 0), (22741339 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(348, 0), (22806876 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(349, 0), (22872413 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(350, 0), (22937950 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(351, 0), (23003487 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(352, 0), (23069024 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(353, 0), (23134561 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(354, 0), (23200098 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(355, 0), (23265635 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(356, 0), (23331172 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(357, 0), (23396709 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(359, 0), (23527783 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(360, 0), (23593320 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(361, 0), (23658857 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(362, 0), (23724394 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(363, 0), (23789931 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(364, 0), (23855468 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(365, 0), (23921005 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(366, 0), (23986542 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(367, 0), (24052079 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(368, 0), (24117616 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(369, 0), (24183153 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(370, 0), (24248690 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(371, 0), (24314227 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(372, 0), (24379764 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(373, 0), (24445301 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(374, 0), (24510838 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(375, 0), (24576375 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(376, 0), (24641912 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(377, 0), (24707449 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(378, 0), (24772986 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(379, 0), (24838523 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(380, 0), (24904060 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(381, 0), (24969597 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(382, 0), (25035134 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(383, 0), (25100671 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(384, 0), (25166208 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(388, 0), (25428356 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(390, 0), (25559430 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(391, 0), (25624967 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(392, 0), (25690504 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(393, 0), (25756041 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(394, 0), (25821578 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(396, 0), (25952652 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(397, 0), (26018189 << 32) | ((0 << 16) | 0 & 0xFFFF)},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(400, 0), (26214800 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(403, 0), (26411411 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(404, 0), (26476948 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(405, 0), (26542485 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(406, 0), (26608022 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(407, 0), (26673559 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(408, 0), (26739096 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(409, 0), (26804633 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(410, 0), (26870182 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(411, 0), (26935707 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(412, 0), (27001244 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(413, 0), (27066781 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(414, 0), (27132318 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(415, 0), (27197855 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(420, 0), (27525540 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(426, 0), (27918762 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(427, 0), (27984299 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(428, 0), (28049836 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(429, 0), (28115373 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(430, 0), (28180910 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(431, 0), (28246447 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(432, 0), (28311984 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(433, 0), (28377521 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(434, 0), (28443081 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(435, 0), (28508618 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(436, 0), (28574155 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(437, 0), (28639669 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(438, 0), (28705206 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(441, 0), (28901817 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(443, 0), (29032892 << 32) | ((0 << 16) | 0 & 0xFFFF)},
  {std::make_tuple(450, 0), (29491645 << 32) | ((0 << 16) | 0 & 0xFFFF)},
}
const std::map<std::tuple<std::string, short>, UItemName> JavaItemToUItemName = {
  {std::make_tuple(256, 0), Item::IronShovel},
  {std::make_tuple(257, 0), Item::IronPickaxe},
  {std::make_tuple(258, 0), Item::IronAxe},
  {std::make_tuple(259, 0), Item::FlintAndSteel},
  {std::make_tuple(260, 0), Item::Apple},
  {std::make_tuple(261, 0), Item::Bow},
  {std::make_tuple(262, 0), Item::Arrow},
  {std::make_tuple(263, 0), Item::Coal_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(264, 0), Item::Diamond},
  {std::make_tuple(265, 0), Item::IronIngot},
  {std::make_tuple(266, 0), Item::GoldIngot},
  {std::make_tuple(267, 0), Item::IronSword},
  {std::make_tuple(268, 0), Item::WoodenSword},
  {std::make_tuple(269, 0), Item::WoodenShovel},
  {std::make_tuple(270, 0), Item::WoodenPickaxe},
  {std::make_tuple(271, 0), Item::WoodenAxe},
  {std::make_tuple(272, 0), Item::StoneSword},
  {std::make_tuple(273, 0), Item::StoneShovel},
  {std::make_tuple(274, 0), Item::StonePickaxe},
  {std::make_tuple(275, 0), Item::StoneAxe},
  {std::make_tuple(276, 0), Item::DiamondSword},
  {std::make_tuple(277, 0), Item::DiamondShovel},
  {std::make_tuple(278, 0), Item::DiamondPickaxe},
  {std::make_tuple(279, 0), Item::DiamondAxe},
  {std::make_tuple(280, 0), Item::Stick},
  {std::make_tuple(281, 0), Item::Bowl},
  {std::make_tuple(282, 0), Item::MushroomStew},
  {std::make_tuple(283, 0), Item::GoldenSword},
  {std::make_tuple(284, 0), Item::GoldenShovel},
  {std::make_tuple(285, 0), Item::GoldenPickaxe},
  {std::make_tuple(286, 0), Item::GoldenAxe},
  {std::make_tuple(287, 0), Item::String},
  {std::make_tuple(288, 0), Item::Feather},
  {std::make_tuple(289, 0), Item::Gunpowder},
  {std::make_tuple(290, 0), Item::WoodenHoe},
  {std::make_tuple(291, 0), Item::StoneHoe},
  {std::make_tuple(292, 0), Item::IronHoe},
  {std::make_tuple(293, 0), Item::DiamondHoe},
  {std::make_tuple(294, 0), Item::GoldenHoe},
  {std::make_tuple(295, 0), Item::WheatSeeds},
  {std::make_tuple(296, 0), Item::Wheat},
  {std::make_tuple(297, 0), Item::Bread},
  {std::make_tuple(298, 0), Item::LeatherHelmet},
  {std::make_tuple(299, 0), Item::LeatherChestplate},
  {std::make_tuple(300, 0), Item::LeatherLeggings},
  {std::make_tuple(301, 0), Item::LeatherBoots},
  {std::make_tuple(302, 0), Item::ChainmailHelmet},
  {std::make_tuple(303, 0), Item::ChainmailChestplate},
  {std::make_tuple(304, 0), Item::ChainmailLeggings},
  {std::make_tuple(305, 0), Item::ChainmailBoots},
  {std::make_tuple(306, 0), Item::IronHelmet},
  {std::make_tuple(307, 0), Item::IronChestplate},
  {std::make_tuple(308, 0), Item::IronLeggings},
  {std::make_tuple(309, 0), Item::IronBoots},
  {std::make_tuple(310, 0), Item::DiamondHelmet},
  {std::make_tuple(311, 0), Item::DiamondChestplate},
  {std::make_tuple(312, 0), Item::DiamondLeggings},
  {std::make_tuple(313, 0), Item::DiamondBoots},
  {std::make_tuple(314, 0), Item::GoldenHelmet},
  {std::make_tuple(315, 0), Item::GoldenChestplate},
  {std::make_tuple(316, 0), Item::GoldenLeggings},
  {std::make_tuple(317, 0), Item::GoldenBoots},
  {std::make_tuple(318, 0), Item::Flint},
  {std::make_tuple(319, 0), Item::Porkchop},
  {std::make_tuple(320, 0), Item::CookedPorkchop},
  {std::make_tuple(321, 0), Item::Painting},
  {std::make_tuple(322, 0), Item::GoldenApple_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(323, 0), Item::Sign},
  {std::make_tuple(324, 0), Item::WoodenDoor},
  {std::make_tuple(325, 0), Item::Bucket},
  {std::make_tuple(328, 0), Item::Minecart},
  {std::make_tuple(329, 0), Item::Saddle},
  {std::make_tuple(330, 0), Item::IronDoor},
  {std::make_tuple(331, 0), Item::Redstone},
  {std::make_tuple(332, 0), Item::Snowball},
  {std::make_tuple(333, 0), Item::Boat},
  {std::make_tuple(334, 0), Item::Leather},
  {std::make_tuple(336, 0), Item::Brick},
  {std::make_tuple(337, 0), Item::ClayBall},
  {std::make_tuple(338, 0), Item::Reeds},
  {std::make_tuple(339, 0), Item::Paper},
  {std::make_tuple(340, 0), Item::Book},
  {std::make_tuple(341, 0), Item::SlimeBall},
  {std::make_tuple(342, 0), Item::ChestMinecart},
  {std::make_tuple(344, 0), Item::Egg},
  {std::make_tuple(345, 0), Item::Compass},
  {std::make_tuple(346, 0), Item::FishingRod},
  {std::make_tuple(347, 0), Item::Clock},
  {std::make_tuple(348, 0), Item::GlowstoneDust},
  {std::make_tuple(349, 0), Item::Fish_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(350, 0), Item::CookedFish_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(351, 0), Item::Dye_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(352, 0), Item::Bone},
  {std::make_tuple(353, 0), Item::Sugar},
  {std::make_tuple(354, 0), Item::Cake},
  {std::make_tuple(355, 0), Item::Bed},
  {std::make_tuple(356, 0), Item::Repeater},
  {std::make_tuple(357, 0), Item::Cookie},
  {std::make_tuple(359, 0), Item::Shears},
  {std::make_tuple(360, 0), Item::Melon},
  {std::make_tuple(361, 0), Item::PumpkinSeeds},
  {std::make_tuple(362, 0), Item::MelonSeeds},
  {std::make_tuple(363, 0), Item::Beef},
  {std::make_tuple(364, 0), Item::CookedBeef},
  {std::make_tuple(365, 0), Item::Chicken},
  {std::make_tuple(366, 0), Item::CookedChicken},
  {std::make_tuple(367, 0), Item::RottenFlesh},
  {std::make_tuple(368, 0), Item::EnderPearl},
  {std::make_tuple(369, 0), Item::BlazeRod},
  {std::make_tuple(370, 0), Item::GhastTear},
  {std::make_tuple(371, 0), Item::GoldNugget},
  {std::make_tuple(372, 0), Item::NetherWart},
  {std::make_tuple(373, 0), Item::Potion_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(374, 0), Item::GlassBottle},
  {std::make_tuple(375, 0), Item::SpiderEye},
  {std::make_tuple(376, 0), Item::FermentedSpiderEye},
  {std::make_tuple(377, 0), Item::BlazePowder},
  {std::make_tuple(378, 0), Item::MagmaCream},
  {std::make_tuple(379, 0), Item::BrewingStand},
  {std::make_tuple(380, 0), Item::Cauldron},
  {std::make_tuple(381, 0), Item::EnderEye},
  {std::make_tuple(382, 0), Item::SpeckledMelon},
  {std::make_tuple(383, 0), Item::SpawnEgg_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(384, 0), Item::ExperienceBottle},
  {std::make_tuple(388, 0), Item::Emerald},
  {std::make_tuple(390, 0), Item::FlowerPot},
  {std::make_tuple(391, 0), Item::Carrot},
  {std::make_tuple(392, 0), Item::Potato},
  {std::make_tuple(393, 0), Item::BakedPotato},
  {std::make_tuple(394, 0), Item::PoisonousPotato},
  {std::make_tuple(396, 0), Item::GoldenCarrot},
  {std::make_tuple(397, 0), Item::Skull_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(400, 0), Item::PumpkinPie},
  {std::make_tuple(403, 0), Item::EnchantedBook},
  {std::make_tuple(404, 0), Item::Comparator},
  {std::make_tuple(405, 0), Item::Netherbrick},
  {std::make_tuple(406, 0), Item::Quartz},
  {std::make_tuple(407, 0), Item::TntMinecart},
  {std::make_tuple(408, 0), Item::HopperMinecart},
  {std::make_tuple(409, 0), Item::PrismarineShard},
  {std::make_tuple(410, 0), Item::PrismarineCrystals},
  {std::make_tuple(411, 0), Item::Rabbit},
  {std::make_tuple(412, 0), Item::CookedRabbit},
  {std::make_tuple(413, 0), Item::RabbitStew},
  {std::make_tuple(414, 0), Item::RabbitFoot},
  {std::make_tuple(415, 0), Item::RabbitHide},
  {std::make_tuple(420, 0), Item::Lead},
  {std::make_tuple(426, 0), Item::EndCrystal},
  {std::make_tuple(427, 0), Item::SpruceDoor},
  {std::make_tuple(428, 0), Item::BirchDoor},
  {std::make_tuple(429, 0), Item::JungleDoor},
  {std::make_tuple(430, 0), Item::AcaciaDoor},
  {std::make_tuple(431, 0), Item::DarkOakDoor},
  {std::make_tuple(432, 0), Item::ChorusFruit},
  {std::make_tuple(433, 0), Item::ChorusFruitPopped},
  {std::make_tuple(434, 0), Item::Beetroot},
  {std::make_tuple(435, 0), Item::BeetrootSeeds},
  {std::make_tuple(436, 0), Item::BeetrootSoup},
  {std::make_tuple(437, 0), Item::DragonBreath},
  {std::make_tuple(438, 0), Item::SplashPotion},
  {std::make_tuple(441, 0), Item::LingeringPotion},
  {std::make_tuple(443, 0), Item::Elytra},
  {std::make_tuple(450, 0), Item::ShulkerShell},
}
const std::map<std::tuple<std::string, short>, std::tuple<short, short>> JavaItemToPocketItem = {
    {std::make_tuple(256, 0), std::make_tuple(256, 0)},
    {std::make_tuple(257, 0), std::make_tuple(257, 0)},
    {std::make_tuple(258, 0), std::make_tuple(258, 0)},
    {std::make_tuple(259, 0), std::make_tuple(259, 0)},
    {std::make_tuple(260, 0), std::make_tuple(260, 0)},
    {std::make_tuple(261, 0), std::make_tuple(261, 0)},
    {std::make_tuple(262, 0), std::make_tuple(262, 0)},
    {std::make_tuple(263, 0), std::make_tuple(263, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(264, 0), std::make_tuple(264, 0)},
    {std::make_tuple(265, 0), std::make_tuple(265, 0)},
    {std::make_tuple(266, 0), std::make_tuple(266, 0)},
    {std::make_tuple(267, 0), std::make_tuple(267, 0)},
    {std::make_tuple(268, 0), std::make_tuple(268, 0)},
    {std::make_tuple(269, 0), std::make_tuple(269, 0)},
    {std::make_tuple(270, 0), std::make_tuple(270, 0)},
    {std::make_tuple(271, 0), std::make_tuple(271, 0)},
    {std::make_tuple(272, 0), std::make_tuple(272, 0)},
    {std::make_tuple(273, 0), std::make_tuple(273, 0)},
    {std::make_tuple(274, 0), std::make_tuple(274, 0)},
    {std::make_tuple(275, 0), std::make_tuple(275, 0)},
    {std::make_tuple(276, 0), std::make_tuple(276, 0)},
    {std::make_tuple(277, 0), std::make_tuple(277, 0)},
    {std::make_tuple(278, 0), std::make_tuple(278, 0)},
    {std::make_tuple(279, 0), std::make_tuple(279, 0)},
    {std::make_tuple(280, 0), std::make_tuple(280, 0)},
    {std::make_tuple(281, 0), std::make_tuple(281, 0)},
    {std::make_tuple(282, 0), std::make_tuple(282, 0)},
    {std::make_tuple(283, 0), std::make_tuple(283, 0)},
    {std::make_tuple(284, 0), std::make_tuple(284, 0)},
    {std::make_tuple(285, 0), std::make_tuple(285, 0)},
    {std::make_tuple(286, 0), std::make_tuple(286, 0)},
    {std::make_tuple(287, 0), std::make_tuple(287, 0)},
    {std::make_tuple(288, 0), std::make_tuple(288, 0)},
    {std::make_tuple(289, 0), std::make_tuple(289, 0)},
    {std::make_tuple(290, 0), std::make_tuple(290, 0)},
    {std::make_tuple(291, 0), std::make_tuple(291, 0)},
    {std::make_tuple(292, 0), std::make_tuple(292, 0)},
    {std::make_tuple(293, 0), std::make_tuple(293, 0)},
    {std::make_tuple(294, 0), std::make_tuple(294, 0)},
    {std::make_tuple(295, 0), std::make_tuple(295, 0)},
    {std::make_tuple(296, 0), std::make_tuple(296, 0)},
    {std::make_tuple(297, 0), std::make_tuple(297, 0)},
    {std::make_tuple(298, 0), std::make_tuple(298, 0)},
    {std::make_tuple(299, 0), std::make_tuple(299, 0)},
    {std::make_tuple(300, 0), std::make_tuple(300, 0)},
    {std::make_tuple(301, 0), std::make_tuple(301, 0)},
    {std::make_tuple(302, 0), std::make_tuple(302, 0)},
    {std::make_tuple(303, 0), std::make_tuple(303, 0)},
    {std::make_tuple(304, 0), std::make_tuple(304, 0)},
    {std::make_tuple(305, 0), std::make_tuple(305, 0)},
    {std::make_tuple(306, 0), std::make_tuple(306, 0)},
    {std::make_tuple(307, 0), std::make_tuple(307, 0)},
    {std::make_tuple(308, 0), std::make_tuple(308, 0)},
    {std::make_tuple(309, 0), std::make_tuple(309, 0)},
    {std::make_tuple(310, 0), std::make_tuple(310, 0)},
    {std::make_tuple(311, 0), std::make_tuple(311, 0)},
    {std::make_tuple(312, 0), std::make_tuple(312, 0)},
    {std::make_tuple(313, 0), std::make_tuple(313, 0)},
    {std::make_tuple(314, 0), std::make_tuple(314, 0)},
    {std::make_tuple(315, 0), std::make_tuple(315, 0)},
    {std::make_tuple(316, 0), std::make_tuple(316, 0)},
    {std::make_tuple(317, 0), std::make_tuple(317, 0)},
    {std::make_tuple(318, 0), std::make_tuple(318, 0)},
    {std::make_tuple(319, 0), std::make_tuple(319, 0)},
    {std::make_tuple(320, 0), std::make_tuple(320, 0)},
    {std::make_tuple(321, 0), std::make_tuple(321, 0)},
    {std::make_tuple(322, 0), std::make_tuple(322, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(323, 0), std::make_tuple(323, 0)},
    {std::make_tuple(324, 0), std::make_tuple(324, 0)},
    {std::make_tuple(325, 0), std::make_tuple(325, 0)},
    {std::make_tuple(328, 0), std::make_tuple(328, 0)},
    {std::make_tuple(329, 0), std::make_tuple(329, 0)},
    {std::make_tuple(330, 0), std::make_tuple(330, 0)},
    {std::make_tuple(331, 0), std::make_tuple(331, 0)},
    {std::make_tuple(332, 0), std::make_tuple(332, 0)},
    {std::make_tuple(333, 0), std::make_tuple(333, 0)},
    {std::make_tuple(334, 0), std::make_tuple(334, 0)},
    {std::make_tuple(336, 0), std::make_tuple(336, 0)},
    {std::make_tuple(337, 0), std::make_tuple(337, 0)},
    {std::make_tuple(338, 0), std::make_tuple(338, 0)},
    {std::make_tuple(339, 0), std::make_tuple(339, 0)},
    {std::make_tuple(340, 0), std::make_tuple(340, 0)},
    {std::make_tuple(341, 0), std::make_tuple(341, 0)},
    {std::make_tuple(342, 0), std::make_tuple(342, 0)},
    {std::make_tuple(344, 0), std::make_tuple(344, 0)},
    {std::make_tuple(345, 0), std::make_tuple(345, 0)},
    {std::make_tuple(346, 0), std::make_tuple(346, 0)},
    {std::make_tuple(347, 0), std::make_tuple(347, 0)},
    {std::make_tuple(348, 0), std::make_tuple(348, 0)},
    {std::make_tuple(349, 0), std::make_tuple(349, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(350, 0), std::make_tuple(350, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(351, 0), std::make_tuple(351, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(352, 0), std::make_tuple(352, 0)},
    {std::make_tuple(353, 0), std::make_tuple(353, 0)},
    {std::make_tuple(354, 0), std::make_tuple(354, 0)},
    {std::make_tuple(355, 0), std::make_tuple(355, 0)},
    {std::make_tuple(356, 0), std::make_tuple(356, 0)},
    {std::make_tuple(357, 0), std::make_tuple(357, 0)},
    {std::make_tuple(359, 0), std::make_tuple(359, 0)},
    {std::make_tuple(360, 0), std::make_tuple(360, 0)},
    {std::make_tuple(361, 0), std::make_tuple(361, 0)},
    {std::make_tuple(362, 0), std::make_tuple(362, 0)},
    {std::make_tuple(363, 0), std::make_tuple(363, 0)},
    {std::make_tuple(364, 0), std::make_tuple(364, 0)},
    {std::make_tuple(365, 0), std::make_tuple(365, 0)},
    {std::make_tuple(366, 0), std::make_tuple(366, 0)},
    {std::make_tuple(367, 0), std::make_tuple(367, 0)},
    {std::make_tuple(368, 0), std::make_tuple(368, 0)},
    {std::make_tuple(369, 0), std::make_tuple(369, 0)},
    {std::make_tuple(370, 0), std::make_tuple(370, 0)},
    {std::make_tuple(371, 0), std::make_tuple(371, 0)},
    {std::make_tuple(372, 0), std::make_tuple(372, 0)},
    {std::make_tuple(373, 0), std::make_tuple(373, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(374, 0), std::make_tuple(374, 0)},
    {std::make_tuple(375, 0), std::make_tuple(375, 0)},
    {std::make_tuple(376, 0), std::make_tuple(376, 0)},
    {std::make_tuple(377, 0), std::make_tuple(377, 0)},
    {std::make_tuple(378, 0), std::make_tuple(378, 0)},
    {std::make_tuple(379, 0), std::make_tuple(379, 0)},
    {std::make_tuple(380, 0), std::make_tuple(380, 0)},
    {std::make_tuple(381, 0), std::make_tuple(381, 0)},
    {std::make_tuple(382, 0), std::make_tuple(382, 0)},
    {std::make_tuple(383, 0), std::make_tuple(383, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(384, 0), std::make_tuple(384, 0)},
    {std::make_tuple(388, 0), std::make_tuple(388, 0)},
    {std::make_tuple(390, 0), std::make_tuple(390, 0)},
    {std::make_tuple(391, 0), std::make_tuple(391, 0)},
    {std::make_tuple(392, 0), std::make_tuple(392, 0)},
    {std::make_tuple(393, 0), std::make_tuple(393, 0)},
    {std::make_tuple(394, 0), std::make_tuple(394, 0)},
    {std::make_tuple(396, 0), std::make_tuple(396, 0)},
    {std::make_tuple(397, 0), std::make_tuple(397, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(400, 0), std::make_tuple(400, 0)},
    {std::make_tuple(403, 0), std::make_tuple(403, 0)},
    {std::make_tuple(404, 0), std::make_tuple(404, 0)},
    {std::make_tuple(405, 0), std::make_tuple(405, 0)},
    {std::make_tuple(406, 0), std::make_tuple(406, 0)},
    {std::make_tuple(407, 0), std::make_tuple(407, 0)},
    {std::make_tuple(408, 0), std::make_tuple(408, 0)},
    {std::make_tuple(409, 0), std::make_tuple(409, 0)},
    {std::make_tuple(410, 0), std::make_tuple(422, 0)},
    {std::make_tuple(411, 0), std::make_tuple(411, 0)},
    {std::make_tuple(412, 0), std::make_tuple(412, 0)},
    {std::make_tuple(413, 0), std::make_tuple(413, 0)},
    {std::make_tuple(414, 0), std::make_tuple(414, 0)},
    {std::make_tuple(415, 0), std::make_tuple(415, 0)},
    {std::make_tuple(420, 0), std::make_tuple(420, 0)},
    {std::make_tuple(426, 0), std::make_tuple(426, 0)},
    {std::make_tuple(427, 0), std::make_tuple(427, 0)},
    {std::make_tuple(428, 0), std::make_tuple(428, 0)},
    {std::make_tuple(429, 0), std::make_tuple(429, 0)},
    {std::make_tuple(430, 0), std::make_tuple(430, 0)},
    {std::make_tuple(431, 0), std::make_tuple(431, 0)},
    {std::make_tuple(432, 0), std::make_tuple(432, 0)},
    {std::make_tuple(433, 0), std::make_tuple(433, 0)},
    {std::make_tuple(434, 0), std::make_tuple(457, 0)},
    {std::make_tuple(435, 0), std::make_tuple(458, 0)},
    {std::make_tuple(436, 0), std::make_tuple(459, 0)},
    {std::make_tuple(437, 0), std::make_tuple(437, 0)},
    {std::make_tuple(438, 0), std::make_tuple(438, 0)},
    {std::make_tuple(441, 0), std::make_tuple(441, 0)},
    {std::make_tuple(443, 0), std::make_tuple(444, 0)},
    {std::make_tuple(450, 0), std::make_tuple(445, 0)},
}
} // NS Andromeda
/*
UNACCOUTNED FOR MCPE ITEMS
['    coal                                    0', '    charcoal                                1', '    bucket                                  0', '    milk                                    1', '    bucketWater                             8', '    bucketLava                              10', '    oak                                     0', '    spruce                                  1', '    birch                                   2', '    jungle                                  3', '    acacia                                  4', '    big_oak                                 5', '    black/ink_sac                         0', '    red                                     1', '    green                                   2', '    brown/cocoa_beans                     3', '    blue                                    4', '    purple                                  5', '    cyan                                    6', '    silver                                  7', '    gray                                    8', '    pink                                    9', '    lime                                    10', '    yellow                                  11', '    lightBlue                               12', '    magenta                                 13', '    orange                                  14', '    white/Bonemeal                        15', 'map_filled                      358', '    See Entities section for a list of data values', '    Note: all entities can be spawned using spawn eggs', 'fireball                        385', 'frame                           389', 'emptyMap                        395', '    skeleton                                0', '    wither                                  1', '    zombie                                  2', '    player                                  3', '    creeper                                 4', '    dragon                                  5', 'carrotOnAStick                  398', 'netherStar                      399', 'hopper                          410', 'horsearmorleather               416', 'horsearmoriron                  417', 'horsearmorgold                  418', 'horsearmordiamond               419', 'nameTag                         421', 'muttonRaw                       423', 'muttonCooked                    424', 'salmon                          460', 'clownfish                       461', 'pufferfish                      462', 'cooked_salmon                   463', 'appleEnchanted                  466']
*/
