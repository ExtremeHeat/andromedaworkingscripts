blocks_with_data_diffs = []
found = []

with open('C:/users/extreme/desktop/development/andromeda/PCPE BlockIDs2.csv') as f:
    csv_file_lines = f.read().splitlines()


def doPocketToJava():
	for line in csv_file_lines:
		if line[0] == "#":
			continue

		line_data = line.split(",")

		pc_block_name = line_data[0]
		pe_block_name = line_data[1]
		pc_block_id   = line_data[2]
		pe_block_id   = line_data[3]

		if pc_block_id != pe_block_id or "*" in pe_block_name:
			if pe_block_id == "255" or pe_block_id in found:
				continue
			print "case %s: // %s => %s" % (pe_block_id, pe_block_name, pc_block_name)
			found.append(pe_block_id)
			print "	blockArray[i] = %s;" % (pc_block_id)
			if "*" in pe_block_name or "$" in pe_block_name:
				print "	// BLOCK DATA DIFFERENCE! Do this manually."
    			print "	break;"


def doJavaToPocket():
	for line in csv_file_lines:
		if line[0] == "#":
			continue

		line_data = line.split(",")

		pc_block_name = line_data[0]
		pe_block_name = line_data[1]
		pc_block_id   = line_data[2]
		pe_block_id   = line_data[3]

		if pc_block_id != pe_block_id or "*" in pe_block_name:
			if pc_block_id == "255" or pc_block_id in found:
				continue
			print "case %s: // %s => %s" % (pc_block_id, pc_block_name, pe_block_name)
			found.append(pc_block_id)
			print "	outBlock = %s;" % ("pc_stash_id" if pe_block_id == "255" else pe_block_id)
			if pe_block_id == "255":
				print '	printf("%%d: Unimplemented block [%s/%s] could not be converted to pocket\\n", XYZ);' % (pc_block_name, pc_block_id)
			if "*" in pe_block_name or "$" in pe_block_name:
				print "	// BLOCK DATA DIFFERENCE! Do this manually."
    			print "	break;"



#doPocketToJava()
doJavaToPocket()