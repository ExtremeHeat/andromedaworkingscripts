blockentity_lines = []

with open('blockentities.txt') as f:
    blockentity_lines = f.read().splitlines()

print "#pragma once"
print ""
print "#include <map>"
print "#include <string>"
print "#include <tuple>"
print '#include "data/uniform/ABlockEntities.h"'
print '#include "Types.h"'
print "\n//This file was automatically generated\n"
print "namespace Andromeda {"
BEGIN_MULTIMAP = "const std::map<Tile, std::tuple<std::string, std::string>> UBlockEntities = {"
DATA_LINE = '	{Tile::%s, std::make_tuple("%s", "%s")},'
END = "};"
BEGIN_SWITCH = "// "
BEGIN_ENUM = "enum class Tile : BlockEntityID {"

enum_arr = []
datalines = []
pepc_imp = []
pe_cases = []
pc_cases = []
idpc_arr = []
idpe_arr = []

i=0
for line in blockentity_lines:
	if "#" in line:
		continue
	slices = line.split(" ")
	#print slices[0], slices[2]
	cla_name = ' '.join(slices[0].replace("_", ' ').split()).title().replace(" ", "")
	enum_arr.append("	%s," % cla_name)
	datalines.append(DATA_LINE % (cla_name, slices[0], slices[2]))
	i+=1
	pepc_imp.append('#include "blockentities/%sEntity.h"' % cla_name)
	idpc_arr.append('if (blockentity_name == "%s") return Tile::%s;' % (slices[0], cla_name));
	idpe_arr.append('if (blockentity_name == "%s") return Tile::%s;' % (slices[2], cla_name));
	pc_cases.append('if (blockentity_name == "%s") return new BlockEntities::%sEntity(nbt_tag);' % (slices[0], cla_name));
	pe_cases.append('if (blockentity_name == "%s") return new BlockEntities::%sEntity(nbt_tag);' % (slices[2], cla_name));


print BEGIN_ENUM
for i in enum_arr:
	print i
print END

print BEGIN_MULTIMAP
print '	pc, pe'
for dataline in datalines:
	print dataline
print END

print "/*\n"
for imp in pepc_imp:
	print imp
print ""
print "PC BLOCKENTITY NAME TO CLASS"
print BEGIN_SWITCH
for pc_case in pc_cases:
	print pc_case
#print END

print "PE BLOCKENTITY NAME TO CLASS"

print BEGIN_SWITCH
for pe_case in pe_cases:
	print pe_case
#print END

print "PC BLOCKENTITY NAME TO ID"

print BEGIN_SWITCH
for i in idpc_arr:
	print i
#print END

print "PE BLOCKENTITY NAME TO ID"

print BEGIN_SWITCH
for i in idpe_arr:
	print i
#print END

print "*/"

print END, "// Andromeda"