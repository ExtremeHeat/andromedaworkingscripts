#pragma once

#include <map>
#include <tuple>

#include "Types.h"

// This file was automatically generated

namespace Andromeda {
namespace Items {
const UItemName IronShovel = "iron_shovel";
const UItemName IronPickaxe = "iron_pickaxe";
const UItemName IronAxe = "iron_axe";
const UItemName FlintAndSteel = "flint_and_steel";
const UItemName Apple = "apple";
const UItemName Bow = "bow";
const UItemName Arrow = "arrow";
const UItemName Coal_ = "coal" USES METADATA;
const UItemName Diamond = "diamond";
const UItemName IronIngot = "iron_ingot";
const UItemName GoldIngot = "gold_ingot";
const UItemName IronSword = "iron_sword";
const UItemName WoodenSword = "wooden_sword";
const UItemName WoodenShovel = "wooden_shovel";
const UItemName WoodenPickaxe = "wooden_pickaxe";
const UItemName WoodenAxe = "wooden_axe";
const UItemName StoneSword = "stone_sword";
const UItemName StoneShovel = "stone_shovel";
const UItemName StonePickaxe = "stone_pickaxe";
const UItemName StoneAxe = "stone_axe";
const UItemName DiamondSword = "diamond_sword";
const UItemName DiamondShovel = "diamond_shovel";
const UItemName DiamondPickaxe = "diamond_pickaxe";
const UItemName DiamondAxe = "diamond_axe";
const UItemName Stick = "stick";
const UItemName Bowl = "bowl";
const UItemName MushroomStew = "mushroom_stew";
const UItemName GoldenSword = "golden_sword";
const UItemName GoldenShovel = "golden_shovel";
const UItemName GoldenPickaxe = "golden_pickaxe";
const UItemName GoldenAxe = "golden_axe";
const UItemName String = "string";
const UItemName Feather = "feather";
const UItemName Gunpowder = "gunpowder";
const UItemName WoodenHoe = "wooden_hoe";
const UItemName StoneHoe = "stone_hoe";
const UItemName IronHoe = "iron_hoe";
const UItemName DiamondHoe = "diamond_hoe";
const UItemName GoldenHoe = "golden_hoe";
const UItemName WheatSeeds = "wheat_seeds";
const UItemName Wheat = "wheat";
const UItemName Bread = "bread";
const UItemName LeatherHelmet = "leather_helmet";
const UItemName LeatherChestplate = "leather_chestplate";
const UItemName LeatherLeggings = "leather_leggings";
const UItemName LeatherBoots = "leather_boots";
const UItemName ChainmailHelmet = "chainmail_helmet";
const UItemName ChainmailChestplate = "chainmail_chestplate";
const UItemName ChainmailLeggings = "chainmail_leggings";
const UItemName ChainmailBoots = "chainmail_boots";
const UItemName IronHelmet = "iron_helmet";
const UItemName IronChestplate = "iron_chestplate";
const UItemName IronLeggings = "iron_leggings";
const UItemName IronBoots = "iron_boots";
const UItemName DiamondHelmet = "diamond_helmet";
const UItemName DiamondChestplate = "diamond_chestplate";
const UItemName DiamondLeggings = "diamond_leggings";
const UItemName DiamondBoots = "diamond_boots";
const UItemName GoldenHelmet = "golden_helmet";
const UItemName GoldenChestplate = "golden_chestplate";
const UItemName GoldenLeggings = "golden_leggings";
const UItemName GoldenBoots = "golden_boots";
const UItemName Flint = "flint";
const UItemName Porkchop = "porkchop";
const UItemName CookedPorkchop = "cooked_porkchop";
const UItemName Painting = "painting";
const UItemName GoldenApple_ = "golden_apple" USES METADATA;
const UItemName Sign = "sign";
const UItemName WoodenDoor = "wooden_door";
const UItemName Bucket = "bucket";
const UItemName WaterBucket = DO THIS MANUALLY;
const UItemName LavaBucket = DO THIS MANUALLY;
const UItemName Minecart = "minecart";
const UItemName Saddle = "saddle";
const UItemName IronDoor = "iron_door";
const UItemName Redstone = "redstone";
const UItemName Snowball = "snowball";
const UItemName Boat = "boat";
const UItemName Leather = "leather";
const UItemName MilkBucket = DO THIS MANUALLY;
const UItemName Brick = "brick";
const UItemName ClayBall = "clay_ball";
const UItemName Reeds = "reeds";
const UItemName Paper = "paper";
const UItemName Book = "book";
const UItemName SlimeBall = "slime_ball";
const UItemName ChestMinecart = "chest_minecart";
const UItemName FurnaceMinecart = DO THIS MANUALLY;
const UItemName Egg = "egg";
const UItemName Compass = "compass";
const UItemName FishingRod = "fishing_rod";
const UItemName Clock = "clock";
const UItemName GlowstoneDust = "glowstone_dust";
const UItemName Fish_ = "fish" USES METADATA;
const UItemName CookedFish_ = "cooked_fish" USES METADATA;
const UItemName Dye_ = "dye" USES METADATA;
const UItemName Bone = "bone";
const UItemName Sugar = "sugar";
const UItemName Cake = "cake";
const UItemName Bed = "bed";
const UItemName Repeater = "repeater";
const UItemName Cookie = "cookie";
const UItemName FilledMap_ = USES METADATA;
const UItemName Shears = "shears";
const UItemName Melon = "melon";
const UItemName PumpkinSeeds = "pumpkin_seeds";
const UItemName MelonSeeds = "melon_seeds";
const UItemName Beef = "beef";
const UItemName CookedBeef = "cooked_beef";
const UItemName Chicken = "chicken";
const UItemName CookedChicken = "cooked_chicken";
const UItemName RottenFlesh = "rotten_flesh";
const UItemName EnderPearl = "ender_pearl";
const UItemName BlazeRod = "blaze_rod";
const UItemName GhastTear = "ghast_tear";
const UItemName GoldNugget = "gold_nugget";
const UItemName NetherWart = "nether_wart";
const UItemName Potion_ = "potion" USES METADATA;
const UItemName GlassBottle = "glass_bottle";
const UItemName SpiderEye = "spider_eye";
const UItemName FermentedSpiderEye = "fermented_spider_eye";
const UItemName BlazePowder = "blaze_powder";
const UItemName MagmaCream = "magma_cream";
const UItemName BrewingStand = "brewing_stand";
const UItemName Cauldron = "cauldron";
const UItemName EnderEye = "ender_eye";
const UItemName SpeckledMelon = "speckled_melon";
const UItemName SpawnEgg_ = "spawn_egg" USES METADATA;
const UItemName ExperienceBottle = "experience_bottle";
const UItemName FireCharge = DO THIS MANUALLY;
const UItemName WritableBook = DO THIS MANUALLY;
const UItemName WrittenBook = DO THIS MANUALLY;
const UItemName Emerald = "emerald";
const UItemName ItemFrame = DO THIS MANUALLY;
const UItemName FlowerPot = "flower_pot";
const UItemName Carrot = "carrot";
const UItemName Potato = "potato";
const UItemName BakedPotato = "baked_potato";
const UItemName PoisonousPotato = "poisonous_potato";
const UItemName Map = DO THIS MANUALLY;
const UItemName GoldenCarrot = "golden_carrot";
const UItemName Skull_ = "skull" USES METADATA;
const UItemName CarrotOnAStick = DO THIS MANUALLY;
const UItemName NetherStar = DO THIS MANUALLY;
const UItemName PumpkinPie = "pumpkin_pie";
const UItemName Fireworks = DO THIS MANUALLY;
const UItemName FireworkCharge = DO THIS MANUALLY;
const UItemName EnchantedBook = "enchanted_book";
const UItemName Comparator = "comparator";
const UItemName Netherbrick = "netherbrick";
const UItemName Quartz = "quartz";
const UItemName TntMinecart = "tnt_minecart";
const UItemName HopperMinecart = "hopper_minecart";
const UItemName PrismarineShard = "prismarine_shard";
const UItemName PrismarineCrystals = "prismarine_crystals";
const UItemName Rabbit = "rabbit";
const UItemName CookedRabbit = "cooked_rabbit";
const UItemName RabbitStew = "rabbit_stew";
const UItemName RabbitFoot = "rabbit_foot";
const UItemName RabbitHide = "rabbit_hide";
const UItemName ArmorStand = DO THIS MANUALLY;
const UItemName IronHorseArmor = DO THIS MANUALLY;
const UItemName GoldenHorseArmor = DO THIS MANUALLY;
const UItemName DiamondHorseArmor = DO THIS MANUALLY;
const UItemName Lead = "lead";
const UItemName NameTag = DO THIS MANUALLY;
const UItemName CommandBlockMinecart = DO THIS MANUALLY;
const UItemName Mutton = DO THIS MANUALLY;
const UItemName CookedMutton = DO THIS MANUALLY;
const UItemName Banner = DO THIS MANUALLY;
const UItemName EndCrystal = "end_crystal";
const UItemName SpruceDoor = "spruce_door";
const UItemName BirchDoor = "birch_door";
const UItemName JungleDoor = "jungle_door";
const UItemName AcaciaDoor = "acacia_door";
const UItemName DarkOakDoor = "dark_oak_door";
const UItemName ChorusFruit = "chorus_fruit";
const UItemName ChorusFruitPopped = "chorus_fruit_popped";
const UItemName Beetroot = "beetroot";
const UItemName BeetrootSeeds = "beetroot_seeds";
const UItemName BeetrootSoup = "beetroot_soup";
const UItemName DragonBreath = "dragon_breath";
const UItemName SplashPotion = "splash_potion";
const UItemName SpectralArrow = DO THIS MANUALLY;
const UItemName TippedArrow = DO THIS MANUALLY;
const UItemName LingeringPotion = "lingering_potion";
const UItemName Shield = DO THIS MANUALLY;
const UItemName Elytra = "elytra";
const UItemName SpruceBoat = DO THIS MANUALLY;
const UItemName BirchBoat = DO THIS MANUALLY;
const UItemName JungleBoat = DO THIS MANUALLY;
const UItemName AcaciaBoat = DO THIS MANUALLY;
const UItemName DarkOakBoat = DO THIS MANUALLY;
const UItemName TotemOfUndying = DO THIS MANUALLY;
const UItemName ShulkerShell = "shulker_shell";
// skipped  451	1C3		(unused)
const UItemName IronNugget = DO THIS MANUALLY;
const UItemName Record13 = DO THIS MANUALLY;
const UItemName RecordCat = DO THIS MANUALLY;
const UItemName RecordBlocks = DO THIS MANUALLY;
const UItemName RecordChirp = DO THIS MANUALLY;
const UItemName RecordFar = DO THIS MANUALLY;
const UItemName RecordMall = DO THIS MANUALLY;
const UItemName RecordMellohi = DO THIS MANUALLY;
const UItemName RecordStal = DO THIS MANUALLY;
const UItemName RecordStrad = DO THIS MANUALLY;
const UItemName RecordWard = DO THIS MANUALLY;
const UItemName Record11 = DO THIS MANUALLY;
const UItemName RecordWait = DO THIS MANUALLY;
} // NS Andromeda::Items
#pragma region
#define ITEM_IRONSHOVEL                (16777472 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_IRONPICKAXE               (16843009 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_IRONAXE                   (16908546 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_FLINTANDSTEEL             (16974083 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_APPLE                     (17039620 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BOW                       (17105157 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_ARROW                     (17170694 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_COAL_                     (17236231 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_DIAMOND                   (17301768 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_IRONINGOT                 (17367305 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_GOLDINGOT                 (17432842 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_IRONSWORD                 (17498379 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_WOODENSWORD               (17563916 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_WOODENSHOVEL              (17629453 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_WOODENPICKAXE             (17694990 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_WOODENAXE                 (17760527 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_STONESWORD                (17826064 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_STONESHOVEL               (17891601 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_STONEPICKAXE              (17957138 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_STONEAXE                  (18022675 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_DIAMONDSWORD              (18088212 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_DIAMONDSHOVEL             (18153749 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_DIAMONDPICKAXE            (18219286 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_DIAMONDAXE                (18284823 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_STICK                     (18350360 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BOWL                      (18415897 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_MUSHROOMSTEW              (18481434 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_GOLDENSWORD               (18546971 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_GOLDENSHOVEL              (18612508 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_GOLDENPICKAXE             (18678045 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_GOLDENAXE                 (18743582 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_STRING                    (18809119 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_FEATHER                   (18874656 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_GUNPOWDER                 (18940193 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_WOODENHOE                 (19005730 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_STONEHOE                  (19071267 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_IRONHOE                   (19136804 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_DIAMONDHOE                (19202341 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_GOLDENHOE                 (19267878 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_WHEATSEEDS                (19333415 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_WHEAT                     (19398952 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BREAD                     (19464489 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_LEATHERHELMET             (19530026 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_LEATHERCHESTPLATE         (19595563 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_LEATHERLEGGINGS           (19661100 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_LEATHERBOOTS              (19726637 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_CHAINMAILHELMET           (19792174 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_CHAINMAILCHESTPLATE       (19857711 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_CHAINMAILLEGGINGS         (19923248 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_CHAINMAILBOOTS            (19988785 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_IRONHELMET                (20054322 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_IRONCHESTPLATE            (20119859 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_IRONLEGGINGS              (20185396 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_IRONBOOTS                 (20250933 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_DIAMONDHELMET             (20316470 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_DIAMONDCHESTPLATE         (20382007 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_DIAMONDLEGGINGS           (20447544 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_DIAMONDBOOTS              (20513081 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_GOLDENHELMET              (20578618 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_GOLDENCHESTPLATE          (20644155 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_GOLDENLEGGINGS            (20709692 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_GOLDENBOOTS               (20775229 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_FLINT                     (20840766 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_PORKCHOP                  (20906303 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_COOKEDPORKCHOP            (20971840 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_PAINTING                  (21037377 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_GOLDENAPPLE_              (21102914 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_SIGN                      (21168451 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_WOODENDOOR                (21233988 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BUCKET                    (21299525 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_MINECART                  (21496136 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_SADDLE                    (21561673 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_IRONDOOR                  (21627210 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_REDSTONE                  (21692747 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_SNOWBALL                  (21758284 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BOAT                      (21823821 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_LEATHER                   (21889358 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BRICK                     (22020432 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_CLAYBALL                  (22085969 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_REEDS                     (22151506 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_PAPER                     (22217043 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BOOK                      (22282580 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_SLIMEBALL                 (22348117 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_CHESTMINECART             (22413654 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_EGG                       (22544728 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_COMPASS                   (22610265 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_FISHINGROD                (22675802 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_CLOCK                     (22741339 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_GLOWSTONEDUST             (22806876 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_FISH_                     (22872413 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_COOKEDFISH_               (22937950 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_DYE_                      (23003487 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BONE                      (23069024 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_SUGAR                     (23134561 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_CAKE                      (23200098 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BED                       (23265635 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_REPEATER                  (23331172 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_COOKIE                    (23396709 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_SHEARS                    (23527783 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_MELON                     (23593320 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_PUMPKINSEEDS              (23658857 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_MELONSEEDS                (23724394 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BEEF                      (23789931 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_COOKEDBEEF                (23855468 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_CHICKEN                   (23921005 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_COOKEDCHICKEN             (23986542 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_ROTTENFLESH               (24052079 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_ENDERPEARL                (24117616 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BLAZEROD                  (24183153 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_GHASTTEAR                 (24248690 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_GOLDNUGGET                (24314227 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_NETHERWART                (24379764 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_POTION_                   (24445301 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_GLASSBOTTLE               (24510838 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_SPIDEREYE                 (24576375 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_FERMENTEDSPIDEREYE        (24641912 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BLAZEPOWDER               (24707449 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_MAGMACREAM                (24772986 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BREWINGSTAND              (24838523 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_CAULDRON                  (24904060 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_ENDEREYE                  (24969597 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_SPECKLEDMELON             (25035134 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_SPAWNEGG_                 (25100671 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_EXPERIENCEBOTTLE          (25166208 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_EMERALD                   (25428356 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_FLOWERPOT                 (25559430 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_CARROT                    (25624967 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_POTATO                    (25690504 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BAKEDPOTATO               (25756041 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_POISONOUSPOTATO           (25821578 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_GOLDENCARROT              (25952652 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_SKULL_                    (26018189 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_PUMPKINPIE                (26214800 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_ENCHANTEDBOOK             (26411411 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_COMPARATOR                (26476948 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_NETHERBRICK               (26542485 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_QUARTZ                    (26608022 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_TNTMINECART               (26673559 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_HOPPERMINECART            (26739096 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_PRISMARINESHARD           (26804633 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_PRISMARINECRYSTALS        (26870182 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_RABBIT                    (26935707 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_COOKEDRABBIT              (27001244 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_RABBITSTEW                (27066781 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_RABBITFOOT                (27132318 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_RABBITHIDE                (27197855 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_LEAD                      (27525540 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_ENDCRYSTAL                (27918762 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_SPRUCEDOOR                (27984299 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BIRCHDOOR                 (28049836 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_JUNGLEDOOR                (28115373 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_ACACIADOOR                (28180910 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_DARKOAKDOOR               (28246447 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_CHORUSFRUIT               (28311984 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_CHORUSFRUITPOPPED         (28377521 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BEETROOT                  (28443081 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BEETROOTSEEDS             (28508618 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_BEETROOTSOUP              (28574155 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_DRAGONBREATH              (28639669 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_SPLASHPOTION              (28705206 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_LINGERINGPOTION           (28901817 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_ELYTRA                    (29032892 << 32) | ((0 << 16) | 0 & 0xFFFF)
#define ITEM_SHULKERSHELL              (29491645 << 32) | ((0 << 16) | 0 & 0xFFFF)
#pragma endregion
const std::map<UItemName, UItemID> = {
     {Items::IronShovel, ITEM_IRONSHOVEL},
     {Items::IronPickaxe, ITEM_IRONPICKAXE},
     {Items::IronAxe, ITEM_IRONAXE},
     {Items::FlintAndSteel, ITEM_FLINTANDSTEEL},
     {Items::Apple, ITEM_APPLE},
     {Items::Bow, ITEM_BOW},
     {Items::Arrow, ITEM_ARROW},
     {Items::Coal_, ITEM_COAL_},// NEEDS MANUAL METADATA ADDITIONS
     {Items::Diamond, ITEM_DIAMOND},
     {Items::IronIngot, ITEM_IRONINGOT},
     {Items::GoldIngot, ITEM_GOLDINGOT},
     {Items::IronSword, ITEM_IRONSWORD},
     {Items::WoodenSword, ITEM_WOODENSWORD},
     {Items::WoodenShovel, ITEM_WOODENSHOVEL},
     {Items::WoodenPickaxe, ITEM_WOODENPICKAXE},
     {Items::WoodenAxe, ITEM_WOODENAXE},
     {Items::StoneSword, ITEM_STONESWORD},
     {Items::StoneShovel, ITEM_STONESHOVEL},
     {Items::StonePickaxe, ITEM_STONEPICKAXE},
     {Items::StoneAxe, ITEM_STONEAXE},
     {Items::DiamondSword, ITEM_DIAMONDSWORD},
     {Items::DiamondShovel, ITEM_DIAMONDSHOVEL},
     {Items::DiamondPickaxe, ITEM_DIAMONDPICKAXE},
     {Items::DiamondAxe, ITEM_DIAMONDAXE},
     {Items::Stick, ITEM_STICK},
     {Items::Bowl, ITEM_BOWL},
     {Items::MushroomStew, ITEM_MUSHROOMSTEW},
     {Items::GoldenSword, ITEM_GOLDENSWORD},
     {Items::GoldenShovel, ITEM_GOLDENSHOVEL},
     {Items::GoldenPickaxe, ITEM_GOLDENPICKAXE},
     {Items::GoldenAxe, ITEM_GOLDENAXE},
     {Items::String, ITEM_STRING},
     {Items::Feather, ITEM_FEATHER},
     {Items::Gunpowder, ITEM_GUNPOWDER},
     {Items::WoodenHoe, ITEM_WOODENHOE},
     {Items::StoneHoe, ITEM_STONEHOE},
     {Items::IronHoe, ITEM_IRONHOE},
     {Items::DiamondHoe, ITEM_DIAMONDHOE},
     {Items::GoldenHoe, ITEM_GOLDENHOE},
     {Items::WheatSeeds, ITEM_WHEATSEEDS},
     {Items::Wheat, ITEM_WHEAT},
     {Items::Bread, ITEM_BREAD},
     {Items::LeatherHelmet, ITEM_LEATHERHELMET},
     {Items::LeatherChestplate, ITEM_LEATHERCHESTPLATE},
     {Items::LeatherLeggings, ITEM_LEATHERLEGGINGS},
     {Items::LeatherBoots, ITEM_LEATHERBOOTS},
     {Items::ChainmailHelmet, ITEM_CHAINMAILHELMET},
     {Items::ChainmailChestplate, ITEM_CHAINMAILCHESTPLATE},
     {Items::ChainmailLeggings, ITEM_CHAINMAILLEGGINGS},
     {Items::ChainmailBoots, ITEM_CHAINMAILBOOTS},
     {Items::IronHelmet, ITEM_IRONHELMET},
     {Items::IronChestplate, ITEM_IRONCHESTPLATE},
     {Items::IronLeggings, ITEM_IRONLEGGINGS},
     {Items::IronBoots, ITEM_IRONBOOTS},
     {Items::DiamondHelmet, ITEM_DIAMONDHELMET},
     {Items::DiamondChestplate, ITEM_DIAMONDCHESTPLATE},
     {Items::DiamondLeggings, ITEM_DIAMONDLEGGINGS},
     {Items::DiamondBoots, ITEM_DIAMONDBOOTS},
     {Items::GoldenHelmet, ITEM_GOLDENHELMET},
     {Items::GoldenChestplate, ITEM_GOLDENCHESTPLATE},
     {Items::GoldenLeggings, ITEM_GOLDENLEGGINGS},
     {Items::GoldenBoots, ITEM_GOLDENBOOTS},
     {Items::Flint, ITEM_FLINT},
     {Items::Porkchop, ITEM_PORKCHOP},
     {Items::CookedPorkchop, ITEM_COOKEDPORKCHOP},
     {Items::Painting, ITEM_PAINTING},
     {Items::GoldenApple_, ITEM_GOLDENAPPLE_},// NEEDS MANUAL METADATA ADDITIONS
     {Items::Sign, ITEM_SIGN},
     {Items::WoodenDoor, ITEM_WOODENDOOR},
     {Items::Bucket, ITEM_BUCKET},
     {Items::Minecart, ITEM_MINECART},
     {Items::Saddle, ITEM_SADDLE},
     {Items::IronDoor, ITEM_IRONDOOR},
     {Items::Redstone, ITEM_REDSTONE},
     {Items::Snowball, ITEM_SNOWBALL},
     {Items::Boat, ITEM_BOAT},
     {Items::Leather, ITEM_LEATHER},
     {Items::Brick, ITEM_BRICK},
     {Items::ClayBall, ITEM_CLAYBALL},
     {Items::Reeds, ITEM_REEDS},
     {Items::Paper, ITEM_PAPER},
     {Items::Book, ITEM_BOOK},
     {Items::SlimeBall, ITEM_SLIMEBALL},
     {Items::ChestMinecart, ITEM_CHESTMINECART},
     {Items::Egg, ITEM_EGG},
     {Items::Compass, ITEM_COMPASS},
     {Items::FishingRod, ITEM_FISHINGROD},
     {Items::Clock, ITEM_CLOCK},
     {Items::GlowstoneDust, ITEM_GLOWSTONEDUST},
     {Items::Fish_, ITEM_FISH_},// NEEDS MANUAL METADATA ADDITIONS
     {Items::CookedFish_, ITEM_COOKEDFISH_},// NEEDS MANUAL METADATA ADDITIONS
     {Items::Dye_, ITEM_DYE_},// NEEDS MANUAL METADATA ADDITIONS
     {Items::Bone, ITEM_BONE},
     {Items::Sugar, ITEM_SUGAR},
     {Items::Cake, ITEM_CAKE},
     {Items::Bed, ITEM_BED},
     {Items::Repeater, ITEM_REPEATER},
     {Items::Cookie, ITEM_COOKIE},
     {Items::Shears, ITEM_SHEARS},
     {Items::Melon, ITEM_MELON},
     {Items::PumpkinSeeds, ITEM_PUMPKINSEEDS},
     {Items::MelonSeeds, ITEM_MELONSEEDS},
     {Items::Beef, ITEM_BEEF},
     {Items::CookedBeef, ITEM_COOKEDBEEF},
     {Items::Chicken, ITEM_CHICKEN},
     {Items::CookedChicken, ITEM_COOKEDCHICKEN},
     {Items::RottenFlesh, ITEM_ROTTENFLESH},
     {Items::EnderPearl, ITEM_ENDERPEARL},
     {Items::BlazeRod, ITEM_BLAZEROD},
     {Items::GhastTear, ITEM_GHASTTEAR},
     {Items::GoldNugget, ITEM_GOLDNUGGET},
     {Items::NetherWart, ITEM_NETHERWART},
     {Items::Potion_, ITEM_POTION_},// NEEDS MANUAL METADATA ADDITIONS
     {Items::GlassBottle, ITEM_GLASSBOTTLE},
     {Items::SpiderEye, ITEM_SPIDEREYE},
     {Items::FermentedSpiderEye, ITEM_FERMENTEDSPIDEREYE},
     {Items::BlazePowder, ITEM_BLAZEPOWDER},
     {Items::MagmaCream, ITEM_MAGMACREAM},
     {Items::BrewingStand, ITEM_BREWINGSTAND},
     {Items::Cauldron, ITEM_CAULDRON},
     {Items::EnderEye, ITEM_ENDEREYE},
     {Items::SpeckledMelon, ITEM_SPECKLEDMELON},
     {Items::SpawnEgg_, ITEM_SPAWNEGG_},// NEEDS MANUAL METADATA ADDITIONS
     {Items::ExperienceBottle, ITEM_EXPERIENCEBOTTLE},
     {Items::Emerald, ITEM_EMERALD},
     {Items::FlowerPot, ITEM_FLOWERPOT},
     {Items::Carrot, ITEM_CARROT},
     {Items::Potato, ITEM_POTATO},
     {Items::BakedPotato, ITEM_BAKEDPOTATO},
     {Items::PoisonousPotato, ITEM_POISONOUSPOTATO},
     {Items::GoldenCarrot, ITEM_GOLDENCARROT},
     {Items::Skull_, ITEM_SKULL_},// NEEDS MANUAL METADATA ADDITIONS
     {Items::PumpkinPie, ITEM_PUMPKINPIE},
     {Items::EnchantedBook, ITEM_ENCHANTEDBOOK},
     {Items::Comparator, ITEM_COMPARATOR},
     {Items::Netherbrick, ITEM_NETHERBRICK},
     {Items::Quartz, ITEM_QUARTZ},
     {Items::TntMinecart, ITEM_TNTMINECART},
     {Items::HopperMinecart, ITEM_HOPPERMINECART},
     {Items::PrismarineShard, ITEM_PRISMARINESHARD},
     {Items::PrismarineCrystals, ITEM_PRISMARINECRYSTALS},
     {Items::Rabbit, ITEM_RABBIT},
     {Items::CookedRabbit, ITEM_COOKEDRABBIT},
     {Items::RabbitStew, ITEM_RABBITSTEW},
     {Items::RabbitFoot, ITEM_RABBITFOOT},
     {Items::RabbitHide, ITEM_RABBITHIDE},
     {Items::Lead, ITEM_LEAD},
     {Items::EndCrystal, ITEM_ENDCRYSTAL},
     {Items::SpruceDoor, ITEM_SPRUCEDOOR},
     {Items::BirchDoor, ITEM_BIRCHDOOR},
     {Items::JungleDoor, ITEM_JUNGLEDOOR},
     {Items::AcaciaDoor, ITEM_ACACIADOOR},
     {Items::DarkOakDoor, ITEM_DARKOAKDOOR},
     {Items::ChorusFruit, ITEM_CHORUSFRUIT},
     {Items::ChorusFruitPopped, ITEM_CHORUSFRUITPOPPED},
     {Items::Beetroot, ITEM_BEETROOT},
     {Items::BeetrootSeeds, ITEM_BEETROOTSEEDS},
     {Items::BeetrootSoup, ITEM_BEETROOTSOUP},
     {Items::DragonBreath, ITEM_DRAGONBREATH},
     {Items::SplashPotion, ITEM_SPLASHPOTION},
     {Items::LingeringPotion, ITEM_LINGERINGPOTION},
     {Items::Elytra, ITEM_ELYTRA},
     {Items::ShulkerShell, ITEM_SHULKERSHELL},
}
const std::map<std::tuple<short, short>, UItemID> PocketItemToUItemID = {
  {std::make_tuple(256, 0), ITEM_IRONSHOVEL},
  {std::make_tuple(257, 0), ITEM_IRONPICKAXE},
  {std::make_tuple(258, 0), ITEM_IRONAXE},
  {std::make_tuple(259, 0), ITEM_FLINTANDSTEEL},
  {std::make_tuple(260, 0), ITEM_APPLE},
  {std::make_tuple(261, 0), ITEM_BOW},
  {std::make_tuple(262, 0), ITEM_ARROW},
  {std::make_tuple(263, 0), ITEM_COAL_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(264, 0), ITEM_DIAMOND},
  {std::make_tuple(265, 0), ITEM_IRONINGOT},
  {std::make_tuple(266, 0), ITEM_GOLDINGOT},
  {std::make_tuple(267, 0), ITEM_IRONSWORD},
  {std::make_tuple(268, 0), ITEM_WOODENSWORD},
  {std::make_tuple(269, 0), ITEM_WOODENSHOVEL},
  {std::make_tuple(270, 0), ITEM_WOODENPICKAXE},
  {std::make_tuple(271, 0), ITEM_WOODENAXE},
  {std::make_tuple(272, 0), ITEM_STONESWORD},
  {std::make_tuple(273, 0), ITEM_STONESHOVEL},
  {std::make_tuple(274, 0), ITEM_STONEPICKAXE},
  {std::make_tuple(275, 0), ITEM_STONEAXE},
  {std::make_tuple(276, 0), ITEM_DIAMONDSWORD},
  {std::make_tuple(277, 0), ITEM_DIAMONDSHOVEL},
  {std::make_tuple(278, 0), ITEM_DIAMONDPICKAXE},
  {std::make_tuple(279, 0), ITEM_DIAMONDAXE},
  {std::make_tuple(280, 0), ITEM_STICK},
  {std::make_tuple(281, 0), ITEM_BOWL},
  {std::make_tuple(282, 0), ITEM_MUSHROOMSTEW},
  {std::make_tuple(283, 0), ITEM_GOLDENSWORD},
  {std::make_tuple(284, 0), ITEM_GOLDENSHOVEL},
  {std::make_tuple(285, 0), ITEM_GOLDENPICKAXE},
  {std::make_tuple(286, 0), ITEM_GOLDENAXE},
  {std::make_tuple(287, 0), ITEM_STRING},
  {std::make_tuple(288, 0), ITEM_FEATHER},
  {std::make_tuple(289, 0), ITEM_GUNPOWDER},
  {std::make_tuple(290, 0), ITEM_WOODENHOE},
  {std::make_tuple(291, 0), ITEM_STONEHOE},
  {std::make_tuple(292, 0), ITEM_IRONHOE},
  {std::make_tuple(293, 0), ITEM_DIAMONDHOE},
  {std::make_tuple(294, 0), ITEM_GOLDENHOE},
  {std::make_tuple(295, 0), ITEM_WHEATSEEDS},
  {std::make_tuple(296, 0), ITEM_WHEAT},
  {std::make_tuple(297, 0), ITEM_BREAD},
  {std::make_tuple(298, 0), ITEM_LEATHERHELMET},
  {std::make_tuple(299, 0), ITEM_LEATHERCHESTPLATE},
  {std::make_tuple(300, 0), ITEM_LEATHERLEGGINGS},
  {std::make_tuple(301, 0), ITEM_LEATHERBOOTS},
  {std::make_tuple(302, 0), ITEM_CHAINMAILHELMET},
  {std::make_tuple(303, 0), ITEM_CHAINMAILCHESTPLATE},
  {std::make_tuple(304, 0), ITEM_CHAINMAILLEGGINGS},
  {std::make_tuple(305, 0), ITEM_CHAINMAILBOOTS},
  {std::make_tuple(306, 0), ITEM_IRONHELMET},
  {std::make_tuple(307, 0), ITEM_IRONCHESTPLATE},
  {std::make_tuple(308, 0), ITEM_IRONLEGGINGS},
  {std::make_tuple(309, 0), ITEM_IRONBOOTS},
  {std::make_tuple(310, 0), ITEM_DIAMONDHELMET},
  {std::make_tuple(311, 0), ITEM_DIAMONDCHESTPLATE},
  {std::make_tuple(312, 0), ITEM_DIAMONDLEGGINGS},
  {std::make_tuple(313, 0), ITEM_DIAMONDBOOTS},
  {std::make_tuple(314, 0), ITEM_GOLDENHELMET},
  {std::make_tuple(315, 0), ITEM_GOLDENCHESTPLATE},
  {std::make_tuple(316, 0), ITEM_GOLDENLEGGINGS},
  {std::make_tuple(317, 0), ITEM_GOLDENBOOTS},
  {std::make_tuple(318, 0), ITEM_FLINT},
  {std::make_tuple(319, 0), ITEM_PORKCHOP},
  {std::make_tuple(320, 0), ITEM_COOKEDPORKCHOP},
  {std::make_tuple(321, 0), ITEM_PAINTING},
  {std::make_tuple(322, 0), ITEM_GOLDENAPPLE_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(323, 0), ITEM_SIGN},
  {std::make_tuple(324, 0), ITEM_WOODENDOOR},
  {std::make_tuple(325, 0), ITEM_BUCKET},
  {std::make_tuple(328, 0), ITEM_MINECART},
  {std::make_tuple(329, 0), ITEM_SADDLE},
  {std::make_tuple(330, 0), ITEM_IRONDOOR},
  {std::make_tuple(331, 0), ITEM_REDSTONE},
  {std::make_tuple(332, 0), ITEM_SNOWBALL},
  {std::make_tuple(333, 0), ITEM_BOAT},
  {std::make_tuple(334, 0), ITEM_LEATHER},
  {std::make_tuple(336, 0), ITEM_BRICK},
  {std::make_tuple(337, 0), ITEM_CLAYBALL},
  {std::make_tuple(338, 0), ITEM_REEDS},
  {std::make_tuple(339, 0), ITEM_PAPER},
  {std::make_tuple(340, 0), ITEM_BOOK},
  {std::make_tuple(341, 0), ITEM_SLIMEBALL},
  {std::make_tuple(342, 0), ITEM_CHESTMINECART},
  {std::make_tuple(344, 0), ITEM_EGG},
  {std::make_tuple(345, 0), ITEM_COMPASS},
  {std::make_tuple(346, 0), ITEM_FISHINGROD},
  {std::make_tuple(347, 0), ITEM_CLOCK},
  {std::make_tuple(348, 0), ITEM_GLOWSTONEDUST},
  {std::make_tuple(349, 0), ITEM_FISH_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(350, 0), ITEM_COOKEDFISH_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(351, 0), ITEM_DYE_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(352, 0), ITEM_BONE},
  {std::make_tuple(353, 0), ITEM_SUGAR},
  {std::make_tuple(354, 0), ITEM_CAKE},
  {std::make_tuple(355, 0), ITEM_BED},
  {std::make_tuple(356, 0), ITEM_REPEATER},
  {std::make_tuple(357, 0), ITEM_COOKIE},
  {std::make_tuple(359, 0), ITEM_SHEARS},
  {std::make_tuple(360, 0), ITEM_MELON},
  {std::make_tuple(361, 0), ITEM_PUMPKINSEEDS},
  {std::make_tuple(362, 0), ITEM_MELONSEEDS},
  {std::make_tuple(363, 0), ITEM_BEEF},
  {std::make_tuple(364, 0), ITEM_COOKEDBEEF},
  {std::make_tuple(365, 0), ITEM_CHICKEN},
  {std::make_tuple(366, 0), ITEM_COOKEDCHICKEN},
  {std::make_tuple(367, 0), ITEM_ROTTENFLESH},
  {std::make_tuple(368, 0), ITEM_ENDERPEARL},
  {std::make_tuple(369, 0), ITEM_BLAZEROD},
  {std::make_tuple(370, 0), ITEM_GHASTTEAR},
  {std::make_tuple(371, 0), ITEM_GOLDNUGGET},
  {std::make_tuple(372, 0), ITEM_NETHERWART},
  {std::make_tuple(373, 0), ITEM_POTION_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(374, 0), ITEM_GLASSBOTTLE},
  {std::make_tuple(375, 0), ITEM_SPIDEREYE},
  {std::make_tuple(376, 0), ITEM_FERMENTEDSPIDEREYE},
  {std::make_tuple(377, 0), ITEM_BLAZEPOWDER},
  {std::make_tuple(378, 0), ITEM_MAGMACREAM},
  {std::make_tuple(379, 0), ITEM_BREWINGSTAND},
  {std::make_tuple(380, 0), ITEM_CAULDRON},
  {std::make_tuple(381, 0), ITEM_ENDEREYE},
  {std::make_tuple(382, 0), ITEM_SPECKLEDMELON},
  {std::make_tuple(383, 0), ITEM_SPAWNEGG_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(384, 0), ITEM_EXPERIENCEBOTTLE},
  {std::make_tuple(388, 0), ITEM_EMERALD},
  {std::make_tuple(390, 0), ITEM_FLOWERPOT},
  {std::make_tuple(391, 0), ITEM_CARROT},
  {std::make_tuple(392, 0), ITEM_POTATO},
  {std::make_tuple(393, 0), ITEM_BAKEDPOTATO},
  {std::make_tuple(394, 0), ITEM_POISONOUSPOTATO},
  {std::make_tuple(396, 0), ITEM_GOLDENCARROT},
  {std::make_tuple(397, 0), ITEM_SKULL_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(400, 0), ITEM_PUMPKINPIE},
  {std::make_tuple(403, 0), ITEM_ENCHANTEDBOOK},
  {std::make_tuple(404, 0), ITEM_COMPARATOR},
  {std::make_tuple(405, 0), ITEM_NETHERBRICK},
  {std::make_tuple(406, 0), ITEM_QUARTZ},
  {std::make_tuple(407, 0), ITEM_TNTMINECART},
  {std::make_tuple(408, 0), ITEM_HOPPERMINECART},
  {std::make_tuple(409, 0), ITEM_PRISMARINESHARD},
  {std::make_tuple(422, 0), ITEM_PRISMARINECRYSTALS},
  {std::make_tuple(411, 0), ITEM_RABBIT},
  {std::make_tuple(412, 0), ITEM_COOKEDRABBIT},
  {std::make_tuple(413, 0), ITEM_RABBITSTEW},
  {std::make_tuple(414, 0), ITEM_RABBITFOOT},
  {std::make_tuple(415, 0), ITEM_RABBITHIDE},
  {std::make_tuple(420, 0), ITEM_LEAD},
  {std::make_tuple(426, 0), ITEM_ENDCRYSTAL},
  {std::make_tuple(427, 0), ITEM_SPRUCEDOOR},
  {std::make_tuple(428, 0), ITEM_BIRCHDOOR},
  {std::make_tuple(429, 0), ITEM_JUNGLEDOOR},
  {std::make_tuple(430, 0), ITEM_ACACIADOOR},
  {std::make_tuple(431, 0), ITEM_DARKOAKDOOR},
  {std::make_tuple(432, 0), ITEM_CHORUSFRUIT},
  {std::make_tuple(433, 0), ITEM_CHORUSFRUITPOPPED},
  {std::make_tuple(457, 0), ITEM_BEETROOT},
  {std::make_tuple(458, 0), ITEM_BEETROOTSEEDS},
  {std::make_tuple(459, 0), ITEM_BEETROOTSOUP},
  {std::make_tuple(437, 0), ITEM_DRAGONBREATH},
  {std::make_tuple(438, 0), ITEM_SPLASHPOTION},
  {std::make_tuple(441, 0), ITEM_LINGERINGPOTION},
  {std::make_tuple(444, 0), ITEM_ELYTRA},
  {std::make_tuple(445, 0), ITEM_SHULKERSHELL},
}
const std::map<std::tuple<short, short>, UItemName> PocketItemToUItemName = {
  {std::make_tuple(256, 0), Item::IronShovel},
  {std::make_tuple(257, 0), Item::IronPickaxe},
  {std::make_tuple(258, 0), Item::IronAxe},
  {std::make_tuple(259, 0), Item::FlintAndSteel},
  {std::make_tuple(260, 0), Item::Apple},
  {std::make_tuple(261, 0), Item::Bow},
  {std::make_tuple(262, 0), Item::Arrow},
  {std::make_tuple(263, 0), Item::Coal_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(264, 0), Item::Diamond},
  {std::make_tuple(265, 0), Item::IronIngot},
  {std::make_tuple(266, 0), Item::GoldIngot},
  {std::make_tuple(267, 0), Item::IronSword},
  {std::make_tuple(268, 0), Item::WoodenSword},
  {std::make_tuple(269, 0), Item::WoodenShovel},
  {std::make_tuple(270, 0), Item::WoodenPickaxe},
  {std::make_tuple(271, 0), Item::WoodenAxe},
  {std::make_tuple(272, 0), Item::StoneSword},
  {std::make_tuple(273, 0), Item::StoneShovel},
  {std::make_tuple(274, 0), Item::StonePickaxe},
  {std::make_tuple(275, 0), Item::StoneAxe},
  {std::make_tuple(276, 0), Item::DiamondSword},
  {std::make_tuple(277, 0), Item::DiamondShovel},
  {std::make_tuple(278, 0), Item::DiamondPickaxe},
  {std::make_tuple(279, 0), Item::DiamondAxe},
  {std::make_tuple(280, 0), Item::Stick},
  {std::make_tuple(281, 0), Item::Bowl},
  {std::make_tuple(282, 0), Item::MushroomStew},
  {std::make_tuple(283, 0), Item::GoldenSword},
  {std::make_tuple(284, 0), Item::GoldenShovel},
  {std::make_tuple(285, 0), Item::GoldenPickaxe},
  {std::make_tuple(286, 0), Item::GoldenAxe},
  {std::make_tuple(287, 0), Item::String},
  {std::make_tuple(288, 0), Item::Feather},
  {std::make_tuple(289, 0), Item::Gunpowder},
  {std::make_tuple(290, 0), Item::WoodenHoe},
  {std::make_tuple(291, 0), Item::StoneHoe},
  {std::make_tuple(292, 0), Item::IronHoe},
  {std::make_tuple(293, 0), Item::DiamondHoe},
  {std::make_tuple(294, 0), Item::GoldenHoe},
  {std::make_tuple(295, 0), Item::WheatSeeds},
  {std::make_tuple(296, 0), Item::Wheat},
  {std::make_tuple(297, 0), Item::Bread},
  {std::make_tuple(298, 0), Item::LeatherHelmet},
  {std::make_tuple(299, 0), Item::LeatherChestplate},
  {std::make_tuple(300, 0), Item::LeatherLeggings},
  {std::make_tuple(301, 0), Item::LeatherBoots},
  {std::make_tuple(302, 0), Item::ChainmailHelmet},
  {std::make_tuple(303, 0), Item::ChainmailChestplate},
  {std::make_tuple(304, 0), Item::ChainmailLeggings},
  {std::make_tuple(305, 0), Item::ChainmailBoots},
  {std::make_tuple(306, 0), Item::IronHelmet},
  {std::make_tuple(307, 0), Item::IronChestplate},
  {std::make_tuple(308, 0), Item::IronLeggings},
  {std::make_tuple(309, 0), Item::IronBoots},
  {std::make_tuple(310, 0), Item::DiamondHelmet},
  {std::make_tuple(311, 0), Item::DiamondChestplate},
  {std::make_tuple(312, 0), Item::DiamondLeggings},
  {std::make_tuple(313, 0), Item::DiamondBoots},
  {std::make_tuple(314, 0), Item::GoldenHelmet},
  {std::make_tuple(315, 0), Item::GoldenChestplate},
  {std::make_tuple(316, 0), Item::GoldenLeggings},
  {std::make_tuple(317, 0), Item::GoldenBoots},
  {std::make_tuple(318, 0), Item::Flint},
  {std::make_tuple(319, 0), Item::Porkchop},
  {std::make_tuple(320, 0), Item::CookedPorkchop},
  {std::make_tuple(321, 0), Item::Painting},
  {std::make_tuple(322, 0), Item::GoldenApple_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(323, 0), Item::Sign},
  {std::make_tuple(324, 0), Item::WoodenDoor},
  {std::make_tuple(325, 0), Item::Bucket},
  {std::make_tuple(328, 0), Item::Minecart},
  {std::make_tuple(329, 0), Item::Saddle},
  {std::make_tuple(330, 0), Item::IronDoor},
  {std::make_tuple(331, 0), Item::Redstone},
  {std::make_tuple(332, 0), Item::Snowball},
  {std::make_tuple(333, 0), Item::Boat},
  {std::make_tuple(334, 0), Item::Leather},
  {std::make_tuple(336, 0), Item::Brick},
  {std::make_tuple(337, 0), Item::ClayBall},
  {std::make_tuple(338, 0), Item::Reeds},
  {std::make_tuple(339, 0), Item::Paper},
  {std::make_tuple(340, 0), Item::Book},
  {std::make_tuple(341, 0), Item::SlimeBall},
  {std::make_tuple(342, 0), Item::ChestMinecart},
  {std::make_tuple(344, 0), Item::Egg},
  {std::make_tuple(345, 0), Item::Compass},
  {std::make_tuple(346, 0), Item::FishingRod},
  {std::make_tuple(347, 0), Item::Clock},
  {std::make_tuple(348, 0), Item::GlowstoneDust},
  {std::make_tuple(349, 0), Item::Fish_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(350, 0), Item::CookedFish_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(351, 0), Item::Dye_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(352, 0), Item::Bone},
  {std::make_tuple(353, 0), Item::Sugar},
  {std::make_tuple(354, 0), Item::Cake},
  {std::make_tuple(355, 0), Item::Bed},
  {std::make_tuple(356, 0), Item::Repeater},
  {std::make_tuple(357, 0), Item::Cookie},
  {std::make_tuple(359, 0), Item::Shears},
  {std::make_tuple(360, 0), Item::Melon},
  {std::make_tuple(361, 0), Item::PumpkinSeeds},
  {std::make_tuple(362, 0), Item::MelonSeeds},
  {std::make_tuple(363, 0), Item::Beef},
  {std::make_tuple(364, 0), Item::CookedBeef},
  {std::make_tuple(365, 0), Item::Chicken},
  {std::make_tuple(366, 0), Item::CookedChicken},
  {std::make_tuple(367, 0), Item::RottenFlesh},
  {std::make_tuple(368, 0), Item::EnderPearl},
  {std::make_tuple(369, 0), Item::BlazeRod},
  {std::make_tuple(370, 0), Item::GhastTear},
  {std::make_tuple(371, 0), Item::GoldNugget},
  {std::make_tuple(372, 0), Item::NetherWart},
  {std::make_tuple(373, 0), Item::Potion_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(374, 0), Item::GlassBottle},
  {std::make_tuple(375, 0), Item::SpiderEye},
  {std::make_tuple(376, 0), Item::FermentedSpiderEye},
  {std::make_tuple(377, 0), Item::BlazePowder},
  {std::make_tuple(378, 0), Item::MagmaCream},
  {std::make_tuple(379, 0), Item::BrewingStand},
  {std::make_tuple(380, 0), Item::Cauldron},
  {std::make_tuple(381, 0), Item::EnderEye},
  {std::make_tuple(382, 0), Item::SpeckledMelon},
  {std::make_tuple(383, 0), Item::SpawnEgg_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(384, 0), Item::ExperienceBottle},
  {std::make_tuple(388, 0), Item::Emerald},
  {std::make_tuple(390, 0), Item::FlowerPot},
  {std::make_tuple(391, 0), Item::Carrot},
  {std::make_tuple(392, 0), Item::Potato},
  {std::make_tuple(393, 0), Item::BakedPotato},
  {std::make_tuple(394, 0), Item::PoisonousPotato},
  {std::make_tuple(396, 0), Item::GoldenCarrot},
  {std::make_tuple(397, 0), Item::Skull_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(400, 0), Item::PumpkinPie},
  {std::make_tuple(403, 0), Item::EnchantedBook},
  {std::make_tuple(404, 0), Item::Comparator},
  {std::make_tuple(405, 0), Item::Netherbrick},
  {std::make_tuple(406, 0), Item::Quartz},
  {std::make_tuple(407, 0), Item::TntMinecart},
  {std::make_tuple(408, 0), Item::HopperMinecart},
  {std::make_tuple(409, 0), Item::PrismarineShard},
  {std::make_tuple(422, 0), Item::PrismarineCrystals},
  {std::make_tuple(411, 0), Item::Rabbit},
  {std::make_tuple(412, 0), Item::CookedRabbit},
  {std::make_tuple(413, 0), Item::RabbitStew},
  {std::make_tuple(414, 0), Item::RabbitFoot},
  {std::make_tuple(415, 0), Item::RabbitHide},
  {std::make_tuple(420, 0), Item::Lead},
  {std::make_tuple(426, 0), Item::EndCrystal},
  {std::make_tuple(427, 0), Item::SpruceDoor},
  {std::make_tuple(428, 0), Item::BirchDoor},
  {std::make_tuple(429, 0), Item::JungleDoor},
  {std::make_tuple(430, 0), Item::AcaciaDoor},
  {std::make_tuple(431, 0), Item::DarkOakDoor},
  {std::make_tuple(432, 0), Item::ChorusFruit},
  {std::make_tuple(433, 0), Item::ChorusFruitPopped},
  {std::make_tuple(457, 0), Item::Beetroot},
  {std::make_tuple(458, 0), Item::BeetrootSeeds},
  {std::make_tuple(459, 0), Item::BeetrootSoup},
  {std::make_tuple(437, 0), Item::DragonBreath},
  {std::make_tuple(438, 0), Item::SplashPotion},
  {std::make_tuple(441, 0), Item::LingeringPotion},
  {std::make_tuple(444, 0), Item::Elytra},
  {std::make_tuple(445, 0), Item::ShulkerShell},
}
const std::map<std::tuple<short, short>, std::tuple<std::string, short>> PocketItemToJavaItem = {
    {std::make_tuple(256, 0), std::make_tuple(256, 0)},
    {std::make_tuple(257, 0), std::make_tuple(257, 0)},
    {std::make_tuple(258, 0), std::make_tuple(258, 0)},
    {std::make_tuple(259, 0), std::make_tuple(259, 0)},
    {std::make_tuple(260, 0), std::make_tuple(260, 0)},
    {std::make_tuple(261, 0), std::make_tuple(261, 0)},
    {std::make_tuple(262, 0), std::make_tuple(262, 0)},
    {std::make_tuple(263, 0), std::make_tuple(263, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(264, 0), std::make_tuple(264, 0)},
    {std::make_tuple(265, 0), std::make_tuple(265, 0)},
    {std::make_tuple(266, 0), std::make_tuple(266, 0)},
    {std::make_tuple(267, 0), std::make_tuple(267, 0)},
    {std::make_tuple(268, 0), std::make_tuple(268, 0)},
    {std::make_tuple(269, 0), std::make_tuple(269, 0)},
    {std::make_tuple(270, 0), std::make_tuple(270, 0)},
    {std::make_tuple(271, 0), std::make_tuple(271, 0)},
    {std::make_tuple(272, 0), std::make_tuple(272, 0)},
    {std::make_tuple(273, 0), std::make_tuple(273, 0)},
    {std::make_tuple(274, 0), std::make_tuple(274, 0)},
    {std::make_tuple(275, 0), std::make_tuple(275, 0)},
    {std::make_tuple(276, 0), std::make_tuple(276, 0)},
    {std::make_tuple(277, 0), std::make_tuple(277, 0)},
    {std::make_tuple(278, 0), std::make_tuple(278, 0)},
    {std::make_tuple(279, 0), std::make_tuple(279, 0)},
    {std::make_tuple(280, 0), std::make_tuple(280, 0)},
    {std::make_tuple(281, 0), std::make_tuple(281, 0)},
    {std::make_tuple(282, 0), std::make_tuple(282, 0)},
    {std::make_tuple(283, 0), std::make_tuple(283, 0)},
    {std::make_tuple(284, 0), std::make_tuple(284, 0)},
    {std::make_tuple(285, 0), std::make_tuple(285, 0)},
    {std::make_tuple(286, 0), std::make_tuple(286, 0)},
    {std::make_tuple(287, 0), std::make_tuple(287, 0)},
    {std::make_tuple(288, 0), std::make_tuple(288, 0)},
    {std::make_tuple(289, 0), std::make_tuple(289, 0)},
    {std::make_tuple(290, 0), std::make_tuple(290, 0)},
    {std::make_tuple(291, 0), std::make_tuple(291, 0)},
    {std::make_tuple(292, 0), std::make_tuple(292, 0)},
    {std::make_tuple(293, 0), std::make_tuple(293, 0)},
    {std::make_tuple(294, 0), std::make_tuple(294, 0)},
    {std::make_tuple(295, 0), std::make_tuple(295, 0)},
    {std::make_tuple(296, 0), std::make_tuple(296, 0)},
    {std::make_tuple(297, 0), std::make_tuple(297, 0)},
    {std::make_tuple(298, 0), std::make_tuple(298, 0)},
    {std::make_tuple(299, 0), std::make_tuple(299, 0)},
    {std::make_tuple(300, 0), std::make_tuple(300, 0)},
    {std::make_tuple(301, 0), std::make_tuple(301, 0)},
    {std::make_tuple(302, 0), std::make_tuple(302, 0)},
    {std::make_tuple(303, 0), std::make_tuple(303, 0)},
    {std::make_tuple(304, 0), std::make_tuple(304, 0)},
    {std::make_tuple(305, 0), std::make_tuple(305, 0)},
    {std::make_tuple(306, 0), std::make_tuple(306, 0)},
    {std::make_tuple(307, 0), std::make_tuple(307, 0)},
    {std::make_tuple(308, 0), std::make_tuple(308, 0)},
    {std::make_tuple(309, 0), std::make_tuple(309, 0)},
    {std::make_tuple(310, 0), std::make_tuple(310, 0)},
    {std::make_tuple(311, 0), std::make_tuple(311, 0)},
    {std::make_tuple(312, 0), std::make_tuple(312, 0)},
    {std::make_tuple(313, 0), std::make_tuple(313, 0)},
    {std::make_tuple(314, 0), std::make_tuple(314, 0)},
    {std::make_tuple(315, 0), std::make_tuple(315, 0)},
    {std::make_tuple(316, 0), std::make_tuple(316, 0)},
    {std::make_tuple(317, 0), std::make_tuple(317, 0)},
    {std::make_tuple(318, 0), std::make_tuple(318, 0)},
    {std::make_tuple(319, 0), std::make_tuple(319, 0)},
    {std::make_tuple(320, 0), std::make_tuple(320, 0)},
    {std::make_tuple(321, 0), std::make_tuple(321, 0)},
    {std::make_tuple(322, 0), std::make_tuple(322, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(323, 0), std::make_tuple(323, 0)},
    {std::make_tuple(324, 0), std::make_tuple(324, 0)},
    {std::make_tuple(325, 0), std::make_tuple(325, 0)},
    {std::make_tuple(328, 0), std::make_tuple(328, 0)},
    {std::make_tuple(329, 0), std::make_tuple(329, 0)},
    {std::make_tuple(330, 0), std::make_tuple(330, 0)},
    {std::make_tuple(331, 0), std::make_tuple(331, 0)},
    {std::make_tuple(332, 0), std::make_tuple(332, 0)},
    {std::make_tuple(333, 0), std::make_tuple(333, 0)},
    {std::make_tuple(334, 0), std::make_tuple(334, 0)},
    {std::make_tuple(336, 0), std::make_tuple(336, 0)},
    {std::make_tuple(337, 0), std::make_tuple(337, 0)},
    {std::make_tuple(338, 0), std::make_tuple(338, 0)},
    {std::make_tuple(339, 0), std::make_tuple(339, 0)},
    {std::make_tuple(340, 0), std::make_tuple(340, 0)},
    {std::make_tuple(341, 0), std::make_tuple(341, 0)},
    {std::make_tuple(342, 0), std::make_tuple(342, 0)},
    {std::make_tuple(344, 0), std::make_tuple(344, 0)},
    {std::make_tuple(345, 0), std::make_tuple(345, 0)},
    {std::make_tuple(346, 0), std::make_tuple(346, 0)},
    {std::make_tuple(347, 0), std::make_tuple(347, 0)},
    {std::make_tuple(348, 0), std::make_tuple(348, 0)},
    {std::make_tuple(349, 0), std::make_tuple(349, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(350, 0), std::make_tuple(350, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(351, 0), std::make_tuple(351, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(352, 0), std::make_tuple(352, 0)},
    {std::make_tuple(353, 0), std::make_tuple(353, 0)},
    {std::make_tuple(354, 0), std::make_tuple(354, 0)},
    {std::make_tuple(355, 0), std::make_tuple(355, 0)},
    {std::make_tuple(356, 0), std::make_tuple(356, 0)},
    {std::make_tuple(357, 0), std::make_tuple(357, 0)},
    {std::make_tuple(359, 0), std::make_tuple(359, 0)},
    {std::make_tuple(360, 0), std::make_tuple(360, 0)},
    {std::make_tuple(361, 0), std::make_tuple(361, 0)},
    {std::make_tuple(362, 0), std::make_tuple(362, 0)},
    {std::make_tuple(363, 0), std::make_tuple(363, 0)},
    {std::make_tuple(364, 0), std::make_tuple(364, 0)},
    {std::make_tuple(365, 0), std::make_tuple(365, 0)},
    {std::make_tuple(366, 0), std::make_tuple(366, 0)},
    {std::make_tuple(367, 0), std::make_tuple(367, 0)},
    {std::make_tuple(368, 0), std::make_tuple(368, 0)},
    {std::make_tuple(369, 0), std::make_tuple(369, 0)},
    {std::make_tuple(370, 0), std::make_tuple(370, 0)},
    {std::make_tuple(371, 0), std::make_tuple(371, 0)},
    {std::make_tuple(372, 0), std::make_tuple(372, 0)},
    {std::make_tuple(373, 0), std::make_tuple(373, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(374, 0), std::make_tuple(374, 0)},
    {std::make_tuple(375, 0), std::make_tuple(375, 0)},
    {std::make_tuple(376, 0), std::make_tuple(376, 0)},
    {std::make_tuple(377, 0), std::make_tuple(377, 0)},
    {std::make_tuple(378, 0), std::make_tuple(378, 0)},
    {std::make_tuple(379, 0), std::make_tuple(379, 0)},
    {std::make_tuple(380, 0), std::make_tuple(380, 0)},
    {std::make_tuple(381, 0), std::make_tuple(381, 0)},
    {std::make_tuple(382, 0), std::make_tuple(382, 0)},
    {std::make_tuple(383, 0), std::make_tuple(383, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(384, 0), std::make_tuple(384, 0)},
    {std::make_tuple(388, 0), std::make_tuple(388, 0)},
    {std::make_tuple(390, 0), std::make_tuple(390, 0)},
    {std::make_tuple(391, 0), std::make_tuple(391, 0)},
    {std::make_tuple(392, 0), std::make_tuple(392, 0)},
    {std::make_tuple(393, 0), std::make_tuple(393, 0)},
    {std::make_tuple(394, 0), std::make_tuple(394, 0)},
    {std::make_tuple(396, 0), std::make_tuple(396, 0)},
    {std::make_tuple(397, 0), std::make_tuple(397, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(400, 0), std::make_tuple(400, 0)},
    {std::make_tuple(403, 0), std::make_tuple(403, 0)},
    {std::make_tuple(404, 0), std::make_tuple(404, 0)},
    {std::make_tuple(405, 0), std::make_tuple(405, 0)},
    {std::make_tuple(406, 0), std::make_tuple(406, 0)},
    {std::make_tuple(407, 0), std::make_tuple(407, 0)},
    {std::make_tuple(408, 0), std::make_tuple(408, 0)},
    {std::make_tuple(409, 0), std::make_tuple(409, 0)},
    {std::make_tuple(422, 0), std::make_tuple(410, 0)},
    {std::make_tuple(411, 0), std::make_tuple(411, 0)},
    {std::make_tuple(412, 0), std::make_tuple(412, 0)},
    {std::make_tuple(413, 0), std::make_tuple(413, 0)},
    {std::make_tuple(414, 0), std::make_tuple(414, 0)},
    {std::make_tuple(415, 0), std::make_tuple(415, 0)},
    {std::make_tuple(420, 0), std::make_tuple(420, 0)},
    {std::make_tuple(426, 0), std::make_tuple(426, 0)},
    {std::make_tuple(427, 0), std::make_tuple(427, 0)},
    {std::make_tuple(428, 0), std::make_tuple(428, 0)},
    {std::make_tuple(429, 0), std::make_tuple(429, 0)},
    {std::make_tuple(430, 0), std::make_tuple(430, 0)},
    {std::make_tuple(431, 0), std::make_tuple(431, 0)},
    {std::make_tuple(432, 0), std::make_tuple(432, 0)},
    {std::make_tuple(433, 0), std::make_tuple(433, 0)},
    {std::make_tuple(457, 0), std::make_tuple(434, 0)},
    {std::make_tuple(458, 0), std::make_tuple(435, 0)},
    {std::make_tuple(459, 0), std::make_tuple(436, 0)},
    {std::make_tuple(437, 0), std::make_tuple(437, 0)},
    {std::make_tuple(438, 0), std::make_tuple(438, 0)},
    {std::make_tuple(441, 0), std::make_tuple(441, 0)},
    {std::make_tuple(444, 0), std::make_tuple(443, 0)},
    {std::make_tuple(445, 0), std::make_tuple(450, 0)},
}
const std::map<std::tuple<std::string, short>, UItemID> JavaItemToUItemID = {
  {std::make_tuple(256, 0), ITEM_IRONSHOVEL},
  {std::make_tuple(257, 0), ITEM_IRONPICKAXE},
  {std::make_tuple(258, 0), ITEM_IRONAXE},
  {std::make_tuple(259, 0), ITEM_FLINTANDSTEEL},
  {std::make_tuple(260, 0), ITEM_APPLE},
  {std::make_tuple(261, 0), ITEM_BOW},
  {std::make_tuple(262, 0), ITEM_ARROW},
  {std::make_tuple(263, 0), ITEM_COAL_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(264, 0), ITEM_DIAMOND},
  {std::make_tuple(265, 0), ITEM_IRONINGOT},
  {std::make_tuple(266, 0), ITEM_GOLDINGOT},
  {std::make_tuple(267, 0), ITEM_IRONSWORD},
  {std::make_tuple(268, 0), ITEM_WOODENSWORD},
  {std::make_tuple(269, 0), ITEM_WOODENSHOVEL},
  {std::make_tuple(270, 0), ITEM_WOODENPICKAXE},
  {std::make_tuple(271, 0), ITEM_WOODENAXE},
  {std::make_tuple(272, 0), ITEM_STONESWORD},
  {std::make_tuple(273, 0), ITEM_STONESHOVEL},
  {std::make_tuple(274, 0), ITEM_STONEPICKAXE},
  {std::make_tuple(275, 0), ITEM_STONEAXE},
  {std::make_tuple(276, 0), ITEM_DIAMONDSWORD},
  {std::make_tuple(277, 0), ITEM_DIAMONDSHOVEL},
  {std::make_tuple(278, 0), ITEM_DIAMONDPICKAXE},
  {std::make_tuple(279, 0), ITEM_DIAMONDAXE},
  {std::make_tuple(280, 0), ITEM_STICK},
  {std::make_tuple(281, 0), ITEM_BOWL},
  {std::make_tuple(282, 0), ITEM_MUSHROOMSTEW},
  {std::make_tuple(283, 0), ITEM_GOLDENSWORD},
  {std::make_tuple(284, 0), ITEM_GOLDENSHOVEL},
  {std::make_tuple(285, 0), ITEM_GOLDENPICKAXE},
  {std::make_tuple(286, 0), ITEM_GOLDENAXE},
  {std::make_tuple(287, 0), ITEM_STRING},
  {std::make_tuple(288, 0), ITEM_FEATHER},
  {std::make_tuple(289, 0), ITEM_GUNPOWDER},
  {std::make_tuple(290, 0), ITEM_WOODENHOE},
  {std::make_tuple(291, 0), ITEM_STONEHOE},
  {std::make_tuple(292, 0), ITEM_IRONHOE},
  {std::make_tuple(293, 0), ITEM_DIAMONDHOE},
  {std::make_tuple(294, 0), ITEM_GOLDENHOE},
  {std::make_tuple(295, 0), ITEM_WHEATSEEDS},
  {std::make_tuple(296, 0), ITEM_WHEAT},
  {std::make_tuple(297, 0), ITEM_BREAD},
  {std::make_tuple(298, 0), ITEM_LEATHERHELMET},
  {std::make_tuple(299, 0), ITEM_LEATHERCHESTPLATE},
  {std::make_tuple(300, 0), ITEM_LEATHERLEGGINGS},
  {std::make_tuple(301, 0), ITEM_LEATHERBOOTS},
  {std::make_tuple(302, 0), ITEM_CHAINMAILHELMET},
  {std::make_tuple(303, 0), ITEM_CHAINMAILCHESTPLATE},
  {std::make_tuple(304, 0), ITEM_CHAINMAILLEGGINGS},
  {std::make_tuple(305, 0), ITEM_CHAINMAILBOOTS},
  {std::make_tuple(306, 0), ITEM_IRONHELMET},
  {std::make_tuple(307, 0), ITEM_IRONCHESTPLATE},
  {std::make_tuple(308, 0), ITEM_IRONLEGGINGS},
  {std::make_tuple(309, 0), ITEM_IRONBOOTS},
  {std::make_tuple(310, 0), ITEM_DIAMONDHELMET},
  {std::make_tuple(311, 0), ITEM_DIAMONDCHESTPLATE},
  {std::make_tuple(312, 0), ITEM_DIAMONDLEGGINGS},
  {std::make_tuple(313, 0), ITEM_DIAMONDBOOTS},
  {std::make_tuple(314, 0), ITEM_GOLDENHELMET},
  {std::make_tuple(315, 0), ITEM_GOLDENCHESTPLATE},
  {std::make_tuple(316, 0), ITEM_GOLDENLEGGINGS},
  {std::make_tuple(317, 0), ITEM_GOLDENBOOTS},
  {std::make_tuple(318, 0), ITEM_FLINT},
  {std::make_tuple(319, 0), ITEM_PORKCHOP},
  {std::make_tuple(320, 0), ITEM_COOKEDPORKCHOP},
  {std::make_tuple(321, 0), ITEM_PAINTING},
  {std::make_tuple(322, 0), ITEM_GOLDENAPPLE_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(323, 0), ITEM_SIGN},
  {std::make_tuple(324, 0), ITEM_WOODENDOOR},
  {std::make_tuple(325, 0), ITEM_BUCKET},
  {std::make_tuple(328, 0), ITEM_MINECART},
  {std::make_tuple(329, 0), ITEM_SADDLE},
  {std::make_tuple(330, 0), ITEM_IRONDOOR},
  {std::make_tuple(331, 0), ITEM_REDSTONE},
  {std::make_tuple(332, 0), ITEM_SNOWBALL},
  {std::make_tuple(333, 0), ITEM_BOAT},
  {std::make_tuple(334, 0), ITEM_LEATHER},
  {std::make_tuple(336, 0), ITEM_BRICK},
  {std::make_tuple(337, 0), ITEM_CLAYBALL},
  {std::make_tuple(338, 0), ITEM_REEDS},
  {std::make_tuple(339, 0), ITEM_PAPER},
  {std::make_tuple(340, 0), ITEM_BOOK},
  {std::make_tuple(341, 0), ITEM_SLIMEBALL},
  {std::make_tuple(342, 0), ITEM_CHESTMINECART},
  {std::make_tuple(344, 0), ITEM_EGG},
  {std::make_tuple(345, 0), ITEM_COMPASS},
  {std::make_tuple(346, 0), ITEM_FISHINGROD},
  {std::make_tuple(347, 0), ITEM_CLOCK},
  {std::make_tuple(348, 0), ITEM_GLOWSTONEDUST},
  {std::make_tuple(349, 0), ITEM_FISH_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(350, 0), ITEM_COOKEDFISH_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(351, 0), ITEM_DYE_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(352, 0), ITEM_BONE},
  {std::make_tuple(353, 0), ITEM_SUGAR},
  {std::make_tuple(354, 0), ITEM_CAKE},
  {std::make_tuple(355, 0), ITEM_BED},
  {std::make_tuple(356, 0), ITEM_REPEATER},
  {std::make_tuple(357, 0), ITEM_COOKIE},
  {std::make_tuple(359, 0), ITEM_SHEARS},
  {std::make_tuple(360, 0), ITEM_MELON},
  {std::make_tuple(361, 0), ITEM_PUMPKINSEEDS},
  {std::make_tuple(362, 0), ITEM_MELONSEEDS},
  {std::make_tuple(363, 0), ITEM_BEEF},
  {std::make_tuple(364, 0), ITEM_COOKEDBEEF},
  {std::make_tuple(365, 0), ITEM_CHICKEN},
  {std::make_tuple(366, 0), ITEM_COOKEDCHICKEN},
  {std::make_tuple(367, 0), ITEM_ROTTENFLESH},
  {std::make_tuple(368, 0), ITEM_ENDERPEARL},
  {std::make_tuple(369, 0), ITEM_BLAZEROD},
  {std::make_tuple(370, 0), ITEM_GHASTTEAR},
  {std::make_tuple(371, 0), ITEM_GOLDNUGGET},
  {std::make_tuple(372, 0), ITEM_NETHERWART},
  {std::make_tuple(373, 0), ITEM_POTION_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(374, 0), ITEM_GLASSBOTTLE},
  {std::make_tuple(375, 0), ITEM_SPIDEREYE},
  {std::make_tuple(376, 0), ITEM_FERMENTEDSPIDEREYE},
  {std::make_tuple(377, 0), ITEM_BLAZEPOWDER},
  {std::make_tuple(378, 0), ITEM_MAGMACREAM},
  {std::make_tuple(379, 0), ITEM_BREWINGSTAND},
  {std::make_tuple(380, 0), ITEM_CAULDRON},
  {std::make_tuple(381, 0), ITEM_ENDEREYE},
  {std::make_tuple(382, 0), ITEM_SPECKLEDMELON},
  {std::make_tuple(383, 0), ITEM_SPAWNEGG_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(384, 0), ITEM_EXPERIENCEBOTTLE},
  {std::make_tuple(388, 0), ITEM_EMERALD},
  {std::make_tuple(390, 0), ITEM_FLOWERPOT},
  {std::make_tuple(391, 0), ITEM_CARROT},
  {std::make_tuple(392, 0), ITEM_POTATO},
  {std::make_tuple(393, 0), ITEM_BAKEDPOTATO},
  {std::make_tuple(394, 0), ITEM_POISONOUSPOTATO},
  {std::make_tuple(396, 0), ITEM_GOLDENCARROT},
  {std::make_tuple(397, 0), ITEM_SKULL_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(400, 0), ITEM_PUMPKINPIE},
  {std::make_tuple(403, 0), ITEM_ENCHANTEDBOOK},
  {std::make_tuple(404, 0), ITEM_COMPARATOR},
  {std::make_tuple(405, 0), ITEM_NETHERBRICK},
  {std::make_tuple(406, 0), ITEM_QUARTZ},
  {std::make_tuple(407, 0), ITEM_TNTMINECART},
  {std::make_tuple(408, 0), ITEM_HOPPERMINECART},
  {std::make_tuple(409, 0), ITEM_PRISMARINESHARD},
  {std::make_tuple(410, 0), ITEM_PRISMARINECRYSTALS},
  {std::make_tuple(411, 0), ITEM_RABBIT},
  {std::make_tuple(412, 0), ITEM_COOKEDRABBIT},
  {std::make_tuple(413, 0), ITEM_RABBITSTEW},
  {std::make_tuple(414, 0), ITEM_RABBITFOOT},
  {std::make_tuple(415, 0), ITEM_RABBITHIDE},
  {std::make_tuple(420, 0), ITEM_LEAD},
  {std::make_tuple(426, 0), ITEM_ENDCRYSTAL},
  {std::make_tuple(427, 0), ITEM_SPRUCEDOOR},
  {std::make_tuple(428, 0), ITEM_BIRCHDOOR},
  {std::make_tuple(429, 0), ITEM_JUNGLEDOOR},
  {std::make_tuple(430, 0), ITEM_ACACIADOOR},
  {std::make_tuple(431, 0), ITEM_DARKOAKDOOR},
  {std::make_tuple(432, 0), ITEM_CHORUSFRUIT},
  {std::make_tuple(433, 0), ITEM_CHORUSFRUITPOPPED},
  {std::make_tuple(434, 0), ITEM_BEETROOT},
  {std::make_tuple(435, 0), ITEM_BEETROOTSEEDS},
  {std::make_tuple(436, 0), ITEM_BEETROOTSOUP},
  {std::make_tuple(437, 0), ITEM_DRAGONBREATH},
  {std::make_tuple(438, 0), ITEM_SPLASHPOTION},
  {std::make_tuple(441, 0), ITEM_LINGERINGPOTION},
  {std::make_tuple(443, 0), ITEM_ELYTRA},
  {std::make_tuple(450, 0), ITEM_SHULKERSHELL},
}
const std::map<std::tuple<std::string, short>, UItemName> JavaItemToUItemName = {
  {std::make_tuple(256, 0), Item::IronShovel},
  {std::make_tuple(257, 0), Item::IronPickaxe},
  {std::make_tuple(258, 0), Item::IronAxe},
  {std::make_tuple(259, 0), Item::FlintAndSteel},
  {std::make_tuple(260, 0), Item::Apple},
  {std::make_tuple(261, 0), Item::Bow},
  {std::make_tuple(262, 0), Item::Arrow},
  {std::make_tuple(263, 0), Item::Coal_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(264, 0), Item::Diamond},
  {std::make_tuple(265, 0), Item::IronIngot},
  {std::make_tuple(266, 0), Item::GoldIngot},
  {std::make_tuple(267, 0), Item::IronSword},
  {std::make_tuple(268, 0), Item::WoodenSword},
  {std::make_tuple(269, 0), Item::WoodenShovel},
  {std::make_tuple(270, 0), Item::WoodenPickaxe},
  {std::make_tuple(271, 0), Item::WoodenAxe},
  {std::make_tuple(272, 0), Item::StoneSword},
  {std::make_tuple(273, 0), Item::StoneShovel},
  {std::make_tuple(274, 0), Item::StonePickaxe},
  {std::make_tuple(275, 0), Item::StoneAxe},
  {std::make_tuple(276, 0), Item::DiamondSword},
  {std::make_tuple(277, 0), Item::DiamondShovel},
  {std::make_tuple(278, 0), Item::DiamondPickaxe},
  {std::make_tuple(279, 0), Item::DiamondAxe},
  {std::make_tuple(280, 0), Item::Stick},
  {std::make_tuple(281, 0), Item::Bowl},
  {std::make_tuple(282, 0), Item::MushroomStew},
  {std::make_tuple(283, 0), Item::GoldenSword},
  {std::make_tuple(284, 0), Item::GoldenShovel},
  {std::make_tuple(285, 0), Item::GoldenPickaxe},
  {std::make_tuple(286, 0), Item::GoldenAxe},
  {std::make_tuple(287, 0), Item::String},
  {std::make_tuple(288, 0), Item::Feather},
  {std::make_tuple(289, 0), Item::Gunpowder},
  {std::make_tuple(290, 0), Item::WoodenHoe},
  {std::make_tuple(291, 0), Item::StoneHoe},
  {std::make_tuple(292, 0), Item::IronHoe},
  {std::make_tuple(293, 0), Item::DiamondHoe},
  {std::make_tuple(294, 0), Item::GoldenHoe},
  {std::make_tuple(295, 0), Item::WheatSeeds},
  {std::make_tuple(296, 0), Item::Wheat},
  {std::make_tuple(297, 0), Item::Bread},
  {std::make_tuple(298, 0), Item::LeatherHelmet},
  {std::make_tuple(299, 0), Item::LeatherChestplate},
  {std::make_tuple(300, 0), Item::LeatherLeggings},
  {std::make_tuple(301, 0), Item::LeatherBoots},
  {std::make_tuple(302, 0), Item::ChainmailHelmet},
  {std::make_tuple(303, 0), Item::ChainmailChestplate},
  {std::make_tuple(304, 0), Item::ChainmailLeggings},
  {std::make_tuple(305, 0), Item::ChainmailBoots},
  {std::make_tuple(306, 0), Item::IronHelmet},
  {std::make_tuple(307, 0), Item::IronChestplate},
  {std::make_tuple(308, 0), Item::IronLeggings},
  {std::make_tuple(309, 0), Item::IronBoots},
  {std::make_tuple(310, 0), Item::DiamondHelmet},
  {std::make_tuple(311, 0), Item::DiamondChestplate},
  {std::make_tuple(312, 0), Item::DiamondLeggings},
  {std::make_tuple(313, 0), Item::DiamondBoots},
  {std::make_tuple(314, 0), Item::GoldenHelmet},
  {std::make_tuple(315, 0), Item::GoldenChestplate},
  {std::make_tuple(316, 0), Item::GoldenLeggings},
  {std::make_tuple(317, 0), Item::GoldenBoots},
  {std::make_tuple(318, 0), Item::Flint},
  {std::make_tuple(319, 0), Item::Porkchop},
  {std::make_tuple(320, 0), Item::CookedPorkchop},
  {std::make_tuple(321, 0), Item::Painting},
  {std::make_tuple(322, 0), Item::GoldenApple_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(323, 0), Item::Sign},
  {std::make_tuple(324, 0), Item::WoodenDoor},
  {std::make_tuple(325, 0), Item::Bucket},
  {std::make_tuple(328, 0), Item::Minecart},
  {std::make_tuple(329, 0), Item::Saddle},
  {std::make_tuple(330, 0), Item::IronDoor},
  {std::make_tuple(331, 0), Item::Redstone},
  {std::make_tuple(332, 0), Item::Snowball},
  {std::make_tuple(333, 0), Item::Boat},
  {std::make_tuple(334, 0), Item::Leather},
  {std::make_tuple(336, 0), Item::Brick},
  {std::make_tuple(337, 0), Item::ClayBall},
  {std::make_tuple(338, 0), Item::Reeds},
  {std::make_tuple(339, 0), Item::Paper},
  {std::make_tuple(340, 0), Item::Book},
  {std::make_tuple(341, 0), Item::SlimeBall},
  {std::make_tuple(342, 0), Item::ChestMinecart},
  {std::make_tuple(344, 0), Item::Egg},
  {std::make_tuple(345, 0), Item::Compass},
  {std::make_tuple(346, 0), Item::FishingRod},
  {std::make_tuple(347, 0), Item::Clock},
  {std::make_tuple(348, 0), Item::GlowstoneDust},
  {std::make_tuple(349, 0), Item::Fish_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(350, 0), Item::CookedFish_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(351, 0), Item::Dye_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(352, 0), Item::Bone},
  {std::make_tuple(353, 0), Item::Sugar},
  {std::make_tuple(354, 0), Item::Cake},
  {std::make_tuple(355, 0), Item::Bed},
  {std::make_tuple(356, 0), Item::Repeater},
  {std::make_tuple(357, 0), Item::Cookie},
  {std::make_tuple(359, 0), Item::Shears},
  {std::make_tuple(360, 0), Item::Melon},
  {std::make_tuple(361, 0), Item::PumpkinSeeds},
  {std::make_tuple(362, 0), Item::MelonSeeds},
  {std::make_tuple(363, 0), Item::Beef},
  {std::make_tuple(364, 0), Item::CookedBeef},
  {std::make_tuple(365, 0), Item::Chicken},
  {std::make_tuple(366, 0), Item::CookedChicken},
  {std::make_tuple(367, 0), Item::RottenFlesh},
  {std::make_tuple(368, 0), Item::EnderPearl},
  {std::make_tuple(369, 0), Item::BlazeRod},
  {std::make_tuple(370, 0), Item::GhastTear},
  {std::make_tuple(371, 0), Item::GoldNugget},
  {std::make_tuple(372, 0), Item::NetherWart},
  {std::make_tuple(373, 0), Item::Potion_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(374, 0), Item::GlassBottle},
  {std::make_tuple(375, 0), Item::SpiderEye},
  {std::make_tuple(376, 0), Item::FermentedSpiderEye},
  {std::make_tuple(377, 0), Item::BlazePowder},
  {std::make_tuple(378, 0), Item::MagmaCream},
  {std::make_tuple(379, 0), Item::BrewingStand},
  {std::make_tuple(380, 0), Item::Cauldron},
  {std::make_tuple(381, 0), Item::EnderEye},
  {std::make_tuple(382, 0), Item::SpeckledMelon},
  {std::make_tuple(383, 0), Item::SpawnEgg_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(384, 0), Item::ExperienceBottle},
  {std::make_tuple(388, 0), Item::Emerald},
  {std::make_tuple(390, 0), Item::FlowerPot},
  {std::make_tuple(391, 0), Item::Carrot},
  {std::make_tuple(392, 0), Item::Potato},
  {std::make_tuple(393, 0), Item::BakedPotato},
  {std::make_tuple(394, 0), Item::PoisonousPotato},
  {std::make_tuple(396, 0), Item::GoldenCarrot},
  {std::make_tuple(397, 0), Item::Skull_},// NEEDS MANUAL METADATA ADDITIONS
  {std::make_tuple(400, 0), Item::PumpkinPie},
  {std::make_tuple(403, 0), Item::EnchantedBook},
  {std::make_tuple(404, 0), Item::Comparator},
  {std::make_tuple(405, 0), Item::Netherbrick},
  {std::make_tuple(406, 0), Item::Quartz},
  {std::make_tuple(407, 0), Item::TntMinecart},
  {std::make_tuple(408, 0), Item::HopperMinecart},
  {std::make_tuple(409, 0), Item::PrismarineShard},
  {std::make_tuple(410, 0), Item::PrismarineCrystals},
  {std::make_tuple(411, 0), Item::Rabbit},
  {std::make_tuple(412, 0), Item::CookedRabbit},
  {std::make_tuple(413, 0), Item::RabbitStew},
  {std::make_tuple(414, 0), Item::RabbitFoot},
  {std::make_tuple(415, 0), Item::RabbitHide},
  {std::make_tuple(420, 0), Item::Lead},
  {std::make_tuple(426, 0), Item::EndCrystal},
  {std::make_tuple(427, 0), Item::SpruceDoor},
  {std::make_tuple(428, 0), Item::BirchDoor},
  {std::make_tuple(429, 0), Item::JungleDoor},
  {std::make_tuple(430, 0), Item::AcaciaDoor},
  {std::make_tuple(431, 0), Item::DarkOakDoor},
  {std::make_tuple(432, 0), Item::ChorusFruit},
  {std::make_tuple(433, 0), Item::ChorusFruitPopped},
  {std::make_tuple(434, 0), Item::Beetroot},
  {std::make_tuple(435, 0), Item::BeetrootSeeds},
  {std::make_tuple(436, 0), Item::BeetrootSoup},
  {std::make_tuple(437, 0), Item::DragonBreath},
  {std::make_tuple(438, 0), Item::SplashPotion},
  {std::make_tuple(441, 0), Item::LingeringPotion},
  {std::make_tuple(443, 0), Item::Elytra},
  {std::make_tuple(450, 0), Item::ShulkerShell},
}
const std::map<std::tuple<std::string, short>, std::tuple<short, short>> JavaItemToPocketItem = {
    {std::make_tuple(256, 0), std::make_tuple(256, 0)},
    {std::make_tuple(257, 0), std::make_tuple(257, 0)},
    {std::make_tuple(258, 0), std::make_tuple(258, 0)},
    {std::make_tuple(259, 0), std::make_tuple(259, 0)},
    {std::make_tuple(260, 0), std::make_tuple(260, 0)},
    {std::make_tuple(261, 0), std::make_tuple(261, 0)},
    {std::make_tuple(262, 0), std::make_tuple(262, 0)},
    {std::make_tuple(263, 0), std::make_tuple(263, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(264, 0), std::make_tuple(264, 0)},
    {std::make_tuple(265, 0), std::make_tuple(265, 0)},
    {std::make_tuple(266, 0), std::make_tuple(266, 0)},
    {std::make_tuple(267, 0), std::make_tuple(267, 0)},
    {std::make_tuple(268, 0), std::make_tuple(268, 0)},
    {std::make_tuple(269, 0), std::make_tuple(269, 0)},
    {std::make_tuple(270, 0), std::make_tuple(270, 0)},
    {std::make_tuple(271, 0), std::make_tuple(271, 0)},
    {std::make_tuple(272, 0), std::make_tuple(272, 0)},
    {std::make_tuple(273, 0), std::make_tuple(273, 0)},
    {std::make_tuple(274, 0), std::make_tuple(274, 0)},
    {std::make_tuple(275, 0), std::make_tuple(275, 0)},
    {std::make_tuple(276, 0), std::make_tuple(276, 0)},
    {std::make_tuple(277, 0), std::make_tuple(277, 0)},
    {std::make_tuple(278, 0), std::make_tuple(278, 0)},
    {std::make_tuple(279, 0), std::make_tuple(279, 0)},
    {std::make_tuple(280, 0), std::make_tuple(280, 0)},
    {std::make_tuple(281, 0), std::make_tuple(281, 0)},
    {std::make_tuple(282, 0), std::make_tuple(282, 0)},
    {std::make_tuple(283, 0), std::make_tuple(283, 0)},
    {std::make_tuple(284, 0), std::make_tuple(284, 0)},
    {std::make_tuple(285, 0), std::make_tuple(285, 0)},
    {std::make_tuple(286, 0), std::make_tuple(286, 0)},
    {std::make_tuple(287, 0), std::make_tuple(287, 0)},
    {std::make_tuple(288, 0), std::make_tuple(288, 0)},
    {std::make_tuple(289, 0), std::make_tuple(289, 0)},
    {std::make_tuple(290, 0), std::make_tuple(290, 0)},
    {std::make_tuple(291, 0), std::make_tuple(291, 0)},
    {std::make_tuple(292, 0), std::make_tuple(292, 0)},
    {std::make_tuple(293, 0), std::make_tuple(293, 0)},
    {std::make_tuple(294, 0), std::make_tuple(294, 0)},
    {std::make_tuple(295, 0), std::make_tuple(295, 0)},
    {std::make_tuple(296, 0), std::make_tuple(296, 0)},
    {std::make_tuple(297, 0), std::make_tuple(297, 0)},
    {std::make_tuple(298, 0), std::make_tuple(298, 0)},
    {std::make_tuple(299, 0), std::make_tuple(299, 0)},
    {std::make_tuple(300, 0), std::make_tuple(300, 0)},
    {std::make_tuple(301, 0), std::make_tuple(301, 0)},
    {std::make_tuple(302, 0), std::make_tuple(302, 0)},
    {std::make_tuple(303, 0), std::make_tuple(303, 0)},
    {std::make_tuple(304, 0), std::make_tuple(304, 0)},
    {std::make_tuple(305, 0), std::make_tuple(305, 0)},
    {std::make_tuple(306, 0), std::make_tuple(306, 0)},
    {std::make_tuple(307, 0), std::make_tuple(307, 0)},
    {std::make_tuple(308, 0), std::make_tuple(308, 0)},
    {std::make_tuple(309, 0), std::make_tuple(309, 0)},
    {std::make_tuple(310, 0), std::make_tuple(310, 0)},
    {std::make_tuple(311, 0), std::make_tuple(311, 0)},
    {std::make_tuple(312, 0), std::make_tuple(312, 0)},
    {std::make_tuple(313, 0), std::make_tuple(313, 0)},
    {std::make_tuple(314, 0), std::make_tuple(314, 0)},
    {std::make_tuple(315, 0), std::make_tuple(315, 0)},
    {std::make_tuple(316, 0), std::make_tuple(316, 0)},
    {std::make_tuple(317, 0), std::make_tuple(317, 0)},
    {std::make_tuple(318, 0), std::make_tuple(318, 0)},
    {std::make_tuple(319, 0), std::make_tuple(319, 0)},
    {std::make_tuple(320, 0), std::make_tuple(320, 0)},
    {std::make_tuple(321, 0), std::make_tuple(321, 0)},
    {std::make_tuple(322, 0), std::make_tuple(322, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(323, 0), std::make_tuple(323, 0)},
    {std::make_tuple(324, 0), std::make_tuple(324, 0)},
    {std::make_tuple(325, 0), std::make_tuple(325, 0)},
    {std::make_tuple(328, 0), std::make_tuple(328, 0)},
    {std::make_tuple(329, 0), std::make_tuple(329, 0)},
    {std::make_tuple(330, 0), std::make_tuple(330, 0)},
    {std::make_tuple(331, 0), std::make_tuple(331, 0)},
    {std::make_tuple(332, 0), std::make_tuple(332, 0)},
    {std::make_tuple(333, 0), std::make_tuple(333, 0)},
    {std::make_tuple(334, 0), std::make_tuple(334, 0)},
    {std::make_tuple(336, 0), std::make_tuple(336, 0)},
    {std::make_tuple(337, 0), std::make_tuple(337, 0)},
    {std::make_tuple(338, 0), std::make_tuple(338, 0)},
    {std::make_tuple(339, 0), std::make_tuple(339, 0)},
    {std::make_tuple(340, 0), std::make_tuple(340, 0)},
    {std::make_tuple(341, 0), std::make_tuple(341, 0)},
    {std::make_tuple(342, 0), std::make_tuple(342, 0)},
    {std::make_tuple(344, 0), std::make_tuple(344, 0)},
    {std::make_tuple(345, 0), std::make_tuple(345, 0)},
    {std::make_tuple(346, 0), std::make_tuple(346, 0)},
    {std::make_tuple(347, 0), std::make_tuple(347, 0)},
    {std::make_tuple(348, 0), std::make_tuple(348, 0)},
    {std::make_tuple(349, 0), std::make_tuple(349, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(350, 0), std::make_tuple(350, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(351, 0), std::make_tuple(351, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(352, 0), std::make_tuple(352, 0)},
    {std::make_tuple(353, 0), std::make_tuple(353, 0)},
    {std::make_tuple(354, 0), std::make_tuple(354, 0)},
    {std::make_tuple(355, 0), std::make_tuple(355, 0)},
    {std::make_tuple(356, 0), std::make_tuple(356, 0)},
    {std::make_tuple(357, 0), std::make_tuple(357, 0)},
    {std::make_tuple(359, 0), std::make_tuple(359, 0)},
    {std::make_tuple(360, 0), std::make_tuple(360, 0)},
    {std::make_tuple(361, 0), std::make_tuple(361, 0)},
    {std::make_tuple(362, 0), std::make_tuple(362, 0)},
    {std::make_tuple(363, 0), std::make_tuple(363, 0)},
    {std::make_tuple(364, 0), std::make_tuple(364, 0)},
    {std::make_tuple(365, 0), std::make_tuple(365, 0)},
    {std::make_tuple(366, 0), std::make_tuple(366, 0)},
    {std::make_tuple(367, 0), std::make_tuple(367, 0)},
    {std::make_tuple(368, 0), std::make_tuple(368, 0)},
    {std::make_tuple(369, 0), std::make_tuple(369, 0)},
    {std::make_tuple(370, 0), std::make_tuple(370, 0)},
    {std::make_tuple(371, 0), std::make_tuple(371, 0)},
    {std::make_tuple(372, 0), std::make_tuple(372, 0)},
    {std::make_tuple(373, 0), std::make_tuple(373, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(374, 0), std::make_tuple(374, 0)},
    {std::make_tuple(375, 0), std::make_tuple(375, 0)},
    {std::make_tuple(376, 0), std::make_tuple(376, 0)},
    {std::make_tuple(377, 0), std::make_tuple(377, 0)},
    {std::make_tuple(378, 0), std::make_tuple(378, 0)},
    {std::make_tuple(379, 0), std::make_tuple(379, 0)},
    {std::make_tuple(380, 0), std::make_tuple(380, 0)},
    {std::make_tuple(381, 0), std::make_tuple(381, 0)},
    {std::make_tuple(382, 0), std::make_tuple(382, 0)},
    {std::make_tuple(383, 0), std::make_tuple(383, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(384, 0), std::make_tuple(384, 0)},
    {std::make_tuple(388, 0), std::make_tuple(388, 0)},
    {std::make_tuple(390, 0), std::make_tuple(390, 0)},
    {std::make_tuple(391, 0), std::make_tuple(391, 0)},
    {std::make_tuple(392, 0), std::make_tuple(392, 0)},
    {std::make_tuple(393, 0), std::make_tuple(393, 0)},
    {std::make_tuple(394, 0), std::make_tuple(394, 0)},
    {std::make_tuple(396, 0), std::make_tuple(396, 0)},
    {std::make_tuple(397, 0), std::make_tuple(397, 0)},// NEEDS MANUAL METADATA ADDITIONS
    {std::make_tuple(400, 0), std::make_tuple(400, 0)},
    {std::make_tuple(403, 0), std::make_tuple(403, 0)},
    {std::make_tuple(404, 0), std::make_tuple(404, 0)},
    {std::make_tuple(405, 0), std::make_tuple(405, 0)},
    {std::make_tuple(406, 0), std::make_tuple(406, 0)},
    {std::make_tuple(407, 0), std::make_tuple(407, 0)},
    {std::make_tuple(408, 0), std::make_tuple(408, 0)},
    {std::make_tuple(409, 0), std::make_tuple(409, 0)},
    {std::make_tuple(410, 0), std::make_tuple(422, 0)},
    {std::make_tuple(411, 0), std::make_tuple(411, 0)},
    {std::make_tuple(412, 0), std::make_tuple(412, 0)},
    {std::make_tuple(413, 0), std::make_tuple(413, 0)},
    {std::make_tuple(414, 0), std::make_tuple(414, 0)},
    {std::make_tuple(415, 0), std::make_tuple(415, 0)},
    {std::make_tuple(420, 0), std::make_tuple(420, 0)},
    {std::make_tuple(426, 0), std::make_tuple(426, 0)},
    {std::make_tuple(427, 0), std::make_tuple(427, 0)},
    {std::make_tuple(428, 0), std::make_tuple(428, 0)},
    {std::make_tuple(429, 0), std::make_tuple(429, 0)},
    {std::make_tuple(430, 0), std::make_tuple(430, 0)},
    {std::make_tuple(431, 0), std::make_tuple(431, 0)},
    {std::make_tuple(432, 0), std::make_tuple(432, 0)},
    {std::make_tuple(433, 0), std::make_tuple(433, 0)},
    {std::make_tuple(434, 0), std::make_tuple(457, 0)},
    {std::make_tuple(435, 0), std::make_tuple(458, 0)},
    {std::make_tuple(436, 0), std::make_tuple(459, 0)},
    {std::make_tuple(437, 0), std::make_tuple(437, 0)},
    {std::make_tuple(438, 0), std::make_tuple(438, 0)},
    {std::make_tuple(441, 0), std::make_tuple(441, 0)},
    {std::make_tuple(443, 0), std::make_tuple(444, 0)},
    {std::make_tuple(450, 0), std::make_tuple(445, 0)},
}
#pragma region
#undef ITEM_IRONSHOVEL
#undef ITEM_IRONPICKAXE
#undef ITEM_IRONAXE
#undef ITEM_FLINTANDSTEEL
#undef ITEM_APPLE
#undef ITEM_BOW
#undef ITEM_ARROW
#undef ITEM_COAL_
#undef ITEM_DIAMOND
#undef ITEM_IRONINGOT
#undef ITEM_GOLDINGOT
#undef ITEM_IRONSWORD
#undef ITEM_WOODENSWORD
#undef ITEM_WOODENSHOVEL
#undef ITEM_WOODENPICKAXE
#undef ITEM_WOODENAXE
#undef ITEM_STONESWORD
#undef ITEM_STONESHOVEL
#undef ITEM_STONEPICKAXE
#undef ITEM_STONEAXE
#undef ITEM_DIAMONDSWORD
#undef ITEM_DIAMONDSHOVEL
#undef ITEM_DIAMONDPICKAXE
#undef ITEM_DIAMONDAXE
#undef ITEM_STICK
#undef ITEM_BOWL
#undef ITEM_MUSHROOMSTEW
#undef ITEM_GOLDENSWORD
#undef ITEM_GOLDENSHOVEL
#undef ITEM_GOLDENPICKAXE
#undef ITEM_GOLDENAXE
#undef ITEM_STRING
#undef ITEM_FEATHER
#undef ITEM_GUNPOWDER
#undef ITEM_WOODENHOE
#undef ITEM_STONEHOE
#undef ITEM_IRONHOE
#undef ITEM_DIAMONDHOE
#undef ITEM_GOLDENHOE
#undef ITEM_WHEATSEEDS
#undef ITEM_WHEAT
#undef ITEM_BREAD
#undef ITEM_LEATHERHELMET
#undef ITEM_LEATHERCHESTPLATE
#undef ITEM_LEATHERLEGGINGS
#undef ITEM_LEATHERBOOTS
#undef ITEM_CHAINMAILHELMET
#undef ITEM_CHAINMAILCHESTPLATE
#undef ITEM_CHAINMAILLEGGINGS
#undef ITEM_CHAINMAILBOOTS
#undef ITEM_IRONHELMET
#undef ITEM_IRONCHESTPLATE
#undef ITEM_IRONLEGGINGS
#undef ITEM_IRONBOOTS
#undef ITEM_DIAMONDHELMET
#undef ITEM_DIAMONDCHESTPLATE
#undef ITEM_DIAMONDLEGGINGS
#undef ITEM_DIAMONDBOOTS
#undef ITEM_GOLDENHELMET
#undef ITEM_GOLDENCHESTPLATE
#undef ITEM_GOLDENLEGGINGS
#undef ITEM_GOLDENBOOTS
#undef ITEM_FLINT
#undef ITEM_PORKCHOP
#undef ITEM_COOKEDPORKCHOP
#undef ITEM_PAINTING
#undef ITEM_GOLDENAPPLE_
#undef ITEM_SIGN
#undef ITEM_WOODENDOOR
#undef ITEM_BUCKET
#undef ITEM_MINECART
#undef ITEM_SADDLE
#undef ITEM_IRONDOOR
#undef ITEM_REDSTONE
#undef ITEM_SNOWBALL
#undef ITEM_BOAT
#undef ITEM_LEATHER
#undef ITEM_BRICK
#undef ITEM_CLAYBALL
#undef ITEM_REEDS
#undef ITEM_PAPER
#undef ITEM_BOOK
#undef ITEM_SLIMEBALL
#undef ITEM_CHESTMINECART
#undef ITEM_EGG
#undef ITEM_COMPASS
#undef ITEM_FISHINGROD
#undef ITEM_CLOCK
#undef ITEM_GLOWSTONEDUST
#undef ITEM_FISH_
#undef ITEM_COOKEDFISH_
#undef ITEM_DYE_
#undef ITEM_BONE
#undef ITEM_SUGAR
#undef ITEM_CAKE
#undef ITEM_BED
#undef ITEM_REPEATER
#undef ITEM_COOKIE
#undef ITEM_SHEARS
#undef ITEM_MELON
#undef ITEM_PUMPKINSEEDS
#undef ITEM_MELONSEEDS
#undef ITEM_BEEF
#undef ITEM_COOKEDBEEF
#undef ITEM_CHICKEN
#undef ITEM_COOKEDCHICKEN
#undef ITEM_ROTTENFLESH
#undef ITEM_ENDERPEARL
#undef ITEM_BLAZEROD
#undef ITEM_GHASTTEAR
#undef ITEM_GOLDNUGGET
#undef ITEM_NETHERWART
#undef ITEM_POTION_
#undef ITEM_GLASSBOTTLE
#undef ITEM_SPIDEREYE
#undef ITEM_FERMENTEDSPIDEREYE
#undef ITEM_BLAZEPOWDER
#undef ITEM_MAGMACREAM
#undef ITEM_BREWINGSTAND
#undef ITEM_CAULDRON
#undef ITEM_ENDEREYE
#undef ITEM_SPECKLEDMELON
#undef ITEM_SPAWNEGG_
#undef ITEM_EXPERIENCEBOTTLE
#undef ITEM_EMERALD
#undef ITEM_FLOWERPOT
#undef ITEM_CARROT
#undef ITEM_POTATO
#undef ITEM_BAKEDPOTATO
#undef ITEM_POISONOUSPOTATO
#undef ITEM_GOLDENCARROT
#undef ITEM_SKULL_
#undef ITEM_PUMPKINPIE
#undef ITEM_ENCHANTEDBOOK
#undef ITEM_COMPARATOR
#undef ITEM_NETHERBRICK
#undef ITEM_QUARTZ
#undef ITEM_TNTMINECART
#undef ITEM_HOPPERMINECART
#undef ITEM_PRISMARINESHARD
#undef ITEM_PRISMARINECRYSTALS
#undef ITEM_RABBIT
#undef ITEM_COOKEDRABBIT
#undef ITEM_RABBITSTEW
#undef ITEM_RABBITFOOT
#undef ITEM_RABBITHIDE
#undef ITEM_LEAD
#undef ITEM_ENDCRYSTAL
#undef ITEM_SPRUCEDOOR
#undef ITEM_BIRCHDOOR
#undef ITEM_JUNGLEDOOR
#undef ITEM_ACACIADOOR
#undef ITEM_DARKOAKDOOR
#undef ITEM_CHORUSFRUIT
#undef ITEM_CHORUSFRUITPOPPED
#undef ITEM_BEETROOT
#undef ITEM_BEETROOTSEEDS
#undef ITEM_BEETROOTSOUP
#undef ITEM_DRAGONBREATH
#undef ITEM_SPLASHPOTION
#undef ITEM_LINGERINGPOTION
#undef ITEM_ELYTRA
#undef ITEM_SHULKERSHELL
#pragma endregion
} // NS Andromeda
/*
UNACCOUTNED FOR MCPE ITEMS
['    coal                                    0', '    charcoal                                1', '    bucket                                  0', '    milk                                    1', '    bucketWater                             8', '    bucketLava                              10', '    oak                                     0', '    spruce                                  1', '    birch                                   2', '    jungle                                  3', '    acacia                                  4', '    big_oak                                 5', '    black/ink_sac                         0', '    red                                     1', '    green                                   2', '    brown/cocoa_beans                     3', '    blue                                    4', '    purple                                  5', '    cyan                                    6', '    silver                                  7', '    gray                                    8', '    pink                                    9', '    lime                                    10', '    yellow                                  11', '    lightBlue                               12', '    magenta                                 13', '    orange                                  14', '    white/Bonemeal                        15', 'map_filled                      358', '    See Entities section for a list of data values', '    Note: all entities can be spawned using spawn eggs', 'fireball                        385', 'frame                           389', 'emptyMap                        395', '    skeleton                                0', '    wither                                  1', '    zombie                                  2', '    player                                  3', '    creeper                                 4', '    dragon                                  5', 'carrotOnAStick                  398', 'netherStar                      399', 'hopper                          410', 'horsearmorleather               416', 'horsearmoriron                  417', 'horsearmorgold                  418', 'horsearmordiamond               419', 'nameTag                         421', 'muttonRaw                       423', 'muttonCooked                    424', 'salmon                          460', 'clownfish                       461', 'pufferfish                      462', 'cooked_salmon                   463', 'appleEnchanted                  466']
*/
