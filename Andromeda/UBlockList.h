#pragma once

#include <map>
#include "Types.h"

// This file was automatically generated
// To generate, use `python blocks.py`

namespace Andromeda {
namespace Blocks {
const UBlockName Air = "air";
const UBlockName Stone = "stone";
const UBlockName Grass = "grass";
const UBlockName Dirt = "dirt";
const UBlockName Cobblestone = "cobblestone";
const UBlockName Wood = "wood";
const UBlockName Sapling = "sapling";
const UBlockName Bedrock = "bedrock";
const UBlockName FlowingWater = "flowing_water";
const UBlockName Water = "water";
const UBlockName FlowingLava = "flowing_lava";
const UBlockName Lava = "lava";
const UBlockName Sand = "sand";
const UBlockName Gravel = "gravel";
const UBlockName GoldOre = "gold_ore";
const UBlockName IronOre = "iron_ore";
const UBlockName CoalOre = "coal_ore";
const UBlockName Log = "log";
const UBlockName Leaves = "leaves";
const UBlockName Sponge = "sponge";
const UBlockName Glass = "glass";
const UBlockName LapisOre = "lapis_ore";
const UBlockName LapisBlock = "lapis_block";
const UBlockName Dispenser = "dispenser";
const UBlockName Sandstone = "sandstone";
const UBlockName Noteblock = "noteblock";
const UBlockName Bed = "bed";
const UBlockName GoldenRail = "golden_rail";
const UBlockName DetectorRail = "detector_rail";
const UBlockName StickyPiston = "sticky_piston";
const UBlockName Web = "web";
const UBlockName Tallgrass = "tallgrass";
const UBlockName Deadbush = "deadbush";
const UBlockName Piston = "piston";
const UBlockName PistonHead = "piston_head";
const UBlockName Wool = "wool";
const UBlockName PistonMovingPiece = "piston_moving_piece";
const UBlockName YellowFlower = "yellow_flower";
const UBlockName RedFlower = "red_flower";
const UBlockName BrownMushroom = "brown_mushroom";
const UBlockName RedMushroom = "red_mushroom";
const UBlockName GoldBlock = "gold_block";
const UBlockName IronBlock = "iron_block";
const UBlockName DoubleStoneSlab = "double_stone_slab";
const UBlockName StoneSlab = "stone_slab";
const UBlockName Brick = "brick";
const UBlockName Tnt = "tnt";
const UBlockName Bookshelf = "bookshelf";
const UBlockName MossyCobblestone = "mossy_cobblestone";
const UBlockName Obsidian = "obsidian";
const UBlockName Torch = "torch";
const UBlockName Fire = "fire";
const UBlockName MobSpawner = "mob_spawner";
const UBlockName OakStairs = "oak_stairs";
const UBlockName Chest = "chest";
const UBlockName RedstoneWire = "redstone_wire";
const UBlockName DiamondOre = "diamond_ore";
const UBlockName DiamondBlock = "diamond_block";
const UBlockName CraftingTable = "crafting_table";
const UBlockName Wheat = "wheat";
const UBlockName Farmland = "farmland";
const UBlockName Furnace = "furnace";
const UBlockName BurningFurnace = "burning_furnace";
const UBlockName SignPost = "sign_post";
const UBlockName WoodenDoor = "wooden_door";
const UBlockName Ladder = "ladder";
const UBlockName Rail = "rail";
const UBlockName CobblestoneStairs = "cobblestone_stairs";
const UBlockName WallSign = "wall_sign";
const UBlockName Lever = "lever";
const UBlockName StonePressurePlate = "stone_pressure_plate";
const UBlockName IronDoor = "iron_door";
const UBlockName WoodenPressurePlate = "wooden_pressure_plate";
const UBlockName RedstoneOre = "redstone_ore";
const UBlockName GlowingRedstoneOre = "glowing_redstone_ore";
const UBlockName RedstoneTorchOff = "redstone_torch_off";
const UBlockName RedstoneTorchOn = "redstone_torch_on";
const UBlockName StoneButton = "stone_button";
const UBlockName SnowLayer = "snow_layer";
const UBlockName Ice = "ice";
const UBlockName SnowBlock = "snow_block";
const UBlockName Cactus = "cactus";
const UBlockName Clay = "clay";
const UBlockName SugarCaneBlock = "sugar_cane_block";
const UBlockName Jukebox = "jukebox";
const UBlockName Fence = "fence";
const UBlockName Pumpkin = "pumpkin";
const UBlockName Netherrack = "netherrack";
const UBlockName SoulSand = "soul_sand";
const UBlockName Glowstone = "glowstone";
const UBlockName Portal = "portal";
const UBlockName JackOLantern = "jack_o_lantern";
const UBlockName Cake = "cake";
const UBlockName UnpoweredRepeater = "unpowered_repeater";
const UBlockName PoweredRepeater = "powered_repeater";
const UBlockName StainedGlass = "stained_glass";
const UBlockName TrapDoor = "trap_door";
const UBlockName MonsterEgg = "monster_egg";
const UBlockName SmoothBrick = "smooth_brick";
const UBlockName BrownMushroomBlock = "brown_mushroom_block";
const UBlockName RedMushroomBlock = "red_mushroom_block";
const UBlockName IronBars = "iron_bars";
const UBlockName GlassPane = "glass_pane";
const UBlockName MelonBlock = "melon_block";
const UBlockName PumpkinStem = "pumpkin_stem";
const UBlockName MelonStem = "melon_stem";
const UBlockName Vine = "vine";
const UBlockName FenceGate = "fence_gate";
const UBlockName BrickStairs = "brick_stairs";
const UBlockName SmoothStairs = "smooth_stairs";
const UBlockName Mycel = "mycel";
const UBlockName WaterLily = "water_lily";
const UBlockName NetherBrick = "nether_brick";
const UBlockName NetherBrickFence = "nether_brick_fence";
const UBlockName NetherBrickStairs = "nether_brick_stairs";
const UBlockName NetherWart = "nether_wart";
const UBlockName EnchantingTable = "enchanting_table";
const UBlockName BrewingStand = "brewing_stand";
const UBlockName Cauldron = "cauldron";
const UBlockName EndPortal = "end_portal";
const UBlockName EndPortalFrame = "end_portal_frame";
const UBlockName EndStone = "end_stone";
const UBlockName DragonEgg = "dragon_egg";
const UBlockName RedstoneLampOff = "redstone_lamp_off";
const UBlockName RedstoneLampOn = "redstone_lamp_on";
const UBlockName DoubleWoodenSlab = "double_wooden_slab";
const UBlockName WoodenSlab = "wooden_slab";
const UBlockName Cocoa = "cocoa";
const UBlockName SandstoneStairs = "sandstone_stairs";
const UBlockName EmeraldOre = "emerald_ore";
const UBlockName EnderChest = "ender_chest";
const UBlockName TripwireHook = "tripwire_hook";
const UBlockName Tripwire = "tripwire";
const UBlockName EmeraldBlock = "emerald_block";
const UBlockName SpruceStairs = "spruce_stairs";
const UBlockName BirchStairs = "birch_stairs";
const UBlockName JungleStairs = "jungle_stairs";
const UBlockName Command = "command";
const UBlockName Beacon = "beacon";
const UBlockName CobblestoneWall = "cobblestone_wall";
const UBlockName FlowerPot = "flower_pot";
const UBlockName Carrot = "carrot";
const UBlockName Potato = "potato";
const UBlockName WoodenButton = "wooden_button";
const UBlockName Skull = "skull";
const UBlockName Anvil = "anvil";
const UBlockName TrappedChest = "trapped_chest";
const UBlockName GoldPressurePlate = "gold_pressure_plate";
const UBlockName IronPressurePlate = "iron_pressure_plate";
const UBlockName RedstoneComparatorOff = "redstone_comparator_off";
const UBlockName RedstoneComparatorOn = "redstone_comparator_on";
const UBlockName DaylightDetector = "daylight_detector";
const UBlockName RedstoneBlock = "redstone_block";
const UBlockName QuartzOre = "quartz_ore";
const UBlockName Hopper = "hopper";
const UBlockName QuartzBlock = "quartz_block";
const UBlockName QuartzStairs = "quartz_stairs";
const UBlockName ActivatorRail = "activator_rail";
const UBlockName Dropper = "dropper";
const UBlockName StainedHardenedClay = "stained_hardened_clay";
const UBlockName StainedGlassPane = "stained_glass_pane";
const UBlockName Leaves2 = "leaves_2";
const UBlockName Log2 = "log_2";
const UBlockName AcaciaStairs = "acacia_stairs";
const UBlockName DarkOakStairs = "dark_oak_stairs";
const UBlockName SlimeBlock = "slime_block";
const UBlockName Barrier = "barrier";
const UBlockName IronTrapdoor = "iron_trapdoor";
const UBlockName Prismarine = "prismarine";
const UBlockName SeaLantern = "sea_lantern";
const UBlockName HayBlock = "hay_block";
const UBlockName Carpet = "carpet";
const UBlockName HardenedClay = "hardened_clay";
const UBlockName CoalBlock = "coal_block";
const UBlockName PackedIce = "packed_ice";
const UBlockName DoublePlant = "double_plant";
const UBlockName StandingBanner = "standing_banner";
const UBlockName WallBanner = "wall_banner";
const UBlockName DaylightDetectorInverted = "daylight_detector_inverted";
const UBlockName RedSandstone = "red_sandstone";
const UBlockName RedSandstoneStairs = "red_sandstone_stairs";
const UBlockName DoubleStoneSlab2 = "double_stone_slab2";
const UBlockName StoneSlab2 = "stone_slab2";
const UBlockName SpruceFenceGate = "spruce_fence_gate";
const UBlockName BirchFenceGate = "birch_fence_gate";
const UBlockName JungleFenceGate = "jungle_fence_gate";
const UBlockName DarkOakFenceGate = "dark_oak_fence_gate";
const UBlockName AcaciaFenceGate = "acacia_fence_gate";
const UBlockName SpruceFence = "spruce_fence";
const UBlockName BirchFence = "birch_fence";
const UBlockName JungleFence = "jungle_fence";
const UBlockName DarkOakFence = "dark_oak_fence";
const UBlockName AcaciaFence = "acacia_fence";
const UBlockName SpruceDoor = "spruce_door";
const UBlockName BirchDoor = "birch_door";
const UBlockName JungleDoor = "jungle_door";
const UBlockName AcaciaDoor = "acacia_door";
const UBlockName DarkOakDoor = "dark_oak_door";
const UBlockName EndRod = "end_rod";
const UBlockName ChorusPlant = "chorus_plant";
const UBlockName ChorusFlower = "chorus_flower";
const UBlockName PurpurBlock = "purpur_block";
const UBlockName PurpurPillar = "purpur_pillar";
const UBlockName PurpurStairs = "purpur_stairs";
const UBlockName PurpurDoubleSlab = "purpur_double_slab";
const UBlockName PurpurSlab = "purpur_slab";
const UBlockName EndBricks = "end_bricks";
const UBlockName BeetrootBlock = "beetroot_block";
const UBlockName GrassPath = "grass_path";
const UBlockName EndGateway = "end_gateway";
const UBlockName CommandRepeating = "command_repeating";
const UBlockName CommandChain = "command_chain";
const UBlockName FrostedIce = "frosted_ice";
const UBlockName Magma = "magma";
const UBlockName NetherWartBlock = "nether_wart_block";
const UBlockName RedNetherBrick = "red_nether_brick";
const UBlockName BoneBlock = "bone_block";
const UBlockName StructureVoid = "structure_void";
const UBlockName Observer = "observer";
const UBlockName WhiteShulkerBox = "white_shulker_box";
const UBlockName OrangeShulkerBox = "orange_shulker_box";
const UBlockName MagentaShulkerBox = "magenta_shulker_box";
const UBlockName LightBlueShulkerBox = "light_blue_shulker_box";
const UBlockName YellowShulkerBox = "yellow_shulker_box";
const UBlockName LimeShulkerBox = "lime_shulker_box";
const UBlockName PinkShulkerBox = "pink_shulker_box";
const UBlockName GrayShulkerBox = "gray_shulker_box";
const UBlockName SilverShulkerBox = "silver_shulker_box";
const UBlockName CyanShulkerBox = "cyan_shulker_box";
const UBlockName PurpleShulkerBox = "purple_shulker_box";
const UBlockName BlueShulkerBox = "blue_shulker_box";
const UBlockName BrownShulkerBox = "brown_shulker_box";
const UBlockName GreenShulkerBox = "green_shulker_box";
const UBlockName RedShulkerBox = "red_shulker_box";
const UBlockName BlackShulkerBox = "black_shulker_box";
const UBlockName StructureBlock = "structure_block";

} // NS Andromeda::Blocks 

std::map<UBlockName, short> UBlockIDs = {
     {Blocks::Air, 0},
     {Blocks::Stone, 257},
     {Blocks::Grass, 514},
     {Blocks::Dirt, 771},
     {Blocks::Cobblestone, 1028},
     {Blocks::Wood, 1285},
     {Blocks::Sapling, 1542},
     {Blocks::Bedrock, 1799},
     {Blocks::FlowingWater, 2056},
     {Blocks::Water, 2313},
     {Blocks::FlowingLava, 2570},
     {Blocks::Lava, 2827},
     {Blocks::Sand, 3084},
     {Blocks::Gravel, 3341},
     {Blocks::GoldOre, 3598},
     {Blocks::IronOre, 3855},
     {Blocks::CoalOre, 4112},
     {Blocks::Log, 4369},
     {Blocks::Leaves, 4626},
     {Blocks::Sponge, 4883},
     {Blocks::Glass, 5140},
     {Blocks::LapisOre, 5397},
     {Blocks::LapisBlock, 5654},
     {Blocks::Dispenser, 5911},
     {Blocks::Sandstone, 6168},
     {Blocks::Noteblock, 6425},
     {Blocks::Bed, 6682},
     {Blocks::GoldenRail, 6939},
     {Blocks::DetectorRail, 7196},
     {Blocks::StickyPiston, 7453},
     {Blocks::Web, 7710},
     {Blocks::Tallgrass, 7967},
     {Blocks::Deadbush, 8224},
     {Blocks::Piston, 8481},
     {Blocks::PistonHead, 8738},
     {Blocks::Wool, 8995},
     {Blocks::PistonMovingPiece, 9466},
     {Blocks::YellowFlower, 9509},
     {Blocks::RedFlower, 9766},
     {Blocks::BrownMushroom, 10023},
     {Blocks::RedMushroom, 10280},
     {Blocks::GoldBlock, 10537},
     {Blocks::IronBlock, 10794},
     {Blocks::DoubleStoneSlab, 11051},
     {Blocks::StoneSlab, 11308},
     {Blocks::Brick, 11565},
     {Blocks::Tnt, 11822},
     {Blocks::Bookshelf, 12079},
     {Blocks::MossyCobblestone, 12336},
     {Blocks::Obsidian, 12593},
     {Blocks::Torch, 12850},
     {Blocks::Fire, 13107},
     {Blocks::MobSpawner, 13364},
     {Blocks::OakStairs, 13621},
     {Blocks::Chest, 13878},
     {Blocks::RedstoneWire, 14135},
     {Blocks::DiamondOre, 14392},
     {Blocks::DiamondBlock, 14649},
     {Blocks::CraftingTable, 14906},
     {Blocks::Wheat, 15163},
     {Blocks::Farmland, 15420},
     {Blocks::Furnace, 15677},
     {Blocks::BurningFurnace, 15934},
     {Blocks::SignPost, 16191},
     {Blocks::WoodenDoor, 16448},
     {Blocks::Ladder, 16705},
     {Blocks::Rail, 16962},
     {Blocks::CobblestoneStairs, 17219},
     {Blocks::WallSign, 17476},
     {Blocks::Lever, 17733},
     {Blocks::StonePressurePlate, 17990},
     {Blocks::IronDoor, 18247},
     {Blocks::WoodenPressurePlate, 18504},
     {Blocks::RedstoneOre, 18761},
     {Blocks::GlowingRedstoneOre, 19018},
     {Blocks::RedstoneTorchOff, 19275},
     {Blocks::RedstoneTorchOn, 19532},
     {Blocks::StoneButton, 19789},
     {Blocks::SnowLayer, 20046},
     {Blocks::Ice, 20303},
     {Blocks::SnowBlock, 20560},
     {Blocks::Cactus, 20817},
     {Blocks::Clay, 21074},
     {Blocks::SugarCaneBlock, 21331},
     {Blocks::Jukebox, 21759},
     {Blocks::Fence, 21845},
     {Blocks::Pumpkin, 22102},
     {Blocks::Netherrack, 22359},
     {Blocks::SoulSand, 22616},
     {Blocks::Glowstone, 22873},
     {Blocks::Portal, 23130},
     {Blocks::JackOLantern, 23387},
     {Blocks::Cake, 23644},
     {Blocks::UnpoweredRepeater, 23901},
     {Blocks::PoweredRepeater, 24158},
     {Blocks::StainedGlass, 24480},
     {Blocks::TrapDoor, 24672},
     {Blocks::MonsterEgg, 24929},
     {Blocks::SmoothBrick, 25186},
     {Blocks::BrownMushroomBlock, 25443},
     {Blocks::RedMushroomBlock, 25700},
     {Blocks::IronBars, 25957},
     {Blocks::GlassPane, 26214},
     {Blocks::MelonBlock, 26471},
     {Blocks::PumpkinStem, 26728},
     {Blocks::MelonStem, 26985},
     {Blocks::Vine, 27242},
     {Blocks::FenceGate, 27499},
     {Blocks::BrickStairs, 27756},
     {Blocks::SmoothStairs, 28013},
     {Blocks::Mycel, 28270},
     {Blocks::WaterLily, 28527},
     {Blocks::NetherBrick, 28784},
     {Blocks::NetherBrickFence, 29041},
     {Blocks::NetherBrickStairs, 29298},
     {Blocks::NetherWart, 29555},
     {Blocks::EnchantingTable, 29812},
     {Blocks::BrewingStand, 30069},
     {Blocks::Cauldron, 30326},
     {Blocks::EndPortal, 30583},
     {Blocks::EndPortalFrame, 30840},
     {Blocks::EndStone, 31097},
     {Blocks::DragonEgg, 31354},
     {Blocks::RedstoneLampOff, 31611},
     {Blocks::RedstoneLampOn, 31868},
     {Blocks::DoubleWoodenSlab, 32157},
     {Blocks::WoodenSlab, 32414},
     {Blocks::Cocoa, 32639},
     {Blocks::SandstoneStairs, 32896},
     {Blocks::EmeraldOre, 33153},
     {Blocks::EnderChest, 33410},
     {Blocks::TripwireHook, 33667},
     {Blocks::Tripwire, 33924},
     {Blocks::EmeraldBlock, 34181},
     {Blocks::SpruceStairs, 34438},
     {Blocks::BirchStairs, 34695},
     {Blocks::JungleStairs, 34952},
     {Blocks::Command, 35327},
     {Blocks::Beacon, 35466},
     {Blocks::CobblestoneWall, 35723},
     {Blocks::FlowerPot, 35980},
     {Blocks::Carrot, 36237},
     {Blocks::Potato, 36494},
     {Blocks::WoodenButton, 36751},
     {Blocks::Skull, 37008},
     {Blocks::Anvil, 37265},
     {Blocks::TrappedChest, 37522},
     {Blocks::GoldPressurePlate, 37779},
     {Blocks::IronPressurePlate, 38036},
     {Blocks::RedstoneComparatorOff, 38293},
     {Blocks::RedstoneComparatorOn, 38550},
     {Blocks::DaylightDetector, 38807},
     {Blocks::RedstoneBlock, 39064},
     {Blocks::QuartzOre, 39321},
     {Blocks::Hopper, 39578},
     {Blocks::QuartzBlock, 39835},
     {Blocks::QuartzStairs, 40092},
     {Blocks::ActivatorRail, 40318},
     {Blocks::Dropper, 40573},
     {Blocks::StainedHardenedClay, 40863},
     {Blocks::StainedGlassPane, 41120},
     {Blocks::Leaves2, 41377},
     {Blocks::Log2, 41634},
     {Blocks::AcaciaStairs, 41891},
     {Blocks::DarkOakStairs, 42148},
     {Blocks::SlimeBlock, 42405},
     {Blocks::Barrier, 42751},
     {Blocks::IronTrapdoor, 42919},
     {Blocks::Prismarine, 43176},
     {Blocks::SeaLantern, 43433},
     {Blocks::HayBlock, 43690},
     {Blocks::Carpet, 43947},
     {Blocks::HardenedClay, 44204},
     {Blocks::CoalBlock, 44461},
     {Blocks::PackedIce, 44718},
     {Blocks::DoublePlant, 44975},
     {Blocks::StandingBanner, 45311},
     {Blocks::WallBanner, 45567},
     {Blocks::DaylightDetectorInverted, 45746},
     {Blocks::RedSandstone, 46003},
     {Blocks::RedSandstoneStairs, 46260},
     {Blocks::DoubleStoneSlab2, 46517},
     {Blocks::StoneSlab2, 46773},
     {Blocks::SpruceFenceGate, 47031},
     {Blocks::BirchFenceGate, 47288},
     {Blocks::JungleFenceGate, 47545},
     {Blocks::DarkOakFenceGate, 47802},
     {Blocks::AcaciaFenceGate, 48059},
     {Blocks::SpruceFence, 48383},
     {Blocks::BirchFence, 48639},
     {Blocks::JungleFence, 48895},
     {Blocks::DarkOakFence, 49151},
     {Blocks::AcaciaFence, 49407},
     {Blocks::SpruceDoor, 49601},
     {Blocks::BirchDoor, 49858},
     {Blocks::JungleDoor, 50115},
     {Blocks::AcaciaDoor, 50372},
     {Blocks::DarkOakDoor, 50629},
     {Blocks::EndRod, 50896},
     {Blocks::ChorusPlant, 51184},
     {Blocks::ChorusFlower, 51400},
     {Blocks::PurpurBlock, 51657},
     {Blocks::PurpurPillar, 51967},
     {Blocks::PurpurStairs, 52171},
     {Blocks::PurpurDoubleSlab, 52479},
     {Blocks::PurpurSlab, 52735},
     {Blocks::EndBricks, 52942},
     {Blocks::BeetrootBlock, 53236},
     {Blocks::GrassPath, 53446},
     {Blocks::EndGateway, 53713},
     {Blocks::CommandRepeating, 54015},
     {Blocks::CommandChain, 54271},
     {Blocks::FrostedIce, 54527},
     {Blocks::Magma, 54783},
     {Blocks::NetherWartBlock, 55039},
     {Blocks::RedNetherBrick, 55295},
     {Blocks::BoneBlock, 55551},
     {Blocks::StructureVoid, 55807},
     {Blocks::Observer, 56059},
     {Blocks::WhiteShulkerBox, 56319},
     {Blocks::OrangeShulkerBox, 56575},
     {Blocks::MagentaShulkerBox, 56831},
     {Blocks::LightBlueShulkerBox, 57087},
     {Blocks::YellowShulkerBox, 57343},
     {Blocks::LimeShulkerBox, 57599},
     {Blocks::PinkShulkerBox, 57855},
     {Blocks::GrayShulkerBox, 58111},
     {Blocks::SilverShulkerBox, 58367},
     {Blocks::CyanShulkerBox, 58623},
     {Blocks::PurpleShulkerBox, 58879},
     {Blocks::BlueShulkerBox, 59135},
     {Blocks::BrownShulkerBox, 59391},
     {Blocks::GreenShulkerBox, 59647},
     {Blocks::RedShulkerBox, 59903},
     {Blocks::BlackShulkerBox, 60159},
     {Blocks::StructureBlock, 65535},
};
const char *pocketMap[255] = {
"air",  "stone",  "grass",  "dirt",  "cobblestone",  "wood",  "sapling",  "bedrock",  "flowing_water",  "water",  "flowing_lava",  "lava",  "sand",  "gravel",  "gold_ore",  "iron_ore",  "coal_ore",  "log",  "leaves",  "sponge",  "glass",  "lapis_ore",  "lapis_block",  "dispenser",  "sandstone",  "noteblock",  "bed",  "golden_rail",  "detector_rail",  "sticky_piston",  "web",  "tallgrass",  "deadbush",  "piston",  "piston_head",  "wool",  nullptr,  "yellow_flower",  "red_flower",  "brown_mushroom",  "red_mushroom",  "gold_block",  "iron_block",  "double_stone_slab",  "stone_slab",  "brick",  "tnt",  "bookshelf",  "mossy_cobblestone",  "obsidian",  "torch",  "fire",  "mob_spawner",  "oak_stairs",  "chest",  "redstone_wire",  "diamond_ore",  "diamond_block",  "crafting_table",  "wheat",  "farmland",  "furnace",  "burning_furnace",  "sign_post",  "wooden_door",  "ladder",  "rail",  "cobblestone_stairs",  "wall_sign",  "lever",  "stone_pressure_plate",  "iron_door",  "wooden_pressure_plate",  "redstone_ore",  "glowing_redstone_ore",  "redstone_torch_off",  "redstone_torch_on",  "stone_button",  "snow_layer",  "ice",  "snow_block",  "cactus",  "clay",  "sugar_cane_block",  nullptr,  "fence",  "pumpkin",  "netherrack",  "soul_sand",  "glowstone",  "portal",  "jack_o_lantern",  "cake",  "unpowered_repeater",  "powered_repeater",  nullptr,  "trap_door",  "monster_egg",  "smooth_brick",  "brown_mushroom_block",  "red_mushroom_block",  "iron_bars",  "glass_pane",  "melon_block",  "pumpkin_stem",  "melon_stem",  "vine",  "fence_gate",  "brick_stairs",  "smooth_stairs",  "mycel",  "water_lily",  "nether_brick",  "nether_brick_fence",  "nether_brick_stairs",  "nether_wart",  "enchanting_table",  "brewing_stand",  "cauldron",  "end_portal",  "end_portal_frame",  "end_stone",  "dragon_egg",  "redstone_lamp_off",  "redstone_lamp_on",  "dropper",  "activator_rail",  "cocoa",  "sandstone_stairs",  "emerald_ore",  "ender_chest",  "tripwire_hook",  "tripwire",  "emerald_block",  "spruce_stairs",  "birch_stairs",  "jungle_stairs",  nullptr,  "beacon",  "cobblestone_wall",  "flower_pot",  "carrot",  "potato",  "wooden_button",  "skull",  "anvil",  "trapped_chest",  "gold_pressure_plate",  "iron_pressure_plate",  "redstone_comparator_off",  "redstone_comparator_on",  "daylight_detector",  "redstone_block",  "quartz_ore",  "hopper",  "quartz_block",  "quartz_stairs",  "double_wooden_slab",  "wooden_slab",  "stained_hardened_clay",  "stained_glass",  "leaves_2",  "log_2",  "acacia_stairs",  "dark_oak_stairs",  "slime_block",  nullptr,  "iron_trapdoor",  "prismarine",  "sea_lantern",  "hay_block",  "carpet",  "hardened_clay",  "coal_block",  "packed_ice",  "double_plant",  nullptr,  nullptr,  "daylight_detector_inverted",  "red_sandstone",  "red_sandstone_stairs",  "double_stone_slab2",  nullptr,  "spruce_fence_gate",  "birch_fence_gate",  "jungle_fence_gate",  "dark_oak_fence_gate",  "acacia_fence_gate",  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  "spruce_door",  "birch_door",  "jungle_door",  "acacia_door",  "dark_oak_door",  "grass_path",  nullptr,  "chorus_flower",  "purpur_block",  nullptr,  "purpur_stairs",  nullptr,  nullptr,  "end_bricks",  nullptr,  "end_rod",  "end_gateway",  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  "chorus_plant",  nullptr,  nullptr,  nullptr,  "beetroot_block",  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  "piston_moving_piece",  "observer",  nullptr,  nullptr,  nullptr,  nullptr,  
}; 

const char *javaMap[255] = {
"air",  "stone",  "grass",  "dirt",  "cobblestone",  "wood",  "sapling",  "bedrock",  "flowing_water",  "water",  "flowing_lava",  "lava",  "sand",  "gravel",  "gold_ore",  "iron_ore",  "coal_ore",  "log",  "leaves",  "sponge",  "glass",  "lapis_ore",  "lapis_block",  "dispenser",  "sandstone",  "noteblock",  "bed",  "golden_rail",  "detector_rail",  "sticky_piston",  "web",  "tallgrass",  "deadbush",  "piston",  "piston_head",  "wool",  "piston_moving_piece",  "yellow_flower",  "red_flower",  "brown_mushroom",  "red_mushroom",  "gold_block",  "iron_block",  "double_stone_slab",  "stone_slab",  "brick",  "tnt",  "bookshelf",  "mossy_cobblestone",  "obsidian",  "torch",  "fire",  "mob_spawner",  "oak_stairs",  "chest",  "redstone_wire",  "diamond_ore",  "diamond_block",  "crafting_table",  "wheat",  "farmland",  "furnace",  "burning_furnace",  "sign_post",  "wooden_door",  "ladder",  "rail",  "cobblestone_stairs",  "wall_sign",  "lever",  "stone_pressure_plate",  "iron_door",  "wooden_pressure_plate",  "redstone_ore",  "glowing_redstone_ore",  "redstone_torch_off",  "redstone_torch_on",  "stone_button",  "snow_layer",  "ice",  "snow_block",  "cactus",  "clay",  "sugar_cane_block",  "jukebox",  "fence",  "pumpkin",  "netherrack",  "soul_sand",  "glowstone",  "portal",  "jack_o_lantern",  "cake",  "unpowered_repeater",  "powered_repeater",  "stained_glass",  "trap_door",  "monster_egg",  "smooth_brick",  "brown_mushroom_block",  "red_mushroom_block",  "iron_bars",  "glass_pane",  "melon_block",  "pumpkin_stem",  "melon_stem",  "vine",  "fence_gate",  "brick_stairs",  "smooth_stairs",  "mycel",  "water_lily",  "nether_brick",  "nether_brick_fence",  "nether_brick_stairs",  "nether_wart",  "enchanting_table",  "brewing_stand",  "cauldron",  "end_portal",  "end_portal_frame",  "end_stone",  "dragon_egg",  "redstone_lamp_off",  "redstone_lamp_on",  "double_wooden_slab",  "wooden_slab",  "cocoa",  "sandstone_stairs",  "emerald_ore",  "ender_chest",  "tripwire_hook",  "tripwire",  "emerald_block",  "spruce_stairs",  "birch_stairs",  "jungle_stairs",  "command",  "beacon",  "cobblestone_wall",  "flower_pot",  "carrot",  "potato",  "wooden_button",  "skull",  "anvil",  "trapped_chest",  "gold_pressure_plate",  "iron_pressure_plate",  "redstone_comparator_off",  "redstone_comparator_on",  "daylight_detector",  "redstone_block",  "quartz_ore",  "hopper",  "quartz_block",  "quartz_stairs",  "activator_rail",  "dropper",  "stained_hardened_clay",  "stained_glass_pane",  "leaves_2",  "log_2",  "acacia_stairs",  "dark_oak_stairs",  "slime_block",  "barrier",  "iron_trapdoor",  "prismarine",  "sea_lantern",  "hay_block",  "carpet",  "hardened_clay",  "coal_block",  "packed_ice",  "double_plant",  "standing_banner",  "wall_banner",  "daylight_detector_inverted",  "red_sandstone",  "red_sandstone_stairs",  "double_stone_slab2",  "stone_slab2",  "spruce_fence_gate",  "birch_fence_gate",  "jungle_fence_gate",  "dark_oak_fence_gate",  "acacia_fence_gate",  "spruce_fence",  "birch_fence",  "jungle_fence",  "dark_oak_fence",  "acacia_fence",  "spruce_door",  "birch_door",  "jungle_door",  "acacia_door",  "dark_oak_door",  "end_rod",  "chorus_plant",  "chorus_flower",  "purpur_block",  "purpur_pillar",  "purpur_stairs",  "purpur_double_slab",  "purpur_slab",  "end_bricks",  "beetroot_block",  "grass_path",  "end_gateway",  "command_repeating",  "command_chain",  "frosted_ice",  "magma",  "nether_wart_block",  "red_nether_brick",  "bone_block",  "structure_void",  "observer",  "white_shulker_box",  "orange_shulker_box",  "magenta_shulker_box",  "light_blue_shulker_box",  "yellow_shulker_box",  "lime_shulker_box",  "pink_shulker_box",  "gray_shulker_box",  "silver_shulker_box",  "cyan_shulker_box",  "purple_shulker_box",  "blue_shulker_box",  "brown_shulker_box",  "green_shulker_box",  "red_shulker_box",  "black_shulker_box",  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  nullptr,  "structure_block",  
};

unsigned char pcDiffs[] {36,84,95,125,126,137,157,158,166,176,177,182,188,189,190,191,192,198,199,202,204,205,207,208,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,};
unsigned char peDiffs[] {250,160,157,158,126,125,181,208,240,244,198,251,};

} // NS Andromeda
