import re

# the third iteration !

item_ids = []

with open('C:/users/extreme/desktop/development/andromeda/export_pepcitems.csv') as f:
    item_ids  = f.read().splitlines()


print "#pragma once"
print ""
print '#include <map>'
print '#include <utility>'
print ""
print '#include "Types.h"'
print "\n// This file was automatically generated\n"
print "namespace Andromeda {"


BEGIN_PP_REGION = '#pragma region'
END_PP_REGION = '#pragma endregion'

BEGIN_ENUM = 'enum class UItem : UItemID {'

BEGIN_MAP_UNAME_TO_UID      = "const std::map<UItemName, UItemID> UItemNameToUItemID = {"
BEGIN_MAP_PEID_TO_UID       = "const std::map<std::pair<short, short>, UItemID> PocketItemToUItemID = {"
BEGIN_MAP_PEID_TO_UNAME     = "const std::map<std::pair<short, short>, UItemName> PocketItemToUItemName = {"
BEGIN_MAP_PEID_TO_JAVA      = "const std::map<std::pair<short, short>, std::pair<std::string, short>> PocketItemToJavaItem = {"

BEGIN_MAP_UID    = "const std::map<UItemID, std::tuple<std::pair<std::string, short>, std::pair<short, short>>> UItemM = {"
BEGIN_MAP_JAVAID = "const std::map<std::pair<std::string, short>, std::tuple<UItemID, std::pair<short, short>>> JavaItemM = {"
BEGIN_MAP_PEID   = "const std::map<std::pair<short, short>, std::tuple<UItemID, std::pair<std::string, short>>> PocketItemM = {"

DEFINE_MACRO = "#define %-*s %s"

DATA_LINE_UID       = '\t{{ {UItemID:25}, std::make_tuple(std::make_pair("{JavaItemName}", {JavaMetadata}), std::make_pair({PocketId}, {PocketMetadata}))}},'
DATA_LINE_JAVAID    = '\t{{ std::make_pair("{JavaItemName}", {JavaMetadata}), std::make_tuple({UItemID}, std::make_pair({PocketId}, {PocketMetadata}))}},'
DATA_LINE_POCKETID  = '\t{{ std::make_pair({PocketId}, {PocketMetadata}), std::make_tuple({UItemID}, std::make_pair("{JavaItemName}", {JavaMetadata}))}},'
DATA_LINE_ENUM      = '\t%s, '

END_TAG = "};"

consts = []
pp_defines = []
m_uid = []
m_javaid = []
m_pocketid = []

#print "namespace Items {"

I = 0

for i in item_ids:
    if i[0] == "#":
        continue

    s = i.split(",")
    uid_name    = s[4]
    java_id     = s[0]
    pocket_id   = s[1]
    java_meta   = s[2]
    pocket_meta = s[3]


    fname = ' '.join(uid_name.replace("_", ' ').split()).title().replace(" ", "").replace(".","_")

    """
    if pocket_id == "-1":
        print "// Unimplemnted item in pocket: ", s
    if "*" in fname:
        fname = fname.replace("*", "")
        uid_name = uid_name.replace("*", "")
        print 'const UItemName %s = "%s"; /* SPECIAL DATA */' % (fname, uid_name.strip())
    else:
        print 'const UItemName %s = "%s";' % (fname, uid_name.strip())
        """

    pp_str_append = ""
    if pocket_id == "-1":
        pp_str_append += " /* UNIMPLMENTED POCKET ITEM */"

    if "*" in fname or "*" in uid_name:
        pp_str_append += " /* SPECIAL DATA */"
        fname = fname.replace("*", "")
        uid_name = uid_name.replace("*", "")

    usesMetadata = False
    javaName = uid_name.split(".")[0].strip()
    id_macro_name = "UITEM_%s" % fname.upper()
    consts.append(DATA_LINE_ENUM % fname);
    pp_defines.append(DEFINE_MACRO % (30, id_macro_name, "static_cast<UItemID>(UItem::"+fname+")") + pp_str_append)

    m_uid.append(DATA_LINE_UID.format(UItemID = id_macro_name, JavaItemName = javaName, JavaMetadata = java_meta, PocketId = pocket_id, PocketMetadata = pocket_meta))
    m_javaid.append(DATA_LINE_JAVAID.format(UItemID = id_macro_name, JavaItemName = javaName, JavaMetadata = java_meta, PocketId = pocket_id, PocketMetadata = pocket_meta))
    m_pocketid.append(DATA_LINE_POCKETID.format(UItemID = id_macro_name, JavaItemName = javaName, JavaMetadata = java_meta, PocketId = pocket_id, PocketMetadata = pocket_meta))

    I += 1


#print "} // NS Andromeda::Items"

print BEGIN_ENUM
for i in consts:
    print i
print END_TAG

print BEGIN_PP_REGION
print "// 	UItemID (64-bit unsigned) = (((short(JavaID) << 16) | (short(PocketID) & 0xFFFF)) << 32) | ((short(JavaMetadata) << 16) | (short(PocketMetadata) & 0xFFFF))"
for i in pp_defines:
    print i
print END_PP_REGION

print BEGIN_MAP_UID
for i in m_uid:
    print i
print END_TAG

print BEGIN_MAP_JAVAID
for i in m_javaid:
    print i
print END_TAG

print BEGIN_MAP_PEID
for i in m_pocketid:
    print i
print END_TAG

# print BEGIN_PP_REGION
# for i in pp_defines:
#     print " ".join(i.replace("define", "undef").split(" ")[0:2])
# print END_PP_REGION


print "} // NS Andromeda"

print "/*"

for i in consts:
    print i.replace("\t", "static Item* m").replace(",", ";")

print "*/"

"""
print "/*"

print "UNACCOUTNED FOR MCPE ITEMS"
print mcpe_item_ids

print "*/"
"""