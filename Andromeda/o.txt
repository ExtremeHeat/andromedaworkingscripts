const UItemName IronShovel = "iron_shovel";
['iron_shovel', '256']
const UItemName IronPickaxe = "iron_pickaxe";
['iron_pickaxe', '257']
const UItemName IronAxe = "iron_axe";
['iron_axe', '258']
const UItemName FlintAndSteel = "flint_and_steel";
['flint_and_steel', '259']
const UItemName Apple = "apple";
['apple', '260']
const UItemName Bow = "bow";
['bow', '261']
const UItemName Arrow = "arrow";
['arrow', '262']
const UItemName Coal_ = "coal" USES METADATA;
['coal', '263']
const UItemName Diamond = "diamond";
['diamond', '264']
const UItemName IronIngot = "iron_ingot";
['iron_ingot', '265']
const UItemName GoldIngot = "gold_ingot";
['gold_ingot', '266']
const UItemName IronSword = "iron_sword";
['iron_sword', '267']
const UItemName WoodenSword = "wooden_sword";
['wooden_sword', '268']
const UItemName WoodenShovel = "wooden_shovel";
['wooden_shovel', '269']
const UItemName WoodenPickaxe = "wooden_pickaxe";
['wooden_pickaxe', '270']
const UItemName WoodenAxe = "wooden_axe";
['wooden_axe', '271']
const UItemName StoneSword = "stone_sword";
['stone_sword', '272']
const UItemName StoneShovel = "stone_shovel";
['stone_shovel', '273']
const UItemName StonePickaxe = "stone_pickaxe";
['stone_pickaxe', '274']
const UItemName StoneAxe = "stone_axe";
['stone_axe', '275']
const UItemName DiamondSword = "diamond_sword";
['diamond_sword', '276']
const UItemName DiamondShovel = "diamond_shovel";
['diamond_shovel', '277']
const UItemName DiamondPickaxe = "diamond_pickaxe";
['diamond_pickaxe', '278']
const UItemName DiamondAxe = "diamond_axe";
['diamond_axe', '279']
const UItemName Stick = "stick";
['stick', '280']
const UItemName Bowl = "bowl";
['bowl', '281']
const UItemName MushroomStew = "mushroom_stew";
['mushroom_stew', '282']
const UItemName GoldenSword = "golden_sword";
['golden_sword', '283']
const UItemName GoldenShovel = "golden_shovel";
['golden_shovel', '284']
const UItemName GoldenPickaxe = "golden_pickaxe";
['golden_pickaxe', '285']
const UItemName GoldenAxe = "golden_axe";
['golden_axe', '286']
const UItemName String = "string";
['string', '287']
const UItemName Feather = "feather";
['feather', '288']
const UItemName Gunpowder = "gunpowder";
['gunpowder', '289']
const UItemName WoodenHoe = "wooden_hoe";
['wooden_hoe', '290']
const UItemName StoneHoe = "stone_hoe";
['stone_hoe', '291']
const UItemName IronHoe = "iron_hoe";
['iron_hoe', '292']
const UItemName DiamondHoe = "diamond_hoe";
['diamond_hoe', '293']
const UItemName GoldenHoe = "golden_hoe";
['golden_hoe', '294']
const UItemName WheatSeeds = "wheat_seeds";
['wheat_seeds', '295']
const UItemName Wheat = "wheat";
['wheat_seeds', '295']
const UItemName Bread = "bread";
['bread', '297']
const UItemName LeatherHelmet = "leather_helmet";
['leather_helmet', '298']
const UItemName LeatherChestplate = "leather_chestplate";
['leather_chestplate', '299']
const UItemName LeatherLeggings = "leather_leggings";
['leather_leggings', '300']
const UItemName LeatherBoots = "leather_boots";
['leather_boots', '301']
const UItemName ChainmailHelmet = "chainmail_helmet";
['chainmail_helmet', '302']
const UItemName ChainmailChestplate = "chainmail_chestplate";
['chainmail_chestplate', '303']
const UItemName ChainmailLeggings = "chainmail_leggings";
['chainmail_leggings', '304']
const UItemName ChainmailBoots = "chainmail_boots";
['chainmail_boots', '305']
const UItemName IronHelmet = "iron_helmet";
['iron_helmet', '306']
const UItemName IronChestplate = "iron_chestplate";
['iron_chestplate', '307']
const UItemName IronLeggings = "iron_leggings";
['iron_leggings', '308']
const UItemName IronBoots = "iron_boots";
['iron_boots', '309']
const UItemName DiamondHelmet = "diamond_helmet";
['diamond_helmet', '310']
const UItemName DiamondChestplate = "diamond_chestplate";
['diamond_chestplate', '311']
const UItemName DiamondLeggings = "diamond_leggings";
['diamond_leggings', '312']
const UItemName DiamondBoots = "diamond_boots";
['diamond_boots', '313']
const UItemName GoldenHelmet = "golden_helmet";
['golden_helmet', '314']
const UItemName GoldenChestplate = "golden_chestplate";
['golden_chestplate', '315']
const UItemName GoldenLeggings = "golden_leggings";
['golden_leggings', '316']
const UItemName GoldenBoots = "golden_boots";
['golden_boots', '317']
const UItemName Flint = "flint";
['flint_and_steel', '259']
const UItemName Porkchop = "porkchop";
['porkchop', '319']
const UItemName CookedPorkchop = "cooked_porkchop";
['cooked_porkchop', '320']
const UItemName Painting = "painting";
['painting', '321']
const UItemName GoldenApple_ = "golden_apple" USES METADATA;
['golden_apple', '322']
const UItemName Sign = "sign";
['sign', '323']
const UItemName WoodenDoor = "wooden_door";
['wooden_door', '324']
const UItemName Bucket = "bucket";
['bucket', '325']
const UItemName WaterBucket = DO THIS MANUALLY;
const UItemName LavaBucket = DO THIS MANUALLY;
const UItemName Minecart = "minecart";
['minecart', '328']
const UItemName Saddle = "saddle";
['saddle', '329']
const UItemName IronDoor = "iron_door";
['iron_door', '330']
const UItemName Redstone = "redstone";
['redstone', '331']
const UItemName Snowball = "snowball";
['snowball', '332']
const UItemName Boat = "boat";
['boat', '333']
const UItemName Leather = "leather";
['leather_helmet', '298']
const UItemName MilkBucket = DO THIS MANUALLY;
const UItemName Brick = "brick";
['brick', '336']
const UItemName ClayBall = "clay_ball";
['clay_ball', '337']
const UItemName Reeds = "reeds";
['reeds', '338']
const UItemName Paper = "paper";
['paper', '339']
const UItemName Book = "book";
['book', '340']
const UItemName SlimeBall = "slime_ball";
['slime_ball', '341']
const UItemName ChestMinecart = "chest_minecart";
['chest_minecart', '342']
const UItemName FurnaceMinecart = DO THIS MANUALLY;
const UItemName Egg = "egg";
['leather_leggings', '300']
const UItemName Compass = "compass";
['compass', '345']
const UItemName FishingRod = "fishing_rod";
['fishing_rod', '346']
const UItemName Clock = "clock";
['clock', '347']
const UItemName GlowstoneDust = "glowstone_dust";
['glowstone_dust', '348']
const UItemName Fish_ = "fish" USES METADATA;
['fishing_rod', '346']
const UItemName CookedFish_ = "cooked_fish" USES METADATA;
['cooked_fish', '350']
const UItemName Dye_ = "dye" USES METADATA;
['dye', '351']
const UItemName Bone = "bone";
['bone', '352']
const UItemName Sugar = "sugar";
['sugar', '353']
const UItemName Cake = "cake";
['cake', '354']
const UItemName Bed = "bed";
['bed', '355']
const UItemName Repeater = "repeater";
['repeater', '356']
const UItemName Cookie = "cookie";
['cookie', '357']
const UItemName FilledMap_ = USES METADATA;
const UItemName Shears = "shears";
['shears', '359']
const UItemName Melon = "melon";
['melon', '360']
const UItemName PumpkinSeeds = "pumpkin_seeds";
['pumpkin_seeds', '361']
const UItemName MelonSeeds = "melon_seeds";
['melon_seeds', '362']
const UItemName Beef = "beef";
['beef', '363']
const UItemName CookedBeef = "cooked_beef";
['cooked_beef', '364']
const UItemName Chicken = "chicken";
['chicken', '365']
const UItemName CookedChicken = "cooked_chicken";
['cooked_chicken', '366']
const UItemName RottenFlesh = "rotten_flesh";
['rotten_flesh', '367']
const UItemName EnderPearl = "ender_pearl";
['ender_pearl', '368']
const UItemName BlazeRod = "blaze_rod";
['blaze_rod', '369']
const UItemName GhastTear = "ghast_tear";
['ghast_tear', '370']
const UItemName GoldNugget = "gold_nugget";
['gold_nugget', '371']
const UItemName NetherWart = "nether_wart";
['nether_wart', '372']
const UItemName Potion_ = "potion" USES METADATA;
['potion', '373']
const UItemName GlassBottle = "glass_bottle";
['glass_bottle', '374']
const UItemName SpiderEye = "spider_eye";
['spider_eye', '375']
const UItemName FermentedSpiderEye = "fermented_spider_eye";
['fermented_spider_eye', '376']
const UItemName BlazePowder = "blaze_powder";
['blaze_powder', '377']
const UItemName MagmaCream = "magma_cream";
['magma_cream', '378']
const UItemName BrewingStand = "brewing_stand";
['brewing_stand', '379']
const UItemName Cauldron = "cauldron";
['cauldron', '380']
const UItemName EnderEye = "ender_eye";
['ender_eye', '381']
const UItemName SpeckledMelon = "speckled_melon";
['speckled_melon', '382']
const UItemName SpawnEgg_ = "spawn_egg" USES METADATA;
['spawn_egg', '383']
const UItemName ExperienceBottle = "experience_bottle";
['experience_bottle', '384']
const UItemName FireCharge = DO THIS MANUALLY;
const UItemName WritableBook = DO THIS MANUALLY;
const UItemName WrittenBook = DO THIS MANUALLY;
const UItemName Emerald = "emerald";
['emerald', '388']
const UItemName ItemFrame = DO THIS MANUALLY;
const UItemName FlowerPot = "flower_pot";
['flower_pot', '390']
const UItemName Carrot = "carrot";
['carrot', '391']
const UItemName Potato = "potato";
['potato', '392']
const UItemName BakedPotato = "baked_potato";
['baked_potato', '393']
const UItemName PoisonousPotato = "poisonous_potato";
['poisonous_potato', '394']
const UItemName Map = "map";
['map_filled', '358']
const UItemName GoldenCarrot = "golden_carrot";
['golden_carrot', '396']
const UItemName Skull_ = "skull" USES METADATA;
['skull', '397']
const UItemName CarrotOnAStick = DO THIS MANUALLY;
const UItemName NetherStar = DO THIS MANUALLY;
const UItemName PumpkinPie = "pumpkin_pie";
['pumpkin_pie', '400']
const UItemName Fireworks = DO THIS MANUALLY;
const UItemName FireworkCharge = DO THIS MANUALLY;
const UItemName EnchantedBook = "enchanted_book";
['enchanted_book', '403']
const UItemName Comparator = "comparator";
['comparator', '404']
const UItemName Netherbrick = "netherbrick";
['netherbrick', '405']
const UItemName Quartz = "quartz";
['quartz', '406']
const UItemName TntMinecart = "tnt_minecart";
['tnt_minecart', '407']
const UItemName HopperMinecart = "hopper_minecart";
['hopper_minecart', '408']
const UItemName PrismarineShard = "prismarine_shard";
['prismarine_shard', '409']
const UItemName PrismarineCrystals = "prismarine_crystals";
['prismarine_crystals', '422']
const UItemName Rabbit = "rabbit";
['rabbit', '411']
const UItemName CookedRabbit = "cooked_rabbit";
['cooked_rabbit', '412']
const UItemName RabbitStew = "rabbit_stew";
['rabbit_stew', '413']
const UItemName RabbitFoot = "rabbit_foot";
['rabbit_foot', '414']
const UItemName RabbitHide = "rabbit_hide";
['rabbit_hide', '415']
const UItemName ArmorStand = DO THIS MANUALLY;
const UItemName IronHorseArmor = DO THIS MANUALLY;
const UItemName GoldenHorseArmor = DO THIS MANUALLY;
const UItemName DiamondHorseArmor = DO THIS MANUALLY;
const UItemName Lead = "lead";
['lead', '420']
const UItemName NameTag = DO THIS MANUALLY;
const UItemName CommandBlockMinecart = DO THIS MANUALLY;
const UItemName Mutton = "mutton";
['muttonRaw', '423']
const UItemName CookedMutton = DO THIS MANUALLY;
const UItemName Banner = DO THIS MANUALLY;
const UItemName EndCrystal = "end_crystal";
['end_crystal', '426']
const UItemName SpruceDoor = "spruce_door";
['spruce_door', '427']
const UItemName BirchDoor = "birch_door";
['birch_door', '428']
const UItemName JungleDoor = "jungle_door";
['jungle_door', '429']
const UItemName AcaciaDoor = "acacia_door";
['acacia_door', '430']
const UItemName DarkOakDoor = "dark_oak_door";
['dark_oak_door', '431']
const UItemName ChorusFruit = "chorus_fruit";
['chorus_fruit', '432']
const UItemName ChorusFruitPopped = "chorus_fruit_popped";
['chorus_fruit_popped', '433']
const UItemName Beetroot = "beetroot";
['beetroot', '457']
const UItemName BeetrootSeeds = "beetroot_seeds";
['beetroot_seeds', '458']
const UItemName BeetrootSoup = "beetroot_soup";
['beetroot_soup', '459']
const UItemName DragonBreath = "dragon_breath";
['dragon_breath', '437']
const UItemName SplashPotion = "splash_potion";
['splash_potion', '438']
const UItemName SpectralArrow = DO THIS MANUALLY;
const UItemName TippedArrow = DO THIS MANUALLY;
const UItemName LingeringPotion = "lingering_potion";
['lingering_potion', '441']
const UItemName Shield = DO THIS MANUALLY;
const UItemName Elytra = "elytra";
['elytra', '444']
const UItemName SpruceBoat = DO THIS MANUALLY;
const UItemName BirchBoat = DO THIS MANUALLY;
const UItemName JungleBoat = DO THIS MANUALLY;
const UItemName AcaciaBoat = DO THIS MANUALLY;
const UItemName DarkOakBoat = DO THIS MANUALLY;
const UItemName TotemOfUndying = DO THIS MANUALLY;
const UItemName ShulkerShell = "shulker_shell";
['shulker_shell', '445']
// skipped  451	1C3		(unused)
const UItemName IronNugget = DO THIS MANUALLY;
const UItemName Record13 = DO THIS MANUALLY;
const UItemName RecordCat = DO THIS MANUALLY;
const UItemName RecordBlocks = DO THIS MANUALLY;
const UItemName RecordChirp = DO THIS MANUALLY;
const UItemName RecordFar = DO THIS MANUALLY;
const UItemName RecordMall = DO THIS MANUALLY;
const UItemName RecordMellohi = DO THIS MANUALLY;
const UItemName RecordStal = DO THIS MANUALLY;
const UItemName RecordStrad = DO THIS MANUALLY;
const UItemName RecordWard = DO THIS MANUALLY;
const UItemName Record11 = DO THIS MANUALLY;
const UItemName RecordWait = DO THIS MANUALLY;
const std::map<UItemName, UItemID> = {
}
const std::map<std::tuple<short, short>, UItemID> = {
}
