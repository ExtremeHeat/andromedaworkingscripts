import re

mcpe_item_ids = []
pc_item_ids = []


with open('C:/users/extreme/desktop/development/andromeda/mcpe_item_list100.txt') as f:
    mcpe_item_ids  = f.read().splitlines()

with open('C:/users/extreme/desktop/development/andromeda/mc_item_list111.txt') as f:
    pc_item_ids = f.read().splitlines()


print "#pragma once"
print ""
print '#include <map>'
print '#include <tuple>'
print ""
print '#include "Types.h"'
print "\n// This file was automatically generated\n"
print "namespace Andromeda {"


BEGIN_PP_REGION = '#pragma region'
END_PP_REGION = '#pragma endregion'

BEGIN_MAP_UNAME_TO_UID      = "const std::map<UItemName, UItemID> = UItemNameToUItemID {"
BEGIN_MAP_PEID_TO_UID       = "const std::map<std::tuple<short, short>, UItemID> PocketItemToUItemID = {"
BEGIN_MAP_PEID_TO_UNAME     = "const std::map<std::tuple<short, short>, UItemName> PocketItemToUItemName = {"
BEGIN_MAP_PEID_TO_JAVA      = "const std::map<std::tuple<short, short>, std::tuple<std::string, short>> PocketItemToJavaItem = {"
BEGIN_MAP_JAVAID_TO_UID     = "const std::map<std::tuple<std::string, short>, UItemID> JavaItemToUItemID = {"
BEGIN_MAP_JAVAID_TO_UNAME   = "const std::map<std::tuple<std::string, short>, UItemName> JavaItemToUItemName = {"
BEGIN_MAP_JAVAID_TO_PEID    = "const std::map<std::tuple<std::string, short>, std::tuple<short, short>> JavaItemToPocketItem = {"

DEFINE_MACRO = "#define %-*s %s"
DATA_LINE_UNAME_TO_UID = '     {Items::%s, %s},'
DATA_LINE_TUPLE_TO_ID = '  {std::make_tuple(%s, %s), %s},'
DATA_LINE_TUPLE_TO_TUPLE = '    {std::make_tuple(%s, %s), std::make_tuple(%s, %s)},'
END_TAG = "}"

pp_defines = []
Muname_to_uid = []
Mpeid_to_uid = []
Mpeid_to_uname = []
Mpeid_to_java = []
Mjavaid_to_uid = []
Mjavaid_to_uname = []
Mjavaid_to_peid = []
M_UNAME_TO_UID = []
M_TUPLE_TO_ID = []

print "namespace Items {"

for pc_item in pc_item_ids:
    s = pc_item.split("\t")
    if len(s) == 1:
        continue

    try:
        name = s[2].split(":")[1]
    except Exception:
        print "// skipped ", pc_item
        continue

    fname = ' '.join(name.replace("_", ' ').split()).title().replace(" ", "")
    found = False
    for mcpe_item in mcpe_item_ids:
        if name + " " in mcpe_item:
            #print "-- Found!"
            found = True
            usesMetadata = False
            if pc_item.endswith(" B"):
                fname += "_"
                print 'const UItemName %s = "%s" USES METADATA;' % (fname, name)
                usesMetadata = True
            else:
                print 'const UItemName %s = "%s";' % (fname, name)
            n = re.sub(' +',' ', mcpe_item.lstrip()).split(" ")
            i = 0
            int1 = (int(s[0]) << 16) | int(n[1]) & 0xFFFF;
            int2s = "((0 << 16) | 0 & 0xFFFF)"
            #n[1] ;

            #b_pc = 
            #i >> s[0]
            #print n[1]

            """
            Java ID     = s[0]
            Pocket ID   = n[1]

            /*
	UItemID (64-bit unsigned) = (((short(JavaID) << 16) | (short(PocketID) & 0xFFFF)) << 32) | ((short(JavaMetadata) << 16) | (short(PocketMetadata) & 0xFFFF))
*/
            """
            id_macro_name = "ITEM_%s" % fname.upper()
            pp_defines.append(DEFINE_MACRO % (30, id_macro_name, "(" + str(int1) + " << 32) | " + int2s))
            Muname_to_uid.append(DATA_LINE_UNAME_TO_UID % (fname, id_macro_name ) + ("// NEEDS MANUAL METADATA ADDITIONS" if usesMetadata else ""))
            Mpeid_to_uid.append(DATA_LINE_TUPLE_TO_ID % (n[1], 0, id_macro_name ) + ("// NEEDS MANUAL METADATA ADDITIONS" if usesMetadata else ""))
            Mpeid_to_uname.append(DATA_LINE_TUPLE_TO_ID % (n[1], 0, "Item::" + fname) + ("// NEEDS MANUAL METADATA ADDITIONS" if usesMetadata else ""))
            Mpeid_to_java.append(DATA_LINE_TUPLE_TO_TUPLE % (n[1], 0, s[0], 0) + ("// NEEDS MANUAL METADATA ADDITIONS" if usesMetadata else ""))
            Mjavaid_to_uid.append(DATA_LINE_TUPLE_TO_ID % (s[0], 0, id_macro_name ) + ("// NEEDS MANUAL METADATA ADDITIONS" if usesMetadata else ""))
            Mjavaid_to_uname.append(DATA_LINE_TUPLE_TO_ID % (s[0], 0, "Item::" + fname) + ("// NEEDS MANUAL METADATA ADDITIONS" if usesMetadata else ""))
            Mjavaid_to_peid.append(DATA_LINE_TUPLE_TO_TUPLE % (s[0], 0, n[1], 0) + ("// NEEDS MANUAL METADATA ADDITIONS" if usesMetadata else ""))
            mcpe_item_ids = [x for x in mcpe_item_ids if not x == mcpe_item]
            break
        elif name.replace("_","") in mcpe_item_ids:
            found = True
            print "============================================"
            break

    if found == False:
        if pc_item.endswith(" B"):
            print 'const UItemName %s_ = USES METADATA;' % (fname)
        else:
            print 'const UItemName %s = DO THIS MANUALLY;' % (fname)
        #print "ID: ", s[0]
        #print "SName: ", s[2]
        #print "DisplayName: ", s[3]


print "} // NS Andromeda::Items"

print BEGIN_PP_REGION
for i in pp_defines:
    print i
print END_PP_REGION

print BEGIN_MAP_UNAME_TO_UID
for i in Muname_to_uid:
    print i
print END_TAG

print BEGIN_MAP_PEID_TO_UID
for i in Mpeid_to_uid:
    print i
print END_TAG

print BEGIN_MAP_PEID_TO_UNAME
for i in Mpeid_to_uname:
    print i
print END_TAG

print BEGIN_MAP_PEID_TO_JAVA
for i in Mpeid_to_java:
    print i
print END_TAG

print BEGIN_MAP_JAVAID_TO_UID
for i in Mjavaid_to_uid:
    print i
print END_TAG

print BEGIN_MAP_JAVAID_TO_UNAME
for i in Mjavaid_to_uname:
    print i
print END_TAG

print BEGIN_MAP_JAVAID_TO_PEID
for i in Mjavaid_to_peid:
    print i
print END_TAG

print BEGIN_PP_REGION
for i in pp_defines:
    print " ".join(i.replace("define", "undef").split(" ")[0:2])
print END_PP_REGION


print "} // NS Andromeda"

print "/*"

print "UNACCOUTNED FOR MCPE ITEMS"
print mcpe_item_ids

print "*/"