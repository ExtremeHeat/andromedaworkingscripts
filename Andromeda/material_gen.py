s = """    public static final Material air = new MaterialTransparent(MapColor.airColor);
    public static final Material grass = new Material(MapColor.grassColor);
    public static final Material ground = new Material(MapColor.dirtColor);
    public static final Material wood = (new Material(MapColor.woodColor)).setBurning();
    public static final Material rock = (new Material(MapColor.stoneColor)).setRequiresTool();
    public static final Material iron = (new Material(MapColor.ironColor)).setRequiresTool();
    public static final Material anvil = (new Material(MapColor.ironColor)).setRequiresTool().setImmovableMobility();
    public static final Material water = (new MaterialLiquid(MapColor.waterColor)).setNoPushMobility();
    public static final Material lava = (new MaterialLiquid(MapColor.tntColor)).setNoPushMobility();
    public static final Material leaves = (new Material(MapColor.foliageColor)).setBurning().setTranslucent().setNoPushMobility();
    public static final Material plants = (new MaterialLogic(MapColor.foliageColor)).setNoPushMobility();
    public static final Material vine = (new MaterialLogic(MapColor.foliageColor)).setBurning().setNoPushMobility().setReplaceable();
    public static final Material sponge = new Material(MapColor.yellowColor);
    public static final Material cloth = (new Material(MapColor.clothColor)).setBurning();
    public static final Material fire = (new MaterialTransparent(MapColor.airColor)).setNoPushMobility();
    public static final Material sand = new Material(MapColor.sandColor);
    public static final Material circuits = (new MaterialLogic(MapColor.airColor)).setNoPushMobility();
    public static final Material carpet = (new MaterialLogic(MapColor.clothColor)).setBurning();
    public static final Material glass = (new Material(MapColor.airColor)).setTranslucent().setAdventureModeExempt();
    public static final Material redstoneLight = (new Material(MapColor.airColor)).setAdventureModeExempt();
    public static final Material tnt = (new Material(MapColor.tntColor)).setBurning().setTranslucent();
    public static final Material coral = (new Material(MapColor.foliageColor)).setNoPushMobility();
    public static final Material ice = (new Material(MapColor.iceColor)).setTranslucent().setAdventureModeExempt();
    public static final Material packedIce = (new Material(MapColor.iceColor)).setAdventureModeExempt();
    public static final Material snow = (new MaterialLogic(MapColor.snowColor)).setReplaceable().setTranslucent().setRequiresTool().setNoPushMobility();
    public static final Material craftedSnow = (new Material(MapColor.snowColor)).setRequiresTool();
    public static final Material cactus = (new Material(MapColor.foliageColor)).setTranslucent().setNoPushMobility();
    public static final Material clay = new Material(MapColor.clayColor);
    public static final Material gourd = (new Material(MapColor.foliageColor)).setNoPushMobility();
    public static final Material dragonEgg = (new Material(MapColor.foliageColor)).setNoPushMobility();
    public static final Material portal = (new MaterialPortal(MapColor.airColor)).setImmovableMobility();
    public static final Material cake = (new Material(MapColor.airColor)).setNoPushMobility();
    public static final Material web = (new Material(MapColor.clothColor).disableBlockingMovement();
    public static final Material piston = (new Material(MapColor.stoneColor)).setImmovableMobility();
    public static final Material barrier = (new Material(MapColor.airColor)).setRequiresTool().setImmovableMobility();
"""


def get_is_solid(line):
	if "MaterialLiquid" in line or "MaterialLogic" in line or "MaterialPortal" in line or "MaterialTransparent" in line:
		return "false"
	return "true"

def get_is_transparent(line):
	if "MaterialLogic" in line or "MaterialPortal" in line or "MaterialTransparent" in line:
		return "true"
	return "false"

def get_is_flamable(line):
	if "setBurning" in line:
		return "true"
	return "false"

def get_is_replacable(line):
	if "setReplaceable" in line:
		return "true"
	return "false"

def get_is_blockingmovement(line):
	if "disableBlockingMovement" not in line or "MaterialTransparent" in line or "MaterialPortal" in line or "MaterialLogic" in line or "MaterialLiquid" in line:
		return "false"
	return "true"

for line in s.split("\n"):
	print """static struct Material %s = {
			%s, // IsSolid
			%s, // IsTransparent/!BlocksLight
			%s, // IsFlamable
			%s, // IsReplacable
			%s, // BlocksMovement
			%d
		};""" % (line.split(" ")[4+4], get_is_solid(line), get_is_transparent(line), get_is_flamable(line), get_is_replacable(line), get_is_blockingmovement(line), 0)