mcpe_block_ids = []
mcpe_item_ids  = []

pc_materials_ids = []


with open('C:/users/extreme/desktop/development/andromeda/mcpe_block_list100.txt') as f:
    mcpe_block_ids = f.read().splitlines()


with open('C:/users/extreme/desktop/development/andromeda/mcpe_item_list100.txt') as f:
    mcpe_item_ids  = f.read().splitlines()

with open('C:/users/extreme/desktop/development/andromeda/pc_block_ids.txt') as f:
    pc_materials_ids = f.read().splitlines()


class Mat():

	def __init__(self, pc_name, pe_name, pc_id, pe_id):
		self.pc_name = str(pc_name).lower()
		self.pe_name = str(pe_name).lower()
		self.pc_id = str(pc_id)
		self.pe_id = str(pe_id)


	def get_csv(self):
		return self.pc_name + "," + self.pe_name + "," + self.pc_id + "," + self.pe_id

oks = 0
nos = 0
mats = []

for pc_material_id in pc_materials_ids:
	s = pc_material_id.split("(")
	try:
		s2 = s[1].split(",")
	except Exception:
		continue
	block_name = s[0]
	block_id = int(s2[0].replace(")", ""))
	if block_id > 255:
		continue

	#print "testing ", block_name

	found = False
	
	for mcpe_block_id in mcpe_block_ids:
		#print "Testing", block_name.lower(), "in", mcpe_block_id.lower()
		if mcpe_block_id[0] == " ":
			continue
		jn = block_name.lower().strip()
		pe_str = mcpe_block_id.lower()
		pe_spt = ' '.join(pe_str.split()).split(' ')
		pe_name = pe_spt[0]
		pe_id = pe_spt[1]
		if int(pe_id) > 255:
			continue
		if jn in pe_str:
			#print "OK!", block_id, pe_id, pe_name, jn
			found = True
			mats.append(Mat(block_name, pe_name, block_id, pe_id))
			break
		elif block_name.lower().replace("_", "").strip() in mcpe_block_id.lower():
			#print "ALT OK!", block_name.lower().strip()
			mats.append(Mat(block_name, pe_name, block_id, pe_id))
			found = True
			break

	if not found:
		print "Did not find", block_name
		mats.append(Mat(block_name, "", block_id, ''))
		nos += 1
	else:
		oks += 1
print oks, "OKs"
print nos, "NOs"
for mat in mats:
	print mat.get_csv()
#print mcpe_block_ids, mcpe_item_ids, pc_materials_ids 