import re

mcpe_item_ids = []
pc_item_ids = []


with open('C:/users/extreme/desktop/development/andromeda/mcpe_item_list100.txt') as f:
    mcpe_item_ids  = f.read().splitlines()

with open('C:/users/extreme/desktop/development/andromeda/mc_item_list111.txt') as f:
    pc_item_ids = f.read().splitlines()

class Line():
    JavaID = 0
    PocketID = 0
    PocketMeta = 0
    JavaMeta = 0
    UName = ""
    
    def __init__(self, javaID, pocketID, javaMeta, pocketMeta, _UName):
        self.JavaID = javaID
        self.PocketID = pocketID
        self.JavaMeta = javaMeta
        self.PocketMeta = pocketMeta
        self.UName = _UName

        # JavaID,PocketID,JavaMeta,PocketMeta,UnifiedName

    def to_line(self):
        return "%-*s, %-*s, %-*s, %-*s, %-*s" % (10, self.JavaID, 10, self.PocketID, 10, self.JavaMeta, 10, self.PocketMeta, 10, self.UName)

lines = []

for pc_item in pc_item_ids:
    s = pc_item.split("\t")
    if len(s) == 1:
        continue

    try:
        name = s[2].split(":")[1]
    except Exception:
        print "// skipped ", pc_item
        continue

    fname = ' '.join(name.replace("_", ' ').split()).title().replace(" ", "")
    found = False
    #print s
    for mcpe_item in mcpe_item_ids:
        if name + " " in mcpe_item:
            #print "-- Found!"
            found = True
            usesMetadata = False
            n = re.sub(' +',' ', mcpe_item.lstrip()).split(" ")

            if pc_item.endswith(" B"):
                fname += "_$META"
                usesMetadata = True
            #else:

            lines.append(Line(s[0], n[1], 0, 0, s[2].replace("minecraft:","") + (".$META" if usesMetadata else "")))

            """
            Java ID     = s[0]
            Pocket ID   = n[1]

            /*
	UItemID (64-bit unsigned) = (((short(JavaID) << 16) | (short(PocketID) & 0xFFFF)) << 32) | ((short(JavaMetadata) << 16) | (short(PocketMetadata) & 0xFFFF))
*/
            """
            
            mcpe_item_ids = [x for x in mcpe_item_ids if not x == mcpe_item]
            break

    if found == False:
        if pc_item.endswith(" B"):
            lines.append(Line(s[0], 'NUL', 0, 0, s[2].replace("minecraft:","") + ".$META"))
        else:
            lines.append(Line(s[0], 'NUL', 0, 0, s[2].replace("minecraft:","")))

        #print "ID: ", s[0]
        #print "SName: ", s[2]
        #print "DisplayName: ", s[3]


for line in lines:
    print line.to_line()

#print "UNACCOUNTED MCPE ITEMS"
#print mcpe_item_ids