#pragma once

#include <map>
#include <utility>

#include "Types.h"

// This file was automatically generated

namespace Andromeda {
enum class UItem : UItemID {
	IronShovel, 
	IronPickaxe, 
	IronAxe, 
	FlintAndSteel, 
	Apple, 
	Bow, 
	Arrow, 
	Coal_Coal, 
	Coal_Charcoal, 
	Diamond, 
	IronIngot, 
	GoldIngot, 
	IronSword, 
	WoodenSword, 
	WoodenShovel, 
	WoodenPickaxe, 
	WoodenAxe, 
	StoneSword, 
	StoneShovel, 
	StonePickaxe, 
	StoneAxe, 
	DiamondSword, 
	DiamondShovel, 
	DiamondPickaxe, 
	DiamondAxe, 
	Stick, 
	Bowl, 
	MushroomStew, 
	GoldenSword, 
	GoldenShovel, 
	GoldenPickaxe, 
	GoldenAxe, 
	String, 
	Feather, 
	Gunpowder, 
	WoodenHoe, 
	StoneHoe, 
	IronHoe, 
	DiamondHoe, 
	GoldenHoe, 
	WheatSeeds, 
	Wheat, 
	Bread, 
	LeatherHelmet, 
	LeatherChestplate, 
	LeatherLeggings, 
	LeatherBoots, 
	ChainmailHelmet, 
	ChainmailChestplate, 
	ChainmailLeggings, 
	ChainmailBoots, 
	IronHelmet, 
	IronChestplate, 
	IronLeggings, 
	IronBoots, 
	DiamondHelmet, 
	DiamondChestplate, 
	DiamondLeggings, 
	DiamondBoots, 
	GoldenHelmet, 
	GoldenChestplate, 
	GoldenLeggings, 
	GoldenBoots, 
	Flint, 
	Porkchop, 
	CookedPorkchop, 
	Painting, 
	GoldenApple_Normal, 
	GoldenApple_Enchanted, 
	Sign, 
	WoodenDoor, 
	Bucket, 
	WaterBucket, 
	LavaBucket, 
	Minecart, 
	Saddle, 
	IronDoor, 
	Redstone, 
	Snowball, 
	Boat, 
	Leather, 
	MilkBucket, 
	Brick, 
	ClayBall, 
	Reeds, 
	Paper, 
	Book, 
	SlimeBall, 
	ChestMinecart, 
	FurnaceMinecart, 
	Egg, 
	Compass, 
	FishingRod, 
	Clock, 
	GlowstoneDust, 
	Fish_RawFish, 
	Fish_RawSalmon, 
	Fish_Clownfish, 
	Fish_Pufferfish, 
	CookedFish_CookedFish, 
	CookedFish_CookedSalmon, 
	Dye_Black, 
	Dye_Red, 
	Dye_Green, 
	Dye_Brown, 
	Dye_Blue, 
	Dye_Purple, 
	Dye_Cyan, 
	Dye_Silver, 
	Dye_Gray, 
	Dye_Pink, 
	Dye_Lime, 
	Dye_Yellow, 
	Dye_Lightblue, 
	Dye_Magenta, 
	Dye_Orange, 
	Dye_White, 
	Bone, 
	Sugar, 
	Cake, 
	Bed, 
	Repeater, 
	Cookie, 
	FilledMap, 
	Shears, 
	Melon, 
	PumpkinSeeds, 
	MelonSeeds, 
	Beef, 
	CookedBeef, 
	Chicken, 
	CookedChicken, 
	RottenFlesh, 
	EnderPearl, 
	BlazeRod, 
	GhastTear, 
	GoldNugget, 
	NetherWart, 
	Potion, 
	GlassBottle, 
	SpiderEye, 
	FermentedSpiderEye, 
	BlazePowder, 
	MagmaCream, 
	BrewingStand, 
	Cauldron, 
	EnderEye, 
	SpeckledMelon, 
	SpawnEgg, 
	ExperienceBottle, 
	FireCharge, 
	WritableBook, 
	WrittenBook, 
	Emerald, 
	ItemFrame, 
	FlowerPot, 
	Carrot, 
	Potato, 
	BakedPotato, 
	PoisonousPotato, 
	Map, 
	GoldenCarrot, 
	Skull_Skeleton, 
	Skull_Wither, 
	Skull_Zombie, 
	Skull_Player, 
	Skull_Creeper, 
	CarrotOnAStick, 
	NetherStar, 
	PumpkinPie, 
	Fireworks, 
	FireworkCharge, 
	EnchantedBook, 
	Comparator, 
	Netherbrick, 
	Quartz, 
	TntMinecart, 
	HopperMinecart, 
	PrismarineShard, 
	PrismarineCrystals, 
	Rabbit, 
	CookedRabbit, 
	RabbitStew, 
	RabbitFoot, 
	RabbitHide, 
	ArmorStand, 
	IronHorseArmor, 
	GoldenHorseArmor, 
	DiamondHorseArmor, 
	Lead, 
	NameTag, 
	CommandBlockMinecart, 
	Mutton, 
	CookedMutton, 
	Banner, 
	EndCrystal, 
	SpruceDoor, 
	BirchDoor, 
	JungleDoor, 
	AcaciaDoor, 
	DarkOakDoor, 
	ChorusFruit, 
	ChorusFruitPopped, 
	Beetroot, 
	BeetrootSeeds, 
	BeetrootSoup, 
	DragonBreath, 
	SplashPotion, 
	SpectralArrow, 
	TippedArrow, 
	LingeringPotion, 
	Shield, 
	Elytra, 
	SpruceBoat, 
	BirchBoat, 
	JungleBoat, 
	AcaciaBoat, 
	DarkOakBoat, 
	TotemOfUndying, 
	ShulkerShell, 
	IronNugget, 
	Record13, 
	RecordCat, 
	RecordBlocks, 
	RecordChirp, 
	RecordFar, 
	RecordMall, 
	RecordMellohi, 
	RecordStal, 
	RecordStrad, 
	RecordWard, 
	Record11, 
	RecordWait, 
};
#pragma region
// 	UItemID (64-bit unsigned) = (((short(JavaID) << 16) | (short(PocketID) & 0xFFFF)) << 32) | ((short(JavaMetadata) << 16) | (short(PocketMetadata) & 0xFFFF))
#define UITEM_IRONSHOVEL               static_cast<UItemID>(UItem::IronShovel)
#define UITEM_IRONPICKAXE              static_cast<UItemID>(UItem::IronPickaxe)
#define UITEM_IRONAXE                  static_cast<UItemID>(UItem::IronAxe)
#define UITEM_FLINTANDSTEEL            static_cast<UItemID>(UItem::FlintAndSteel)
#define UITEM_APPLE                    static_cast<UItemID>(UItem::Apple)
#define UITEM_BOW                      static_cast<UItemID>(UItem::Bow)
#define UITEM_ARROW                    static_cast<UItemID>(UItem::Arrow)
#define UITEM_COAL_COAL                static_cast<UItemID>(UItem::Coal_Coal)
#define UITEM_COAL_CHARCOAL            static_cast<UItemID>(UItem::Coal_Charcoal)
#define UITEM_DIAMOND                  static_cast<UItemID>(UItem::Diamond)
#define UITEM_IRONINGOT                static_cast<UItemID>(UItem::IronIngot)
#define UITEM_GOLDINGOT                static_cast<UItemID>(UItem::GoldIngot)
#define UITEM_IRONSWORD                static_cast<UItemID>(UItem::IronSword)
#define UITEM_WOODENSWORD              static_cast<UItemID>(UItem::WoodenSword)
#define UITEM_WOODENSHOVEL             static_cast<UItemID>(UItem::WoodenShovel)
#define UITEM_WOODENPICKAXE            static_cast<UItemID>(UItem::WoodenPickaxe)
#define UITEM_WOODENAXE                static_cast<UItemID>(UItem::WoodenAxe)
#define UITEM_STONESWORD               static_cast<UItemID>(UItem::StoneSword)
#define UITEM_STONESHOVEL              static_cast<UItemID>(UItem::StoneShovel)
#define UITEM_STONEPICKAXE             static_cast<UItemID>(UItem::StonePickaxe)
#define UITEM_STONEAXE                 static_cast<UItemID>(UItem::StoneAxe)
#define UITEM_DIAMONDSWORD             static_cast<UItemID>(UItem::DiamondSword)
#define UITEM_DIAMONDSHOVEL            static_cast<UItemID>(UItem::DiamondShovel)
#define UITEM_DIAMONDPICKAXE           static_cast<UItemID>(UItem::DiamondPickaxe)
#define UITEM_DIAMONDAXE               static_cast<UItemID>(UItem::DiamondAxe)
#define UITEM_STICK                    static_cast<UItemID>(UItem::Stick)
#define UITEM_BOWL                     static_cast<UItemID>(UItem::Bowl)
#define UITEM_MUSHROOMSTEW             static_cast<UItemID>(UItem::MushroomStew)
#define UITEM_GOLDENSWORD              static_cast<UItemID>(UItem::GoldenSword)
#define UITEM_GOLDENSHOVEL             static_cast<UItemID>(UItem::GoldenShovel)
#define UITEM_GOLDENPICKAXE            static_cast<UItemID>(UItem::GoldenPickaxe)
#define UITEM_GOLDENAXE                static_cast<UItemID>(UItem::GoldenAxe)
#define UITEM_STRING                   static_cast<UItemID>(UItem::String)
#define UITEM_FEATHER                  static_cast<UItemID>(UItem::Feather)
#define UITEM_GUNPOWDER                static_cast<UItemID>(UItem::Gunpowder)
#define UITEM_WOODENHOE                static_cast<UItemID>(UItem::WoodenHoe)
#define UITEM_STONEHOE                 static_cast<UItemID>(UItem::StoneHoe)
#define UITEM_IRONHOE                  static_cast<UItemID>(UItem::IronHoe)
#define UITEM_DIAMONDHOE               static_cast<UItemID>(UItem::DiamondHoe)
#define UITEM_GOLDENHOE                static_cast<UItemID>(UItem::GoldenHoe)
#define UITEM_WHEATSEEDS               static_cast<UItemID>(UItem::WheatSeeds)
#define UITEM_WHEAT                    static_cast<UItemID>(UItem::Wheat)
#define UITEM_BREAD                    static_cast<UItemID>(UItem::Bread)
#define UITEM_LEATHERHELMET            static_cast<UItemID>(UItem::LeatherHelmet)
#define UITEM_LEATHERCHESTPLATE        static_cast<UItemID>(UItem::LeatherChestplate)
#define UITEM_LEATHERLEGGINGS          static_cast<UItemID>(UItem::LeatherLeggings)
#define UITEM_LEATHERBOOTS             static_cast<UItemID>(UItem::LeatherBoots)
#define UITEM_CHAINMAILHELMET          static_cast<UItemID>(UItem::ChainmailHelmet)
#define UITEM_CHAINMAILCHESTPLATE      static_cast<UItemID>(UItem::ChainmailChestplate)
#define UITEM_CHAINMAILLEGGINGS        static_cast<UItemID>(UItem::ChainmailLeggings)
#define UITEM_CHAINMAILBOOTS           static_cast<UItemID>(UItem::ChainmailBoots)
#define UITEM_IRONHELMET               static_cast<UItemID>(UItem::IronHelmet)
#define UITEM_IRONCHESTPLATE           static_cast<UItemID>(UItem::IronChestplate)
#define UITEM_IRONLEGGINGS             static_cast<UItemID>(UItem::IronLeggings)
#define UITEM_IRONBOOTS                static_cast<UItemID>(UItem::IronBoots)
#define UITEM_DIAMONDHELMET            static_cast<UItemID>(UItem::DiamondHelmet)
#define UITEM_DIAMONDCHESTPLATE        static_cast<UItemID>(UItem::DiamondChestplate)
#define UITEM_DIAMONDLEGGINGS          static_cast<UItemID>(UItem::DiamondLeggings)
#define UITEM_DIAMONDBOOTS             static_cast<UItemID>(UItem::DiamondBoots)
#define UITEM_GOLDENHELMET             static_cast<UItemID>(UItem::GoldenHelmet)
#define UITEM_GOLDENCHESTPLATE         static_cast<UItemID>(UItem::GoldenChestplate)
#define UITEM_GOLDENLEGGINGS           static_cast<UItemID>(UItem::GoldenLeggings)
#define UITEM_GOLDENBOOTS              static_cast<UItemID>(UItem::GoldenBoots)
#define UITEM_FLINT                    static_cast<UItemID>(UItem::Flint)
#define UITEM_PORKCHOP                 static_cast<UItemID>(UItem::Porkchop)
#define UITEM_COOKEDPORKCHOP           static_cast<UItemID>(UItem::CookedPorkchop)
#define UITEM_PAINTING                 static_cast<UItemID>(UItem::Painting)
#define UITEM_GOLDENAPPLE_NORMAL       static_cast<UItemID>(UItem::GoldenApple_Normal)
#define UITEM_GOLDENAPPLE_ENCHANTED    static_cast<UItemID>(UItem::GoldenApple_Enchanted)
#define UITEM_SIGN                     static_cast<UItemID>(UItem::Sign)
#define UITEM_WOODENDOOR               static_cast<UItemID>(UItem::WoodenDoor)
#define UITEM_BUCKET                   static_cast<UItemID>(UItem::Bucket)
#define UITEM_WATERBUCKET              static_cast<UItemID>(UItem::WaterBucket)
#define UITEM_LAVABUCKET               static_cast<UItemID>(UItem::LavaBucket)
#define UITEM_MINECART                 static_cast<UItemID>(UItem::Minecart)
#define UITEM_SADDLE                   static_cast<UItemID>(UItem::Saddle)
#define UITEM_IRONDOOR                 static_cast<UItemID>(UItem::IronDoor)
#define UITEM_REDSTONE                 static_cast<UItemID>(UItem::Redstone)
#define UITEM_SNOWBALL                 static_cast<UItemID>(UItem::Snowball)
#define UITEM_BOAT                     static_cast<UItemID>(UItem::Boat)
#define UITEM_LEATHER                  static_cast<UItemID>(UItem::Leather)
#define UITEM_MILKBUCKET               static_cast<UItemID>(UItem::MilkBucket)
#define UITEM_BRICK                    static_cast<UItemID>(UItem::Brick)
#define UITEM_CLAYBALL                 static_cast<UItemID>(UItem::ClayBall)
#define UITEM_REEDS                    static_cast<UItemID>(UItem::Reeds)
#define UITEM_PAPER                    static_cast<UItemID>(UItem::Paper)
#define UITEM_BOOK                     static_cast<UItemID>(UItem::Book)
#define UITEM_SLIMEBALL                static_cast<UItemID>(UItem::SlimeBall)
#define UITEM_CHESTMINECART            static_cast<UItemID>(UItem::ChestMinecart)
#define UITEM_FURNACEMINECART          static_cast<UItemID>(UItem::FurnaceMinecart) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_EGG                      static_cast<UItemID>(UItem::Egg)
#define UITEM_COMPASS                  static_cast<UItemID>(UItem::Compass)
#define UITEM_FISHINGROD               static_cast<UItemID>(UItem::FishingRod)
#define UITEM_CLOCK                    static_cast<UItemID>(UItem::Clock)
#define UITEM_GLOWSTONEDUST            static_cast<UItemID>(UItem::GlowstoneDust)
#define UITEM_FISH_RAWFISH             static_cast<UItemID>(UItem::Fish_RawFish)
#define UITEM_FISH_RAWSALMON           static_cast<UItemID>(UItem::Fish_RawSalmon)
#define UITEM_FISH_CLOWNFISH           static_cast<UItemID>(UItem::Fish_Clownfish)
#define UITEM_FISH_PUFFERFISH          static_cast<UItemID>(UItem::Fish_Pufferfish)
#define UITEM_COOKEDFISH_COOKEDFISH    static_cast<UItemID>(UItem::CookedFish_CookedFish)
#define UITEM_COOKEDFISH_COOKEDSALMON  static_cast<UItemID>(UItem::CookedFish_CookedSalmon)
#define UITEM_DYE_BLACK                static_cast<UItemID>(UItem::Dye_Black)
#define UITEM_DYE_RED                  static_cast<UItemID>(UItem::Dye_Red)
#define UITEM_DYE_GREEN                static_cast<UItemID>(UItem::Dye_Green)
#define UITEM_DYE_BROWN                static_cast<UItemID>(UItem::Dye_Brown)
#define UITEM_DYE_BLUE                 static_cast<UItemID>(UItem::Dye_Blue)
#define UITEM_DYE_PURPLE               static_cast<UItemID>(UItem::Dye_Purple)
#define UITEM_DYE_CYAN                 static_cast<UItemID>(UItem::Dye_Cyan)
#define UITEM_DYE_SILVER               static_cast<UItemID>(UItem::Dye_Silver)
#define UITEM_DYE_GRAY                 static_cast<UItemID>(UItem::Dye_Gray)
#define UITEM_DYE_PINK                 static_cast<UItemID>(UItem::Dye_Pink)
#define UITEM_DYE_LIME                 static_cast<UItemID>(UItem::Dye_Lime)
#define UITEM_DYE_YELLOW               static_cast<UItemID>(UItem::Dye_Yellow)
#define UITEM_DYE_LIGHTBLUE            static_cast<UItemID>(UItem::Dye_Lightblue)
#define UITEM_DYE_MAGENTA              static_cast<UItemID>(UItem::Dye_Magenta)
#define UITEM_DYE_ORANGE               static_cast<UItemID>(UItem::Dye_Orange)
#define UITEM_DYE_WHITE                static_cast<UItemID>(UItem::Dye_White)
#define UITEM_BONE                     static_cast<UItemID>(UItem::Bone)
#define UITEM_SUGAR                    static_cast<UItemID>(UItem::Sugar)
#define UITEM_CAKE                     static_cast<UItemID>(UItem::Cake)
#define UITEM_BED                      static_cast<UItemID>(UItem::Bed)
#define UITEM_REPEATER                 static_cast<UItemID>(UItem::Repeater)
#define UITEM_COOKIE                   static_cast<UItemID>(UItem::Cookie)
#define UITEM_FILLEDMAP                static_cast<UItemID>(UItem::FilledMap) /* SPECIAL DATA */
#define UITEM_SHEARS                   static_cast<UItemID>(UItem::Shears)
#define UITEM_MELON                    static_cast<UItemID>(UItem::Melon)
#define UITEM_PUMPKINSEEDS             static_cast<UItemID>(UItem::PumpkinSeeds)
#define UITEM_MELONSEEDS               static_cast<UItemID>(UItem::MelonSeeds)
#define UITEM_BEEF                     static_cast<UItemID>(UItem::Beef)
#define UITEM_COOKEDBEEF               static_cast<UItemID>(UItem::CookedBeef)
#define UITEM_CHICKEN                  static_cast<UItemID>(UItem::Chicken)
#define UITEM_COOKEDCHICKEN            static_cast<UItemID>(UItem::CookedChicken)
#define UITEM_ROTTENFLESH              static_cast<UItemID>(UItem::RottenFlesh)
#define UITEM_ENDERPEARL               static_cast<UItemID>(UItem::EnderPearl)
#define UITEM_BLAZEROD                 static_cast<UItemID>(UItem::BlazeRod)
#define UITEM_GHASTTEAR                static_cast<UItemID>(UItem::GhastTear)
#define UITEM_GOLDNUGGET               static_cast<UItemID>(UItem::GoldNugget)
#define UITEM_NETHERWART               static_cast<UItemID>(UItem::NetherWart)
#define UITEM_POTION                   static_cast<UItemID>(UItem::Potion) /* SPECIAL DATA */
#define UITEM_GLASSBOTTLE              static_cast<UItemID>(UItem::GlassBottle)
#define UITEM_SPIDEREYE                static_cast<UItemID>(UItem::SpiderEye)
#define UITEM_FERMENTEDSPIDEREYE       static_cast<UItemID>(UItem::FermentedSpiderEye)
#define UITEM_BLAZEPOWDER              static_cast<UItemID>(UItem::BlazePowder)
#define UITEM_MAGMACREAM               static_cast<UItemID>(UItem::MagmaCream)
#define UITEM_BREWINGSTAND             static_cast<UItemID>(UItem::BrewingStand)
#define UITEM_CAULDRON                 static_cast<UItemID>(UItem::Cauldron)
#define UITEM_ENDEREYE                 static_cast<UItemID>(UItem::EnderEye)
#define UITEM_SPECKLEDMELON            static_cast<UItemID>(UItem::SpeckledMelon)
#define UITEM_SPAWNEGG                 static_cast<UItemID>(UItem::SpawnEgg) /* SPECIAL DATA */
#define UITEM_EXPERIENCEBOTTLE         static_cast<UItemID>(UItem::ExperienceBottle)
#define UITEM_FIRECHARGE               static_cast<UItemID>(UItem::FireCharge)
#define UITEM_WRITABLEBOOK             static_cast<UItemID>(UItem::WritableBook) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_WRITTENBOOK              static_cast<UItemID>(UItem::WrittenBook) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_EMERALD                  static_cast<UItemID>(UItem::Emerald)
#define UITEM_ITEMFRAME                static_cast<UItemID>(UItem::ItemFrame)
#define UITEM_FLOWERPOT                static_cast<UItemID>(UItem::FlowerPot)
#define UITEM_CARROT                   static_cast<UItemID>(UItem::Carrot)
#define UITEM_POTATO                   static_cast<UItemID>(UItem::Potato)
#define UITEM_BAKEDPOTATO              static_cast<UItemID>(UItem::BakedPotato)
#define UITEM_POISONOUSPOTATO          static_cast<UItemID>(UItem::PoisonousPotato)
#define UITEM_MAP                      static_cast<UItemID>(UItem::Map)
#define UITEM_GOLDENCARROT             static_cast<UItemID>(UItem::GoldenCarrot)
#define UITEM_SKULL_SKELETON           static_cast<UItemID>(UItem::Skull_Skeleton)
#define UITEM_SKULL_WITHER             static_cast<UItemID>(UItem::Skull_Wither)
#define UITEM_SKULL_ZOMBIE             static_cast<UItemID>(UItem::Skull_Zombie)
#define UITEM_SKULL_PLAYER             static_cast<UItemID>(UItem::Skull_Player)
#define UITEM_SKULL_CREEPER            static_cast<UItemID>(UItem::Skull_Creeper)
#define UITEM_CARROTONASTICK           static_cast<UItemID>(UItem::CarrotOnAStick)
#define UITEM_NETHERSTAR               static_cast<UItemID>(UItem::NetherStar)
#define UITEM_PUMPKINPIE               static_cast<UItemID>(UItem::PumpkinPie)
#define UITEM_FIREWORKS                static_cast<UItemID>(UItem::Fireworks) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_FIREWORKCHARGE           static_cast<UItemID>(UItem::FireworkCharge) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_ENCHANTEDBOOK            static_cast<UItemID>(UItem::EnchantedBook)
#define UITEM_COMPARATOR               static_cast<UItemID>(UItem::Comparator)
#define UITEM_NETHERBRICK              static_cast<UItemID>(UItem::Netherbrick)
#define UITEM_QUARTZ                   static_cast<UItemID>(UItem::Quartz)
#define UITEM_TNTMINECART              static_cast<UItemID>(UItem::TntMinecart)
#define UITEM_HOPPERMINECART           static_cast<UItemID>(UItem::HopperMinecart)
#define UITEM_PRISMARINESHARD          static_cast<UItemID>(UItem::PrismarineShard)
#define UITEM_PRISMARINECRYSTALS       static_cast<UItemID>(UItem::PrismarineCrystals)
#define UITEM_RABBIT                   static_cast<UItemID>(UItem::Rabbit)
#define UITEM_COOKEDRABBIT             static_cast<UItemID>(UItem::CookedRabbit)
#define UITEM_RABBITSTEW               static_cast<UItemID>(UItem::RabbitStew)
#define UITEM_RABBITFOOT               static_cast<UItemID>(UItem::RabbitFoot)
#define UITEM_RABBITHIDE               static_cast<UItemID>(UItem::RabbitHide)
#define UITEM_ARMORSTAND               static_cast<UItemID>(UItem::ArmorStand) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_IRONHORSEARMOR           static_cast<UItemID>(UItem::IronHorseArmor)
#define UITEM_GOLDENHORSEARMOR         static_cast<UItemID>(UItem::GoldenHorseArmor)
#define UITEM_DIAMONDHORSEARMOR        static_cast<UItemID>(UItem::DiamondHorseArmor)
#define UITEM_LEAD                     static_cast<UItemID>(UItem::Lead)
#define UITEM_NAMETAG                  static_cast<UItemID>(UItem::NameTag)
#define UITEM_COMMANDBLOCKMINECART     static_cast<UItemID>(UItem::CommandBlockMinecart) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_MUTTON                   static_cast<UItemID>(UItem::Mutton)
#define UITEM_COOKEDMUTTON             static_cast<UItemID>(UItem::CookedMutton)
#define UITEM_BANNER                   static_cast<UItemID>(UItem::Banner) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_ENDCRYSTAL               static_cast<UItemID>(UItem::EndCrystal)
#define UITEM_SPRUCEDOOR               static_cast<UItemID>(UItem::SpruceDoor)
#define UITEM_BIRCHDOOR                static_cast<UItemID>(UItem::BirchDoor)
#define UITEM_JUNGLEDOOR               static_cast<UItemID>(UItem::JungleDoor)
#define UITEM_ACACIADOOR               static_cast<UItemID>(UItem::AcaciaDoor)
#define UITEM_DARKOAKDOOR              static_cast<UItemID>(UItem::DarkOakDoor)
#define UITEM_CHORUSFRUIT              static_cast<UItemID>(UItem::ChorusFruit)
#define UITEM_CHORUSFRUITPOPPED        static_cast<UItemID>(UItem::ChorusFruitPopped)
#define UITEM_BEETROOT                 static_cast<UItemID>(UItem::Beetroot)
#define UITEM_BEETROOTSEEDS            static_cast<UItemID>(UItem::BeetrootSeeds)
#define UITEM_BEETROOTSOUP             static_cast<UItemID>(UItem::BeetrootSoup)
#define UITEM_DRAGONBREATH             static_cast<UItemID>(UItem::DragonBreath)
#define UITEM_SPLASHPOTION             static_cast<UItemID>(UItem::SplashPotion)
#define UITEM_SPECTRALARROW            static_cast<UItemID>(UItem::SpectralArrow) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_TIPPEDARROW              static_cast<UItemID>(UItem::TippedArrow) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_LINGERINGPOTION          static_cast<UItemID>(UItem::LingeringPotion)
#define UITEM_SHIELD                   static_cast<UItemID>(UItem::Shield) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_ELYTRA                   static_cast<UItemID>(UItem::Elytra)
#define UITEM_SPRUCEBOAT               static_cast<UItemID>(UItem::SpruceBoat)
#define UITEM_BIRCHBOAT                static_cast<UItemID>(UItem::BirchBoat)
#define UITEM_JUNGLEBOAT               static_cast<UItemID>(UItem::JungleBoat)
#define UITEM_ACACIABOAT               static_cast<UItemID>(UItem::AcaciaBoat)
#define UITEM_DARKOAKBOAT              static_cast<UItemID>(UItem::DarkOakBoat)
#define UITEM_TOTEMOFUNDYING           static_cast<UItemID>(UItem::TotemOfUndying) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_SHULKERSHELL             static_cast<UItemID>(UItem::ShulkerShell)
#define UITEM_IRONNUGGET               static_cast<UItemID>(UItem::IronNugget) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_RECORD13                 static_cast<UItemID>(UItem::Record13) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_RECORDCAT                static_cast<UItemID>(UItem::RecordCat) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_RECORDBLOCKS             static_cast<UItemID>(UItem::RecordBlocks) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_RECORDCHIRP              static_cast<UItemID>(UItem::RecordChirp) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_RECORDFAR                static_cast<UItemID>(UItem::RecordFar) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_RECORDMALL               static_cast<UItemID>(UItem::RecordMall) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_RECORDMELLOHI            static_cast<UItemID>(UItem::RecordMellohi) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_RECORDSTAL               static_cast<UItemID>(UItem::RecordStal) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_RECORDSTRAD              static_cast<UItemID>(UItem::RecordStrad) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_RECORDWARD               static_cast<UItemID>(UItem::RecordWard) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_RECORD11                 static_cast<UItemID>(UItem::Record11) /* UNIMPLMENTED POCKET ITEM */
#define UITEM_RECORDWAIT               static_cast<UItemID>(UItem::RecordWait) /* UNIMPLMENTED POCKET ITEM */
#pragma endregion
const std::map<UItemID, std::tuple<std::pair<std::string, short>, std::pair<short, short>>> UItemM = {
	{ UITEM_IRONSHOVEL         , std::make_tuple(std::make_pair("iron_shovel", 0), std::make_pair(256, 0))},
	{ UITEM_IRONPICKAXE        , std::make_tuple(std::make_pair("iron_pickaxe", 0), std::make_pair(257, 0))},
	{ UITEM_IRONAXE            , std::make_tuple(std::make_pair("iron_axe", 0), std::make_pair(258, 0))},
	{ UITEM_FLINTANDSTEEL      , std::make_tuple(std::make_pair("flint_and_steel", 0), std::make_pair(259, 0))},
	{ UITEM_APPLE              , std::make_tuple(std::make_pair("apple", 0), std::make_pair(260, 0))},
	{ UITEM_BOW                , std::make_tuple(std::make_pair("bow", 0), std::make_pair(261, 0))},
	{ UITEM_ARROW              , std::make_tuple(std::make_pair("arrow", 0), std::make_pair(262, 0))},
	{ UITEM_COAL_COAL          , std::make_tuple(std::make_pair("coal", 0), std::make_pair(263, 0))},
	{ UITEM_COAL_CHARCOAL      , std::make_tuple(std::make_pair("coal", 1), std::make_pair(263, 1))},
	{ UITEM_DIAMOND            , std::make_tuple(std::make_pair("diamond", 0), std::make_pair(264, 0))},
	{ UITEM_IRONINGOT          , std::make_tuple(std::make_pair("iron_ingot", 0), std::make_pair(265, 0))},
	{ UITEM_GOLDINGOT          , std::make_tuple(std::make_pair("gold_ingot", 0), std::make_pair(266, 0))},
	{ UITEM_IRONSWORD          , std::make_tuple(std::make_pair("iron_sword", 0), std::make_pair(267, 0))},
	{ UITEM_WOODENSWORD        , std::make_tuple(std::make_pair("wooden_sword", 0), std::make_pair(268, 0))},
	{ UITEM_WOODENSHOVEL       , std::make_tuple(std::make_pair("wooden_shovel", 0), std::make_pair(269, 0))},
	{ UITEM_WOODENPICKAXE      , std::make_tuple(std::make_pair("wooden_pickaxe", 0), std::make_pair(270, 0))},
	{ UITEM_WOODENAXE          , std::make_tuple(std::make_pair("wooden_axe", 0), std::make_pair(271, 0))},
	{ UITEM_STONESWORD         , std::make_tuple(std::make_pair("stone_sword", 0), std::make_pair(272, 0))},
	{ UITEM_STONESHOVEL        , std::make_tuple(std::make_pair("stone_shovel", 0), std::make_pair(273, 0))},
	{ UITEM_STONEPICKAXE       , std::make_tuple(std::make_pair("stone_pickaxe", 0), std::make_pair(274, 0))},
	{ UITEM_STONEAXE           , std::make_tuple(std::make_pair("stone_axe", 0), std::make_pair(275, 0))},
	{ UITEM_DIAMONDSWORD       , std::make_tuple(std::make_pair("diamond_sword", 0), std::make_pair(276, 0))},
	{ UITEM_DIAMONDSHOVEL      , std::make_tuple(std::make_pair("diamond_shovel", 0), std::make_pair(277, 0))},
	{ UITEM_DIAMONDPICKAXE     , std::make_tuple(std::make_pair("diamond_pickaxe", 0), std::make_pair(278, 0))},
	{ UITEM_DIAMONDAXE         , std::make_tuple(std::make_pair("diamond_axe", 0), std::make_pair(279, 0))},
	{ UITEM_STICK              , std::make_tuple(std::make_pair("stick", 0), std::make_pair(280, 0))},
	{ UITEM_BOWL               , std::make_tuple(std::make_pair("bowl", 0), std::make_pair(281, 0))},
	{ UITEM_MUSHROOMSTEW       , std::make_tuple(std::make_pair("mushroom_stew", 0), std::make_pair(282, 0))},
	{ UITEM_GOLDENSWORD        , std::make_tuple(std::make_pair("golden_sword", 0), std::make_pair(283, 0))},
	{ UITEM_GOLDENSHOVEL       , std::make_tuple(std::make_pair("golden_shovel", 0), std::make_pair(284, 0))},
	{ UITEM_GOLDENPICKAXE      , std::make_tuple(std::make_pair("golden_pickaxe", 0), std::make_pair(285, 0))},
	{ UITEM_GOLDENAXE          , std::make_tuple(std::make_pair("golden_axe", 0), std::make_pair(286, 0))},
	{ UITEM_STRING             , std::make_tuple(std::make_pair("string", 0), std::make_pair(287, 0))},
	{ UITEM_FEATHER            , std::make_tuple(std::make_pair("feather", 0), std::make_pair(288, 0))},
	{ UITEM_GUNPOWDER          , std::make_tuple(std::make_pair("gunpowder", 0), std::make_pair(289, 0))},
	{ UITEM_WOODENHOE          , std::make_tuple(std::make_pair("wooden_hoe", 0), std::make_pair(290, 0))},
	{ UITEM_STONEHOE           , std::make_tuple(std::make_pair("stone_hoe", 0), std::make_pair(291, 0))},
	{ UITEM_IRONHOE            , std::make_tuple(std::make_pair("iron_hoe", 0), std::make_pair(292, 0))},
	{ UITEM_DIAMONDHOE         , std::make_tuple(std::make_pair("diamond_hoe", 0), std::make_pair(293, 0))},
	{ UITEM_GOLDENHOE          , std::make_tuple(std::make_pair("golden_hoe", 0), std::make_pair(294, 0))},
	{ UITEM_WHEATSEEDS         , std::make_tuple(std::make_pair("wheat_seeds", 0), std::make_pair(295, 0))},
	{ UITEM_WHEAT              , std::make_tuple(std::make_pair("wheat", 0), std::make_pair(296, 0))},
	{ UITEM_BREAD              , std::make_tuple(std::make_pair("bread", 0), std::make_pair(297, 0))},
	{ UITEM_LEATHERHELMET      , std::make_tuple(std::make_pair("leather_helmet", 0), std::make_pair(298, 0))},
	{ UITEM_LEATHERCHESTPLATE  , std::make_tuple(std::make_pair("leather_chestplate", 0), std::make_pair(299, 0))},
	{ UITEM_LEATHERLEGGINGS    , std::make_tuple(std::make_pair("leather_leggings", 0), std::make_pair(300, 0))},
	{ UITEM_LEATHERBOOTS       , std::make_tuple(std::make_pair("leather_boots", 0), std::make_pair(301, 0))},
	{ UITEM_CHAINMAILHELMET    , std::make_tuple(std::make_pair("chainmail_helmet", 0), std::make_pair(302, 0))},
	{ UITEM_CHAINMAILCHESTPLATE, std::make_tuple(std::make_pair("chainmail_chestplate", 0), std::make_pair(303, 0))},
	{ UITEM_CHAINMAILLEGGINGS  , std::make_tuple(std::make_pair("chainmail_leggings", 0), std::make_pair(304, 0))},
	{ UITEM_CHAINMAILBOOTS     , std::make_tuple(std::make_pair("chainmail_boots", 0), std::make_pair(305, 0))},
	{ UITEM_IRONHELMET         , std::make_tuple(std::make_pair("iron_helmet", 0), std::make_pair(306, 0))},
	{ UITEM_IRONCHESTPLATE     , std::make_tuple(std::make_pair("iron_chestplate", 0), std::make_pair(307, 0))},
	{ UITEM_IRONLEGGINGS       , std::make_tuple(std::make_pair("iron_leggings", 0), std::make_pair(308, 0))},
	{ UITEM_IRONBOOTS          , std::make_tuple(std::make_pair("iron_boots", 0), std::make_pair(309, 0))},
	{ UITEM_DIAMONDHELMET      , std::make_tuple(std::make_pair("diamond_helmet", 0), std::make_pair(310, 0))},
	{ UITEM_DIAMONDCHESTPLATE  , std::make_tuple(std::make_pair("diamond_chestplate", 0), std::make_pair(311, 0))},
	{ UITEM_DIAMONDLEGGINGS    , std::make_tuple(std::make_pair("diamond_leggings", 0), std::make_pair(312, 0))},
	{ UITEM_DIAMONDBOOTS       , std::make_tuple(std::make_pair("diamond_boots", 0), std::make_pair(313, 0))},
	{ UITEM_GOLDENHELMET       , std::make_tuple(std::make_pair("golden_helmet", 0), std::make_pair(314, 0))},
	{ UITEM_GOLDENCHESTPLATE   , std::make_tuple(std::make_pair("golden_chestplate", 0), std::make_pair(315, 0))},
	{ UITEM_GOLDENLEGGINGS     , std::make_tuple(std::make_pair("golden_leggings", 0), std::make_pair(316, 0))},
	{ UITEM_GOLDENBOOTS        , std::make_tuple(std::make_pair("golden_boots", 0), std::make_pair(317, 0))},
	{ UITEM_FLINT              , std::make_tuple(std::make_pair("flint", 0), std::make_pair(318, 0))},
	{ UITEM_PORKCHOP           , std::make_tuple(std::make_pair("porkchop", 0), std::make_pair(319, 0))},
	{ UITEM_COOKEDPORKCHOP     , std::make_tuple(std::make_pair("cooked_porkchop", 0), std::make_pair(320, 0))},
	{ UITEM_PAINTING           , std::make_tuple(std::make_pair("painting", 0), std::make_pair(321, 0))},
	{ UITEM_GOLDENAPPLE_NORMAL , std::make_tuple(std::make_pair("golden_apple", 0), std::make_pair(322, 0))},
	{ UITEM_GOLDENAPPLE_ENCHANTED, std::make_tuple(std::make_pair("golden_apple", 1), std::make_pair(466, 0))},
	{ UITEM_SIGN               , std::make_tuple(std::make_pair("sign", 0), std::make_pair(323, 0))},
	{ UITEM_WOODENDOOR         , std::make_tuple(std::make_pair("wooden_door", 0), std::make_pair(324, 0))},
	{ UITEM_BUCKET             , std::make_tuple(std::make_pair("bucket", 0), std::make_pair(325, 0))},
	{ UITEM_WATERBUCKET        , std::make_tuple(std::make_pair("water_bucket", 0), std::make_pair(325, 8))},
	{ UITEM_LAVABUCKET         , std::make_tuple(std::make_pair("lava_bucket", 0), std::make_pair(325, 10))},
	{ UITEM_MINECART           , std::make_tuple(std::make_pair("minecart", 0), std::make_pair(328, 0))},
	{ UITEM_SADDLE             , std::make_tuple(std::make_pair("saddle", 0), std::make_pair(329, 0))},
	{ UITEM_IRONDOOR           , std::make_tuple(std::make_pair("iron_door", 0), std::make_pair(330, 0))},
	{ UITEM_REDSTONE           , std::make_tuple(std::make_pair("redstone", 0), std::make_pair(331, 0))},
	{ UITEM_SNOWBALL           , std::make_tuple(std::make_pair("snowball", 0), std::make_pair(332, 0))},
	{ UITEM_BOAT               , std::make_tuple(std::make_pair("boat", 0), std::make_pair(333, 0))},
	{ UITEM_LEATHER            , std::make_tuple(std::make_pair("leather", 0), std::make_pair(334, 0))},
	{ UITEM_MILKBUCKET         , std::make_tuple(std::make_pair("milk_bucket", 0), std::make_pair(325, 1))},
	{ UITEM_BRICK              , std::make_tuple(std::make_pair("brick", 0), std::make_pair(336, 0))},
	{ UITEM_CLAYBALL           , std::make_tuple(std::make_pair("clay_ball", 0), std::make_pair(337, 0))},
	{ UITEM_REEDS              , std::make_tuple(std::make_pair("reeds", 0), std::make_pair(338, 0))},
	{ UITEM_PAPER              , std::make_tuple(std::make_pair("paper", 0), std::make_pair(339, 0))},
	{ UITEM_BOOK               , std::make_tuple(std::make_pair("book", 0), std::make_pair(340, 0))},
	{ UITEM_SLIMEBALL          , std::make_tuple(std::make_pair("slime_ball", 0), std::make_pair(341, 0))},
	{ UITEM_CHESTMINECART      , std::make_tuple(std::make_pair("chest_minecart", 0), std::make_pair(342, 0))},
	{ UITEM_FURNACEMINECART    , std::make_tuple(std::make_pair("furnace_minecart", 0), std::make_pair(-1, -1))},
	{ UITEM_EGG                , std::make_tuple(std::make_pair("egg", 0), std::make_pair(344, 0))},
	{ UITEM_COMPASS            , std::make_tuple(std::make_pair("compass", 0), std::make_pair(345, 0))},
	{ UITEM_FISHINGROD         , std::make_tuple(std::make_pair("fishing_rod", 0), std::make_pair(346, 0))},
	{ UITEM_CLOCK              , std::make_tuple(std::make_pair("clock", 0), std::make_pair(347, 0))},
	{ UITEM_GLOWSTONEDUST      , std::make_tuple(std::make_pair("glowstone_dust", 0), std::make_pair(348, 0))},
	{ UITEM_FISH_RAWFISH       , std::make_tuple(std::make_pair("fish", 0), std::make_pair(349, 0))},
	{ UITEM_FISH_RAWSALMON     , std::make_tuple(std::make_pair("fish", 1), std::make_pair(460, 0))},
	{ UITEM_FISH_CLOWNFISH     , std::make_tuple(std::make_pair("fish", 2), std::make_pair(461, 0))},
	{ UITEM_FISH_PUFFERFISH    , std::make_tuple(std::make_pair("fish", 3), std::make_pair(462, 0))},
	{ UITEM_COOKEDFISH_COOKEDFISH, std::make_tuple(std::make_pair("cooked_fish", 0), std::make_pair(350, 0))},
	{ UITEM_COOKEDFISH_COOKEDSALMON, std::make_tuple(std::make_pair("cooked_fish", 1), std::make_pair(463, 0))},
	{ UITEM_DYE_BLACK          , std::make_tuple(std::make_pair("dye", 0), std::make_pair(351, 0))},
	{ UITEM_DYE_RED            , std::make_tuple(std::make_pair("dye", 1), std::make_pair(351, 1))},
	{ UITEM_DYE_GREEN          , std::make_tuple(std::make_pair("dye", 2), std::make_pair(351, 2))},
	{ UITEM_DYE_BROWN          , std::make_tuple(std::make_pair("dye", 3), std::make_pair(351, 3))},
	{ UITEM_DYE_BLUE           , std::make_tuple(std::make_pair("dye", 4), std::make_pair(351, 4))},
	{ UITEM_DYE_PURPLE         , std::make_tuple(std::make_pair("dye", 5), std::make_pair(351, 5))},
	{ UITEM_DYE_CYAN           , std::make_tuple(std::make_pair("dye", 6), std::make_pair(351, 6))},
	{ UITEM_DYE_SILVER         , std::make_tuple(std::make_pair("dye", 7), std::make_pair(351, 7))},
	{ UITEM_DYE_GRAY           , std::make_tuple(std::make_pair("dye", 8), std::make_pair(351, 8))},
	{ UITEM_DYE_PINK           , std::make_tuple(std::make_pair("dye", 9), std::make_pair(351, 9))},
	{ UITEM_DYE_LIME           , std::make_tuple(std::make_pair("dye", 10), std::make_pair(351, 10))},
	{ UITEM_DYE_YELLOW         , std::make_tuple(std::make_pair("dye", 11), std::make_pair(351, 11))},
	{ UITEM_DYE_LIGHTBLUE      , std::make_tuple(std::make_pair("dye", 12), std::make_pair(351, 12))},
	{ UITEM_DYE_MAGENTA        , std::make_tuple(std::make_pair("dye", 13), std::make_pair(351, 13))},
	{ UITEM_DYE_ORANGE         , std::make_tuple(std::make_pair("dye", 14), std::make_pair(351, 14))},
	{ UITEM_DYE_WHITE          , std::make_tuple(std::make_pair("dye", 15), std::make_pair(351, 15))},
	{ UITEM_BONE               , std::make_tuple(std::make_pair("bone", 0), std::make_pair(352, 0))},
	{ UITEM_SUGAR              , std::make_tuple(std::make_pair("sugar", 0), std::make_pair(353, 0))},
	{ UITEM_CAKE               , std::make_tuple(std::make_pair("cake", 0), std::make_pair(354, 0))},
	{ UITEM_BED                , std::make_tuple(std::make_pair("bed", 0), std::make_pair(355, 0))},
	{ UITEM_REPEATER           , std::make_tuple(std::make_pair("repeater", 0), std::make_pair(356, 0))},
	{ UITEM_COOKIE             , std::make_tuple(std::make_pair("cookie", 0), std::make_pair(357, 0))},
	{ UITEM_FILLEDMAP          , std::make_tuple(std::make_pair("filled_map", 0), std::make_pair(358, 0))},
	{ UITEM_SHEARS             , std::make_tuple(std::make_pair("shears", 0), std::make_pair(359, 0))},
	{ UITEM_MELON              , std::make_tuple(std::make_pair("melon", 0), std::make_pair(360, 0))},
	{ UITEM_PUMPKINSEEDS       , std::make_tuple(std::make_pair("pumpkin_seeds", 0), std::make_pair(361, 0))},
	{ UITEM_MELONSEEDS         , std::make_tuple(std::make_pair("melon_seeds", 0), std::make_pair(362, 0))},
	{ UITEM_BEEF               , std::make_tuple(std::make_pair("beef", 0), std::make_pair(363, 0))},
	{ UITEM_COOKEDBEEF         , std::make_tuple(std::make_pair("cooked_beef", 0), std::make_pair(364, 0))},
	{ UITEM_CHICKEN            , std::make_tuple(std::make_pair("chicken", 0), std::make_pair(365, 0))},
	{ UITEM_COOKEDCHICKEN      , std::make_tuple(std::make_pair("cooked_chicken", 0), std::make_pair(366, 0))},
	{ UITEM_ROTTENFLESH        , std::make_tuple(std::make_pair("rotten_flesh", 0), std::make_pair(367, 0))},
	{ UITEM_ENDERPEARL         , std::make_tuple(std::make_pair("ender_pearl", 0), std::make_pair(368, 0))},
	{ UITEM_BLAZEROD           , std::make_tuple(std::make_pair("blaze_rod", 0), std::make_pair(369, 0))},
	{ UITEM_GHASTTEAR          , std::make_tuple(std::make_pair("ghast_tear", 0), std::make_pair(370, 0))},
	{ UITEM_GOLDNUGGET         , std::make_tuple(std::make_pair("gold_nugget", 0), std::make_pair(371, 0))},
	{ UITEM_NETHERWART         , std::make_tuple(std::make_pair("nether_wart", 0), std::make_pair(372, 0))},
	{ UITEM_POTION             , std::make_tuple(std::make_pair("potion", 0), std::make_pair(373, 0))},
	{ UITEM_GLASSBOTTLE        , std::make_tuple(std::make_pair("glass_bottle", 0), std::make_pair(374, 0))},
	{ UITEM_SPIDEREYE          , std::make_tuple(std::make_pair("spider_eye", 0), std::make_pair(375, 0))},
	{ UITEM_FERMENTEDSPIDEREYE , std::make_tuple(std::make_pair("fermented_spider_eye", 0), std::make_pair(376, 0))},
	{ UITEM_BLAZEPOWDER        , std::make_tuple(std::make_pair("blaze_powder", 0), std::make_pair(377, 0))},
	{ UITEM_MAGMACREAM         , std::make_tuple(std::make_pair("magma_cream", 0), std::make_pair(378, 0))},
	{ UITEM_BREWINGSTAND       , std::make_tuple(std::make_pair("brewing_stand", 0), std::make_pair(379, 0))},
	{ UITEM_CAULDRON           , std::make_tuple(std::make_pair("cauldron", 0), std::make_pair(380, 0))},
	{ UITEM_ENDEREYE           , std::make_tuple(std::make_pair("ender_eye", 0), std::make_pair(381, 0))},
	{ UITEM_SPECKLEDMELON      , std::make_tuple(std::make_pair("speckled_melon", 0), std::make_pair(382, 0))},
	{ UITEM_SPAWNEGG           , std::make_tuple(std::make_pair("spawn_egg", 0), std::make_pair(383, 0))},
	{ UITEM_EXPERIENCEBOTTLE   , std::make_tuple(std::make_pair("experience_bottle", 0), std::make_pair(384, 0))},
	{ UITEM_FIRECHARGE         , std::make_tuple(std::make_pair("fire_charge", 0), std::make_pair(385, 0))},
	{ UITEM_WRITABLEBOOK       , std::make_tuple(std::make_pair("writable_book", 0), std::make_pair(-1, -1))},
	{ UITEM_WRITTENBOOK        , std::make_tuple(std::make_pair("written_book", 0), std::make_pair(-1, -1))},
	{ UITEM_EMERALD            , std::make_tuple(std::make_pair("emerald", 0), std::make_pair(388, 0))},
	{ UITEM_ITEMFRAME          , std::make_tuple(std::make_pair("item_frame", 0), std::make_pair(389, 0))},
	{ UITEM_FLOWERPOT          , std::make_tuple(std::make_pair("flower_pot", 0), std::make_pair(390, 0))},
	{ UITEM_CARROT             , std::make_tuple(std::make_pair("carrot", 0), std::make_pair(391, 0))},
	{ UITEM_POTATO             , std::make_tuple(std::make_pair("potato", 0), std::make_pair(392, 0))},
	{ UITEM_BAKEDPOTATO        , std::make_tuple(std::make_pair("baked_potato", 0), std::make_pair(393, 0))},
	{ UITEM_POISONOUSPOTATO    , std::make_tuple(std::make_pair("poisonous_potato", 0), std::make_pair(394, 0))},
	{ UITEM_MAP                , std::make_tuple(std::make_pair("map", 0), std::make_pair(395, 0))},
	{ UITEM_GOLDENCARROT       , std::make_tuple(std::make_pair("golden_carrot", 0), std::make_pair(396, 0))},
	{ UITEM_SKULL_SKELETON     , std::make_tuple(std::make_pair("skull", 0), std::make_pair(397, 0))},
	{ UITEM_SKULL_WITHER       , std::make_tuple(std::make_pair("skull", 1), std::make_pair(397, 1))},
	{ UITEM_SKULL_ZOMBIE       , std::make_tuple(std::make_pair("skull", 2), std::make_pair(397, 2))},
	{ UITEM_SKULL_PLAYER       , std::make_tuple(std::make_pair("skull", 3), std::make_pair(397, 3))},
	{ UITEM_SKULL_CREEPER      , std::make_tuple(std::make_pair("skull", 4), std::make_pair(397, 4))},
	{ UITEM_CARROTONASTICK     , std::make_tuple(std::make_pair("carrot_on_a_stick", 0), std::make_pair(398, 0))},
	{ UITEM_NETHERSTAR         , std::make_tuple(std::make_pair("nether_star", 0), std::make_pair(399, 0))},
	{ UITEM_PUMPKINPIE         , std::make_tuple(std::make_pair("pumpkin_pie", 0), std::make_pair(400, 0))},
	{ UITEM_FIREWORKS          , std::make_tuple(std::make_pair("fireworks", 0), std::make_pair(-1, -1))},
	{ UITEM_FIREWORKCHARGE     , std::make_tuple(std::make_pair("firework_charge", 0), std::make_pair(-1, -1))},
	{ UITEM_ENCHANTEDBOOK      , std::make_tuple(std::make_pair("enchanted_book", 0), std::make_pair(403, 0))},
	{ UITEM_COMPARATOR         , std::make_tuple(std::make_pair("comparator", 0), std::make_pair(404, 0))},
	{ UITEM_NETHERBRICK        , std::make_tuple(std::make_pair("netherbrick", 0), std::make_pair(405, 0))},
	{ UITEM_QUARTZ             , std::make_tuple(std::make_pair("quartz", 0), std::make_pair(406, 0))},
	{ UITEM_TNTMINECART        , std::make_tuple(std::make_pair("tnt_minecart", 0), std::make_pair(407, 0))},
	{ UITEM_HOPPERMINECART     , std::make_tuple(std::make_pair("hopper_minecart", 0), std::make_pair(408, 0))},
	{ UITEM_PRISMARINESHARD    , std::make_tuple(std::make_pair("prismarine_shard", 0), std::make_pair(409, 0))},
	{ UITEM_PRISMARINECRYSTALS , std::make_tuple(std::make_pair("prismarine_crystals", 0), std::make_pair(422, 0))},
	{ UITEM_RABBIT             , std::make_tuple(std::make_pair("rabbit", 0), std::make_pair(411, 0))},
	{ UITEM_COOKEDRABBIT       , std::make_tuple(std::make_pair("cooked_rabbit", 0), std::make_pair(412, 0))},
	{ UITEM_RABBITSTEW         , std::make_tuple(std::make_pair("rabbit_stew", 0), std::make_pair(413, 0))},
	{ UITEM_RABBITFOOT         , std::make_tuple(std::make_pair("rabbit_foot", 0), std::make_pair(414, 0))},
	{ UITEM_RABBITHIDE         , std::make_tuple(std::make_pair("rabbit_hide", 0), std::make_pair(415, 0))},
	{ UITEM_ARMORSTAND         , std::make_tuple(std::make_pair("armor_stand", 0), std::make_pair(-1, -1))},
	{ UITEM_IRONHORSEARMOR     , std::make_tuple(std::make_pair("iron_horse_armor", 0), std::make_pair(417, 0))},
	{ UITEM_GOLDENHORSEARMOR   , std::make_tuple(std::make_pair("golden_horse_armor", 0), std::make_pair(418, 0))},
	{ UITEM_DIAMONDHORSEARMOR  , std::make_tuple(std::make_pair("diamond_horse_armor", 0), std::make_pair(419, 0))},
	{ UITEM_LEAD               , std::make_tuple(std::make_pair("lead", 0), std::make_pair(420, 0))},
	{ UITEM_NAMETAG            , std::make_tuple(std::make_pair("name_tag", 0), std::make_pair(421, 0))},
	{ UITEM_COMMANDBLOCKMINECART, std::make_tuple(std::make_pair("command_block_minecart", 0), std::make_pair(-1, -1))},
	{ UITEM_MUTTON             , std::make_tuple(std::make_pair("mutton", 0), std::make_pair(423, 0))},
	{ UITEM_COOKEDMUTTON       , std::make_tuple(std::make_pair("cooked_mutton", 0), std::make_pair(424, 0))},
	{ UITEM_BANNER             , std::make_tuple(std::make_pair("banner", 0), std::make_pair(-1, -1))},
	{ UITEM_ENDCRYSTAL         , std::make_tuple(std::make_pair("end_crystal", 0), std::make_pair(426, 0))},
	{ UITEM_SPRUCEDOOR         , std::make_tuple(std::make_pair("spruce_door", 0), std::make_pair(427, 0))},
	{ UITEM_BIRCHDOOR          , std::make_tuple(std::make_pair("birch_door", 0), std::make_pair(428, 0))},
	{ UITEM_JUNGLEDOOR         , std::make_tuple(std::make_pair("jungle_door", 0), std::make_pair(429, 0))},
	{ UITEM_ACACIADOOR         , std::make_tuple(std::make_pair("acacia_door", 0), std::make_pair(430, 0))},
	{ UITEM_DARKOAKDOOR        , std::make_tuple(std::make_pair("dark_oak_door", 0), std::make_pair(431, 0))},
	{ UITEM_CHORUSFRUIT        , std::make_tuple(std::make_pair("chorus_fruit", 0), std::make_pair(432, 0))},
	{ UITEM_CHORUSFRUITPOPPED  , std::make_tuple(std::make_pair("chorus_fruit_popped", 0), std::make_pair(433, 0))},
	{ UITEM_BEETROOT           , std::make_tuple(std::make_pair("beetroot", 0), std::make_pair(457, 0))},
	{ UITEM_BEETROOTSEEDS      , std::make_tuple(std::make_pair("beetroot_seeds", 0), std::make_pair(458, 0))},
	{ UITEM_BEETROOTSOUP       , std::make_tuple(std::make_pair("beetroot_soup", 0), std::make_pair(459, 0))},
	{ UITEM_DRAGONBREATH       , std::make_tuple(std::make_pair("dragon_breath", 0), std::make_pair(437, 0))},
	{ UITEM_SPLASHPOTION       , std::make_tuple(std::make_pair("splash_potion", 0), std::make_pair(438, 0))},
	{ UITEM_SPECTRALARROW      , std::make_tuple(std::make_pair("spectral_arrow", 0), std::make_pair(-1, -1))},
	{ UITEM_TIPPEDARROW        , std::make_tuple(std::make_pair("tipped_arrow", 0), std::make_pair(-1, -1))},
	{ UITEM_LINGERINGPOTION    , std::make_tuple(std::make_pair("lingering_potion", 0), std::make_pair(441, 0))},
	{ UITEM_SHIELD             , std::make_tuple(std::make_pair("shield", 0), std::make_pair(-1, -1))},
	{ UITEM_ELYTRA             , std::make_tuple(std::make_pair("elytra", 0), std::make_pair(444, 0))},
	{ UITEM_SPRUCEBOAT         , std::make_tuple(std::make_pair("spruce_boat", 0), std::make_pair(333, 1))},
	{ UITEM_BIRCHBOAT          , std::make_tuple(std::make_pair("birch_boat", 0), std::make_pair(333, 2))},
	{ UITEM_JUNGLEBOAT         , std::make_tuple(std::make_pair("jungle_boat", 0), std::make_pair(333, 3))},
	{ UITEM_ACACIABOAT         , std::make_tuple(std::make_pair("acacia_boat", 0), std::make_pair(333, 4))},
	{ UITEM_DARKOAKBOAT        , std::make_tuple(std::make_pair("dark_oak_boat", 0), std::make_pair(333, 5))},
	{ UITEM_TOTEMOFUNDYING     , std::make_tuple(std::make_pair("totem_of_undying", 0), std::make_pair(-1, -1))},
	{ UITEM_SHULKERSHELL       , std::make_tuple(std::make_pair("shulker_shell", 0), std::make_pair(445, 0))},
	{ UITEM_IRONNUGGET         , std::make_tuple(std::make_pair("iron_nugget", 0), std::make_pair(-1, -1))},
	{ UITEM_RECORD13           , std::make_tuple(std::make_pair("record_13", 0), std::make_pair(-1, -1))},
	{ UITEM_RECORDCAT          , std::make_tuple(std::make_pair("record_cat", 0), std::make_pair(-1, -1))},
	{ UITEM_RECORDBLOCKS       , std::make_tuple(std::make_pair("record_blocks", 0), std::make_pair(-1, -1))},
	{ UITEM_RECORDCHIRP        , std::make_tuple(std::make_pair("record_chirp", 0), std::make_pair(-1, -1))},
	{ UITEM_RECORDFAR          , std::make_tuple(std::make_pair("record_far", 0), std::make_pair(-1, -1))},
	{ UITEM_RECORDMALL         , std::make_tuple(std::make_pair("record_mall", 0), std::make_pair(-1, -1))},
	{ UITEM_RECORDMELLOHI      , std::make_tuple(std::make_pair("record_mellohi", 0), std::make_pair(-1, -1))},
	{ UITEM_RECORDSTAL         , std::make_tuple(std::make_pair("record_stal", 0), std::make_pair(-1, -1))},
	{ UITEM_RECORDSTRAD        , std::make_tuple(std::make_pair("record_strad", 0), std::make_pair(-1, -1))},
	{ UITEM_RECORDWARD         , std::make_tuple(std::make_pair("record_ward", 0), std::make_pair(-1, -1))},
	{ UITEM_RECORD11           , std::make_tuple(std::make_pair("record_11", 0), std::make_pair(-1, -1))},
	{ UITEM_RECORDWAIT         , std::make_tuple(std::make_pair("record_wait", 0), std::make_pair(-1, -1))},
};
const std::map<std::pair<std::string, short>, std::tuple<UItemID, std::pair<short, short>>> JavaItemM = {
	{ std::make_pair("iron_shovel", 0), std::make_tuple(UITEM_IRONSHOVEL, std::make_pair(256, 0))},
	{ std::make_pair("iron_pickaxe", 0), std::make_tuple(UITEM_IRONPICKAXE, std::make_pair(257, 0))},
	{ std::make_pair("iron_axe", 0), std::make_tuple(UITEM_IRONAXE, std::make_pair(258, 0))},
	{ std::make_pair("flint_and_steel", 0), std::make_tuple(UITEM_FLINTANDSTEEL, std::make_pair(259, 0))},
	{ std::make_pair("apple", 0), std::make_tuple(UITEM_APPLE, std::make_pair(260, 0))},
	{ std::make_pair("bow", 0), std::make_tuple(UITEM_BOW, std::make_pair(261, 0))},
	{ std::make_pair("arrow", 0), std::make_tuple(UITEM_ARROW, std::make_pair(262, 0))},
	{ std::make_pair("coal", 0), std::make_tuple(UITEM_COAL_COAL, std::make_pair(263, 0))},
	{ std::make_pair("coal", 1), std::make_tuple(UITEM_COAL_CHARCOAL, std::make_pair(263, 1))},
	{ std::make_pair("diamond", 0), std::make_tuple(UITEM_DIAMOND, std::make_pair(264, 0))},
	{ std::make_pair("iron_ingot", 0), std::make_tuple(UITEM_IRONINGOT, std::make_pair(265, 0))},
	{ std::make_pair("gold_ingot", 0), std::make_tuple(UITEM_GOLDINGOT, std::make_pair(266, 0))},
	{ std::make_pair("iron_sword", 0), std::make_tuple(UITEM_IRONSWORD, std::make_pair(267, 0))},
	{ std::make_pair("wooden_sword", 0), std::make_tuple(UITEM_WOODENSWORD, std::make_pair(268, 0))},
	{ std::make_pair("wooden_shovel", 0), std::make_tuple(UITEM_WOODENSHOVEL, std::make_pair(269, 0))},
	{ std::make_pair("wooden_pickaxe", 0), std::make_tuple(UITEM_WOODENPICKAXE, std::make_pair(270, 0))},
	{ std::make_pair("wooden_axe", 0), std::make_tuple(UITEM_WOODENAXE, std::make_pair(271, 0))},
	{ std::make_pair("stone_sword", 0), std::make_tuple(UITEM_STONESWORD, std::make_pair(272, 0))},
	{ std::make_pair("stone_shovel", 0), std::make_tuple(UITEM_STONESHOVEL, std::make_pair(273, 0))},
	{ std::make_pair("stone_pickaxe", 0), std::make_tuple(UITEM_STONEPICKAXE, std::make_pair(274, 0))},
	{ std::make_pair("stone_axe", 0), std::make_tuple(UITEM_STONEAXE, std::make_pair(275, 0))},
	{ std::make_pair("diamond_sword", 0), std::make_tuple(UITEM_DIAMONDSWORD, std::make_pair(276, 0))},
	{ std::make_pair("diamond_shovel", 0), std::make_tuple(UITEM_DIAMONDSHOVEL, std::make_pair(277, 0))},
	{ std::make_pair("diamond_pickaxe", 0), std::make_tuple(UITEM_DIAMONDPICKAXE, std::make_pair(278, 0))},
	{ std::make_pair("diamond_axe", 0), std::make_tuple(UITEM_DIAMONDAXE, std::make_pair(279, 0))},
	{ std::make_pair("stick", 0), std::make_tuple(UITEM_STICK, std::make_pair(280, 0))},
	{ std::make_pair("bowl", 0), std::make_tuple(UITEM_BOWL, std::make_pair(281, 0))},
	{ std::make_pair("mushroom_stew", 0), std::make_tuple(UITEM_MUSHROOMSTEW, std::make_pair(282, 0))},
	{ std::make_pair("golden_sword", 0), std::make_tuple(UITEM_GOLDENSWORD, std::make_pair(283, 0))},
	{ std::make_pair("golden_shovel", 0), std::make_tuple(UITEM_GOLDENSHOVEL, std::make_pair(284, 0))},
	{ std::make_pair("golden_pickaxe", 0), std::make_tuple(UITEM_GOLDENPICKAXE, std::make_pair(285, 0))},
	{ std::make_pair("golden_axe", 0), std::make_tuple(UITEM_GOLDENAXE, std::make_pair(286, 0))},
	{ std::make_pair("string", 0), std::make_tuple(UITEM_STRING, std::make_pair(287, 0))},
	{ std::make_pair("feather", 0), std::make_tuple(UITEM_FEATHER, std::make_pair(288, 0))},
	{ std::make_pair("gunpowder", 0), std::make_tuple(UITEM_GUNPOWDER, std::make_pair(289, 0))},
	{ std::make_pair("wooden_hoe", 0), std::make_tuple(UITEM_WOODENHOE, std::make_pair(290, 0))},
	{ std::make_pair("stone_hoe", 0), std::make_tuple(UITEM_STONEHOE, std::make_pair(291, 0))},
	{ std::make_pair("iron_hoe", 0), std::make_tuple(UITEM_IRONHOE, std::make_pair(292, 0))},
	{ std::make_pair("diamond_hoe", 0), std::make_tuple(UITEM_DIAMONDHOE, std::make_pair(293, 0))},
	{ std::make_pair("golden_hoe", 0), std::make_tuple(UITEM_GOLDENHOE, std::make_pair(294, 0))},
	{ std::make_pair("wheat_seeds", 0), std::make_tuple(UITEM_WHEATSEEDS, std::make_pair(295, 0))},
	{ std::make_pair("wheat", 0), std::make_tuple(UITEM_WHEAT, std::make_pair(296, 0))},
	{ std::make_pair("bread", 0), std::make_tuple(UITEM_BREAD, std::make_pair(297, 0))},
	{ std::make_pair("leather_helmet", 0), std::make_tuple(UITEM_LEATHERHELMET, std::make_pair(298, 0))},
	{ std::make_pair("leather_chestplate", 0), std::make_tuple(UITEM_LEATHERCHESTPLATE, std::make_pair(299, 0))},
	{ std::make_pair("leather_leggings", 0), std::make_tuple(UITEM_LEATHERLEGGINGS, std::make_pair(300, 0))},
	{ std::make_pair("leather_boots", 0), std::make_tuple(UITEM_LEATHERBOOTS, std::make_pair(301, 0))},
	{ std::make_pair("chainmail_helmet", 0), std::make_tuple(UITEM_CHAINMAILHELMET, std::make_pair(302, 0))},
	{ std::make_pair("chainmail_chestplate", 0), std::make_tuple(UITEM_CHAINMAILCHESTPLATE, std::make_pair(303, 0))},
	{ std::make_pair("chainmail_leggings", 0), std::make_tuple(UITEM_CHAINMAILLEGGINGS, std::make_pair(304, 0))},
	{ std::make_pair("chainmail_boots", 0), std::make_tuple(UITEM_CHAINMAILBOOTS, std::make_pair(305, 0))},
	{ std::make_pair("iron_helmet", 0), std::make_tuple(UITEM_IRONHELMET, std::make_pair(306, 0))},
	{ std::make_pair("iron_chestplate", 0), std::make_tuple(UITEM_IRONCHESTPLATE, std::make_pair(307, 0))},
	{ std::make_pair("iron_leggings", 0), std::make_tuple(UITEM_IRONLEGGINGS, std::make_pair(308, 0))},
	{ std::make_pair("iron_boots", 0), std::make_tuple(UITEM_IRONBOOTS, std::make_pair(309, 0))},
	{ std::make_pair("diamond_helmet", 0), std::make_tuple(UITEM_DIAMONDHELMET, std::make_pair(310, 0))},
	{ std::make_pair("diamond_chestplate", 0), std::make_tuple(UITEM_DIAMONDCHESTPLATE, std::make_pair(311, 0))},
	{ std::make_pair("diamond_leggings", 0), std::make_tuple(UITEM_DIAMONDLEGGINGS, std::make_pair(312, 0))},
	{ std::make_pair("diamond_boots", 0), std::make_tuple(UITEM_DIAMONDBOOTS, std::make_pair(313, 0))},
	{ std::make_pair("golden_helmet", 0), std::make_tuple(UITEM_GOLDENHELMET, std::make_pair(314, 0))},
	{ std::make_pair("golden_chestplate", 0), std::make_tuple(UITEM_GOLDENCHESTPLATE, std::make_pair(315, 0))},
	{ std::make_pair("golden_leggings", 0), std::make_tuple(UITEM_GOLDENLEGGINGS, std::make_pair(316, 0))},
	{ std::make_pair("golden_boots", 0), std::make_tuple(UITEM_GOLDENBOOTS, std::make_pair(317, 0))},
	{ std::make_pair("flint", 0), std::make_tuple(UITEM_FLINT, std::make_pair(318, 0))},
	{ std::make_pair("porkchop", 0), std::make_tuple(UITEM_PORKCHOP, std::make_pair(319, 0))},
	{ std::make_pair("cooked_porkchop", 0), std::make_tuple(UITEM_COOKEDPORKCHOP, std::make_pair(320, 0))},
	{ std::make_pair("painting", 0), std::make_tuple(UITEM_PAINTING, std::make_pair(321, 0))},
	{ std::make_pair("golden_apple", 0), std::make_tuple(UITEM_GOLDENAPPLE_NORMAL, std::make_pair(322, 0))},
	{ std::make_pair("golden_apple", 1), std::make_tuple(UITEM_GOLDENAPPLE_ENCHANTED, std::make_pair(466, 0))},
	{ std::make_pair("sign", 0), std::make_tuple(UITEM_SIGN, std::make_pair(323, 0))},
	{ std::make_pair("wooden_door", 0), std::make_tuple(UITEM_WOODENDOOR, std::make_pair(324, 0))},
	{ std::make_pair("bucket", 0), std::make_tuple(UITEM_BUCKET, std::make_pair(325, 0))},
	{ std::make_pair("water_bucket", 0), std::make_tuple(UITEM_WATERBUCKET, std::make_pair(325, 8))},
	{ std::make_pair("lava_bucket", 0), std::make_tuple(UITEM_LAVABUCKET, std::make_pair(325, 10))},
	{ std::make_pair("minecart", 0), std::make_tuple(UITEM_MINECART, std::make_pair(328, 0))},
	{ std::make_pair("saddle", 0), std::make_tuple(UITEM_SADDLE, std::make_pair(329, 0))},
	{ std::make_pair("iron_door", 0), std::make_tuple(UITEM_IRONDOOR, std::make_pair(330, 0))},
	{ std::make_pair("redstone", 0), std::make_tuple(UITEM_REDSTONE, std::make_pair(331, 0))},
	{ std::make_pair("snowball", 0), std::make_tuple(UITEM_SNOWBALL, std::make_pair(332, 0))},
	{ std::make_pair("boat", 0), std::make_tuple(UITEM_BOAT, std::make_pair(333, 0))},
	{ std::make_pair("leather", 0), std::make_tuple(UITEM_LEATHER, std::make_pair(334, 0))},
	{ std::make_pair("milk_bucket", 0), std::make_tuple(UITEM_MILKBUCKET, std::make_pair(325, 1))},
	{ std::make_pair("brick", 0), std::make_tuple(UITEM_BRICK, std::make_pair(336, 0))},
	{ std::make_pair("clay_ball", 0), std::make_tuple(UITEM_CLAYBALL, std::make_pair(337, 0))},
	{ std::make_pair("reeds", 0), std::make_tuple(UITEM_REEDS, std::make_pair(338, 0))},
	{ std::make_pair("paper", 0), std::make_tuple(UITEM_PAPER, std::make_pair(339, 0))},
	{ std::make_pair("book", 0), std::make_tuple(UITEM_BOOK, std::make_pair(340, 0))},
	{ std::make_pair("slime_ball", 0), std::make_tuple(UITEM_SLIMEBALL, std::make_pair(341, 0))},
	{ std::make_pair("chest_minecart", 0), std::make_tuple(UITEM_CHESTMINECART, std::make_pair(342, 0))},
	{ std::make_pair("furnace_minecart", 0), std::make_tuple(UITEM_FURNACEMINECART, std::make_pair(-1, -1))},
	{ std::make_pair("egg", 0), std::make_tuple(UITEM_EGG, std::make_pair(344, 0))},
	{ std::make_pair("compass", 0), std::make_tuple(UITEM_COMPASS, std::make_pair(345, 0))},
	{ std::make_pair("fishing_rod", 0), std::make_tuple(UITEM_FISHINGROD, std::make_pair(346, 0))},
	{ std::make_pair("clock", 0), std::make_tuple(UITEM_CLOCK, std::make_pair(347, 0))},
	{ std::make_pair("glowstone_dust", 0), std::make_tuple(UITEM_GLOWSTONEDUST, std::make_pair(348, 0))},
	{ std::make_pair("fish", 0), std::make_tuple(UITEM_FISH_RAWFISH, std::make_pair(349, 0))},
	{ std::make_pair("fish", 1), std::make_tuple(UITEM_FISH_RAWSALMON, std::make_pair(460, 0))},
	{ std::make_pair("fish", 2), std::make_tuple(UITEM_FISH_CLOWNFISH, std::make_pair(461, 0))},
	{ std::make_pair("fish", 3), std::make_tuple(UITEM_FISH_PUFFERFISH, std::make_pair(462, 0))},
	{ std::make_pair("cooked_fish", 0), std::make_tuple(UITEM_COOKEDFISH_COOKEDFISH, std::make_pair(350, 0))},
	{ std::make_pair("cooked_fish", 1), std::make_tuple(UITEM_COOKEDFISH_COOKEDSALMON, std::make_pair(463, 0))},
	{ std::make_pair("dye", 0), std::make_tuple(UITEM_DYE_BLACK, std::make_pair(351, 0))},
	{ std::make_pair("dye", 1), std::make_tuple(UITEM_DYE_RED, std::make_pair(351, 1))},
	{ std::make_pair("dye", 2), std::make_tuple(UITEM_DYE_GREEN, std::make_pair(351, 2))},
	{ std::make_pair("dye", 3), std::make_tuple(UITEM_DYE_BROWN, std::make_pair(351, 3))},
	{ std::make_pair("dye", 4), std::make_tuple(UITEM_DYE_BLUE, std::make_pair(351, 4))},
	{ std::make_pair("dye", 5), std::make_tuple(UITEM_DYE_PURPLE, std::make_pair(351, 5))},
	{ std::make_pair("dye", 6), std::make_tuple(UITEM_DYE_CYAN, std::make_pair(351, 6))},
	{ std::make_pair("dye", 7), std::make_tuple(UITEM_DYE_SILVER, std::make_pair(351, 7))},
	{ std::make_pair("dye", 8), std::make_tuple(UITEM_DYE_GRAY, std::make_pair(351, 8))},
	{ std::make_pair("dye", 9), std::make_tuple(UITEM_DYE_PINK, std::make_pair(351, 9))},
	{ std::make_pair("dye", 10), std::make_tuple(UITEM_DYE_LIME, std::make_pair(351, 10))},
	{ std::make_pair("dye", 11), std::make_tuple(UITEM_DYE_YELLOW, std::make_pair(351, 11))},
	{ std::make_pair("dye", 12), std::make_tuple(UITEM_DYE_LIGHTBLUE, std::make_pair(351, 12))},
	{ std::make_pair("dye", 13), std::make_tuple(UITEM_DYE_MAGENTA, std::make_pair(351, 13))},
	{ std::make_pair("dye", 14), std::make_tuple(UITEM_DYE_ORANGE, std::make_pair(351, 14))},
	{ std::make_pair("dye", 15), std::make_tuple(UITEM_DYE_WHITE, std::make_pair(351, 15))},
	{ std::make_pair("bone", 0), std::make_tuple(UITEM_BONE, std::make_pair(352, 0))},
	{ std::make_pair("sugar", 0), std::make_tuple(UITEM_SUGAR, std::make_pair(353, 0))},
	{ std::make_pair("cake", 0), std::make_tuple(UITEM_CAKE, std::make_pair(354, 0))},
	{ std::make_pair("bed", 0), std::make_tuple(UITEM_BED, std::make_pair(355, 0))},
	{ std::make_pair("repeater", 0), std::make_tuple(UITEM_REPEATER, std::make_pair(356, 0))},
	{ std::make_pair("cookie", 0), std::make_tuple(UITEM_COOKIE, std::make_pair(357, 0))},
	{ std::make_pair("filled_map", 0), std::make_tuple(UITEM_FILLEDMAP, std::make_pair(358, 0))},
	{ std::make_pair("shears", 0), std::make_tuple(UITEM_SHEARS, std::make_pair(359, 0))},
	{ std::make_pair("melon", 0), std::make_tuple(UITEM_MELON, std::make_pair(360, 0))},
	{ std::make_pair("pumpkin_seeds", 0), std::make_tuple(UITEM_PUMPKINSEEDS, std::make_pair(361, 0))},
	{ std::make_pair("melon_seeds", 0), std::make_tuple(UITEM_MELONSEEDS, std::make_pair(362, 0))},
	{ std::make_pair("beef", 0), std::make_tuple(UITEM_BEEF, std::make_pair(363, 0))},
	{ std::make_pair("cooked_beef", 0), std::make_tuple(UITEM_COOKEDBEEF, std::make_pair(364, 0))},
	{ std::make_pair("chicken", 0), std::make_tuple(UITEM_CHICKEN, std::make_pair(365, 0))},
	{ std::make_pair("cooked_chicken", 0), std::make_tuple(UITEM_COOKEDCHICKEN, std::make_pair(366, 0))},
	{ std::make_pair("rotten_flesh", 0), std::make_tuple(UITEM_ROTTENFLESH, std::make_pair(367, 0))},
	{ std::make_pair("ender_pearl", 0), std::make_tuple(UITEM_ENDERPEARL, std::make_pair(368, 0))},
	{ std::make_pair("blaze_rod", 0), std::make_tuple(UITEM_BLAZEROD, std::make_pair(369, 0))},
	{ std::make_pair("ghast_tear", 0), std::make_tuple(UITEM_GHASTTEAR, std::make_pair(370, 0))},
	{ std::make_pair("gold_nugget", 0), std::make_tuple(UITEM_GOLDNUGGET, std::make_pair(371, 0))},
	{ std::make_pair("nether_wart", 0), std::make_tuple(UITEM_NETHERWART, std::make_pair(372, 0))},
	{ std::make_pair("potion", 0), std::make_tuple(UITEM_POTION, std::make_pair(373, 0))},
	{ std::make_pair("glass_bottle", 0), std::make_tuple(UITEM_GLASSBOTTLE, std::make_pair(374, 0))},
	{ std::make_pair("spider_eye", 0), std::make_tuple(UITEM_SPIDEREYE, std::make_pair(375, 0))},
	{ std::make_pair("fermented_spider_eye", 0), std::make_tuple(UITEM_FERMENTEDSPIDEREYE, std::make_pair(376, 0))},
	{ std::make_pair("blaze_powder", 0), std::make_tuple(UITEM_BLAZEPOWDER, std::make_pair(377, 0))},
	{ std::make_pair("magma_cream", 0), std::make_tuple(UITEM_MAGMACREAM, std::make_pair(378, 0))},
	{ std::make_pair("brewing_stand", 0), std::make_tuple(UITEM_BREWINGSTAND, std::make_pair(379, 0))},
	{ std::make_pair("cauldron", 0), std::make_tuple(UITEM_CAULDRON, std::make_pair(380, 0))},
	{ std::make_pair("ender_eye", 0), std::make_tuple(UITEM_ENDEREYE, std::make_pair(381, 0))},
	{ std::make_pair("speckled_melon", 0), std::make_tuple(UITEM_SPECKLEDMELON, std::make_pair(382, 0))},
	{ std::make_pair("spawn_egg", 0), std::make_tuple(UITEM_SPAWNEGG, std::make_pair(383, 0))},
	{ std::make_pair("experience_bottle", 0), std::make_tuple(UITEM_EXPERIENCEBOTTLE, std::make_pair(384, 0))},
	{ std::make_pair("fire_charge", 0), std::make_tuple(UITEM_FIRECHARGE, std::make_pair(385, 0))},
	{ std::make_pair("writable_book", 0), std::make_tuple(UITEM_WRITABLEBOOK, std::make_pair(-1, -1))},
	{ std::make_pair("written_book", 0), std::make_tuple(UITEM_WRITTENBOOK, std::make_pair(-1, -1))},
	{ std::make_pair("emerald", 0), std::make_tuple(UITEM_EMERALD, std::make_pair(388, 0))},
	{ std::make_pair("item_frame", 0), std::make_tuple(UITEM_ITEMFRAME, std::make_pair(389, 0))},
	{ std::make_pair("flower_pot", 0), std::make_tuple(UITEM_FLOWERPOT, std::make_pair(390, 0))},
	{ std::make_pair("carrot", 0), std::make_tuple(UITEM_CARROT, std::make_pair(391, 0))},
	{ std::make_pair("potato", 0), std::make_tuple(UITEM_POTATO, std::make_pair(392, 0))},
	{ std::make_pair("baked_potato", 0), std::make_tuple(UITEM_BAKEDPOTATO, std::make_pair(393, 0))},
	{ std::make_pair("poisonous_potato", 0), std::make_tuple(UITEM_POISONOUSPOTATO, std::make_pair(394, 0))},
	{ std::make_pair("map", 0), std::make_tuple(UITEM_MAP, std::make_pair(395, 0))},
	{ std::make_pair("golden_carrot", 0), std::make_tuple(UITEM_GOLDENCARROT, std::make_pair(396, 0))},
	{ std::make_pair("skull", 0), std::make_tuple(UITEM_SKULL_SKELETON, std::make_pair(397, 0))},
	{ std::make_pair("skull", 1), std::make_tuple(UITEM_SKULL_WITHER, std::make_pair(397, 1))},
	{ std::make_pair("skull", 2), std::make_tuple(UITEM_SKULL_ZOMBIE, std::make_pair(397, 2))},
	{ std::make_pair("skull", 3), std::make_tuple(UITEM_SKULL_PLAYER, std::make_pair(397, 3))},
	{ std::make_pair("skull", 4), std::make_tuple(UITEM_SKULL_CREEPER, std::make_pair(397, 4))},
	{ std::make_pair("carrot_on_a_stick", 0), std::make_tuple(UITEM_CARROTONASTICK, std::make_pair(398, 0))},
	{ std::make_pair("nether_star", 0), std::make_tuple(UITEM_NETHERSTAR, std::make_pair(399, 0))},
	{ std::make_pair("pumpkin_pie", 0), std::make_tuple(UITEM_PUMPKINPIE, std::make_pair(400, 0))},
	{ std::make_pair("fireworks", 0), std::make_tuple(UITEM_FIREWORKS, std::make_pair(-1, -1))},
	{ std::make_pair("firework_charge", 0), std::make_tuple(UITEM_FIREWORKCHARGE, std::make_pair(-1, -1))},
	{ std::make_pair("enchanted_book", 0), std::make_tuple(UITEM_ENCHANTEDBOOK, std::make_pair(403, 0))},
	{ std::make_pair("comparator", 0), std::make_tuple(UITEM_COMPARATOR, std::make_pair(404, 0))},
	{ std::make_pair("netherbrick", 0), std::make_tuple(UITEM_NETHERBRICK, std::make_pair(405, 0))},
	{ std::make_pair("quartz", 0), std::make_tuple(UITEM_QUARTZ, std::make_pair(406, 0))},
	{ std::make_pair("tnt_minecart", 0), std::make_tuple(UITEM_TNTMINECART, std::make_pair(407, 0))},
	{ std::make_pair("hopper_minecart", 0), std::make_tuple(UITEM_HOPPERMINECART, std::make_pair(408, 0))},
	{ std::make_pair("prismarine_shard", 0), std::make_tuple(UITEM_PRISMARINESHARD, std::make_pair(409, 0))},
	{ std::make_pair("prismarine_crystals", 0), std::make_tuple(UITEM_PRISMARINECRYSTALS, std::make_pair(422, 0))},
	{ std::make_pair("rabbit", 0), std::make_tuple(UITEM_RABBIT, std::make_pair(411, 0))},
	{ std::make_pair("cooked_rabbit", 0), std::make_tuple(UITEM_COOKEDRABBIT, std::make_pair(412, 0))},
	{ std::make_pair("rabbit_stew", 0), std::make_tuple(UITEM_RABBITSTEW, std::make_pair(413, 0))},
	{ std::make_pair("rabbit_foot", 0), std::make_tuple(UITEM_RABBITFOOT, std::make_pair(414, 0))},
	{ std::make_pair("rabbit_hide", 0), std::make_tuple(UITEM_RABBITHIDE, std::make_pair(415, 0))},
	{ std::make_pair("armor_stand", 0), std::make_tuple(UITEM_ARMORSTAND, std::make_pair(-1, -1))},
	{ std::make_pair("iron_horse_armor", 0), std::make_tuple(UITEM_IRONHORSEARMOR, std::make_pair(417, 0))},
	{ std::make_pair("golden_horse_armor", 0), std::make_tuple(UITEM_GOLDENHORSEARMOR, std::make_pair(418, 0))},
	{ std::make_pair("diamond_horse_armor", 0), std::make_tuple(UITEM_DIAMONDHORSEARMOR, std::make_pair(419, 0))},
	{ std::make_pair("lead", 0), std::make_tuple(UITEM_LEAD, std::make_pair(420, 0))},
	{ std::make_pair("name_tag", 0), std::make_tuple(UITEM_NAMETAG, std::make_pair(421, 0))},
	{ std::make_pair("command_block_minecart", 0), std::make_tuple(UITEM_COMMANDBLOCKMINECART, std::make_pair(-1, -1))},
	{ std::make_pair("mutton", 0), std::make_tuple(UITEM_MUTTON, std::make_pair(423, 0))},
	{ std::make_pair("cooked_mutton", 0), std::make_tuple(UITEM_COOKEDMUTTON, std::make_pair(424, 0))},
	{ std::make_pair("banner", 0), std::make_tuple(UITEM_BANNER, std::make_pair(-1, -1))},
	{ std::make_pair("end_crystal", 0), std::make_tuple(UITEM_ENDCRYSTAL, std::make_pair(426, 0))},
	{ std::make_pair("spruce_door", 0), std::make_tuple(UITEM_SPRUCEDOOR, std::make_pair(427, 0))},
	{ std::make_pair("birch_door", 0), std::make_tuple(UITEM_BIRCHDOOR, std::make_pair(428, 0))},
	{ std::make_pair("jungle_door", 0), std::make_tuple(UITEM_JUNGLEDOOR, std::make_pair(429, 0))},
	{ std::make_pair("acacia_door", 0), std::make_tuple(UITEM_ACACIADOOR, std::make_pair(430, 0))},
	{ std::make_pair("dark_oak_door", 0), std::make_tuple(UITEM_DARKOAKDOOR, std::make_pair(431, 0))},
	{ std::make_pair("chorus_fruit", 0), std::make_tuple(UITEM_CHORUSFRUIT, std::make_pair(432, 0))},
	{ std::make_pair("chorus_fruit_popped", 0), std::make_tuple(UITEM_CHORUSFRUITPOPPED, std::make_pair(433, 0))},
	{ std::make_pair("beetroot", 0), std::make_tuple(UITEM_BEETROOT, std::make_pair(457, 0))},
	{ std::make_pair("beetroot_seeds", 0), std::make_tuple(UITEM_BEETROOTSEEDS, std::make_pair(458, 0))},
	{ std::make_pair("beetroot_soup", 0), std::make_tuple(UITEM_BEETROOTSOUP, std::make_pair(459, 0))},
	{ std::make_pair("dragon_breath", 0), std::make_tuple(UITEM_DRAGONBREATH, std::make_pair(437, 0))},
	{ std::make_pair("splash_potion", 0), std::make_tuple(UITEM_SPLASHPOTION, std::make_pair(438, 0))},
	{ std::make_pair("spectral_arrow", 0), std::make_tuple(UITEM_SPECTRALARROW, std::make_pair(-1, -1))},
	{ std::make_pair("tipped_arrow", 0), std::make_tuple(UITEM_TIPPEDARROW, std::make_pair(-1, -1))},
	{ std::make_pair("lingering_potion", 0), std::make_tuple(UITEM_LINGERINGPOTION, std::make_pair(441, 0))},
	{ std::make_pair("shield", 0), std::make_tuple(UITEM_SHIELD, std::make_pair(-1, -1))},
	{ std::make_pair("elytra", 0), std::make_tuple(UITEM_ELYTRA, std::make_pair(444, 0))},
	{ std::make_pair("spruce_boat", 0), std::make_tuple(UITEM_SPRUCEBOAT, std::make_pair(333, 1))},
	{ std::make_pair("birch_boat", 0), std::make_tuple(UITEM_BIRCHBOAT, std::make_pair(333, 2))},
	{ std::make_pair("jungle_boat", 0), std::make_tuple(UITEM_JUNGLEBOAT, std::make_pair(333, 3))},
	{ std::make_pair("acacia_boat", 0), std::make_tuple(UITEM_ACACIABOAT, std::make_pair(333, 4))},
	{ std::make_pair("dark_oak_boat", 0), std::make_tuple(UITEM_DARKOAKBOAT, std::make_pair(333, 5))},
	{ std::make_pair("totem_of_undying", 0), std::make_tuple(UITEM_TOTEMOFUNDYING, std::make_pair(-1, -1))},
	{ std::make_pair("shulker_shell", 0), std::make_tuple(UITEM_SHULKERSHELL, std::make_pair(445, 0))},
	{ std::make_pair("iron_nugget", 0), std::make_tuple(UITEM_IRONNUGGET, std::make_pair(-1, -1))},
	{ std::make_pair("record_13", 0), std::make_tuple(UITEM_RECORD13, std::make_pair(-1, -1))},
	{ std::make_pair("record_cat", 0), std::make_tuple(UITEM_RECORDCAT, std::make_pair(-1, -1))},
	{ std::make_pair("record_blocks", 0), std::make_tuple(UITEM_RECORDBLOCKS, std::make_pair(-1, -1))},
	{ std::make_pair("record_chirp", 0), std::make_tuple(UITEM_RECORDCHIRP, std::make_pair(-1, -1))},
	{ std::make_pair("record_far", 0), std::make_tuple(UITEM_RECORDFAR, std::make_pair(-1, -1))},
	{ std::make_pair("record_mall", 0), std::make_tuple(UITEM_RECORDMALL, std::make_pair(-1, -1))},
	{ std::make_pair("record_mellohi", 0), std::make_tuple(UITEM_RECORDMELLOHI, std::make_pair(-1, -1))},
	{ std::make_pair("record_stal", 0), std::make_tuple(UITEM_RECORDSTAL, std::make_pair(-1, -1))},
	{ std::make_pair("record_strad", 0), std::make_tuple(UITEM_RECORDSTRAD, std::make_pair(-1, -1))},
	{ std::make_pair("record_ward", 0), std::make_tuple(UITEM_RECORDWARD, std::make_pair(-1, -1))},
	{ std::make_pair("record_11", 0), std::make_tuple(UITEM_RECORD11, std::make_pair(-1, -1))},
	{ std::make_pair("record_wait", 0), std::make_tuple(UITEM_RECORDWAIT, std::make_pair(-1, -1))},
};
const std::map<std::pair<short, short>, std::tuple<UItemID, std::pair<std::string, short>>> PocketItemM = {
	{ std::make_pair(256, 0), std::make_tuple(UITEM_IRONSHOVEL, std::make_pair("iron_shovel", 0))},
	{ std::make_pair(257, 0), std::make_tuple(UITEM_IRONPICKAXE, std::make_pair("iron_pickaxe", 0))},
	{ std::make_pair(258, 0), std::make_tuple(UITEM_IRONAXE, std::make_pair("iron_axe", 0))},
	{ std::make_pair(259, 0), std::make_tuple(UITEM_FLINTANDSTEEL, std::make_pair("flint_and_steel", 0))},
	{ std::make_pair(260, 0), std::make_tuple(UITEM_APPLE, std::make_pair("apple", 0))},
	{ std::make_pair(261, 0), std::make_tuple(UITEM_BOW, std::make_pair("bow", 0))},
	{ std::make_pair(262, 0), std::make_tuple(UITEM_ARROW, std::make_pair("arrow", 0))},
	{ std::make_pair(263, 0), std::make_tuple(UITEM_COAL_COAL, std::make_pair("coal", 0))},
	{ std::make_pair(263, 1), std::make_tuple(UITEM_COAL_CHARCOAL, std::make_pair("coal", 1))},
	{ std::make_pair(264, 0), std::make_tuple(UITEM_DIAMOND, std::make_pair("diamond", 0))},
	{ std::make_pair(265, 0), std::make_tuple(UITEM_IRONINGOT, std::make_pair("iron_ingot", 0))},
	{ std::make_pair(266, 0), std::make_tuple(UITEM_GOLDINGOT, std::make_pair("gold_ingot", 0))},
	{ std::make_pair(267, 0), std::make_tuple(UITEM_IRONSWORD, std::make_pair("iron_sword", 0))},
	{ std::make_pair(268, 0), std::make_tuple(UITEM_WOODENSWORD, std::make_pair("wooden_sword", 0))},
	{ std::make_pair(269, 0), std::make_tuple(UITEM_WOODENSHOVEL, std::make_pair("wooden_shovel", 0))},
	{ std::make_pair(270, 0), std::make_tuple(UITEM_WOODENPICKAXE, std::make_pair("wooden_pickaxe", 0))},
	{ std::make_pair(271, 0), std::make_tuple(UITEM_WOODENAXE, std::make_pair("wooden_axe", 0))},
	{ std::make_pair(272, 0), std::make_tuple(UITEM_STONESWORD, std::make_pair("stone_sword", 0))},
	{ std::make_pair(273, 0), std::make_tuple(UITEM_STONESHOVEL, std::make_pair("stone_shovel", 0))},
	{ std::make_pair(274, 0), std::make_tuple(UITEM_STONEPICKAXE, std::make_pair("stone_pickaxe", 0))},
	{ std::make_pair(275, 0), std::make_tuple(UITEM_STONEAXE, std::make_pair("stone_axe", 0))},
	{ std::make_pair(276, 0), std::make_tuple(UITEM_DIAMONDSWORD, std::make_pair("diamond_sword", 0))},
	{ std::make_pair(277, 0), std::make_tuple(UITEM_DIAMONDSHOVEL, std::make_pair("diamond_shovel", 0))},
	{ std::make_pair(278, 0), std::make_tuple(UITEM_DIAMONDPICKAXE, std::make_pair("diamond_pickaxe", 0))},
	{ std::make_pair(279, 0), std::make_tuple(UITEM_DIAMONDAXE, std::make_pair("diamond_axe", 0))},
	{ std::make_pair(280, 0), std::make_tuple(UITEM_STICK, std::make_pair("stick", 0))},
	{ std::make_pair(281, 0), std::make_tuple(UITEM_BOWL, std::make_pair("bowl", 0))},
	{ std::make_pair(282, 0), std::make_tuple(UITEM_MUSHROOMSTEW, std::make_pair("mushroom_stew", 0))},
	{ std::make_pair(283, 0), std::make_tuple(UITEM_GOLDENSWORD, std::make_pair("golden_sword", 0))},
	{ std::make_pair(284, 0), std::make_tuple(UITEM_GOLDENSHOVEL, std::make_pair("golden_shovel", 0))},
	{ std::make_pair(285, 0), std::make_tuple(UITEM_GOLDENPICKAXE, std::make_pair("golden_pickaxe", 0))},
	{ std::make_pair(286, 0), std::make_tuple(UITEM_GOLDENAXE, std::make_pair("golden_axe", 0))},
	{ std::make_pair(287, 0), std::make_tuple(UITEM_STRING, std::make_pair("string", 0))},
	{ std::make_pair(288, 0), std::make_tuple(UITEM_FEATHER, std::make_pair("feather", 0))},
	{ std::make_pair(289, 0), std::make_tuple(UITEM_GUNPOWDER, std::make_pair("gunpowder", 0))},
	{ std::make_pair(290, 0), std::make_tuple(UITEM_WOODENHOE, std::make_pair("wooden_hoe", 0))},
	{ std::make_pair(291, 0), std::make_tuple(UITEM_STONEHOE, std::make_pair("stone_hoe", 0))},
	{ std::make_pair(292, 0), std::make_tuple(UITEM_IRONHOE, std::make_pair("iron_hoe", 0))},
	{ std::make_pair(293, 0), std::make_tuple(UITEM_DIAMONDHOE, std::make_pair("diamond_hoe", 0))},
	{ std::make_pair(294, 0), std::make_tuple(UITEM_GOLDENHOE, std::make_pair("golden_hoe", 0))},
	{ std::make_pair(295, 0), std::make_tuple(UITEM_WHEATSEEDS, std::make_pair("wheat_seeds", 0))},
	{ std::make_pair(296, 0), std::make_tuple(UITEM_WHEAT, std::make_pair("wheat", 0))},
	{ std::make_pair(297, 0), std::make_tuple(UITEM_BREAD, std::make_pair("bread", 0))},
	{ std::make_pair(298, 0), std::make_tuple(UITEM_LEATHERHELMET, std::make_pair("leather_helmet", 0))},
	{ std::make_pair(299, 0), std::make_tuple(UITEM_LEATHERCHESTPLATE, std::make_pair("leather_chestplate", 0))},
	{ std::make_pair(300, 0), std::make_tuple(UITEM_LEATHERLEGGINGS, std::make_pair("leather_leggings", 0))},
	{ std::make_pair(301, 0), std::make_tuple(UITEM_LEATHERBOOTS, std::make_pair("leather_boots", 0))},
	{ std::make_pair(302, 0), std::make_tuple(UITEM_CHAINMAILHELMET, std::make_pair("chainmail_helmet", 0))},
	{ std::make_pair(303, 0), std::make_tuple(UITEM_CHAINMAILCHESTPLATE, std::make_pair("chainmail_chestplate", 0))},
	{ std::make_pair(304, 0), std::make_tuple(UITEM_CHAINMAILLEGGINGS, std::make_pair("chainmail_leggings", 0))},
	{ std::make_pair(305, 0), std::make_tuple(UITEM_CHAINMAILBOOTS, std::make_pair("chainmail_boots", 0))},
	{ std::make_pair(306, 0), std::make_tuple(UITEM_IRONHELMET, std::make_pair("iron_helmet", 0))},
	{ std::make_pair(307, 0), std::make_tuple(UITEM_IRONCHESTPLATE, std::make_pair("iron_chestplate", 0))},
	{ std::make_pair(308, 0), std::make_tuple(UITEM_IRONLEGGINGS, std::make_pair("iron_leggings", 0))},
	{ std::make_pair(309, 0), std::make_tuple(UITEM_IRONBOOTS, std::make_pair("iron_boots", 0))},
	{ std::make_pair(310, 0), std::make_tuple(UITEM_DIAMONDHELMET, std::make_pair("diamond_helmet", 0))},
	{ std::make_pair(311, 0), std::make_tuple(UITEM_DIAMONDCHESTPLATE, std::make_pair("diamond_chestplate", 0))},
	{ std::make_pair(312, 0), std::make_tuple(UITEM_DIAMONDLEGGINGS, std::make_pair("diamond_leggings", 0))},
	{ std::make_pair(313, 0), std::make_tuple(UITEM_DIAMONDBOOTS, std::make_pair("diamond_boots", 0))},
	{ std::make_pair(314, 0), std::make_tuple(UITEM_GOLDENHELMET, std::make_pair("golden_helmet", 0))},
	{ std::make_pair(315, 0), std::make_tuple(UITEM_GOLDENCHESTPLATE, std::make_pair("golden_chestplate", 0))},
	{ std::make_pair(316, 0), std::make_tuple(UITEM_GOLDENLEGGINGS, std::make_pair("golden_leggings", 0))},
	{ std::make_pair(317, 0), std::make_tuple(UITEM_GOLDENBOOTS, std::make_pair("golden_boots", 0))},
	{ std::make_pair(318, 0), std::make_tuple(UITEM_FLINT, std::make_pair("flint", 0))},
	{ std::make_pair(319, 0), std::make_tuple(UITEM_PORKCHOP, std::make_pair("porkchop", 0))},
	{ std::make_pair(320, 0), std::make_tuple(UITEM_COOKEDPORKCHOP, std::make_pair("cooked_porkchop", 0))},
	{ std::make_pair(321, 0), std::make_tuple(UITEM_PAINTING, std::make_pair("painting", 0))},
	{ std::make_pair(322, 0), std::make_tuple(UITEM_GOLDENAPPLE_NORMAL, std::make_pair("golden_apple", 0))},
	{ std::make_pair(466, 0), std::make_tuple(UITEM_GOLDENAPPLE_ENCHANTED, std::make_pair("golden_apple", 1))},
	{ std::make_pair(323, 0), std::make_tuple(UITEM_SIGN, std::make_pair("sign", 0))},
	{ std::make_pair(324, 0), std::make_tuple(UITEM_WOODENDOOR, std::make_pair("wooden_door", 0))},
	{ std::make_pair(325, 0), std::make_tuple(UITEM_BUCKET, std::make_pair("bucket", 0))},
	{ std::make_pair(325, 8), std::make_tuple(UITEM_WATERBUCKET, std::make_pair("water_bucket", 0))},
	{ std::make_pair(325, 10), std::make_tuple(UITEM_LAVABUCKET, std::make_pair("lava_bucket", 0))},
	{ std::make_pair(328, 0), std::make_tuple(UITEM_MINECART, std::make_pair("minecart", 0))},
	{ std::make_pair(329, 0), std::make_tuple(UITEM_SADDLE, std::make_pair("saddle", 0))},
	{ std::make_pair(330, 0), std::make_tuple(UITEM_IRONDOOR, std::make_pair("iron_door", 0))},
	{ std::make_pair(331, 0), std::make_tuple(UITEM_REDSTONE, std::make_pair("redstone", 0))},
	{ std::make_pair(332, 0), std::make_tuple(UITEM_SNOWBALL, std::make_pair("snowball", 0))},
	{ std::make_pair(333, 0), std::make_tuple(UITEM_BOAT, std::make_pair("boat", 0))},
	{ std::make_pair(334, 0), std::make_tuple(UITEM_LEATHER, std::make_pair("leather", 0))},
	{ std::make_pair(325, 1), std::make_tuple(UITEM_MILKBUCKET, std::make_pair("milk_bucket", 0))},
	{ std::make_pair(336, 0), std::make_tuple(UITEM_BRICK, std::make_pair("brick", 0))},
	{ std::make_pair(337, 0), std::make_tuple(UITEM_CLAYBALL, std::make_pair("clay_ball", 0))},
	{ std::make_pair(338, 0), std::make_tuple(UITEM_REEDS, std::make_pair("reeds", 0))},
	{ std::make_pair(339, 0), std::make_tuple(UITEM_PAPER, std::make_pair("paper", 0))},
	{ std::make_pair(340, 0), std::make_tuple(UITEM_BOOK, std::make_pair("book", 0))},
	{ std::make_pair(341, 0), std::make_tuple(UITEM_SLIMEBALL, std::make_pair("slime_ball", 0))},
	{ std::make_pair(342, 0), std::make_tuple(UITEM_CHESTMINECART, std::make_pair("chest_minecart", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_FURNACEMINECART, std::make_pair("furnace_minecart", 0))},
	{ std::make_pair(344, 0), std::make_tuple(UITEM_EGG, std::make_pair("egg", 0))},
	{ std::make_pair(345, 0), std::make_tuple(UITEM_COMPASS, std::make_pair("compass", 0))},
	{ std::make_pair(346, 0), std::make_tuple(UITEM_FISHINGROD, std::make_pair("fishing_rod", 0))},
	{ std::make_pair(347, 0), std::make_tuple(UITEM_CLOCK, std::make_pair("clock", 0))},
	{ std::make_pair(348, 0), std::make_tuple(UITEM_GLOWSTONEDUST, std::make_pair("glowstone_dust", 0))},
	{ std::make_pair(349, 0), std::make_tuple(UITEM_FISH_RAWFISH, std::make_pair("fish", 0))},
	{ std::make_pair(460, 0), std::make_tuple(UITEM_FISH_RAWSALMON, std::make_pair("fish", 1))},
	{ std::make_pair(461, 0), std::make_tuple(UITEM_FISH_CLOWNFISH, std::make_pair("fish", 2))},
	{ std::make_pair(462, 0), std::make_tuple(UITEM_FISH_PUFFERFISH, std::make_pair("fish", 3))},
	{ std::make_pair(350, 0), std::make_tuple(UITEM_COOKEDFISH_COOKEDFISH, std::make_pair("cooked_fish", 0))},
	{ std::make_pair(463, 0), std::make_tuple(UITEM_COOKEDFISH_COOKEDSALMON, std::make_pair("cooked_fish", 1))},
	{ std::make_pair(351, 0), std::make_tuple(UITEM_DYE_BLACK, std::make_pair("dye", 0))},
	{ std::make_pair(351, 1), std::make_tuple(UITEM_DYE_RED, std::make_pair("dye", 1))},
	{ std::make_pair(351, 2), std::make_tuple(UITEM_DYE_GREEN, std::make_pair("dye", 2))},
	{ std::make_pair(351, 3), std::make_tuple(UITEM_DYE_BROWN, std::make_pair("dye", 3))},
	{ std::make_pair(351, 4), std::make_tuple(UITEM_DYE_BLUE, std::make_pair("dye", 4))},
	{ std::make_pair(351, 5), std::make_tuple(UITEM_DYE_PURPLE, std::make_pair("dye", 5))},
	{ std::make_pair(351, 6), std::make_tuple(UITEM_DYE_CYAN, std::make_pair("dye", 6))},
	{ std::make_pair(351, 7), std::make_tuple(UITEM_DYE_SILVER, std::make_pair("dye", 7))},
	{ std::make_pair(351, 8), std::make_tuple(UITEM_DYE_GRAY, std::make_pair("dye", 8))},
	{ std::make_pair(351, 9), std::make_tuple(UITEM_DYE_PINK, std::make_pair("dye", 9))},
	{ std::make_pair(351, 10), std::make_tuple(UITEM_DYE_LIME, std::make_pair("dye", 10))},
	{ std::make_pair(351, 11), std::make_tuple(UITEM_DYE_YELLOW, std::make_pair("dye", 11))},
	{ std::make_pair(351, 12), std::make_tuple(UITEM_DYE_LIGHTBLUE, std::make_pair("dye", 12))},
	{ std::make_pair(351, 13), std::make_tuple(UITEM_DYE_MAGENTA, std::make_pair("dye", 13))},
	{ std::make_pair(351, 14), std::make_tuple(UITEM_DYE_ORANGE, std::make_pair("dye", 14))},
	{ std::make_pair(351, 15), std::make_tuple(UITEM_DYE_WHITE, std::make_pair("dye", 15))},
	{ std::make_pair(352, 0), std::make_tuple(UITEM_BONE, std::make_pair("bone", 0))},
	{ std::make_pair(353, 0), std::make_tuple(UITEM_SUGAR, std::make_pair("sugar", 0))},
	{ std::make_pair(354, 0), std::make_tuple(UITEM_CAKE, std::make_pair("cake", 0))},
	{ std::make_pair(355, 0), std::make_tuple(UITEM_BED, std::make_pair("bed", 0))},
	{ std::make_pair(356, 0), std::make_tuple(UITEM_REPEATER, std::make_pair("repeater", 0))},
	{ std::make_pair(357, 0), std::make_tuple(UITEM_COOKIE, std::make_pair("cookie", 0))},
	{ std::make_pair(358, 0), std::make_tuple(UITEM_FILLEDMAP, std::make_pair("filled_map", 0))},
	{ std::make_pair(359, 0), std::make_tuple(UITEM_SHEARS, std::make_pair("shears", 0))},
	{ std::make_pair(360, 0), std::make_tuple(UITEM_MELON, std::make_pair("melon", 0))},
	{ std::make_pair(361, 0), std::make_tuple(UITEM_PUMPKINSEEDS, std::make_pair("pumpkin_seeds", 0))},
	{ std::make_pair(362, 0), std::make_tuple(UITEM_MELONSEEDS, std::make_pair("melon_seeds", 0))},
	{ std::make_pair(363, 0), std::make_tuple(UITEM_BEEF, std::make_pair("beef", 0))},
	{ std::make_pair(364, 0), std::make_tuple(UITEM_COOKEDBEEF, std::make_pair("cooked_beef", 0))},
	{ std::make_pair(365, 0), std::make_tuple(UITEM_CHICKEN, std::make_pair("chicken", 0))},
	{ std::make_pair(366, 0), std::make_tuple(UITEM_COOKEDCHICKEN, std::make_pair("cooked_chicken", 0))},
	{ std::make_pair(367, 0), std::make_tuple(UITEM_ROTTENFLESH, std::make_pair("rotten_flesh", 0))},
	{ std::make_pair(368, 0), std::make_tuple(UITEM_ENDERPEARL, std::make_pair("ender_pearl", 0))},
	{ std::make_pair(369, 0), std::make_tuple(UITEM_BLAZEROD, std::make_pair("blaze_rod", 0))},
	{ std::make_pair(370, 0), std::make_tuple(UITEM_GHASTTEAR, std::make_pair("ghast_tear", 0))},
	{ std::make_pair(371, 0), std::make_tuple(UITEM_GOLDNUGGET, std::make_pair("gold_nugget", 0))},
	{ std::make_pair(372, 0), std::make_tuple(UITEM_NETHERWART, std::make_pair("nether_wart", 0))},
	{ std::make_pair(373, 0), std::make_tuple(UITEM_POTION, std::make_pair("potion", 0))},
	{ std::make_pair(374, 0), std::make_tuple(UITEM_GLASSBOTTLE, std::make_pair("glass_bottle", 0))},
	{ std::make_pair(375, 0), std::make_tuple(UITEM_SPIDEREYE, std::make_pair("spider_eye", 0))},
	{ std::make_pair(376, 0), std::make_tuple(UITEM_FERMENTEDSPIDEREYE, std::make_pair("fermented_spider_eye", 0))},
	{ std::make_pair(377, 0), std::make_tuple(UITEM_BLAZEPOWDER, std::make_pair("blaze_powder", 0))},
	{ std::make_pair(378, 0), std::make_tuple(UITEM_MAGMACREAM, std::make_pair("magma_cream", 0))},
	{ std::make_pair(379, 0), std::make_tuple(UITEM_BREWINGSTAND, std::make_pair("brewing_stand", 0))},
	{ std::make_pair(380, 0), std::make_tuple(UITEM_CAULDRON, std::make_pair("cauldron", 0))},
	{ std::make_pair(381, 0), std::make_tuple(UITEM_ENDEREYE, std::make_pair("ender_eye", 0))},
	{ std::make_pair(382, 0), std::make_tuple(UITEM_SPECKLEDMELON, std::make_pair("speckled_melon", 0))},
	{ std::make_pair(383, 0), std::make_tuple(UITEM_SPAWNEGG, std::make_pair("spawn_egg", 0))},
	{ std::make_pair(384, 0), std::make_tuple(UITEM_EXPERIENCEBOTTLE, std::make_pair("experience_bottle", 0))},
	{ std::make_pair(385, 0), std::make_tuple(UITEM_FIRECHARGE, std::make_pair("fire_charge", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_WRITABLEBOOK, std::make_pair("writable_book", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_WRITTENBOOK, std::make_pair("written_book", 0))},
	{ std::make_pair(388, 0), std::make_tuple(UITEM_EMERALD, std::make_pair("emerald", 0))},
	{ std::make_pair(389, 0), std::make_tuple(UITEM_ITEMFRAME, std::make_pair("item_frame", 0))},
	{ std::make_pair(390, 0), std::make_tuple(UITEM_FLOWERPOT, std::make_pair("flower_pot", 0))},
	{ std::make_pair(391, 0), std::make_tuple(UITEM_CARROT, std::make_pair("carrot", 0))},
	{ std::make_pair(392, 0), std::make_tuple(UITEM_POTATO, std::make_pair("potato", 0))},
	{ std::make_pair(393, 0), std::make_tuple(UITEM_BAKEDPOTATO, std::make_pair("baked_potato", 0))},
	{ std::make_pair(394, 0), std::make_tuple(UITEM_POISONOUSPOTATO, std::make_pair("poisonous_potato", 0))},
	{ std::make_pair(395, 0), std::make_tuple(UITEM_MAP, std::make_pair("map", 0))},
	{ std::make_pair(396, 0), std::make_tuple(UITEM_GOLDENCARROT, std::make_pair("golden_carrot", 0))},
	{ std::make_pair(397, 0), std::make_tuple(UITEM_SKULL_SKELETON, std::make_pair("skull", 0))},
	{ std::make_pair(397, 1), std::make_tuple(UITEM_SKULL_WITHER, std::make_pair("skull", 1))},
	{ std::make_pair(397, 2), std::make_tuple(UITEM_SKULL_ZOMBIE, std::make_pair("skull", 2))},
	{ std::make_pair(397, 3), std::make_tuple(UITEM_SKULL_PLAYER, std::make_pair("skull", 3))},
	{ std::make_pair(397, 4), std::make_tuple(UITEM_SKULL_CREEPER, std::make_pair("skull", 4))},
	{ std::make_pair(398, 0), std::make_tuple(UITEM_CARROTONASTICK, std::make_pair("carrot_on_a_stick", 0))},
	{ std::make_pair(399, 0), std::make_tuple(UITEM_NETHERSTAR, std::make_pair("nether_star", 0))},
	{ std::make_pair(400, 0), std::make_tuple(UITEM_PUMPKINPIE, std::make_pair("pumpkin_pie", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_FIREWORKS, std::make_pair("fireworks", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_FIREWORKCHARGE, std::make_pair("firework_charge", 0))},
	{ std::make_pair(403, 0), std::make_tuple(UITEM_ENCHANTEDBOOK, std::make_pair("enchanted_book", 0))},
	{ std::make_pair(404, 0), std::make_tuple(UITEM_COMPARATOR, std::make_pair("comparator", 0))},
	{ std::make_pair(405, 0), std::make_tuple(UITEM_NETHERBRICK, std::make_pair("netherbrick", 0))},
	{ std::make_pair(406, 0), std::make_tuple(UITEM_QUARTZ, std::make_pair("quartz", 0))},
	{ std::make_pair(407, 0), std::make_tuple(UITEM_TNTMINECART, std::make_pair("tnt_minecart", 0))},
	{ std::make_pair(408, 0), std::make_tuple(UITEM_HOPPERMINECART, std::make_pair("hopper_minecart", 0))},
	{ std::make_pair(409, 0), std::make_tuple(UITEM_PRISMARINESHARD, std::make_pair("prismarine_shard", 0))},
	{ std::make_pair(422, 0), std::make_tuple(UITEM_PRISMARINECRYSTALS, std::make_pair("prismarine_crystals", 0))},
	{ std::make_pair(411, 0), std::make_tuple(UITEM_RABBIT, std::make_pair("rabbit", 0))},
	{ std::make_pair(412, 0), std::make_tuple(UITEM_COOKEDRABBIT, std::make_pair("cooked_rabbit", 0))},
	{ std::make_pair(413, 0), std::make_tuple(UITEM_RABBITSTEW, std::make_pair("rabbit_stew", 0))},
	{ std::make_pair(414, 0), std::make_tuple(UITEM_RABBITFOOT, std::make_pair("rabbit_foot", 0))},
	{ std::make_pair(415, 0), std::make_tuple(UITEM_RABBITHIDE, std::make_pair("rabbit_hide", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_ARMORSTAND, std::make_pair("armor_stand", 0))},
	{ std::make_pair(417, 0), std::make_tuple(UITEM_IRONHORSEARMOR, std::make_pair("iron_horse_armor", 0))},
	{ std::make_pair(418, 0), std::make_tuple(UITEM_GOLDENHORSEARMOR, std::make_pair("golden_horse_armor", 0))},
	{ std::make_pair(419, 0), std::make_tuple(UITEM_DIAMONDHORSEARMOR, std::make_pair("diamond_horse_armor", 0))},
	{ std::make_pair(420, 0), std::make_tuple(UITEM_LEAD, std::make_pair("lead", 0))},
	{ std::make_pair(421, 0), std::make_tuple(UITEM_NAMETAG, std::make_pair("name_tag", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_COMMANDBLOCKMINECART, std::make_pair("command_block_minecart", 0))},
	{ std::make_pair(423, 0), std::make_tuple(UITEM_MUTTON, std::make_pair("mutton", 0))},
	{ std::make_pair(424, 0), std::make_tuple(UITEM_COOKEDMUTTON, std::make_pair("cooked_mutton", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_BANNER, std::make_pair("banner", 0))},
	{ std::make_pair(426, 0), std::make_tuple(UITEM_ENDCRYSTAL, std::make_pair("end_crystal", 0))},
	{ std::make_pair(427, 0), std::make_tuple(UITEM_SPRUCEDOOR, std::make_pair("spruce_door", 0))},
	{ std::make_pair(428, 0), std::make_tuple(UITEM_BIRCHDOOR, std::make_pair("birch_door", 0))},
	{ std::make_pair(429, 0), std::make_tuple(UITEM_JUNGLEDOOR, std::make_pair("jungle_door", 0))},
	{ std::make_pair(430, 0), std::make_tuple(UITEM_ACACIADOOR, std::make_pair("acacia_door", 0))},
	{ std::make_pair(431, 0), std::make_tuple(UITEM_DARKOAKDOOR, std::make_pair("dark_oak_door", 0))},
	{ std::make_pair(432, 0), std::make_tuple(UITEM_CHORUSFRUIT, std::make_pair("chorus_fruit", 0))},
	{ std::make_pair(433, 0), std::make_tuple(UITEM_CHORUSFRUITPOPPED, std::make_pair("chorus_fruit_popped", 0))},
	{ std::make_pair(457, 0), std::make_tuple(UITEM_BEETROOT, std::make_pair("beetroot", 0))},
	{ std::make_pair(458, 0), std::make_tuple(UITEM_BEETROOTSEEDS, std::make_pair("beetroot_seeds", 0))},
	{ std::make_pair(459, 0), std::make_tuple(UITEM_BEETROOTSOUP, std::make_pair("beetroot_soup", 0))},
	{ std::make_pair(437, 0), std::make_tuple(UITEM_DRAGONBREATH, std::make_pair("dragon_breath", 0))},
	{ std::make_pair(438, 0), std::make_tuple(UITEM_SPLASHPOTION, std::make_pair("splash_potion", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_SPECTRALARROW, std::make_pair("spectral_arrow", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_TIPPEDARROW, std::make_pair("tipped_arrow", 0))},
	{ std::make_pair(441, 0), std::make_tuple(UITEM_LINGERINGPOTION, std::make_pair("lingering_potion", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_SHIELD, std::make_pair("shield", 0))},
	{ std::make_pair(444, 0), std::make_tuple(UITEM_ELYTRA, std::make_pair("elytra", 0))},
	{ std::make_pair(333, 1), std::make_tuple(UITEM_SPRUCEBOAT, std::make_pair("spruce_boat", 0))},
	{ std::make_pair(333, 2), std::make_tuple(UITEM_BIRCHBOAT, std::make_pair("birch_boat", 0))},
	{ std::make_pair(333, 3), std::make_tuple(UITEM_JUNGLEBOAT, std::make_pair("jungle_boat", 0))},
	{ std::make_pair(333, 4), std::make_tuple(UITEM_ACACIABOAT, std::make_pair("acacia_boat", 0))},
	{ std::make_pair(333, 5), std::make_tuple(UITEM_DARKOAKBOAT, std::make_pair("dark_oak_boat", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_TOTEMOFUNDYING, std::make_pair("totem_of_undying", 0))},
	{ std::make_pair(445, 0), std::make_tuple(UITEM_SHULKERSHELL, std::make_pair("shulker_shell", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_IRONNUGGET, std::make_pair("iron_nugget", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_RECORD13, std::make_pair("record_13", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_RECORDCAT, std::make_pair("record_cat", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_RECORDBLOCKS, std::make_pair("record_blocks", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_RECORDCHIRP, std::make_pair("record_chirp", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_RECORDFAR, std::make_pair("record_far", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_RECORDMALL, std::make_pair("record_mall", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_RECORDMELLOHI, std::make_pair("record_mellohi", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_RECORDSTAL, std::make_pair("record_stal", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_RECORDSTRAD, std::make_pair("record_strad", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_RECORDWARD, std::make_pair("record_ward", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_RECORD11, std::make_pair("record_11", 0))},
	{ std::make_pair(-1, -1), std::make_tuple(UITEM_RECORDWAIT, std::make_pair("record_wait", 0))},
};
} // NS Andromeda
