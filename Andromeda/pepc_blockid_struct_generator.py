#BEGIN_ENUM = "enum UBlockIDs : short {\n"
#END_ENUM = "}"

print "#pragma once"
print ""
print '#include <map>'
print '#include "Types.h"'
print "\n// This file was automatically generated\n// To generate, use `python blocks.py`\n"
print "namespace Andromeda {"
BEGIN_MAP = "const std::map<UBlockName, short> UBlockIDs = {"
DATA_LINE = '     {Blocks::%s, %d},'
END = "};"
BEGIN_CHAR_ARR = "const char *%s[%d] = {"

csv_file_lines = []
const_arr = []
uitem_map_arr = []
diff_ids_arr = []


with open('C:/users/extreme/desktop/development/andromeda/PCPE BlockIDs.csv') as f:
    csv_file_lines = f.read().splitlines()

for line in csv_file_lines:
    if line[0] == "#":
        continue

    line_data = line.split(",")
    pc_block_name = line_data[0]
    pe_block_name = line_data[1]
    pc_block_id   = line_data[2]
    pe_block_id   = line_data[3]

    enum_name = ' '.join(pc_block_name.replace("_", ' ').split()).title().replace(" ", "")

    const_arr.append('const UBlockName %s = "%s";' % (enum_name, pc_block_name))

    ublock_id = int(pc_block_id) << 8 | int(pe_block_id)

    uitem_map_arr.append(DATA_LINE % (enum_name, int(ublock_id)))

    if pc_block_id != pe_block_id:
        diff_ids_arr.append([pc_block_name, pc_block_id, pe_block_id])

    #print enum_name
    
    #print pc_block_name, pe_block_name, pc_block_id, pe_block_id

print "namespace Blocks {"
for const_i in const_arr:
    print const_i
print "\n} // NS Andromeda::Blocks \n"

print BEGIN_MAP
for i in uitem_map_arr:
    print i
print END

print BEGIN_CHAR_ARR % ("pocketMap", 256)
for number in range(0, 256):
    found = False

    for line in csv_file_lines:
        if line[0] == "#":
            continue
        line_data = line.split(",")
        pc_block_name = line_data[0]
        pe_block_id  = int(line_data[3])

        if pe_block_id == number and pe_block_id != 255:
            print '"%s", ' % pc_block_name,
            found = True
            break
        
    if not found:
        print "nullptr, ",
print "\n", END, "\n"

print BEGIN_CHAR_ARR % ("javaMap", 256)
for number in range(0, 256):
    found = False

    for line in csv_file_lines:
        if line[0] == "#":
            continue
        line_data = line.split(",")
        pc_block_name = line_data[0]
        pc_block_id   = int(line_data[2])

        if pc_block_id == number:
            print '"%s", ' % pc_block_name,
            found = True
            break
        
    if not found:
        print "nullptr, ",
print "\n", END
print ""
pc_diff_str = "unsigned char pcDiffs[] {"
pe_diff_str = "unsigned char peDiffs[] {"
for diff_id in diff_ids_arr:
    if diff_id[2] != "255":
        pe_diff_str += str(diff_id[2]) + ","
    pc_diff_str += str(diff_id[1]) + ","
pc_diff_str += "};"
pe_diff_str += "};"
print pc_diff_str
print pe_diff_str
print ""
print "} // NS Andromeda"