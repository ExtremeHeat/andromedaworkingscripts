0  AIR_ID new  Blocks::BlockAir AIR
1  "stone" new  Blocks::BlockStone ROCK
2  "grass" new  Blocks::BlockGrass GRASS
3  "dirt" new  Blocks::BlockDirt GROUND
6  "sapling" new  Blocks::BlockSapling PLANTS
7  "bedrock" new  Blocks::BlockEmptyDrops ROCK
8  "flowing_water" new  Blocks::BlockDynamicLiquid WATER
9  "water" new  Blocks::BlockStaticLiquid WATER
10  "flowing_lava" new  Blocks::BlockDynamicLiquid LAVA
11  "lava" new  Blocks::BlockStaticLiquid LAVA
12  "sand" new  Blocks::BlockSand SAND
13  "gravel" new  Blocks::BlockGravel SAND
14  "gold_ore" new  Blocks::BlockOre ROCK
15  "iron_ore" new  Blocks::BlockOre ROCK
16  "coal_ore" new  Blocks::BlockOre ROCK
17  "log" new  Blocks::BlockOldLog WOOD
18  "leaves" new  Blocks::BlockOldLeaf LEAVES
19  "sponge" new  Blocks::BlockSponge SPONGE
20  "glass" new  Blocks::BlockGlass GLASS
21  "lapis_ore" new  Blocks::BlockOre ROCK
22  "lapis_block" new Block         registerBlock(22, "lapis_block", (new Block(Material.IRON, MapColor.LAPIS)).setHardness(3.0F).setResistance(5.0F).setSoundType(SoundType.STONE).setUnlocalizedName("blockLapis").setCreativeTab(CreativeTabs.BUILDING_BLOCKS));

23  "dispenser" new  Blocks::BlockDispenser ROCK
25  "noteblock" new  Blocks::BlockNote WOOD
26  "bed" new  Blocks::BlockBed CLOTH
27  "golden_rail" new  Blocks::BlockRailPowered CIRCUITS
28  "detector_rail" new  Blocks::BlockRailDetector CIRCUITS
29  "sticky_piston" new  Blocks::BlockPistonBase PISTON
30  "web" new  Blocks::BlockWeb WEB
31  "tallgrass" new  Blocks::BlockTallGrass VINE
32  "deadbush" new  Blocks::BlockDeadBush VINE
33  "piston" new  Blocks::BlockPistonBase PISTON
34  "piston_head" new  Blocks::BlockPistonExtension PISTON
35  "wool" new  Blocks::BlockColored CLOTH
36  "piston_extension" new  Blocks::BlockPistonMoving PISTON
FAILED ON SECOND TRY BlockFlower
37  "yellow_flower" new  Blocks::BlockYellowFlower 
FAILED ON SECOND TRY BlockFlower
38  "red_flower" new  Blocks::BlockRedFlower 
41  "gold_block" new Block         registerBlock(41, "gold_block", (new Block(Material.IRON, MapColor.GOLD)).setHardness(3.0F).setResistance(10.0F).setSoundType(SoundType.METAL).setUnlocalizedName("blockGold").setCreativeTab(CreativeTabs.BUILDING_BLOCKS));

42  "iron_block" new Block         registerBlock(42, "iron_block", (new Block(Material.IRON, MapColor.IRON)).setHardness(5.0F).setResistance(10.0F).setSoundType(SoundType.METAL).setUnlocalizedName("blockIron").setCreativeTab(CreativeTabs.BUILDING_BLOCKS));

43  "double_stone_slab" new  Blocks::BlockDoubleStoneSlab ROCK
44  "stone_slab" new  Blocks::BlockHalfStoneSlab ROCK
46  "tnt" new  Blocks::BlockTNT TNT
47  "bookshelf" new  Blocks::BlockBookshelf WOOD
48  "mossy_cobblestone" new Block         registerBlock(48, "mossy_cobblestone", (new Block(Material.ROCK)).setHardness(2.0F).setResistance(10.0F).setSoundType(SoundType.STONE).setUnlocalizedName("stoneMoss").setCreativeTab(CreativeTabs.BUILDING_BLOCKS));

49  "obsidian" new  Blocks::BlockObsidian ROCK
50  "torch" new  Blocks::BlockTorch CIRCUITS
51  "fire" new  Blocks::BlockFire FIRE
52  "mob_spawner" new  Blocks::BlockMobSpawner ROCK
53  "oak_stairs" new  Blocks::BlockStairs ROCK
54  "chest" new  Blocks::BlockChest WOOD
55  "redstone_wire" new  Blocks::BlockRedstoneWire CIRCUITS
56  "diamond_ore" new  Blocks::BlockOre ROCK
57  "diamond_block" new Block         registerBlock(57, "diamond_block", (new Block(Material.IRON, MapColor.DIAMOND)).setHardness(5.0F).setResistance(10.0F).setSoundType(SoundType.METAL).setUnlocalizedName("blockDiamond").setCreativeTab(CreativeTabs.BUILDING_BLOCKS));

58  "crafting_table" new  Blocks::BlockWorkbench WOOD
59  "wheat" new  Blocks::BlockCrops PLANTS
61  "furnace" new  Blocks::BlockFurnace ROCK
62  "lit_furnace" new  Blocks::BlockFurnace ROCK
63  "standing_sign" new  Blocks::BlockStandingSign WOOD
64  "wooden_door" new  Blocks::BlockDoor WOOD
65  "ladder" new  Blocks::BlockLadder CIRCUITS
66  "rail" new  Blocks::BlockRail CIRCUITS
67  "stone_stairs" new  Blocks::BlockStairs ROCK
68  "wall_sign" new  Blocks::BlockWallSign WOOD
69  "lever" new  Blocks::BlockLever CIRCUITS
70  "stone_pressure_plate" new  Blocks::BlockPressurePlate ROCK
71  "iron_door" new  Blocks::BlockDoor IRON
72  "wooden_pressure_plate" new  Blocks::BlockPressurePlate WOOD
73  "redstone_ore" new  Blocks::BlockRedstoneOre ROCK
74  "lit_redstone_ore" new  Blocks::BlockRedstoneOre ROCK
75  "unlit_redstone_torch" new  Blocks::BlockRedstoneTorch CIRCUITS
76  "redstone_torch" new  Blocks::BlockRedstoneTorch CIRCUITS
77  "stone_button" new  Blocks::BlockButtonStone CIRCUITS
78  "snow_layer" new  Blocks::BlockSnow SNOW
79  "ice" new  Blocks::BlockIce ICE
80  "snow" new  Blocks::BlockSnowBlock CRAFTED_SNOW
81  "cactus" new  Blocks::BlockCactus CACTUS
82  "clay" new  Blocks::BlockClay CLAY
83  "reeds" new  Blocks::BlockReed PLANTS
84  "jukebox" new  Blocks::BlockJukebox WOOD
85  "fence" new  Blocks::BlockFence WOOD
87  "netherrack" new  Blocks::BlockNetherrack ROCK
88  "soul_sand" new  Blocks::BlockSoulSand SAND
89  "glowstone" new  Blocks::BlockGlowstone GLASS
90  "portal" new  Blocks::BlockPortal PORTAL
91  "lit_pumpkin" new  Blocks::BlockPumpkin GOURD
92  "cake" new  Blocks::BlockCake CAKE
93  "unpowered_repeater" new  Blocks::BlockRedstoneRepeater CIRCUITS
94  "powered_repeater" new  Blocks::BlockRedstoneRepeater CIRCUITS
95  "stained_glass" new  Blocks::BlockStainedGlass GLASS
96  "trapdoor" new  Blocks::BlockTrapDoor WOOD
97  "monster_egg" new  Blocks::BlockSilverfish CLAY
99  "brown_mushroom_block" new  Blocks::BlockHugeMushroom WOOD
100  "red_mushroom_block" new  Blocks::BlockHugeMushroom WOOD
101  "iron_bars" new  Blocks::BlockPane IRON
102  "glass_pane" new  Blocks::BlockPane GLASS
104  "pumpkin_stem" new  Blocks::BlockStem PLANTS
105  "melon_stem" new  Blocks::BlockStem PLANTS
106  "vine" new  Blocks::BlockVine VINE
107  "fence_gate" new  Blocks::BlockFenceGate WOOD
108  "brick_stairs" new  Blocks::BlockStairs ROCK
109  "stone_brick_stairs" new  Blocks::BlockStairs ROCK
110  "mycelium" new  Blocks::BlockMycelium GRASS
111  "waterlily" new  Blocks::BlockLilyPad PLANTS
113  "nether_brick_fence" new  Blocks::BlockFence ROCK
114  "nether_brick_stairs" new  Blocks::BlockStairs ROCK
115  "nether_wart" new  Blocks::BlockNetherWart PLANTS
116  "enchanting_table" new  Blocks::BlockEnchantmentTable ROCK
117  "brewing_stand" new  Blocks::BlockBrewingStand IRON
118  "cauldron" new  Blocks::BlockCauldron IRON
119  "end_portal" new  Blocks::BlockEndPortal PORTAL
120  "end_portal_frame" new  Blocks::BlockEndPortalFrame ROCK
121  "end_stone" new Block         registerBlock(121, "end_stone", (new Block(Material.ROCK, MapColor.SAND)).setHardness(3.0F).setResistance(15.0F).setSoundType(SoundType.STONE).setUnlocalizedName("whiteStone").setCreativeTab(CreativeTabs.BUILDING_BLOCKS));

122  "dragon_egg" new  Blocks::BlockDragonEgg DRAGON_EGG
123  "redstone_lamp" new  Blocks::BlockRedstoneLight REDSTONE_LIGHT
124  "lit_redstone_lamp" new  Blocks::BlockRedstoneLight REDSTONE_LIGHT
125  "double_wooden_slab" new  Blocks::BlockDoubleWoodSlab WOOD
126  "wooden_slab" new  Blocks::BlockHalfWoodSlab WOOD
127  "cocoa" new  Blocks::BlockCocoa PLANTS
128  "sandstone_stairs" new  Blocks::BlockStairs ROCK
129  "emerald_ore" new  Blocks::BlockOre ROCK
130  "ender_chest" new  Blocks::BlockEnderChest ROCK
131  "tripwire_hook" new  Blocks::BlockTripWireHook CIRCUITS
132  "tripwire" new  Blocks::BlockTripWire CIRCUITS
133  "emerald_block" new Block         registerBlock(133, "emerald_block", (new Block(Material.IRON, MapColor.EMERALD)).setHardness(5.0F).setResistance(10.0F).setSoundType(SoundType.METAL).setUnlocalizedName("blockEmerald").setCreativeTab(CreativeTabs.BUILDING_BLOCKS));

134  "spruce_stairs" new  Blocks::BlockStairs ROCK
135  "birch_stairs" new  Blocks::BlockStairs ROCK
136  "jungle_stairs" new  Blocks::BlockStairs ROCK
137  "command_block" new  Blocks::BlockCommandBlock IRON
138  "beacon" new  Blocks::BlockBeacon GLASS
139  "cobblestone_wall" new  Blocks::BlockWall ROCK
140  "flower_pot" new  Blocks::BlockFlowerPot CIRCUITS
FAILED ON SECOND TRY BlockCrops
141  "carrots" new  Blocks::BlockCarrot 
FAILED ON SECOND TRY BlockCrops
142  "potatoes" new  Blocks::BlockPotato 
143  "wooden_button" new  Blocks::BlockButtonWood CIRCUITS
144  "skull" new  Blocks::BlockSkull CIRCUITS
145  "anvil" new  Blocks::BlockAnvil ANVIL
146  "trapped_chest" new  Blocks::BlockChest WOOD
147  "light_weighted_pressure_plate" new  Blocks::BlockPressurePlateWeighted IRON
148  "heavy_weighted_pressure_plate" new  Blocks::BlockPressurePlateWeighted IRON
149  "unpowered_comparator" new  Blocks::BlockRedstoneComparator CIRCUITS
150  "powered_comparator" new  Blocks::BlockRedstoneComparator CIRCUITS
151  "daylight_detector" new  Blocks::BlockDaylightDetector WOOD
152  "redstone_block" new  Blocks::BlockCompressedPowered IRON
153  "quartz_ore" new  Blocks::BlockOre ROCK
154  "hopper" new  Blocks::BlockHopper IRON
156  "quartz_stairs" new  Blocks::BlockStairs ROCK
157  "activator_rail" new  Blocks::BlockRailPowered CIRCUITS
158  "dropper" new  Blocks::BlockDropper ROCK
159  "stained_hardened_clay" new  Blocks::BlockColored ROCK
160  "stained_glass_pane" new  Blocks::BlockStainedGlassPane GLASS
161  "leaves2" new  Blocks::BlockNewLeaf LEAVES
162  "log2" new  Blocks::BlockNewLog WOOD
163  "acacia_stairs" new  Blocks::BlockStairs ROCK
164  "dark_oak_stairs" new  Blocks::BlockStairs ROCK
165  "slime" new  Blocks::BlockSlime CLAY
166  "barrier" new  Blocks::BlockBarrier BARRIER
167  "iron_trapdoor" new  Blocks::BlockTrapDoor IRON
168  "prismarine" new  Blocks::BlockPrismarine ROCK
169  "sea_lantern" new  Blocks::BlockSeaLantern GLASS
170  "hay_block" new  Blocks::BlockHay GRASS
171  "carpet" new  Blocks::BlockCarpet CARPET
172  "hardened_clay" new  Blocks::BlockHardenedClay ROCK
173  "coal_block" new Block         registerBlock(173, "coal_block", (new Block(Material.ROCK, MapColor.BLACK)).setHardness(5.0F).setResistance(10.0F).setSoundType(SoundType.STONE).setUnlocalizedName("blockCoal").setCreativeTab(CreativeTabs.BUILDING_BLOCKS));

174  "packed_ice" new  Blocks::BlockPackedIce PACKED_ICE
175  "double_plant" new  Blocks::BlockDoublePlant VINE
 "standing_banner" new  BlockBanner.BlockBannerStanding
 "wall_banner" new  BlockBanner.BlockBannerHanging
178  "daylight_detector_inverted" new  Blocks::BlockDaylightDetector WOOD
180  "red_sandstone_stairs" new  Blocks::BlockStairs ROCK
181  "double_stone_slab2" new  Blocks::BlockDoubleStoneSlabNew ROCK
182  "stone_slab2" new  Blocks::BlockHalfStoneSlabNew ROCK
183  "spruce_fence_gate" new  Blocks::BlockFenceGate WOOD
184  "birch_fence_gate" new  Blocks::BlockFenceGate WOOD
185  "jungle_fence_gate" new  Blocks::BlockFenceGate WOOD
186  "dark_oak_fence_gate" new  Blocks::BlockFenceGate WOOD
187  "acacia_fence_gate" new  Blocks::BlockFenceGate WOOD
188  "spruce_fence" new  Blocks::BlockFence WOOD
189  "birch_fence" new  Blocks::BlockFence WOOD
190  "jungle_fence" new  Blocks::BlockFence WOOD
191  "dark_oak_fence" new  Blocks::BlockFence WOOD
192  "acacia_fence" new  Blocks::BlockFence WOOD
193  "spruce_door" new  Blocks::BlockDoor WOOD
194  "birch_door" new  Blocks::BlockDoor WOOD
195  "jungle_door" new  Blocks::BlockDoor WOOD
196  "acacia_door" new  Blocks::BlockDoor WOOD
197  "dark_oak_door" new  Blocks::BlockDoor WOOD
198  "end_rod" new  Blocks::BlockEndRod CIRCUITS
199  "chorus_plant" new  Blocks::BlockChorusPlant PLANTS
200  "chorus_flower" new  Blocks::BlockChorusFlower PLANTS
202  "purpur_pillar" new  Blocks::BlockRotatedPillar ROCK
203  "purpur_stairs" new  Blocks::BlockStairs ROCK
 "purpur_double_slab" new  BlockPurpurSlab.Double
 "purpur_slab" new  BlockPurpurSlab.Half
206  "end_bricks" new Block         registerBlock(206, "end_bricks", (new Block(Material.ROCK)).setSoundType(SoundType.STONE).setHardness(0.8F).setCreativeTab(CreativeTabs.BUILDING_BLOCKS).setUnlocalizedName("endBricks"));

FAILED ON SECOND TRY BlockCrops
207  "beetroots" new  Blocks::BlockBeetroot 
209  "end_gateway" new  Blocks::BlockEndGateway PORTAL
210  "repeating_command_block" new  Blocks::BlockCommandBlock IRON
211  "chain_command_block" new  Blocks::BlockCommandBlock IRON
212  "frosted_ice" new  Blocks::BlockFrostedIce ICE
255  "structure_block" new  Blocks::BlockStructure IRON
