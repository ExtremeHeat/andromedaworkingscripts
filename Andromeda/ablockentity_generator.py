with open('blockentities.txt') as f:
    blockentity_lines = f.read().splitlines()

for line in blockentity_lines:
	if line.startswith("#"):
		continue
	s = line.split(" ")
	java_name = s[0]
	pocket_name = s[2]
	cla_name = ' '.join(java_name.replace("_", ' ').split()).title().replace(" ", "")
	print 'extern const ABlockEntityID *%s;' % (cla_name)

print 'static void destroy() {'
for line in blockentity_lines:
	if line.startswith("#"):
		continue
	s = line.split(" ")
	java_name = s[0]
	pocket_name = s[2]
	cla_name = ' '.join(java_name.replace("_", ' ').split()).title().replace(" ", "")
	print '\tdelete %s;' % cla_name
print '}'


for line in blockentity_lines:
	if line.startswith("#"):
		continue
	s = line.split(" ")
	java_name = s[0]
	pocket_name = s[2]
	cla_name = ' '.join(java_name.replace("_", ' ').split()).title().replace(" ", "")
	print 'const ABlockEntityID* %s = new ABlockEntityID{ "%s", "%s", "%s" };' % (cla_name,
		java_name, java_name, pocket_name)