import re

BASE_PATH = "C:/Users/Extreme/Development/Tests & Alphas/Minecraft/Modding/MCP-9.28_194/src/minecraft/net/minecraft/block/"

with open(BASE_PATH + "Block.java") as f:
    content = f.readlines()

def read_file(path):
	data = []
	with open(BASE_PATH + path) as f:
   		data = f.readlines()
	return data

def create_file_for_block(block_name, block_type = "Block"):
	print '#include "%s.h"' % block_name
	s = """#pragma once

#include "%(BlockType)s.h"

//- This file was automatically generated
//TODO: MANUALLY SET BLOCK DATA AND TYPES

namespace Andromeda::Blocks {
	class %(BlockName)s : public %(BlockType)s {
	public:
		%(BlockName)s(UBlockName name, Material *material, float _hardness, float blast_resistance, /*short fuel_efficiency, */char light_level = 0) : %(BlockType)s(name, material, _hardness, blast_resistance, /*fuel_efficiency,*/ light_level) {};

	};
}"""
	f = open(block_name + ".h","w+")

	f.write(s % {"BlockName": block_name, "BlockType": block_type})


for line in content:
	if "registerBlock" in line:
		s = line.split(",")
		if "new Block(" in line:
			print  re.search("(\d+)", s[0]).group(1), s[1], "new Block", line
		elif "new" in line:
			block_name = s[2].split(" ")[2].split("(")[0]
			material = ""
			if "." in block_name:
				print s[1], "new ", block_name
				continue
			if "(Material" in line:
				material = line
			blockfiledata = read_file(block_name + ".java")

			block_type = ""

			for l in blockfiledata:
				if "(Material" in l and ";" in l:
					material = l
					break
			if material == "":
				for l in blockfiledata:
					if "extends" in l:
						#block_name2 = l.split(" ")[4].strip()
						block_name2 = re.search("extends (\w+)", l).group(1)
						#print block_name2, "#2"
						bfd2 = read_file(block_name2 + ".java")
						for n in bfd2:
							if "(Material" in n and ";" in n:
								material = n
								break
						if material == "":
							print "FAILED ON SECOND TRY", block_name2
						break
			if "Material." in material:
				material = re.search("Material.(\w+)", material).group(1)
			print re.search("(\d+)", s[0]).group(1), s[1], "new ", "Blocks::" + block_name if len(block_name) > 5 else block_name, material
			create_file_for_block(block_name)
