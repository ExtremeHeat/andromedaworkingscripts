import re

item_ids = []

with open('C:/users/extreme/desktop/development/andromeda/export_pepcitems.csv') as f:
    item_ids  = f.read().splitlines()


print "#pragma once"
print ""
print '#include <map>'
print '#include <utility>'
print ""
print '#include "Types.h"'
print "\n// This file was automatically generated\n"
print "namespace Andromeda {"


BEGIN_PP_REGION = '#pragma region'
END_PP_REGION = '#pragma endregion'

BEGIN_MAP_UNAME_TO_UID      = "const std::map<UItemName, UItemID> UItemNameToUItemID = {"
BEGIN_MAP_PEID_TO_UID       = "const std::map<std::pair<short, short>, UItemID> PocketItemToUItemID = {"
BEGIN_MAP_PEID_TO_UNAME     = "const std::map<std::pair<short, short>, UItemName> PocketItemToUItemName = {"
BEGIN_MAP_PEID_TO_JAVA      = "const std::map<std::pair<short, short>, std::pair<std::string, short>> PocketItemToJavaItem = {"
BEGIN_MAP_JAVAID_TO_UID     = "const std::map<std::pair<std::string, short>, UItemID> JavaItemToUItemID = {"
BEGIN_MAP_JAVAID_TO_UNAME   = "const std::map<std::pair<std::string, short>, UItemName> JavaItemToUItemName = {"
BEGIN_MAP_JAVAID_TO_PEID    = "const std::map<std::pair<std::string, short>, std::pair<short, short>> JavaItemToPocketItem = {"

DEFINE_MACRO = "#define %-*s %s"
DATA_LINE_UNAME_TO_UID = '     {Items::%s, %s},'
DATA_LINE_TUPLE_TO_ID = '  {std::make_pair(%s, %s), %s},'
DATA_LINE_TUPLE_TO_TUPLE = '    {std::make_pair(%s, %s), std::make_pair(%s, %s)},'
END_TAG = "};"

consts = []
pp_defines = []
Muname_to_uid = []
Mpeid_to_uid = []
Mpeid_to_uname = []
Mpeid_to_java = []
Mjavaid_to_uid = []
Mjavaid_to_uname = []
Mjavaid_to_peid = []
M_UNAME_TO_UID = []
M_TUPLE_TO_ID = []

print "namespace Items {"

for i in item_ids:
    if i[0] == "#":
        continue

    s = i.split(",")
    uid_name    = s[4]
    java_id     = s[0]
    pocket_id   = s[1]
    java_meta   = s[2]
    pocket_meta = s[3]


    fname = ' '.join(uid_name.replace("_", ' ').split()).title().replace(" ", "").replace(".","_")

    if pocket_id == "-1":
        print "// Unimplemnted item in pocket: ", s
    if "*" in fname:
        fname = fname.replace("*", "")
        uid_name = uid_name.replace("*", "")
        print 'const UItemName %s = "%s"; /* SPECIAL DATA */' % (fname, uid_name.strip())
    else:
        print 'const UItemName %s = "%s";' % (fname, uid_name.strip())

    if pocket_id == "-1":
        pocket_id = 0
        pocket_meta = 0
        int1 = (int(java_id) << 16) | int(pocket_id) & 0xFFFF;
        int2s = "((%s << 16) | %s & 0xFFFF) /* UNIMPLMENTED POCKET ITEM */" % (java_meta, pocket_meta)
    else:
        int1 = (int(java_id) << 16) | int(pocket_id) & 0xFFFF;
        int2s = "((%s << 16) | %s & 0xFFFF)" % (java_meta, pocket_meta)
    
    usesMetadata = False
    javaName = uid_name.split(".")[0].strip()
    id_macro_name = "ITEM_%s" % fname.upper()
    pp_defines.append(DEFINE_MACRO % (30, id_macro_name, "(" + str(int1) + " << 32) | " + int2s))
    Muname_to_uid.append(DATA_LINE_UNAME_TO_UID % (fname, id_macro_name ) + ("// NEEDS MANUAL METADATA ADDITIONS" if usesMetadata else ""))
    Mpeid_to_uid.append(DATA_LINE_TUPLE_TO_ID % (pocket_id, pocket_meta, "(UItemID)"+id_macro_name ) + ("// NEEDS MANUAL METADATA ADDITIONS" if usesMetadata else ""))
    Mpeid_to_uname.append(DATA_LINE_TUPLE_TO_ID % (pocket_id, pocket_meta, "Items::" + fname) + ("// NEEDS MANUAL METADATA ADDITIONS" if usesMetadata else ""))
    Mpeid_to_java.append(DATA_LINE_TUPLE_TO_TUPLE % (pocket_id, pocket_meta, '"%s"' % javaName, java_meta) + ("// NEEDS MANUAL METADATA ADDITIONS" if usesMetadata else ""))
    Mjavaid_to_uid.append(DATA_LINE_TUPLE_TO_ID % ('"%s"' % javaName, java_meta, "(UItemID)"+id_macro_name ) + ("// NEEDS MANUAL METADATA ADDITIONS" if usesMetadata else ""))
    Mjavaid_to_uname.append(DATA_LINE_TUPLE_TO_ID % ('"%s"' % javaName, java_meta, "Items::" + fname) + ("// NEEDS MANUAL METADATA ADDITIONS" if usesMetadata else ""))
    Mjavaid_to_peid.append(DATA_LINE_TUPLE_TO_TUPLE % ('"%s"' % javaName, java_meta, pocket_id, pocket_meta) + ("// NEEDS MANUAL METADATA ADDITIONS" if usesMetadata else ""))



print "} // NS Andromeda::Items"

print BEGIN_PP_REGION
for i in pp_defines:
    print i
print END_PP_REGION

print BEGIN_MAP_UNAME_TO_UID
for i in Muname_to_uid:
    print i
print END_TAG

print BEGIN_MAP_PEID_TO_UID
for i in Mpeid_to_uid:
    print i
print END_TAG

print BEGIN_MAP_PEID_TO_UNAME
for i in Mpeid_to_uname:
    print i
print END_TAG

print BEGIN_MAP_PEID_TO_JAVA
for i in Mpeid_to_java:
    print i
print END_TAG

print BEGIN_MAP_JAVAID_TO_UID
for i in Mjavaid_to_uid:
    print i
print END_TAG

print BEGIN_MAP_JAVAID_TO_UNAME
for i in Mjavaid_to_uname:
    print i
print END_TAG

print BEGIN_MAP_JAVAID_TO_PEID
for i in Mjavaid_to_peid:
    print i
print END_TAG

print BEGIN_PP_REGION
for i in pp_defines:
    print " ".join(i.replace("define", "undef").split(" ")[0:2])
print END_PP_REGION


print "} // NS Andromeda"
"""
print "/*"

print "UNACCOUTNED FOR MCPE ITEMS"
print mcpe_item_ids

print "*/"
"""